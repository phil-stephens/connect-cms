<!-- // Start SEO -->
<title>{{ render_seo_title($content) }}</title>
<meta name="description" content="{{ $content->getSeo('description') }}"/>

<link rel="canonical" href="{{ $content->getCanonical() }}"/>

<!-- Open Graph data -->
<meta property="og:title" content="{{ $content->getSeo('og_title') }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ Request::url() }}" />
<meta property="og:description" content="{{ $content->getSeo('og_description') }}" />
@if($image = $content->getImagePath(['w' => 400]))
<meta property="og:image" content="{!! $image !!}" />
@endif

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="{{ '@' . $content->getSeo('twitter_site') }}">
<meta name="twitter:title" content="{{ $content->getSeo('twitter_title') }}">
<meta name="twitter:description" content="{{ $content->getSeo('twitter_description') }}">
<meta name="twitter:creator" content="{{ '@' . $content->getSeo('twitter_creator') }}">
@if($image = $content->getImagePath(['w' => 400]))
<meta name="twitter:image" content="{!! $image !!}" />
@endif
<meta name="twitter:url" content="{{ Request::url() }}" />

<!-- Google+ -->
<link href="{{ $content->getSeo('google_plus_publisher') }}" rel="publisher" />
<link href="{{ $content->getSeo('google_plus_author') }}" rel="author" />
<!-- End SEO // -->
