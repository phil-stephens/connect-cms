@extends('default.layouts.default')

@section('content')

    <h1 class="page-header">{{ $content->getTitle() }}</h1>

    {!! $content->getExcerpt(false, '<blockquote>', '</blockquote>') !!}

    {!! $content->getBody() !!}

@endsection