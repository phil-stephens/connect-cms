<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    @include('utils.seo')

    <link rel="stylesheet" href="/core/css/core.css"/>
</head>
<body>

<header>

</header>

<div class="container">
    @yield('content')
</div>

</body>
</html>