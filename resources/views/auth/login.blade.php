@extends('admin.layouts.auth')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">

                <h1 class="page-header">{{ env('CMS_TITLE', 'Fastrack Group') }}</h1>

                @include('flash::message')

                <div class="panel panel-default">
                    <div class="panel-body">

                        @include('admin.layouts.partials.errors')

                        {!! Form::open() !!}
                        <fieldset>
                            <legend>Login</legend>

                            <!-- Email Form Input -->
                            <div class="form-group">
                                {!! Form::label('email', 'Email:') !!}
                                {!! Form::email('email', null, ['class' => 'form-control']) !!}
                            </div>

                            <!-- Password Form Input -->
                            <div class="form-group">
                                {!! Form::label('password', 'Password:') !!}
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                            </div>

                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('remember', 1, null) !!} Remember Me
                                </label>
                            </div>

                            <!-- Submit field -->
                            <div class="form-group">
                                {!! Form::submit('Login', ['class' => 'btn btn-primary btn-lg']) !!}

                                <a href="{{ url('auth/password/email') }}" class="pull-right">Forgotten Your Password?</a>
                            </div>
                            {!! Form::close() !!}

                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection