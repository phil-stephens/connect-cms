@extends('admin.layouts.default')

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/pickadate/default.css"/>
    <link rel="stylesheet" href="/core/css/pickadate/default.date.css"/>
@endsection

@section('content')

    @include('admin.news.articles.partials.tabs', ['tab' => 'edit'])

    @include('admin.layouts.partials.errors')

    {!! Form::model($model) !!}

    {!! Form::hidden('id') !!}

    <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    <div class="form-group">
        {!! Form::label('slug', 'URL slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Published_at Form Input -->
    <div class="form-group">
        {!! Form::label('published_at', 'Published:') !!}
        {!! Form::text('published_at', $model->published_at->format('j F Y'), ['class' => 'form-control published_at']) !!}
    </div>

    <div class="checkbox">
        <label>
            {!! Form::checkbox('draft', 1, null) !!} Draft
        </label>
    </div>

    <!-- User_id Form Input -->
    <div class="form-group">
        {!! Form::label('user_id', 'Author:') !!}
        {!! Form::select('user_id', $users->lists('name', 'id')->all(), null, ['class' => 'form-control']) !!}
    </div>


    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}


    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Remove Article</h3>
        </div>
        <div class="panel-body">
            <p>This cannot be undone.</p>

            {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_article_path', $feed->id, $model->id], 'class' => 'destroy-form']) !!}

            {!! Form::hidden('id', $model->id) !!}

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Remove Article Now', ['class' => 'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="/core/js/pickadate/picker.js"></script>
    <script src="/core/js/pickadate/picker.date.js"></script>

    <script>
        $(document).ready(function() {

            $('.published_at').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true
            });
        });

        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this article?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection