@extends('admin.layouts.default')

@section('content')

    @include('admin.news.articles.partials.tabs', ['tab' => 'seo'])

    @include('admin.layouts.partials.errors')

    {!! Form::model($model) !!}

    {!! Form::hidden('id') !!}

    @include('admin.layouts.partials.form-seo')

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}

@endsection