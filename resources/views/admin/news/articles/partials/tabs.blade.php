<div class="page-header">
    <h1 class="pull-left">Edit Article <small>{{ $model->name }}</small></h1>

    <a href="{{ route('feed_path', $feed->id) }}" class="btn btn-default btn-lg pull-right">Back to Feed</a>
</div>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('edit_article_path', 'Settings', [$feed->id, $model->id]) !!}</li>
{{--    <li role="presentation"@if($tab == 'settings') class="active"@endif>{!! link_to_route('article_settings_path', 'Template Settings', [$feed->id, $model->id]) !!}</li>--}}
    <li role="presentation"@if($tab == 'content') class="active"@endif>{!! link_to_route('content_article_path', 'Content', [$feed->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'slideshow') class="active"@endif>{!! link_to_route('slideshow_article_path', 'Article Images', [$feed->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'seo') class="active"@endif>{!! link_to_route('seo_article_path', 'SEO', [$feed->id, $model->id]) !!}</li>
</ul>