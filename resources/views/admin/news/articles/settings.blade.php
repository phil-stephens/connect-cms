@extends('admin.layouts.default')

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/dropzone.min.css"/>
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('content')

        @include('admin.news.articles.partials.tabs', ['tab' => 'settings'])

        @include('admin.layouts.partials.errors')

        {!! Form::open() !!}

        @foreach($settings as $setting)
            @include('admin.layouts.partials.setting-field')
        @endforeach

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}


@endsection

@section('scripts')
    @parent
    <script src="/core/js/dropzone.min.js"></script>

    @if(env('EDITOR_FORMAT', 'md') === 'html')
    {{--<script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>--}}
    <script src="/core/js/tinymce/tinymce.min.js"></script>
    <script>
        function tinymceInit()
        {
            tinymce.init({
                selector: 'textarea[role="editor"]',
                menubar : false,
                content_css : '/core/css/editor.css',
                statusbar : false,
                plugins: "link code paste",
                toolbar: "bold italic styleselect | bullist numlist | link code",
                valid_elements : '+*[*]',
                convert_urls: false,
                style_formats: [
                    {title: "Header 1", format: "h1"},
                    {title: "Header 2", format: "h2"},
                    {title: "Header 3", format: "h3"},
                    {title: "Header 4", format: "h4"},
                    {title: "Header 5", format: "h5"},
                    {title: "Header 6", format: "h6"},
                    {title: "Blockquote", format: "blockquote"}
                ]
            });
        }

        tinymceInit();
    </script>
    @endif

    <script>
        Dropzone.autoDiscover = false;

        function imageDropzoneInit() {

            $('[role="image-dropzone"]').each(function()
            {
                if(this.dropzone == undefined)
                {
                    var hash = $(this).data('hash');
                    var target = $(this).data('target');
                    var targetKey = $(this).data('target-key');
                    var model = $(this).data('model');
                    var modelId = $(this).data('model-id');
                    var clickable = $('.image-clickable', this);
                    var previews = $('.dropzone-previews', this);
                    var toolbar = $('.toolbar', this);

                    if( ! $('.content-image', this).length)
                    {
                        $(this).append('<img class="img-responsive content-image" />');
                    }

                    var image = $('.content-image', this);

                    $(this).dropzone( {
                        url: '{{ route('content_image_path') }}/' + hash,
                        addRemoveLinks: true,

                        acceptedFiles: 'image/*',
                        maxFiles: 1,
                        maxFilesize: 32,
                        parallelUploads: 1,
                        previewsContainer: previews[0],
                        clickable: clickable[0],
                        init: function() {
                            this.on('addedfile', function(file) {
                                image.css('opacity', 0.3);
                            });

                            this.on('sending', function(file, xhr, formData) {
                                formData.append('_token', $('meta[name="csrf_token"]').attr('content'));
                                formData.append('url_params', '{!! serialize(['w' => 400, 'h' => 300, 'fit' => 'crop']) !!}');
                                if(target != undefined) formData.append('target', target);
                                if(targetKey != undefined) formData.append('targetKey', targetKey);
                                if(model != undefined) formData.append('model', model);
                                if(modelId != undefined) formData.append('modelId', modelId);
                            });

                            this.on("success", function(file, responseText) {
                                console.log(responseText);

                                image.attr('src', responseText.path).css('opacity', 1);

                                image.attr('id', 'image-preview-' + responseText.id);

//                                $('button', toolbar).each(function() {
//                                    $(this).data('id', responseText.id);
//                                })

//                                toolbar.fadeIn();
                                this.removeFile(file);
                            });
                        }
                    });
                }
            })
        }

        {{--function fileDropzoneInit() {--}}

            {{--$('[role="file-dropzone"]').each(function()--}}
            {{--{--}}
                {{--if(this.dropzone == undefined)--}}
                {{--{--}}
                    {{--var hash = $(this).data('hash');--}}
                    {{--var targetId = $(this).data('target-id');--}}
                    {{--var clickable = $('.image-clickable', this);--}}
                    {{--var previews = $('.dropzone-previews', this);--}}
                    {{--var fileInfo = $('.file-info', this);--}}

                    {{--$(this).dropzone( {--}}
                        {{--url: '{{ route('content_file_path') }}/' + hash,--}}
                        {{--addRemoveLinks: true,--}}
                        {{--maxFiles: 1,--}}
                        {{--maxFilesize: 32,--}}
                        {{--parallelUploads: 1,--}}
                        {{--previewsContainer: previews[0],--}}
                        {{--clickable: clickable[0],--}}
                        {{--init: function() {--}}

                            {{--this.on('sending', function(file, xhr, formData) {--}}
                                {{--formData.append('_token', $('meta[name="csrf_token"]').attr('content'));--}}
                                {{--formData.append('id', targetId);--}}
                            {{--});--}}

                            {{--this.on("success", function(file, responseText) {--}}
                                {{--console.log(responseText);--}}

                                {{--fileInfo.html('<i class="fa ' + responseText.mime_class + ' fa-3x"></i> ' + responseText.name + ' <small>' + responseText.size + '</small>');--}}

                                {{--this.removeFile(file);--}}
                            {{--});--}}
                        {{--}--}}
                    {{--});--}}
                {{--}--}}
            {{--})--}}
        {{--}--}}

        imageDropzoneInit();
        //fileDropzoneInit();


        // Image/file toolbar buttons
        $('[role="download"]').on('click', function() {
            if($(this).data('id') != undefined)
            {
                document.location.href = '/.admin/download/' + $(this).data('type') + '/' + $(this).data('id');
            }
        });

        $('[role="trash"]').on('click', function() {
            // Remove image from the content
            var toolbar = $(this).parent('.toolbar');
            alert('#' + $(this).data('type') + '-preview-' + $(this).data('id'));
            var preview = $('#' + $(this).data('type') + '-preview-' + $(this).data('id'));

            // Do it using AJAX
            $.ajax({
                type: "POST",
                url: '{{ route('destroy_upload_path') }}',
                data: {
                    _method: 'DELETE',
                    _token: $('meta[name="csrf_token"]').attr('content'),
                    id: $(this).data('id'),
                    type: $(this).data('type'),
                    target: 'setting',
                    target_key: $(this).data('target-key'),
                    model: $(this).data('model'),
                    model_id: $(this).data('model-id')
                },
                success: function (data) {
                    console.log(data);

                    preview.attr('src', '');

                    toolbar.fadeOut();
                }
            });
        });

    </script>
@endsection
