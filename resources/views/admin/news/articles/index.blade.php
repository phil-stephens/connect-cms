@extends('admin.layouts.default')

@section('content')
    <div class="page-header">
        <h1 class="pull-left">Content Feed <small>{{ $feed->name }}</small></h1>

        <p class="pull-right">
            <a href="{{ route('edit_feed_path', $feed->id) }}" class="btn btn-default btn-lg">Edit</a>
            <a href="{{ route('create_article_path', $feed->id) }}" class="btn btn-primary btn-lg">Create Article</a>
        </p>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Page Title</th>
            <th>Published</th>
            <th>Author</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
            <tr>
                <td>
                    {!! ($article->draft) ? '<small class="text-muted">DRAFT</small> ' : '' !!}
                    {{ $article->name }}
                </td>
                <td>{{ $article->getTitle() }}</td>
                <td>{{ $article->published_at->format('j F Y') }}</td>
                <td>{{ $article->present()->author() }}</td>
                <td>{!! link_to_route('edit_article_path', 'Edit', [$feed->id, $article->id], ['class' => 'btn btn-primary btn-sm']) !!}</td>
                <td>
                    {!! Form::open(['route' => ['destroy_article_path', $feed->id, $article->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    {!! Form::hidden('id', $article->id) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this article?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection