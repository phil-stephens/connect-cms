@extends('admin.layouts.default')

@section('content')

    @include('admin.news.articles.partials.tabs', ['tab' => 'slideshow'])

    @include('admin.layouts.partials.form-slideshow')

@endsection