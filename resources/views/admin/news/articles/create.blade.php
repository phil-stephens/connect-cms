@extends('admin.layouts.default')

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/pickadate/default.css"/>
    <link rel="stylesheet" href="/core/css/pickadate/default.date.css"/>
@endsection

@section('content')


    <div class="page-header">
        <h1 class="pull-left">Create Article <small>{{ $feed->name }}</small></h1>

        <a href="{{ route('feed_path', $feed->id) }}" class="btn btn-default btn-lg pull-right">Back to Feed</a>
    </div>

    @include('admin.layouts.partials.errors')

    {!! Form::open() !!}

    <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    <div class="form-group">
        {!! Form::label('slug', 'URL slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Published_at Form Input -->
    <div class="form-group">
        {!! Form::label('published_at', 'Published:') !!}
        {!! Form::text('published_at', \Carbon\Carbon::today()->format('j F Y'), ['class' => 'form-control published_at']) !!}
    </div>

    <div class="checkbox">
        <label>
            {!! Form::checkbox('draft', 1) !!} Draft
        </label>
    </div>

    <!-- User_id Form Input -->
    <div class="form-group">
        {!! Form::label('user_id', 'Author:') !!}
        {!! Form::select('user_id', $users->lists('name', 'id')->all(), Auth::id(), ['class' => 'form-control']) !!}
    </div>

    <fieldset>
        <legend>Page Content</legend>

        @include('admin.layouts.partials.form-content')
    </fieldset>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create Article', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection

@include('admin.layouts.partials.sluggify')

@section('scripts')
    @parent

    <script src="/core/js/pickadate/picker.js"></script>
    <script src="/core/js/pickadate/picker.date.js"></script>

    <script>
        $(document).ready(function() {

            $('.published_at').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true
            });
        });
    </script>
@endsection