@extends('admin.layouts.default')

@section('content')
    <div class="page-header">
        <h1 class="pull-left">Content Feeds</h1>

        <a href="{{ route('create_feed_path') }}" class="btn btn-primary btn-lg pull-right">Create Content Feed</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Articles</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($feeds as $feed)
            <tr>
                <td>{!! link_to_route('feed_path', $feed->name, $feed->id) !!}</td>
                <td>{!! link_to_route('feed_path', $feed->articles()->count(), $feed->id) !!}</td>
                <td>{!! link_to_route('create_article_path', 'New Article', $feed->id, ['class' => 'btn btn-default btn-sm']) !!}</td>
                <td>{!! link_to_route('edit_feed_path', 'Edit', $feed->id, ['class' => 'btn btn-primary btn-sm']) !!}</td>
                <td>
                    {!! Form::open(['route' => ['destroy_feed_path', $feed->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    {!! Form::hidden('id', $feed->id) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this content feed?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection