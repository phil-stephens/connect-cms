@extends('admin.layouts.default')

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Edit Content Feed <small>{{ $feed->name }}</small></h1>

        <a href="{{ route('feed_path', $feed->id) }}" class="btn btn-default btn-lg pull-right">Articles</a>
    </div>

    @include('admin.layouts.partials.errors')

    {!! Form::model($feed) !!}

    <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    <div class="form-group">
        {!! Form::label('slug', 'URL slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('canonical_site_id', 'Canonical Site:') !!}
        {!! Form::select('canonical_site_id', $sites, null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}

    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Remove Content Feed</h3>
        </div>
        <div class="panel-body">
            <p>This cannot be undone.</p>

            {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_feed_path', $feed->id], 'class' => 'destroy-form']) !!}

            {!! Form::hidden('id', $feed->id) !!}

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Remove Feed Now', ['class' => 'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this content feed?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection