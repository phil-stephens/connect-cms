@extends('admin.layouts.default')

@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
@include('admin.sites.partials.side-nav')
@endsection

@section('content')

@include('admin.sites.partials.tabs', ['tab' => 'export'])

    {!! Form::open() !!}
        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Export!', ['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Upload Page Import File</h3>
    </div>
    <div class="panel-body">

        {!! Form::open(['route' => ['import_site_path', $site->uuid], 'files' => true]) !!}

        <div class="form-group">
            {!! Form::label('file', 'Encoded Export File:') !!}
            {!! Form::file('file', null, ['class' => 'form-control']) !!}
        </div>


        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Upload Now', ['class' => 'btn btn-default']) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
