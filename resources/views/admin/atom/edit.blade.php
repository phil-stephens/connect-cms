@extends('admin.layouts.default')

@section('content')
    <div class="page-header">
        <h1 class="pull-left">
            Block Editor
            <small>{{ class_basename($content->parent) }} - {{ $content->parent->name }}</small>
        </h1>

        <a href="{!! $content->parent->getAdminContentUrl() !!}" class="btn btn-default btn-lg pull-right">Back to Content Editor</a>
    </div>

    {{--<div class="alert alert-info">--}}
        {{--Atoms are a way of combining pre-defined complex layouts and elements onto a single page.--}}
    {{--</div>--}}

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-10">
                    {!! Form::select('atom-select', $select, null, ['class' => 'form-control', 'role' => 'atom-select']) !!}
                </div>

                <div class="col-xs-2">
                    <button class="btn btn-default" role="new-atom-btn">Add Block</button>
                </div>
            </div>

        </div>
    </div>

    {!! Form::open() !!}
    <div class="panel-group" id="atoms" role="tablist" aria-multiselectable="true"></div>
    
    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@endsection


@section('modals')
    @parent
    @if(empty($_media_modal))
        @include('admin.layouts.partials.media-modal')
    @endif
@endsection

@section('scripts')
    @parent

    <script src="/core/js/jscolor/jscolor.js"></script>

@endsection

@section('scripts')
    @parent
    <script src="/core/js/handlebars.js"></script>
    <script src="/core/js/jquery.sortable.min.js"></script>

    @if(env('EDITOR_FORMAT', 'md') === 'html')
    <script src="/core/js/tinymce/tinymce.min.js"></script>
    <script>
        function tinymceInit()
        {
            tinymce.init({
                selector: 'textarea[role="editor"]',
                menubar : false,
                content_css : '/core/css/editor.css',
                statusbar : false,
                plugins: "link code paste",
                toolbar: "bold italic styleselect | bullist numlist | link code | cms_image",
                valid_elements : '+*[*]',
                convert_urls: false,
                style_formats: [
                    {title: "Header 1", format: "h1"},
                    {title: "Header 2", format: "h2"},
                    {title: "Header 3", format: "h3"},
                    {title: "Header 4", format: "h4"},
                    {title: "Header 5", format: "h5"},
                    {title: "Header 6", format: "h6"},
                    {title: "Blockquote", format: "blockquote"}
                ],
                setup : function(ed) {
                    // Add a custom button
                    ed.addButton('cms_image', {
                        title : 'Insert Image',
                        image: '{{ core_asset('img/tinymce-photo-icon.png') }}',
                        onclick : function() {
                            $('#media-browser').modal('show');
                            targetEditor = ed;
                        }
                    });
                }
            });
        }

//        tinymceInit();
    </script>
    @endif

    <script>
        $(function() {
            @foreach($content->atoms as $atom)
            var fieldHash = Math.random().toString(36).slice(2);

            <?php

            $atom->load('settings');

            ?>

            var details = {!! $atom->handlebarsJson() !!};

            details.hash = Math.random().toString(36).slice(2);


            var source   = $('#atom-{{ $atom->key }}-template').html();
            var template = Handlebars.compile(source);
            var html    = template(details);

            $('#atoms').append(html);



//            initSorting();
//            doSorting();

            @endforeach

            @if(env('EDITOR_FORMAT', 'md') === 'html')
            tinymceInit();
            @endif
        });

        $('[role="new-atom-btn"]').on('click', function(e) {
            e.preventDefault();

            var t = $('[role="atom-select"]').val();

            var fieldHash = Math.random().toString(36).slice(2);

            var source   = $('#atom-' + t + '-template').html();
            var template = Handlebars.compile(source);
            var html    = template({hash: fieldHash });

            $('#atoms').append(html);

            @if(env('EDITOR_FORMAT', 'md') === 'html')
            tinymceInit();
            @endif

            initSorting();
            doSorting();
        });

        function initSorting()
        {
            $('#atoms').sortable({
                group: 'atoms',
                itemSelector: 'div.panel',
                handle: 'a.sort-handle',
                containerSelector: 'div.panel-group',
                placeholder: '<div class="panel-placeholder"></div>',
                onDrop: function ($item, container, _super) {
                    $item.removeClass(container.group.options.draggedClass).removeAttr('style');
                    $('body').removeClass(container.group.options.bodyClass);

                    doSorting();

                    _super($item, container);
                }
            });
        }

        initSorting();

        function doSorting()
        {
            var i = 0;
            $('#atoms .panel').each(function() {
                $('.sort-order', this).val(i);

                i++;
            });
        }
    </script>



    @foreach($templates as $template)
        @include('admin.layouts.partials.handlebars.atom')
    @endforeach
@endsection





{{--<div class="panel">--}}
{{--<div class="panel-body">--}}
{{--<p>{{ $template->name }}</p>--}}

{{--@foreach($template->fields as $field)--}}
{{--<div class="form-group">--}}
{{--<label for="">{{ $field->label }}:</label>--}}

{{--@if($field->type == 'plain')--}}
{{--@if(isset($field->pre) || isset($field->post))--}}
{{--<div class="input-group">--}}
{{--@endif--}}

{{--@if(isset($field->pre))--}}
{{--<span class="input-group-addon">{!! $field->pre !!}</span>--}}
{{--@endif--}}

{{--{!! Form::text('setting[' . $field->key . '][value]', null, ['class' => 'form-control']) !!}--}}

{{--@if(isset($field->post))--}}
{{--<span class="input-group-addon">{!! $field>post !!}</span>--}}
{{--@endif--}}

{{--@if(isset($field->pre) || isset($field->post))--}}
{{--</div>--}}
{{--@endif--}}

{{--@elseif($field->type == 'html')--}}

{{--{!! Form::textarea('setting[' . $field->key . '][value]', null, ['class' => 'form-control', 'rows' => 5, 'role' => 'editor']) !!}--}}

{{--@elseif($field->type == 'image')--}}

{{--@include('admin.layouts.partials.image-upload', ['field' => 'setting[' . $field->key . '][uuid]'])--}}

{{--@elseif($field->type == 'colour')--}}

{{--{!! Form::text('setting[' . $field->key . '][value]', null, ['class' => 'form-control color {hash:true}']) !!}--}}

{{--@endif--}}

{{--@if( ! empty($field->description))--}}
{{--<span class="help-block"><i class="fa fa-info-circle"></i> {{ $field->description }}</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--@endforeach--}}
{{--</div>--}}
{{--</div>--}}