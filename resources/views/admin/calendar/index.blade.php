@extends('admin.layouts.default')

@section('content')
    <div class="page-header">
        <h1 class="pull-left">Calendars</h1>

        <a href="{{ route('create_calendar_path') }}" class="btn btn-primary btn-lg pull-right">Create Calendar</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Events</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($calendars as $calendar)
            <tr>
                <td>{!! link_to_route('events_path', $calendar->name, $calendar->id) !!}</td>
                <td>{{ $calendar->events()->count() }}</td>
                <td>
                    {!! link_to_route('create_event_path', 'New Event', $calendar->id, ['class' => 'btn btn-default btn-sm']) !!}
                </td>
                <td>{!! link_to_route('edit_calendar_path', 'Edit', $calendar->id, ['class' => 'btn btn-primary btn-sm']) !!}</td>
                <td>
                    {!! Form::open(['route' => ['destroy_calendar_path', $calendar->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    {!! Form::hidden('id', $calendar->id) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this calendar?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection