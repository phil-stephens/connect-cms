@extends('admin.layouts.default')

@section('content')
    <h1 class="page-header">Create Calendar</h1>

    @include('admin.layouts.partials.errors')

    {!! Form::open() !!}

            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    <div class="form-group">
        {!! Form::label('slug', 'URL slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('canonical_site_id', 'Canonical Site:') !!}
        {!! Form::select('canonical_site_id', $sites, null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create Calendar', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection

@include('admin.layouts.partials.sluggify')
