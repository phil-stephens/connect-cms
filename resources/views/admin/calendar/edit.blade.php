@extends('admin.layouts.default')

@section('content')
    <div class="page-header">
        <h1 class="pull-left">Edit Calendar</h1>

        <a href="{{ route('events_path', $model->id) }}" class="btn btn-default btn-lg pull-right">Events</a>
    </div>

    @include('admin.layouts.partials.errors')

    {!! Form::model($model) !!}

            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    <div class="form-group">
        {!! Form::label('slug', 'URL slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('canonical_site_id', 'Canonical Site:') !!}
        {!! Form::select('canonical_site_id', $sites, null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection

