<div class="page-header">
    <h1 class="pull-left">Edit Event <small>{{ $model->name }}</small></h1>

    <a href="{{ route('events_path', $calendar->id) }}" class="btn btn-default btn-lg pull-right">Back to Calendar</a>
</div>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('edit_event_path', 'Settings', [$calendar->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'content') class="active"@endif>{!! link_to_route('content_event_path', 'Content', [$calendar->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'slideshow') class="active"@endif>{!! link_to_route('slideshow_event_path', 'Event Images', [$calendar->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'seo') class="active"@endif>{!! link_to_route('seo_event_path', 'SEO', [$calendar->id, $model->id]) !!}</li>
</ul>