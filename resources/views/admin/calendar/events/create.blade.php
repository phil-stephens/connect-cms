@extends('admin.layouts.default')

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/pickadate/default.css"/>
    <link rel="stylesheet" href="/core/css/pickadate/default.date.css"/>
@endsection

@section('content')
        <h1 class="page-header">Create Event <small>{{ $calendar->name }}</small></h1>

        @include('admin.layouts.partials.errors')

        {!! Form::open() !!}

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Slug Form Input -->
        <div class="form-group">
            {!! Form::label('slug', 'URL slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Price Form Input -->
        <div class="form-group">
            {!! Form::label('price', 'Price:') !!}
            {!! Form::text('price', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Venue Form Input -->
        <div class="form-group">
            {!! Form::label('venue', 'Venue:') !!}
            {!! Form::text('venue', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Website Form Input -->
        <div class="form-group">
            {!! Form::label('website', 'Website:') !!}
            {!! Form::text('website', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Start_at Form Input -->
        <div class="form-group">
            {!! Form::label('start_at', 'Start at:') !!}
            {!! Form::text('start_at', \Carbon\Carbon::today()->format('j F Y'), ['class' => 'form-control start_at']) !!}
        </div>

        <!-- Finish_at Form Input -->
        <div class="form-group">
            {!! Form::label('finish_at', 'Finish at:') !!}
            {!! Form::text('finish_at', \Carbon\Carbon::tomorrow()->format('j F Y'), ['class' => 'form-control finish_at']) !!}
        </div>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Create Event', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}
@endsection

@include('admin.layouts.partials.sluggify')

@section('scripts')
    @parent

    <script src="/core/js/pickadate/picker.js"></script>
    <script src="/core/js/pickadate/picker.date.js"></script>

    <script>
        $(document).ready(function() {

            var finish_at = $('.finish_at').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true
            });

            $('.start_at').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true,
                onSet: function(context) {
                    var finishPicker = finish_at.pickadate('picker');
                    var minDate = context.select;
                    var finishSelected = finishPicker.get('select');

                    if(finishSelected != null && finishSelected.pick < minDate)
                    {
                        finishPicker.set('select', minDate);
                    }

                    finishPicker.set('min', new Date(minDate));
                }
            });
        });
    </script>
@endsection