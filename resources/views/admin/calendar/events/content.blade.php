@extends('admin.layouts.default')

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/pickadate/default.css"/>
    <link rel="stylesheet" href="/core/css/pickadate/default.date.css"/>
@endsection

@section('content')

        @include('admin.calendar.partials.tabs', ['tab' => 'content'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}

        {!! Form::hidden('id') !!}

        @include('admin.layouts.partials.form-content')

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}

@endsection