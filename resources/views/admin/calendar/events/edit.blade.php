@extends('admin.layouts.default')

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/pickadate/default.css"/>
    <link rel="stylesheet" href="/core/css/pickadate/default.date.css"/>
@endsection

@section('content')

        @include('admin.calendar.partials.tabs', ['tab' => 'edit'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}

        {!! Form::hidden('id') !!}
        
        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Slug Form Input -->
        <div class="form-group">
            {!! Form::label('slug', 'URL slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Price Form Input -->
        <div class="form-group">
            {!! Form::label('price', 'Price:') !!}
            {!! Form::text('price', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Venue Form Input -->
        <div class="form-group">
            {!! Form::label('venue', 'Venue:') !!}
            {!! Form::text('venue', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Website Form Input -->
        <div class="form-group">
            {!! Form::label('website', 'Website:') !!}
            {!! Form::text('website', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Start_at Form Input -->
        <div class="form-group">
            {!! Form::label('start_at', 'Start at:') !!}
            {!! Form::text('start_at', $model->start_at->format('j F Y'), ['class' => 'form-control start_at']) !!}
        </div>

        <!-- Finish_at Form Input -->
        <div class="form-group">
            {!! Form::label('finish_at', 'Finish at:') !!}
            {!! Form::text('finish_at', $model->finish_at->format('j F Y'), ['class' => 'form-control finish_at']) !!}
        </div>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}


        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Remove Event</h3>
            </div>
            <div class="panel-body">
                <p>This cannot be undone.</p>

                {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_event_path', $calendar->id, $model->id], 'class' => 'destroy-form']) !!}

                {!! Form::hidden('id', $model->id) !!}

                <!-- Submit field -->
                <div class="form-group">
                    {!! Form::submit('Remove Event Now', ['class' => 'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection

@section('scripts')
    @parent
    <script src="/core/js/pickadate/picker.js"></script>
    <script src="/core/js/pickadate/picker.date.js"></script>

    <script>
        $( document ).ready(function() {
           var finish_at = $('.finish_at').pickadate({
               format: 'd mmmm yyyy',
               formatSubmit: 'yyyy-mm-dd 00:00:00',
               hiddenName: true
           });

            $('.start_at').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true,
                onSet: function(context) {
                    var finishPicker = finish_at.pickadate('picker');
                    var minDate = context.select;
                    var finishSelected = finishPicker.get('select');

                    if(finishSelected != null && finishSelected.pick < minDate)
                    {
                        finishPicker.set('select', minDate);
                    }

                    finishPicker.set('min', new Date(minDate));
                }
            });
        });

        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this event?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection