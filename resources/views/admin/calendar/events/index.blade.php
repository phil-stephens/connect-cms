@extends('admin.layouts.default')

@section('content')
<div class="page-header">
    <h1 class="pull-left">Events <small>{{ $calendar->name }}</small></h1>

    <p class="pull-right">
        <a href="{{ route('edit_calendar_path', $calendar->id) }}" class="btn btn-default btn-lg">Edit</a>
        <a href="{{ route('create_event_path', $calendar->id) }}" class="btn btn-primary btn-lg">Create Event</a>
    </p>
</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Name</th>
        <th>Date(s)</th>
        <th class="button-column"></th>
        <th class="button-column"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($events as $event)
        @if(\Carbon\Carbon::today()->gt($event->finish_at))
        <tr class="warning text-muted">
        @else
        <tr>
        @endif
            <td>{{ $event->name }}</td>
            <td>
                {{ $event->start_at->format('j F Y') }}
                @if($event->start_at != $event->finish_at)
                    - {{ $event->finish_at->format('j F Y') }}
                @endif
            </td>

            <td>
                 @if($event->ticket_types()->count())
                    {!! link_to_route('boxoffice_path', 'Box Office', $event->uuid, ['class' => 'btn btn-default btn-sm']) !!}
                 @endif
            </td>

            <td>{!! link_to_route('edit_event_path', 'Edit', [$calendar->id, $event->id], ['class' => 'btn btn-primary btn-sm']) !!}</td>
            <td>
                {!! Form::open(['route' => ['destroy_event_path', $calendar->id, $event->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                {!! Form::hidden('id', $event->id) !!}
                <button type="submit" class="btn btn-default btn-sm">Remove</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this event?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection