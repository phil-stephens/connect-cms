@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    @include('admin.sites.partials.tabs', ['tab' => 'aliases'])

<div class="page-header">
    <h2 class="pull-left">Aliases</h2>
</div>

    <div class="alert alert-success">
        Confirm bulk insert of aliases
    </div>

{!! Form::open(['route' => ['create_multiple_alias_path', $site->id]]) !!}
<table class="table table-striped">
    <thead>
        <tr>
            <th class="button-column"></th>
            <th>From</th>
            <th>Redirect To</th>
            <th>Redirection Type</th>
        </tr>
    </thead>
    <tbody>
    <?php $index = 0; ?>
    @foreach($aliases as $alias)
        <tr>
            <td>
                <input type="checkbox" name="alias[{{ $index }}][insert]" value="1" checked>
                <input type="hidden" name="alias[{{ $index }}][from]" value="{{ $alias['from'] }}">
                <input type="hidden" name="alias[{{ $index }}][to]" value="{{ $alias['to'] }}">
                <input type="hidden" name="alias[{{ $index }}][type]" value="{{ $alias['type'] }}">

            </td>
            <td>/{{ $alias['from'] }}</td>
            <td>{{ $alias['to'] }}</td>
            <td>{{ $alias['type'] }}</td>
        </tr>
        <?php $index++; ?>
    @endforeach
    </tbody>
</table>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Insert Aliases', ['class' => 'btn btn-primary']) !!}
    </div>
    
    {!! Form::close() !!}

@endsection