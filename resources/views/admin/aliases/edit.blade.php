@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')


    @include('admin.sites.partials.tabs', ['tab' => 'aliases'])

        <h2 class="page-header">Edit Alias</h2>

        @include('admin.layouts.partials.errors')

        {!! Form::model($alias) !!}

        {!! Form::hidden('site_id', $site->id) !!}

        <!-- From Form Input -->
        <div class="form-group">
            {!! Form::label('from', 'From:') !!}

            <div class="input-group">
                <div class="input-group-addon">/</div>
                {!! Form::text('from', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <!-- To Form Input -->
        <div class="form-group">
            {!! Form::label('to', 'Redirect To:') !!}
            {!! Form::text('to', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Type Form Input -->
        <div class="form-group">
            {!! Form::label('type', 'Redirection Type:') !!}
            {!! Form::text('type', '301', ['class' => 'form-control']) !!}
        </div>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}


        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Remove Alias</h3>
            </div>
            <div class="panel-body">
                <p>This cannot be undone.</p>

                {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_alias_path', $site->id, $alias->id], 'class' => 'destroy-form']) !!}

                {!! Form::hidden('id', $alias->id) !!}

                <!-- Submit field -->
                <div class="form-group">
                    {!! Form::submit('Remove Alias Now', ['class' => 'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to delete this alias?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection