@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    @include('admin.sites.partials.tabs', ['tab' => 'aliases'])

<div class="page-header">

    <h2 class="pull-left">Aliases</h2>

    <a href="{{ route('create_alias_path', $site->id) }}" class="btn btn-primary pull-right">Add a new Alias</a>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th>From</th>
            <th>Redirect To</th>
            <th>Redirection Type</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
    </thead>
    <tbody>
    @foreach($aliases as $alias)
        <tr>
            <td>/{{ $alias->from }}</td>
            <td>{{ $alias->to }}</td>
            <td>{{ $alias->type }}</td>
            <td>{!! link_to_route('edit_alias_path', 'Edit', [$site->id, $alias->id], ['class' => 'btn btn-primary btn-sm']) !!}</td>
            <td>
                {!! Form::open(['route' => ['destroy_alias_path', $site->id, $alias->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    {!! Form::hidden('id', $alias->id) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this alias?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection