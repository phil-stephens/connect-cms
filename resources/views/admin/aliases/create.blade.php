@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')


    @include('admin.sites.partials.tabs', ['tab' => 'aliases'])

        <h2 class="page-header">Add an Alias</h2>

        @include('admin.layouts.partials.errors')
        
        {!! Form::open() !!}

        {!! Form::hidden('site_id', $site->id) !!}

        <!-- From Form Input -->
        <div class="form-group">
            {!! Form::label('from', 'From:') !!}

            <div class="input-group">
                <div class="input-group-addon">/</div>
                {!! Form::text('from', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <!-- To Form Input -->
        <div class="form-group">
            {!! Form::label('to', 'Redirect To:') !!}
            {!! Form::text('to', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Type Form Input -->
        <div class="form-group">
            {!! Form::label('type', 'Redirection Type:') !!}
            {!! Form::text('type', '301', ['class' => 'form-control']) !!}
        </div>
        
        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Add Alias', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>
        
        {!! Form::close() !!}

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Upload Bulk CSV</h3>
            </div>
            <div class="panel-body">
                <p>Upload file...</p>

                {!! Form::open(['route' => ['upload_alias_path', $site->id], 'files' => true]) !!}

                <div class="form-group">
                    {!! Form::label('file', 'CSV File:') !!}
                    {!! Form::file('file', null, ['class' => 'form-control']) !!}
                </div>

                        <!-- To Form Input -->
                <div class="form-group">
                    {!! Form::label('to', 'Redirect To:') !!}
                    {!! Form::text('to', '/', ['class' => 'form-control']) !!}
                </div>

                <!-- Type Form Input -->
                <div class="form-group">
                    {!! Form::label('type', 'Redirection Type:') !!}
                    {!! Form::text('type', '301', ['class' => 'form-control']) !!}
                </div>

                <div class="checkbox">
                    <label>
                        {!! Form::checkbox('use_url_fragment', 1) !!} Include URL fragment in path
                    </label>
                </div>
                        <!-- Submit field -->
                <div class="form-group">
                    {!! Form::submit('Upload CSV Now', ['class' => 'btn btn-default']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection