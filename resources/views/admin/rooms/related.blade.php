@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.rooms.partials.tabs', ['tab' => 'related'])

    @include('admin.layouts.partials.errors')

    <div class="row">
        <div class="col-md-4">

            <div class="panel-group" id="source-accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="rooms-heading">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#source-accordion" href="#collapse-rooms" aria-expanded="true" aria-controls="collapseOne">
                                Available Rooms
                            </a>
                        </h4>
                    </div>

                    <div id="collapse-rooms" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="rooms-heading">

                        {!! Form::open(['class' => 'source-form']) !!}
                        <div class="panel-body">

                            @foreach($rooms as $room)
                                <div class="checkbox"
                                     data-id="{{ $room->id }}"
                                     data-name="{{ $room->name }}">
                                    <label>
                                        <input type="checkbox" name="room[{{ $room->id }}][add]" value="1" />
                                        {{ $room->name }}
                                    </label>
                                </div>
                            @endforeach

                        </div>

                        <div class="panel-footer">
                            {!! Form::submit('Relate to Room', ['class' => 'btn btn-primary btn-sm']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>

        {!! Form::open() !!}
        <div class="col-md-8">

            <div class="panel-group" id="target-list" role="tablist" aria-multiselectable="true">

                @foreach($model->related as $room)
                    <div class="panel panel-default" id="room-{{ $room->id }}">
                        <div class="panel-heading draggable" role="tab" id="room-{{ $room->id }}-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapse-room-{{ $room->id }}" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-angle-down text-muted"></i> {{ $room->name }}

                                </a>
                            </h4>
                        </div>

                        <div id="collapse-room-{{ $room->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="room-{{ $room->id }}-heading">
                            <div class="panel-body">

                                {!! Form::hidden('room[' . $room->id . '][id]', $room->id, ['class' => 'room-id']) !!}


                                        <!-- Label Form Input -->
                                <div class="form-group">
                                    {!! Form::label('room[' . $room->id . '][key]', 'Key:') !!}
                                    {!! Form::text('room[' . $room->id . '][key]', $room->pivot->key, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="panel-footer clearfix">
                                <a href="#" class="btn btn-sm btn-default pull-right" role="remove-room" data-target="{{ $room->id }}">Remove</a>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
            </div>

        </div>

        {!! Form::close() !!}
    </div>


@endsection

@section('scripts')
    @parent

    <script src="/core/js/handlebars.js"></script>

    <script>
        $('.source-form').on('submit', function(e) {

            e.preventDefault();

            var theForm = this;

            $('.checkbox', theForm).each(function()
            {
                if($('[type="checkbox"]', this).is(':checked'))
                {
                    var source   = $("#related-room-template").html();
                    var template = Handlebars.compile(source);
                    var html    = template({
                        hash: Math.random().toString(36).slice(2),
                        name: $(this).data('name'),
                        id: $(this).data('id')
                    });

                    $('#target-list').append(html);

                    $(this).remove();
                }
            });

        });


        $(document).on('click', '[role="remove-room"]', function(e) {
            e.preventDefault();

            var target = $('#room-' + $(this).data('target'));

            var roomId = $('.room-id', target).val();

            if(roomId != undefined)
            {
                // Do something to remove it here
                $.ajax({
                    type: "POST",
                    url: '{{ route('room_destroy_related_path', $model->uuid) }}',
                    data: {_method: 'DELETE', _token: $('meta[name="csrf_token"]').attr('content'), id: roomId},
                    success: function(data)
                    {
                        console.log(data);
                    }
                });
            }


            $(target).remove();
        });
    </script>

    @include('admin.layouts.partials.handlebars.related-room')

@endsection