<h1 class="page-header">Edit {{ $property->present()->roomNoun() }} <small>{{ $model->name }}</small></h1>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'settings') class="active"@endif>{!! link_to_route('edit_room_path', 'Settings', [$property->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'content') class="active"@endif>{!! link_to_route('content_room_path', 'Content', [$property->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'amenities') class="active"@endif>{!! link_to_route('room_amenities_path', 'Features', [$property->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'slideshow') class="active"@endif>{!! link_to_route('slideshow_room_path', $property->present()->roomNoun() . ' Images', [$property->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'subtype') class="active"@endif>{!! link_to_route('subtypes_path', $property->present()->roomNoun() . ' Sub-types', [$model->uuid]) !!}</li>
    <li role="presentation"@if($tab == 'seo') class="active"@endif>{!! link_to_route('seo_room_path', 'SEO', [$property->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'related') class="active"@endif>{!! link_to_route('room_related_path', 'Related ' . $property->present()->roomNoun(2), $model->uuid) !!}</li>
    <li role="presentation"@if($tab == 'integrations') class="active"@endif>{!! link_to_route('room_integration_path', 'Integrations', [$property->uuid, $model->uuid]) !!}</li>
</ul>