@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.rooms.partials.tabs', ['tab' => 'amenities'])

    @include('admin.layouts.partials.amenities-selector', ['modelAmenities' => $model->amenities, 'destroyPath' => route('destroy_room_amenity_path', [$property->id, $model->id]), 'addButtonLabel' => 'Add to Room'])

@endsection
