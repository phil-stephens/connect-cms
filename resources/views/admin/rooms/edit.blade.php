@extends('admin.layouts.default')

@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

        @include('admin.rooms.partials.tabs', ['tab' => 'settings'])


        @include('admin.layouts.partials.errors')
        
        {!! Form::model($model) !!}

        {!! Form::hidden('id') !!}

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Slug Form Input -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Price Form Input -->
        <div class="form-group">
            {!! Form::label('price', 'Price From:') !!}
            {!! Form::text('price', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Capacity Input -->
        <div class="form-group">
            {!! Form::label('capacity', 'Capacity:') !!}
            {!! Form::text('capacity', null, ['class' => 'form-control']) !!}
        </div>

        {{--<!-- Settings[persons] Form Input -->--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('settings[persons]', 'Persons:') !!}--}}
            {{--{!! Form::text('settings[persons]', $model->settings['persons'], ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<!-- Settings[bedrooms] Form Input -->--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('settings[bedrooms]', 'Bedrooms:') !!}--}}
            {{--{!! Form::text('settings[bedrooms]', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<!-- Settings[bathrooms] Form Input -->--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('settings[bathrooms]', 'Bathrooms:') !!}--}}
            {{--{!! Form::text('settings[bathrooms]', $model->settings['bathrooms'], ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        <!-- Bookingbutton_id Form Input -->
        {{--<div class="form-group">--}}
            {{--{!! Form::label('bookingbutton_id', 'BookingButton ID:') !!}--}}
            {{--{!! Form::text('bookingbutton_id', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<fieldset>--}}
            {{--<legend>Room Statistics</legend>--}}

            {{--<table class="table table-striped sortable-table" id="sortable">--}}
                {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th></th>--}}
                        {{--<th>Key</th>--}}
                        {{--<th>Title</th>--}}
                        {{--<th>Value</th>--}}
                        {{--<th><i class="fa fa-trash-o"></i></th>--}}
                    {{--</tr>--}}
                {{--</thead>--}}

                {{--<tbody>--}}
                    {{--@foreach($model->stats as $stat)--}}
                    {{--<tr>--}}
                        {{--<td class="sort-handle"><i class="fa fa-fw fa-bars"></i></td>--}}

                        {{--<td>--}}
                            {{--<!-- Key Form Input -->--}}
                            {{--{!! Form::text('stats[' . $stat->id . '][key]', $stat->key, ['class' => 'form-control']) !!}--}}
                        {{--</td>--}}

                        {{--<td>--}}
                            {{--<!-- Title Form Input -->--}}
                            {{--{!! Form::text('stats[' . $stat->id . '][title]', $stat->title, ['class' => 'form-control']) !!}--}}
                        {{--</td>--}}

                        {{--<td>--}}
                            {{--<!-- Title Form Input -->--}}
                            {{--{!! Form::text('stats[' . $stat->id . '][value]', $stat->value, ['class' => 'form-control']) !!}--}}
                        {{--</td>--}}
                        {{----}}
                        {{--<td>--}}
                            {{--<input type="checkbox" name="stats[{{ $stat->id }}][delete]" value="1"/>--}}
                            {{--{!! Form::hidden('stats[' . $stat->id . '][order]', $stat->order, ['class' => 'sort-order']) !!}--}}
                            {{--{!! Form::hidden('stats[' . $stat->id . '][id]', $stat->id) !!}--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--@endforeach--}}
                {{--</tbody>--}}
            {{--</table>--}}

            {{--<a href="#" class="btn btn-default room-stats-btn pull-right">Add New Room Statistic</a>--}}
        {{--</fieldset>--}}

<fieldset>
    <legend>{{ $model->property->present()->roomNoun() }} Statistics</legend>

    @foreach($model->property->statistics as $statistic)
            <?php
              if( ! $existing = $model->getStat($statistic->key))
              {
                  $existing = (object) [
                      'value'   => null,
                      'description' => null
                  ];
              }
            ?>
        <!--  Form Input -->
        <div class="form-group">
            {!! Form::label('stats[' . $statistic->id . '][value]', $statistic->title . ':') !!}
            {!! Form::text('stats[' . $statistic->id . '][value]', $existing->value, ['class' => 'form-control', 'placeholder' => 'Value']) !!}
        </div>

        <div class="form-group">
            {!! Form::textarea('stats[' . $statistic->id . '][description]', $existing->description, ['class' => 'form-control', 'rows' => 2, 'placeholder' => 'Optional description']) !!}
        </div>
    @endforeach
</fieldset>


<div class="alert alert-info">
    <div class="checkbox">
        <label>
            {!! Form::checkbox('active', 1, null) !!} Room is active
        </label>
        <span class="help-block">If this room is not active it will not appear on any sites associated with this property</span>
    </div>
</div>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}


        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Remove {{ $property->present()->roomNoun() }}</h3>
            </div>
            <div class="panel-body">
                <p>This cannot be undone.</p>

                {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_room_path', $property->id, $model->id], 'class' => 'destroy-form']) !!}

                {!! Form::hidden('id', $model->id) !!}

                <!-- Submit field -->
                <div class="form-group">
                    {!! Form::submit('Remove ' . $property->present()->roomNoun() . ' Now', ['class' => 'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>

        <div class="alert alert-info">
            'Best Rate' JSON URL for this {{ $property->present()->roomNoun(1, false) }} type:
            {!! link_to_route('rate_json_path', null, $model->uuid) !!}
        </div>
@endsection

@section('scripts')
    @parent
    {{--<script src="/core/js/handlebars.js"></script>--}}
    {{--<script src="/core/js/jquery.sortable.min.js"></script>--}}

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this room?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>

    {{--<script>--}}
        {{--function initSorting()--}}
        {{--{--}}
            {{--$('#sortable').sortable({--}}
                {{--group: 'stats',--}}
                {{--containerSelector: 'table',--}}
                {{--itemPath: '> tbody',--}}
                {{--itemSelector: 'tr',--}}
                {{--placeholder: '<tr class="placeholder"/>',--}}
                {{--handle: 'td.sort-handle',--}}
                {{--onDrop: function ($item, container, _super) {--}}
                    {{--doSorting();--}}
                {{--}--}}
            {{--});--}}
        {{--}--}}

        {{--function doSorting()--}}
        {{--{--}}
            {{--var i = 0;--}}
            {{--$('#sortable tbody tr').each(function() {--}}
                {{--$('.sort-order', this).val(i);--}}

                {{--i++;--}}
            {{--});--}}
        {{--}--}}

        {{--$('.room-stats-btn').on('click', function(e)--}}
        {{--{--}}
            {{--e.preventDefault();--}}

            {{--var fieldHash = Math.random().toString(36).slice(2);--}}

            {{--var source   = $("#room-statistic-row-template").html();--}}
            {{--var template = Handlebars.compile(source);--}}
            {{--var html    = template({hash: fieldHash });--}}

            {{--$('#sortable tbody').append(html);--}}


            {{--initSorting();--}}
            {{--doSorting();--}}
        {{--});--}}

        {{--initSorting();--}}


    {{--</script>--}}

    {{--@include('admin.layouts.partials.handlebars.room-statistic')--}}
@endsection