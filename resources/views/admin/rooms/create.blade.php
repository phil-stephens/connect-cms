@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

        <h1 class="page-header">Create {{ $property->present()->roomNoun() }}</h1>

        @include('admin.layouts.partials.errors')
        
        {!! Form::open() !!}

        {!! Form::hidden('property_id', $property->id) !!}

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Slug Form Input -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Price Form Input -->
        <div class="form-group">
            {!! Form::label('price', 'Price From:') !!}
            {!! Form::text('price', null, ['class' => 'form-control']) !!}
        </div>

        {{--<!-- Settings[persons] Form Input -->--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('settings[persons]', 'Persons:') !!}--}}
            {{--{!! Form::text('settings[persons]', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<!-- Settings[bedrooms] Form Input -->--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('settings[bedrooms]', 'Bedrooms:') !!}--}}
            {{--{!! Form::text('settings[bedrooms]', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        {{--<!-- Settings[bathrooms] Form Input -->--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('settings[bathrooms]', 'Bathrooms:') !!}--}}
            {{--{!! Form::text('settings[bathrooms]', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        <!-- Bookingbutton_id Form Input -->
        {{--<div class="form-group">--}}
            {{--{!! Form::label('bookingbutton_id', 'BookingButton ID:') !!}--}}
            {{--{!! Form::text('bookingbutton_id', null, ['class' => 'form-control']) !!}--}}
        {{--</div>--}}

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Create ' . $property->present()->roomNoun(), ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}

@endsection

@include('admin.layouts.partials.sluggify')