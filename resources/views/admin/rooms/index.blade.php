@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

<div class="page-header">

    <h1 class="pull-left">{{ $property->present()->roomNoun(2) }}</h1>

    <a href="{{ route('create_room_path', $property->id) }}" class="btn btn-primary btn-lg pull-right">Create New {{ $property->present()->roomNoun() }}</a>
</div>

<table class="table table-striped sortable-table" id="sortable">
    <thead>
    <tr>
        <th></th>
        <th>{{ $property->present()->roomNoun(1) }} Name</th>
        <th>Price</th>
        <th class="button-column"></th>
        <th class="button-column"></th>
    </tr>
    </thead>
    <tbody>
    @foreach($rooms as $room)
        <tr data-id="{{ $room->id }}">
            <td class="sort-handle"><i class="fa fa-fw fa-bars"></i></td>
            <td>
                <a href="{!! route('edit_room_path', [$property->id, $room->id])  !!}">
                {{ $room->name }}
                @if( ! $room->active)<span class="label label-default text-uppercase">Inactive</span>@endif
                </a>
            </td>
            <td>
                @if( ! empty($room->price))
                ${{ $room->price }}
                @endif
            </td>
            <td>{!! link_to_route('edit_room_path', 'Edit', [$property->id, $room->id], ['class' => 'btn btn-primary btn-sm']) !!}</td>
            <td>
                {!! Form::open(['route' => ['destroy_room_path', $property->id, $room->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                {!! Form::hidden('id', $room->id) !!}
                <button type="submit" class="btn btn-default btn-sm">Remove</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@endsection

@section('scripts')
    @parent
    <script src="/core/js/jquery.sortable.min.js"></script>

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this {{ $property->room_noun }}?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });

        $('#sortable').sortable({
            group: 'slides',
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            handle: 'td.sort-handle',
            onDrop: function ($item, container, _super) {
                // Just loop through the table
                var i = 0;
                var rooms = [];
                $('#sortable tbody tr').each(function() {

                    rooms[ $(this).data('id') ] = i;
//                    $('.sort-order', this).val(i);

                    i++;
                });

                //console.log(JSON.stringify(pages));

                // Submit details via AJAX
                $.ajax({
                    type: "POST",
                    url: '{{ route('sort_rooms_path', $property->id) }}',
                    data: {_token: $('meta[name="csrf_token"]').attr('content'), data: rooms},
                    success: function(data)
                    {
                        console.log(data);
                    }
                });

                _super($item, container);
            }
        });
    </script>
@endsection