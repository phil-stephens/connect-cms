@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.rooms.partials.tabs', ['tab' => 'integrations'])


    <h2 class="page-header">Edit Integration</h2>

    {!! Form::open() !!}

        <div class="form-group">
            {!! Form::label('integration', 'Integration:') !!}
            {!! Form::select('integration', [$integration->name], null, ['class' => 'form-control', 'disabled']) !!}
        </div>

        <!-- Client_id Form Input -->
        <div class="form-group">
            {!! Form::label('client_id', 'Client ID:') !!}
            {!! Form::text('client_id', $integration->pivot->client_id, ['class' => 'form-control']) !!}
        </div>
    
        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@endsection