@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.rooms.partials.tabs', ['tab' => 'integrations'])


    <div class="clearfix">
        <a href="{{ route('room_integration_add_path', [$property->uuid, $model->uuid]) }}" class="btn btn-primary pull-right">Add an Integration</a>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Integration Name</th>
                <th>Type</th>
                <th>Client ID</th>
                <th class="button-column"></th>
            </tr>
        </thead>
        <tbody>
        @foreach($model->integrations as $integration)
            <tr>

                <td>{{ $integration->name }}</td>

                <td>{{ config('integrations.providers.rates.' . class_basename($integration->type))['name'] }}</td>

                <td>{{ $integration->pivot->client_id }}</td>
                <td>
                    {!! link_to_route('room_integration_edit_path', 'Edit', [$property->uuid, $model->uuid, $integration->uuid], ['class' => 'btn btn-sm btn-primary']) !!}
                </td>
            </tr>
        @endforeach
        </tbody>


    </table>

@endsection