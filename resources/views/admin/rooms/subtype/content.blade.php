@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.rooms.partials.tabs', ['tab' => 'subtype'])

    @include('admin.rooms.subtype.partials.tabs', ['tab' => 'content'])



    {!! Form::model($type) !!}

    @include('admin.layouts.partials.form-content', ['model' => $type])

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}

@endsection
