@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.rooms.partials.tabs', ['tab' => 'subtype'])

    <div class="page-header">
        <h2 class="pull-left">Create {{ $property->present()->roomNoun() }} Sub-type</h2>
    </div>

    {!! Form::open() !!}
        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Slug Form Input -->
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>
    
    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create ' . $property->present()->roomNoun() . ' Sub-type', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}

@endsection

@include('admin.layouts.partials.sluggify')