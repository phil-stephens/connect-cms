@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.rooms.partials.tabs', ['tab' => 'subtype'])

    @include('admin.rooms.subtype.partials.tabs', ['tab' => 'edit'])



    {!! Form::model($type) !!}
            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    <div class="form-group">
        {!! Form::label('slug', 'Slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}

    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Remove {{ $property->present()->roomNoun() }} Sub-type</h3>
        </div>
        <div class="panel-body">
            <p>This cannot be undone.</p>

            {!! Form::open(['method' => 'DELETE', 'route' => ['subtype_destroy_path', $type->uuid], 'class' => 'destroy-form']) !!}

            {!! Form::hidden('id', $model->id) !!}

                    <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Remove ' . $property->present()->roomNoun() . ' Sub-type Now', ['class' => 'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this sub-type?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });

    </script>

@endsection