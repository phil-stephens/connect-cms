@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.rooms.partials.tabs', ['tab' => 'subtype'])

    <div class="clearfix">
        <a href="{{ route('subtype_create_path', [$model->uuid]) }}" class="btn btn-primary pull-right">Create a Sub-type</a>
    </div>

    <table class="table table-striped sortable-table" id="sortable">

        <thead>
        <tr>
            <th></th>
            <th>Name</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>

        <tbody>
            @foreach($model->subTypes as $type)
                <tr data-uuid="{{ $type->uuid }}">
                    <td class="sort-handle"><i class="fa fa-fw fa-bars"></i></td>
                    <td>{!! link_to_route('subtype_edit_path', $type->name, [$type->uuid]) !!}</td>
                    <td>{!! link_to_route('subtype_edit_path', 'Edit', [$type->uuid], ['class' => 'btn btn-primary btn-sm']) !!}</td>
                    <td>
                        {!! Form::open(['route' => ['subtype_destroy_path', $type->uuid ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                        {!! Form::hidden('id', $type->id) !!}
                        <button type="submit" class="btn btn-default btn-sm">Remove</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>

    </table>
@endsection

@section('scripts')
    @parent
    <script src="/core/js/jquery.sortable.min.js"></script>

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this {{ $property->room_noun }} sub-type?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });

        $('#sortable').sortable({
            group: 'slides',
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            handle: 'td.sort-handle',
            onDrop: function ($item, container, _super) {
                // Just loop through the table
                var i = 0;
                var subTypes = [];
                $('#sortable tbody tr').each(function() {

                    subTypes[ i ] = $(this).data('uuid');
//                    $('.sort-order', this).val(i);

                    i++;
                });

                // Submit details via AJAX
                $.ajax({
                    type: "POST",
                    url: '{{ route('subtype_sort_path', $model->uuid) }}',
                    data: {_token: $('meta[name="csrf_token"]').attr('content'), data: subTypes },
                    success: function(data)
                    {
                        console.log(data);
                    }
                });

                _super($item, container);
            }
        });
    </script>
@endsection