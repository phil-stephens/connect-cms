<h2 class="page-header">{{ $property->present()->roomNoun() }} Sub-type <small>{{ $type->name }}</small></h2>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('subtype_edit_path', 'Settings', [$type->uuid]) !!}</li>
    <li role="presentation"@if($tab == 'content') class="active"@endif>{!! link_to_route('subtype_content_path', 'Page Content', [$type->uuid]) !!}</li>
    <li role="presentation"@if($tab == 'slideshow') class="active"@endif>{!! link_to_route('subtype_slideshow_path', 'Sub-type Images', [$type->uuid]) !!}</li>
    <li role="presentation"@if($tab == 'seo') class="active"@endif>{!! link_to_route('subtype_seo_path', 'SEO', [$type->uuid]) !!}</li>
</ul>