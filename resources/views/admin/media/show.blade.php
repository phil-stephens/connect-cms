@extends('admin.layouts.default')

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Media Library <small>{{ $library->name }}</small></h1>

        <button class="btn btn-lg pull-right btn-primary" onclick="$('#upload').slideToggle();">Upload Images</button>
    </div>

    <div id="upload" style="display: none;">
        <div id="image-upload" class="slideshow-image-uploader placeholder">
            <div class="dropzone" id="image-clickable">
                <div id="image-preview" class="dropzone-previews"></div>
            </div>
        </div>
    </div>

    <div id="grid">
        <div class="row">
            @foreach($images as $image)
                <div class="col-sm-2 col-xs-3">
                    <div class="thumbnail">
                        <div class="transparent-backdrop">
                            {!! $image->getTag(['w' => '200', 'h' => 200, 'fit' => 'crop'], ['class' => 'img-responsive']) !!}
                        </div>

                        {!! Form::open(['route' => ['image_destroy_path', $image->uuid], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                        <div class="btn-group" role="group">
                            <a role="button" href="{!! $image->getPath(['w' => 1024, 'h' => 1024,]) !!}" class="btn btn-default lightbox" rel="gallery"><i class="fa fa-eye"></i></a>
                            <button type="button" data-toggle="modal" data-target="#edit-image" class="btn btn-default" data-image-uuid="{{ $image->uuid }}"><i class="fa fa-cog"></i></button>
                            <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            @endforeach
        </div>

        {!! $images->render() !!}
    </div>
@endsection

@section('modals')
    <div class="modal fade" tabindex="-1" role="dialog" id="edit-image">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Image</h4>
                </div>
                <div id="editor-form">
                    <div class="modal-body">
                    </div>

                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/dropzone.min.css"/>
    <link rel="stylesheet" href="{!! core_asset('js/fancybox/jquery.fancybox.css') !!}"/>
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('scripts')
    @parent

    <script src="/core/js/dropzone.min.js"></script>
    <script src="/core/js/handlebars.js"></script>
    <script src="/core/js/fancybox/jquery.fancybox.pack.js"></script>
    <script src="{!! core_asset('js/spin.min.js') !!}"></script>

    <script>
        $( function() {

            $('.lightbox').fancybox({
                padding : 0,
                closeBtn: false
            });
        });

        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to permanently remove this image? This cannot be undone.', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });

        Dropzone.autoDiscover = false;

        var imageDropzone = new Dropzone( document.getElementById('image-upload'), {
            url: '{{ route('content_media_path') }}',
            addRemoveLinks: true,
            acceptedFiles: 'image/*',
            maxFiles: 50,
            maxFilesize: {{ env('UPLOAD_LIMIT', 32) }},
            parallelUploads: 1,
            previewsContainer: "#image-preview",
            clickable: "#image-clickable"
        });

        imageDropzone.on('addedfile', function(file) {
            $('#image-upload').removeClass('placeholder');
        });

        imageDropzone.on('sending', function(file, xhr, formData) {
            formData.append('_token', $('meta[name="csrf_token"]').attr('content'));
            formData.append('url_params', '{!! serialize(['w' => 300, 'h' => 200, 'fit' => 'crop']) !!}');
            formData.append('library', '{{ $library->uuid }}');
        });

        imageDropzone.on("success", function(file, responseText) {
            // Add the image to the table
//            this.removeFile(file);

            // might need to add the table here...
//            var source = $("#slideshow-slide-template").html();
//            var template = Handlebars.compile(source);
//            var html = template({id: responseText.id, path: responseText.path, slide_id: responseText.slide_id});
//
//            $('#sortable tbody.list-body').append(html);
        });

        imageDropzone.on('queuecomplete', function() {
//            $('#image-upload').addClass('placeholder');
            location.reload();
        });

        $('#edit-image').on('show.bs.modal', function (e) {
            var btn = $(e.relatedTarget) // Button that triggered the modal

            if(btn.data('image-uuid') != undefined)
            {
                $('#editor-form').spin();

                $('#editor-form').load('{!! route('image_edit_path__ajax') !!}/' + btn.data('image-uuid'));
            } else
            {
                // close the modal?
            }

        });

    </script>


@endsection