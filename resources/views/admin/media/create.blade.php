@extends('admin.layouts.default')

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Create Media Library</h1>
    </div>

    @include('admin.layouts.partials.errors')

    {!! Form::open() !!}

            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    {{--<div class="form-group">--}}
        {{--{!! Form::label('slug', 'Slug:') !!}--}}
        {{--{!! Form::text('slug', null, ['class' => 'form-control']) !!}--}}
    {{--</div>--}}

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create Media Library', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection

{{--@include('admin.layouts.partials.sluggify')--}}