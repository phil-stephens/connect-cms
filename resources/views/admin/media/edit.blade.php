@extends('admin.layouts.default')

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Edit Media Library</h1>
    </div>

    @include('admin.layouts.partials.errors')

    {!! Form::model($library) !!}

            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    {{--<div class="form-group">--}}
        {{--{!! Form::label('slug', 'Slug:') !!}--}}
        {{--{!! Form::text('slug', null, ['class' => 'form-control']) !!}--}}
    {{--</div>--}}

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection