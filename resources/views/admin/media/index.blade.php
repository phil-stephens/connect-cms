@extends('admin.layouts.default')

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Media Libraries</h1>

        {!! link_to_route('library_create_path', 'Create Media Library', [], ['class' => 'btn btn-lg pull-right btn-primary']) !!}
    </div>


    <table class="table table-striped">

        <tbody>
            @foreach($libraries as $library)
                <tr>
                    <td>
                        {!! link_to_route('library_path', $library->name, $library->uuid) !!}
                    </td>
                    <td>
                        {!! link_to_route('library_edit_path', 'Edit', $library->uuid, ['class' => 'btn btn-primary btn-sm']) !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection