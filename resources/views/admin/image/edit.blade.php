{!! Form::model($image, ['route' => ['image_edit_path', $image->uuid]]) !!}
<div class="modal-body">
<div class="row">
    <div class="col-md-5">
        {!! $image->getTag(['w' => 400, 'h' => 400], ['class' => 'img-responsive']) !!}
    </div>

    <div class="col-md-7">
        <!-- Original_name Form Input -->
        <div class="form-group">
            {!! Form::label('original_name', 'Original filename:') !!}
            {!! Form::text('original_name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Title Form Input -->
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Alt Form Input -->
        <div class="form-group">
            {!! Form::label('alt', 'Alt Text:') !!}
            {!! Form::text('alt', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Library_id Form Input -->
        <div class="form-group">
            {!! Form::label('library_id', 'Media Library:') !!}
            {!! Form::select('library_id', $libraries->lists('name', 'id')->all(), null, ['class' => 'form-control']) !!}
        </div>

        <label for="">Default Crop From:</label>
        <table class="table table-striped">
            <tbody>
            <tr>
                @foreach(['top,left', 'top', 'top,right'] as $position)
                    <td class="text-center">{!! Form::radio("crop_from", $position, (bool) ($image->crop_from == $position)) !!}</td>
                @endforeach
            </tr>

            <tr>
                @foreach(['left', '', 'right'] as $position)
                    <td class="text-center">{!! Form::radio("crop_from", $position, (bool) ($image->crop_from == $position)) !!}</td>
                @endforeach
            </tr>

            <tr>
                @foreach(['bottom,left', 'bottom', 'bottom,right'] as $position)
                    <td class="text-center">{!! Form::radio("crop_from", $position, (bool) ($image->crop_from == $position)) !!}</td>
                @endforeach
            </tr>
            </tbody>
        </table>

        <label>
            {!! Form::checkbox("face_detection", true, null) !!}
            Face detection
        </label>
    </div>
</div>

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>
{!! Form::close() !!}