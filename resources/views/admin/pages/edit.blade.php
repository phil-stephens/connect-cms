@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

        @include('admin.pages.partials.tabs', ['tab' => 'edit'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}

        {!! Form::hidden('id') !!}
        
        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Slug Form Input -->
        <div class="form-group">
            {!! Form::label('slug', 'URL slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Chapter Form Input -->
        <div class="form-group">
            {!! Form::label('chapter_id', 'Chapter:') !!}
            {!! Form::select('chapter_id', $chapters, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Template Form Input -->
        <div class="form-group">
            {!! Form::label('template', 'Template:') !!}
            {!! Form::select('template', $templates, null, ['class' => 'form-control']) !!}
        </div>

        <fieldset>
            <legend>Password Protection</legend>

            <div class="checkbox">
                <label>
                    {!! Form::checkbox('protected', 1, null) !!} Page is password-protected
                </label>
            </div>

            <!-- Title_note Form Input -->
            <div class="form-group">
                {!! Form::label('title_note', 'Password:') !!}
                {!! Form::text('title_note', null, ['class' => 'form-control']) !!}
            </div>
        </fieldset>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}

        {{--{!! Form::open(['files' => true, 'route' => ['content_image_path', $uploadHash] ] ) !!}--}}
            {{--{!! Form::file('file') !!}--}}

        {{--{!! Form::hidden('target', 'content') !!}--}}
        {{--{!! Form::hidden('model', get_class($model)) !!}--}}
        {{--{!! Form::hidden('id', $model->id) !!}--}}
        {{--<!-- Submit field -->--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::submit('Go', ['class' => 'btn btn-primary']) !!}--}}
        {{--</div>--}}
        {{--{!! Form::close() !!}--}}

        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Remove Page</h3>
            </div>
            <div class="panel-body">
                <p>This cannot be undone.</p>

                {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_page_path', $site->id, $model->id], 'class' => 'destroy-form']) !!}

                {!! Form::hidden('id', $model->id) !!}

                <!-- Submit field -->
                <div class="form-group">
                    {!! Form::submit('Remove Page Now', ['class' => 'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>


@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this page?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection