<div class="page-header">
    <h1 class="pull-left">Edit Page <small>{{ $model->name }}</small></h1>

    <a href="{!! $model->getUrl($model->site) !!}" target="_blank" class="btn btn-default pull-right"><i class="fa fa-globe"></i> Go to page on site</a>
</div>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('edit_page_path', 'General', [$site->id, $model->id]) !!}</li>
    {{--<li role="presentation"@if($tab == 'settings') class="active"@endif>{!! link_to_route('page_settings_path', 'Template Settings', [$site->id, $model->id]) !!}</li>--}}
    <li role="presentation"@if($tab == 'content') class="active"@endif>{!! link_to_route('content_page_path', 'Content', [$site->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'slideshow') class="active"@endif>{!! link_to_route('slideshow_page_path', 'Page Images', [$site->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'seo') class="active"@endif>{!! link_to_route('seo_page_path', 'SEO', [$site->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'related') class="active"@endif>{!! link_to_route('page_related_path', 'Related Pages', $model->uuid) !!}</li>
    @if(Auth::user()->developer)
    <li role="presentation"@if($tab == 'annotations') class="active"@endif>{!! link_to_route('page_annotations_path', 'Annotations', [$site->id, $model->id]) !!}</li>
    @endif
</ul>


