@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

        <h1 class="page-header">Create Page</h1>

        @include('admin.layouts.partials.errors')

        {!! Form::open() !!}

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Slug Form Input -->
        <div class="form-group">
            {!! Form::label('slug', 'URL slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Chapter Form Input -->
        <div class="form-group">
            {!! Form::label('chapter_id', 'Chapter:') !!}
            {!! Form::select('chapter_id', $chapters, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Template Form Input -->
        <div class="form-group">
            {!! Form::label('template', 'Template:') !!}
            {!! Form::select('template', $templates, 'page', ['class' => 'form-control']) !!}
        </div>

        <fieldset>
            <legend>Page Content</legend>
            
            @include('admin.layouts.partials.form-content')
        </fieldset>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Create Page', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}
@endsection

@include('admin.layouts.partials.sluggify')