@extends('admin.layouts.default')

@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
@include('admin.sites.partials.side-nav')
@endsection

@section('content')

        @include('admin.pages.partials.tabs', ['tab' => 'content'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}

        {!! Form::hidden('id') !!}
        


        @include('admin.layouts.partials.form-content')

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}


@endsection