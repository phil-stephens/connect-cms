@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

        @include('admin.pages.partials.tabs', ['tab' => 'annotations'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($annotation) !!}

        {!! Form::hidden('id') !!}

        <fieldset>
            <legend>Page Title</legend>

            <div class="checkbox">
                <label>
                    {!! Form::checkbox('title', 1, null) !!} Page Title is used
                </label>
            </div>

            <!-- Title_note Form Input -->
            <div class="form-group">
                {!! Form::label('title_note', 'Usage Notes:') !!}
                {!! Form::text('title_note', null, ['class' => 'form-control']) !!}
            </div>
        </fieldset>

        <fieldset>
            <legend>Excerpt</legend>

            <div class="checkbox">
                <label>
                    {!! Form::checkbox('excerpt', 1, null) !!} Excerpt is used
                </label>
            </div>

            <!-- Title_note Form Input -->
            <div class="form-group">
                {!! Form::label('excerpt_note', 'Usage Notes:') !!}
                {!! Form::text('excerpt_note', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Excerpt_character_limit Form Input -->
            <div class="form-group">
                {!! Form::label('excerpt_character_limit', 'Character Limit (guidance only):') !!}
                {!! Form::text('excerpt_character_limit', null, ['class' => 'form-control']) !!}
            </div>
        </fieldset>

        <fieldset>
            <legend>Body</legend>

            <div class="checkbox">
                <label>
                    {!! Form::checkbox('body', 1, null) !!} Body is used
                </label>
            </div>

            <!-- Title_note Form Input -->
            <div class="form-group">
                {!! Form::label('body_note', 'Usage Notes:') !!}
                {!! Form::text('body_note', null, ['class' => 'form-control']) !!}
            </div>
        </fieldset>

        <fieldset>
            <legend>Featured Image</legend>

            <div class="checkbox">
                <label>
                    {!! Form::checkbox('image', 1, null) !!} Featured Image is used
                </label>
            </div>

            <!-- Title_note Form Input -->
            <div class="form-group">
                {!! Form::label('image_note', 'Usage Notes:') !!}
                {!! Form::text('image_note', null, ['class' => 'form-control']) !!}
            </div>
        </fieldset>



        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}



@endsection