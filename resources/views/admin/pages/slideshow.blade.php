@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

        @include('admin.pages.partials.tabs', ['tab' => 'slideshow'])

        @include('admin.layouts.partials.form-slideshow')

@endsection
