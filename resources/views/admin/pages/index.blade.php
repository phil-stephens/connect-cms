@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

<div class="page-header">
    <h1 class="pull-left">Pages</h1>

    <div class="pull-right">
        <a href="{{ route('create_page_path', $site->id) }}" class="btn btn-primary btn-lg">Create Page</a>
        <a href="{{ route('chapter_create_path', $site->id) }}" class="btn btn-default btn-lg">Create Chapter</a>
    </div>
</div>
@foreach($chapters as $chapter)

<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <h2 class="panel-title pull-left">{{ $chapter->name }} <a href="{!! route('chapter_edit_path', [$site->id, $chapter->uuid]) !!}"><i class="fa fa-fw fa-cog"></i></a></h2>

        <a href="#" class="pull-right" onclick="$('#panel-{{ $chapter->uuid }}').slideToggle(); return false;"><i class="fa fa-angle-down"></i></a>
    </div>

    <div class="panel-body" id="panel-{{ $chapter->uuid }}">
        <table class="table table-striped" role="chapter-table" data-chapter-uuid="{{ $chapter->uuid }}">

        </table>
    </div>
</div>

@endforeach

@endsection

@section('scripts')
    @parent

    <script src="{!! core_asset('js/spin.min.js') !!}"></script>
    <script>
        $(document).on('submit', '.destroy-form', function (e) {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this page?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });

        function initChapters()
        {
            $('[role="chapter-table"]').each(function() {
                var uuid = $(this).data('chapter-uuid');
                getPosts('{{ route('chapter_pages_path', $site->id) }}/' + uuid, this);
            });
        }

        function getPosts(path, table)
        {
            $(table).closest('.panel').spin();

            $.ajax({
                url: path,
                dataType: 'json',
            }).done(function (data) {
                $(table).html(data);
                $(table).closest('.panel').spin(false);
            }).fail(function () {
//                alert('Pages could not be loaded.');
            });
        }

        $(document).ready(function() {
            $(document).on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href'), $(this).closest('[role="chapter-table"]'));
                e.preventDefault();
            });

            initChapters();
        });
    </script>
@endsection