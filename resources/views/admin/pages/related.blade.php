@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
@include('admin.sites.partials.side-nav')
@endsection

@section('content')

@include('admin.pages.partials.tabs', ['tab' => 'related'])


<div class="row">
    <div class="col-md-4">

        <div class="panel-group" id="source-accordion" role="tablist" aria-multiselectable="true">
            @foreach($chapters as $index => $pages)
                @if($pages->count())
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="{{ str_slug($index) }}-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#source-accordion" href="#collapse-{{ str_slug($index) }}" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-angle-down text-muted"></i> {{ $index }}
                                </a>
                            </h4>
                        </div>



                        <div id="collapse-{{ str_slug($index) }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{ str_slug($index) }}-heading">

                            {!! Form::open(['class' => 'source-form']) !!}
                            <div class="panel-body">

                                @foreach($pages as $page)
                                    @if( empty($item->internal) )
                                        <?php
                                        $name = $page->name;
                                        ?>

                                        <div class="checkbox"
                                             data-id="{{ $page->id }}"
                                             data-name="{{ $page->name }}">
                                            <label>
                                                <input type="checkbox" name="item[{{ $page->id }}][add]" value="1" />
                                                {{ $page->name }}
                                            </label>
                                        </div>
                                    @endif
                                @endforeach

                            </div>

                            <div class="panel-footer">
                                {!! Form::submit('Relate to Page', ['class' => 'btn btn-primary btn-sm']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>


                    </div>
                @endif
            @endforeach
        </div>

    </div>

    {!! Form::open() !!}
    <div class="col-md-8">

        <div class="panel-group" id="target-list" role="tablist" aria-multiselectable="true">


            @foreach($model->related as $page)
                <div class="panel panel-default" id="page-{{ $page->id }}">
                    <div class="panel-heading draggable" role="tab" id="page-{{ $page->id }}-heading">

                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse-page-{{ $page->id }}" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fa fa-angle-down text-muted"></i> {{ $page->name }}
                            </a>
                        </h4>
                    </div>

                    <div id="collapse-page-{{ $page->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="page-{{ $page->id }}-heading">
                        <div class="panel-body">

                            {!! Form::hidden('page[' . $page->id . '][id]', $page->id, ['class' => 'page-id']) !!}

                                    <!-- Label Form Input -->
                            <div class="form-group">
                                {!! Form::label('page[' . $page->id . '][key]', 'Key:') !!}
                                {!! Form::text('page[' . $page->id . '][key]', $page->pivot->key, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="panel-footer clearfix">
                            <a href="#" class="btn btn-sm btn-default pull-right" role="remove-page" data-target="{{ $page->id }}">Remove</a>
                        </div>
                    </div>
                </div>
            @endforeach


        </div>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

    </div>

    {!! Form::close() !!}

</div>

@endsection


@section('scripts')
    @parent

    <script src="/core/js/handlebars.js"></script>

    <script>
        $('.source-form').on('submit', function(e) {

            e.preventDefault();

            var theForm = this;

            $('.checkbox', theForm).each(function()
            {
                if($('[type="checkbox"]', this).is(':checked'))
                {
                    var source   = $("#related-page-template").html();
                    var template = Handlebars.compile(source);
                    var html    = template({
                        hash: Math.random().toString(36).slice(2),
                        name: $(this).data('name'),
                        id: $(this).data('id')
                    });

                    $('#target-list').append(html);

                    $(this).remove();
                }
            });

        });


        $(document).on('click', '[role="remove-page"]', function(e) {
            e.preventDefault();

            var target = $('#page-' + $(this).data('target'));

            var pageId = $('.page-id', target).val();

            if(pageId != undefined)
            {
                // Do something to remove it here
                $.ajax({
                    type: "POST",
                    url: '{{ route('page_destroy_related_path', $model->uuid) }}',
                    data: {_method: 'DELETE', _token: $('meta[name="csrf_token"]').attr('content'), id: pageId},
                    success: function(data)
                    {
                        console.log(data);
                    }
                });
            }


            $(target).remove();
        });
    </script>

    @include('admin.layouts.partials.handlebars.related-page')
@endsection