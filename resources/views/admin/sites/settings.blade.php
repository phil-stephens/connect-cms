@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

@include('admin.sites.partials.tabs', ['tab' => 'custom_settings'])

@include('admin.layouts.partials.errors')

{!! Form::open() !!}
@foreach($settings as $setting)
    @include('admin.layouts.partials.setting-field', ['model' => $site])
@endforeach
    
    
<!-- Submit field -->
<div class="form-group">
    {!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}
</div>

{!! Form::close() !!}

@endsection

@section('modals')
    @parent
    @if(empty($_media_modal))
        @include('admin.layouts.partials.media-modal')
    @endif
@endsection

@if(env('EDITOR_FORMAT', 'md') === 'html')
@section('scripts')
    @parent

    {{--<script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>--}}
    <script src="/core/js/tinymce/tinymce.min.js"></script>
    <script>
        function tinymceInit()
        {
            tinymce.init({
                selector: 'textarea[role="editor"]',
                menubar : false,
                content_css : '/core/css/editor.css',
                statusbar : false,
                plugins: "link code paste",
                toolbar: "bold italic styleselect | bullist numlist | link code",
                valid_elements : '+*[*]',
                convert_urls: false,
                style_formats: [
                    {title: "Header 1", format: "h1"},
                    {title: "Header 2", format: "h2"},
                    {title: "Header 3", format: "h3"},
                    {title: "Header 4", format: "h4"},
                    {title: "Header 5", format: "h5"},
                    {title: "Header 6", format: "h6"},
                    {title: "Blockquote", format: "blockquote"}
                ]
            });
        }

        tinymceInit();
    </script>
@endsection
@endif

@section('scripts')
    @parent
    <script src="/core/js/handlebars.js"></script>
    <script src="/core/js/jquery.sortable.min.js"></script>
    <script src="/core/js/dropzone.min.js"></script>
    <script src="/core/js/jscolor/jscolor.js"></script>


    <script>
        Dropzone.autoDiscover = false;

        function imageDropzoneInit() {

            $('[role="image-dropzone"]').each(function()
            {
                if(this.dropzone == undefined)
                {
                    var hash = $(this).data('hash');
                    var target = $(this).data('target');
                    var targetId = $(this).data('target-id');
                    var model = $(this).data('model');
                    var clickable = $('.image-clickable', this);
                    var previews = $('.dropzone-previews', this);
                    var toolbar = $('.toolbar', this);

                    if( ! $('.content-image', this).length)
                    {
                        $(this).append('<img class="img-responsive content-image" />');
                    }

                    var image = $('.content-image', this);

                    $(this).dropzone( {
                        url: '{{ route('content_image_path') }}/' + hash,
                        addRemoveLinks: true,

                        acceptedFiles: 'image/*',
                        maxFiles: 1,
                        maxFilesize: 32,
                        parallelUploads: 1,
                        previewsContainer: previews[0],
                        clickable: clickable[0],
                        init: function() {
                            this.on('addedfile', function(file) {
                                image.css('opacity', 0.3);
                            });

                            this.on('sending', function(file, xhr, formData) {
                                formData.append('_token', $('meta[name="csrf_token"]').attr('content'));
                                formData.append('url_params', '{!! serialize(['w' => 400, 'h' => 300, 'fit' => 'crop']) !!}');
                                if(target != undefined) formData.append('target', target);
                                if(model != undefined) formData.append('model', model);
                                if(model != targetId) formData.append('id', targetId);
                            });

                            this.on("success", function(file, responseText) {
                                console.log(responseText);

                                image.attr('src', responseText.path).css('opacity', 1);

                                image.attr('id', 'image-preview-' + responseText.id);

                                $('button', toolbar).each(function() {
                                    $(this).data('id', responseText.id);
                                })

                                toolbar.fadeIn();
                                this.removeFile(file);
                            });
                        }
                    });
                }
            })
        }

        function fileDropzoneInit() {

            $('[role="file-dropzone"]').each(function()
            {
                if(this.dropzone == undefined)
                {
                    var hash = $(this).data('hash');
                    var targetId = $(this).data('target-id');
                    var clickable = $('.image-clickable', this);
                    var previews = $('.dropzone-previews', this);
                    var fileInfo = $('.file-info', this);

                    $(this).dropzone( {
                        url: '{{ route('content_file_path') }}/' + hash,
                        addRemoveLinks: true,
                        maxFiles: 1,
                        maxFilesize: 32,
                        parallelUploads: 1,
                        previewsContainer: previews[0],
                        clickable: clickable[0],
                        init: function() {

                            this.on('sending', function(file, xhr, formData) {
                                formData.append('_token', $('meta[name="csrf_token"]').attr('content'));
                                formData.append('id', targetId);
                            });

                            this.on("success", function(file, responseText) {
                                console.log(responseText);

                                fileInfo.html('<i class="fa ' + responseText.mime_class + ' fa-3x"></i> ' + responseText.name + ' <small>' + responseText.size + '</small>');

                                this.removeFile(file);
                            });
                        }
                    });
                }
            })
        }

        imageDropzoneInit();
//        fileDropzoneInit();



    </script>
@endsection

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/dropzone.min.css"/>

    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection