<div class="page-header">
    <h1 class="pull-left">Website Settings</h1>

    @if(Auth::user()->developer)
    <a href="{{ route('create_site_path') }}" class="btn btn-primary btn-lg pull-right">Create New Website</a>
    @endif
</div>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'settings') class="active"@endif><a href="{{ route('edit_site_path', $site->id) }}">General</a></li>
    <li role="presentation"@if($tab == 'custom_settings') class="active"@endif><a href="{{ route('site_custom_settings_path', $site->id) }}">Theme Settings</a></li>
    <li role="presentation"@if($tab == 'seo') class="active"@endif><a href="{{ route('seo_site_path', $site->id) }}">Site SEO</a></li>
    <li role="presentation"@if($tab == 'domains') class="active"@endif><a href="{{ route('domains_path', $site->id) }}">Domains</a></li>
    <li role="presentation"@if($tab == 'aliases') class="active"@endif><a href="{{ route('aliases_path', $site->id) }}">Aliases</a></li>
    <li role="presentation"@if($tab == 'taxonomies') class="active"@endif><a href="{{ route('taxonomies_path', $site->id) }}">Taxonomies</a></li>
    @if(env('HAS_PROPERTIES', true))
    <li role="presentation"@if($tab == 'properties') class="active"@endif><a href="{{ route('site_properties_path', $site->id) }}">Properties</a></li>
    @endif

    @if(Auth::user()->developer)
        <li role="presentation"@if($tab == 'export') class="active"@endif>{!! link_to_route('export_site_path', 'Export', $site->uuid) !!}</li>
    @endif
</ul>