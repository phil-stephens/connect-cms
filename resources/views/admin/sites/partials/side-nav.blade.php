@section('extra-nav-links')
    <ul class="nav navbar-nav">
        <li><a href="{!! route('site_path', $site->id) !!}">
                <i class="fa fa-fw fa-code"></i> {{ $site->name }}
                @if($site->in_development)
                    &nbsp;<span class="label label-info text-uppercase">Dev</span>
                @else
                    &nbsp;<span class="label label-success text-uppercase">Live</span>
                @endif
            </a></li>
    </ul>
@endsection


<div class="col-sm-2 sidebar secondary">
        <ul class="nav nav-sidebar">
            <li role="presentation"><a href="{{ route('edit_site_path', $site->id) }}"><i class="fa fa-fw fa-cog"></i> Settings</a></li>
            <li role="presentation"><a href="{{ route('navigations_path', $site->id) }}"><i class="fa fa-fw fa-compass"></i> Navigation Groups</a></li>
            <li role="presentation"><a href="{{ route('pages_path', $site->id) }}"><i class="fa fa-fw fa-files-o"></i> Pages</a></li>

            {{--@if(env('HAS_GALLERIES', true))--}}
            {{--<li role="presentation"><a href="{{ route('galleries_path', $site->id) }}"><i class="fa fa-fw fa-picture-o"></i> Galleries</a></li>--}}
            {{--@endif--}}

            @if(env('HAS_VENUES', true))
            <li role="presentation"><a href="{{ route('site_business_path', $site->uuid) }}"><i class="fa fa-fw fa-map-marker"></i> Places Feed</a></li>
            @endif

            {{--@if(env('HAS_CAMPAIGNS', false))--}}
            {{--<li role="presentation"><a href="{{ route('campaigns_path', $site->uuid) }}"><i class="fa fa-fw fa-pie-chart"></i> Campaigns</a></li>--}}
            {{--@endif--}}
        </ul>
</div>