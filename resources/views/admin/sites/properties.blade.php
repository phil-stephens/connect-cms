@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    @include('admin.sites.partials.tabs', ['tab' => 'properties'])

        <h1 class="page-header">Properties</h1>


        {!! Form::open() !!}

        {!! Form::hidden('id', $site->id) !!}
        
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Property</th>
                    <th>Include</th>
                    <th>Exclude</th>
                </tr>
            </thead>
            <tbody>
            @foreach($properties as $property)
                <tr>
                    <td>{!! link_to_route('property_path', $property->name, $property->id) !!}</td>
                    <td>
                        {!! Form::radio("properties[{$property->id}]", 1, (bool) $site->properties->contains($property->id) ) !!}
                    </td>
                    <td>
                        {!! Form::radio("properties[{$property->id}]", 0, (bool) ! $site->properties->contains($property->id)) !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        
        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}

@endsection