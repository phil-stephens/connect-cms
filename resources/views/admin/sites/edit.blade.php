@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

@include('admin.sites.partials.tabs', ['tab' => 'settings'])

@include('admin.layouts.partials.errors')

{!! Form::model($site) !!}

{!! Form::hidden('id') !!}

<!-- Name Form Input -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Theme Form Input -->
<div class="form-group">
    {!! Form::label('theme', 'Theme:') !!}
    {!! Form::text('theme', null, ['class' => 'form-control']) !!}
</div>

@if(env('HAS_PROPERTIES', true))
<div class="form-group">
    {!! Form::label('homepage_type', 'Homepage:') !!}

    <div class="row">
        <div class="col-md-6">
            <div class="radio">
                <label>
                    {!! Form::radio('homepage_type', 'Fastrack\Pages\Page') !!}
                    Static Page
                </label>
                {!! Form::select('homepage_page_id', $pages, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="radio">
                <label>
                    {!! Form::radio('homepage_type', 'Fastrack\Properties\Property') !!}
                    or, Property
                </label>
                {!! Form::select('homepage_property_id', $properties, null, ['class' => 'form-control']) !!}
            </div>

        </div>
    </div>

</div>

<!-- Property_noun Form Input -->
{{--<div class="form-group">--}}
    {{--{!! Form::label('settings[property_noun]', 'Property noun:') !!}--}}
    {{--{!! Form::text('settings[property_noun]', ( ! empty($site->settings['property_noun'])) ? $site->settings['property_noun'] : null, ['class' => 'form-control']) !!}--}}
    {{--<span class="help-block"><i class="fa fa-info-circle"></i> The term used to describe the properties in your portfolio e.g. hotel, resort etc</span>--}}
{{--</div>--}}
@else
<div class="form-group">
    {!! Form::label('homepage_page_id', 'Homepage:') !!}
    {!! Form::hidden('homepage_type', 'Fastrack\Pages\Page') !!}
    {!! Form::select('homepage_page_id', $pages, null, ['class' => 'form-control']) !!}
</div>
@endif

<div class="alert alert-info">
    <div class="checkbox">
        <label>
            {!! Form::checkbox('in_development', 1, null) !!} Site in development
        </label>
        <span class="help-block">If this site is marked 'in development' it will block all robots and disable sitemap.xml</span>
    </div>

    <div class="checkbox">
        <label>
            {!! Form::checkbox('secure', 1, null) !!} Site uses SSL
        </label>
        <span class="help-block">Site will use <code>https://</code> protocol for all generated links</span>
    </div>
</div>

<!-- Submit field -->
<div class="form-group">
    {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
</div>



{!! Form::close() !!}

@if(Auth::user()->developer)
<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">Remove Website</h3>
    </div>
    <div class="panel-body">
        <p>This cannot be undone.</p>

        {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_site_path', $site->id], 'class' => 'destroy-form']) !!}

        {!! Form::hidden('id', $site->id) !!}

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Remove Website Now', ['class' => 'btn btn-danger']) !!}
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endif

@endsection

@if(Auth::user()->developer)
@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this website?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection
@endif