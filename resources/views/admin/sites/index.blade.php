@extends('admin.layouts.default')

@section('content')
<div class="page-header">
    <h1 class="pull-left">Websites</h1>

    @if(Auth::user()->developer)
    <a href="{{ route('create_site_path') }}" class="btn btn-primary btn-lg pull-right">Create New Website</a>
    @endif
</div>


<table class="table table-striped">
    <thead>
        <tr>
            <th class="button-column"></th>
            <th>Site Name</th>
            <th>Domains</th>
            <th class="button-column"></th>
        </tr>
    </thead>

    <tbody>
    @foreach($sites as $site)
        <tr>
            <td class="text-center">
                @if($site->in_development)
                    <span class="label label-info text-uppercase">Dev</span>
                @else
                    <span class="label label-success text-uppercase">Live</span>
                @endif
            </td>
            <td><strong>{!! link_to_route('site_path', $site->name, $site->id) !!}</strong></td>

            <td>
                <small>
                    <ul class="list-unstyled">
                        @foreach($site->domains()->lists('domain')->all() as $domain)
                            <li><a href="http://{{ $domain }}" target="_blank">{{ $domain }}</a></li>
                        @endforeach
                    </ul>
                </small>
            </td>
            <td>
                {!! link_to_route('site_path', 'Explore', $site->id, ['class' => 'btn btn-primary btn-sm']) !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection