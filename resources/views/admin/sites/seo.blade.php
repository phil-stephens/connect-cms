@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')
    @include('admin.sites.partials.tabs', ['tab' => 'seo'])

@include('admin.layouts.partials.errors')

{!! Form::model($site) !!}

{!! Form::hidden('id') !!}

@include('admin.layouts.partials.form-seo', ['include_hide_from_sitemap' => false, 'model' => $site])

<!-- Submit field -->
<div class="form-group">
    {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
</div>

{!! Form::close() !!}

@endsection