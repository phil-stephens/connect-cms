@extends('admin.layouts.default')


@section('content')
    <div class="container-fluid">
        <h1>Create New Website</h1>

        @include('admin.layouts.partials.errors')

        {!! Form::open() !!}

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Domains Form Input -->
        <div class="form-group">
            {!! Form::label('domains', 'Domain(s):') !!}
            {!! Form::text('domains', null, ['class' => 'form-control']) !!}
        </div>

        @if(env('HAS_PROPERTIES', true))
        <!-- Property_noun Form Input -->
        <div class="form-group">
            {!! Form::label('settings[property_noun]', 'Property noun:') !!}
            {!! Form::text('settings[property_noun]', 'hotel', ['class' => 'form-control']) !!}
            <span class="help-block"><i class="fa fa-info-circle"></i> The term used to describe the properties in your portfolio e.g. hotel, resort etc</span>
        </div>
        @endif

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Create Site', ['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection