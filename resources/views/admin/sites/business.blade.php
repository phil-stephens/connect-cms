@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
@include('admin.sites.partials.side-nav')

@endsection

@section('content')

<h1 class="page-header">Places Feed</h1>

@include('admin.layouts.partials.errors')

{!! Form::model($site) !!}

<div class="radio">
    <label>
        {!! Form::radio('business_feed', 'native') !!}
        Native <small>(Default)</small>
    </label>

    <p class="help-block">Places will appear in the order they have been added to the main feed</p>
</div>

{{-- Coming soon! --}}
{{--<div class="radio">--}}
    {{--<label>--}}
        {{--{!! Form::radio('business_feed', 'geo') !!}--}}
        {{--Geo--}}
    {{--</label>--}}

    {{--<p class="help-block">Businesses will appear in order of distance from the primary property associated with the site</p>--}}
{{--</div>--}}

<div class="radio">
    <label>
        {!! Form::radio('business_feed', 'manual') !!}
        Manual
    </label>
    <p class="help-block">Places feed and order on site manually managed</p>
</div>

<!-- Submit field -->
<div class="form-group">
    {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
</div>
{!! Form::close() !!}

<div id="manual" @if($site->business_feed != 'manual') style="display: none;" @endif>
    <h2 class="page-header">Manage Places Feed</h2>


    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default" role="tabpanel">

                {!! Form::open(['class' => 'source-form']) !!}
                <div class="panel-body">

                    @foreach($businesses as $business)
                        <div class="checkbox"
                             data-id="{{ $business->id }}"
                             data-name="{{ $business->name }}">
                            <label>
                                <input type="checkbox" name="item[{{ $business->id }}][add]" value="1" />
                                {{ $business->name }}
                            </label>
                        </div>
                    @endforeach
                </div>

                <div class="panel-footer">
                    {!! Form::submit('Add to Site', ['class' => 'btn btn-primary btn-sm']) !!}
                </div>
                {!! Form::close() !!}
            </div>



        </div>

        {!! Form::open() !!}
        {!! Form::hidden('business_feed', 'manual') !!}
        <div class="col-md-8">

            <div class="panel-group" id="target-list" role="tablist" aria-multiselectable="true">


                @foreach($site->businesses as $business)
                    <div class="panel panel-default" id="business-{{ $business->id }}">
                        <div class="panel-heading draggable" role="tab" id="business-{{ $business->id }}-heading">

                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#collapse-business-{{ $business->id }}" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-angle-down text-muted"></i> {{ $business->name }}
                                </a>
                            </h4>
                        </div>

                        <div id="collapse-business-{{ $business->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="business-{{ $business->id }}-heading">
                            <div class="panel-body">

                                {!! Form::hidden('business[' . $business->id . '][id]', $business->id, ['class' => 'business-id']) !!}
                                {!! Form::hidden('business[' . $business->id . '][order]', $business->pivot->order, ['class' => 'sort-order']) !!}

                            </div>

                            <div class="panel-footer clearfix">
                                <a href="#" class="btn btn-sm btn-default pull-right" role="remove-business" data-target="{{ $business->id }}">Remove</a>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
            </div>

        </div>

        {!! Form::close() !!}

    </div>
</div>

@endsection

@section('scripts')
    @parent
    <script src="/core/js/handlebars.js"></script>
    <script src="/core/js/jquery.sortable.min.js"></script>

    <script>
        $('input[name="business_feed"]').on('click', function() {

            if($(this).val() == 'manual')
            {
                $('#manual').show();
            } else
            {
                $('#manual').hide();
            }
        });


        $('.source-form').on('submit', function(e) {

            e.preventDefault();

            var theForm = this;

            $('.checkbox', theForm).each(function()
            {
                if($('[type="checkbox"]', this).is(':checked'))
                {
                    var source   = $("#business-template").html();
                    var template = Handlebars.compile(source);
                    var html    = template({
                        hash: Math.random().toString(36).slice(2),
                        name: $(this).data('name'),
                        id: $(this).data('id')
                    });

                    $('#target-list').append(html);

                    $(this).remove();
                }
            });

            initSorting();
            doSorting();
        });

        function initSorting()
        {
            $('#target-list').sortable({
                group: 'businesses',
                itemSelector: 'div.panel',
                containerSelector: 'div.panel-group',
                placeholder: '<div class="panel-placeholder"></div>',
                onDrop: function ($item, container, _super) {
                    $item.removeClass(container.group.options.draggedClass).removeAttr('style');
                    $('body').removeClass(container.group.options.bodyClass);

                    doSorting();

                    _super($item, container);
                }
            });
        }

        initSorting();

        function doSorting()
        {
            var i = 0;
            $('#target-list .panel').each(function() {
                $('.sort-order', this).val(i);

                i++;
            });
        }

        $(document).on('click', '[role="remove-business"]', function(e) {
            e.preventDefault();

            var target = $('#business-' + $(this).data('target'));

            var businessId = $('.business-id', target).val();


            if(businessId != undefined)
            {
                // Do something to remove it here
                $.ajax({
                    type: "POST",
                    url: '{{ route('site_remove_business_path', $site->uuid) }}',
                    data: {_method: 'DELETE', _token: $('meta[name="csrf_token"]').attr('content'), id: businessId},
                    success: function(data)
                    {
                        console.log(data);
                    }
                });
            }


            $(target).remove();
        });

    </script>

    @include('admin.layouts.partials.handlebars.business')
@endsection
