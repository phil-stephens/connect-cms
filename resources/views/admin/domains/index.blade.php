@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')


    @include('admin.sites.partials.tabs', ['tab' => 'domains'])

<div class="page-header">

    <h2 class="pull-left">Domains</h2>

    <a href="{{ route('create_domain_path', $site->id) }}" class="btn btn-primary pull-right">Add a new domain</a>
</div>


<table class="table table-striped">
    <thead>
        <tr>
            <th>Domain</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
    </thead>

    <tbody>
    @foreach($domains as $domain)
        <tr>
            <td>{{ $domain->domain }}</td>

            <td>
                @if($domain->id == $site->primary_domain_id)
                    <span class="label label-default">PRIMARY</span>
                    @else
                    {!! Form::open(['route' => ['set_primary_domain_path', $site->id ] ]) !!}
                    {!! Form::hidden('id', $domain->id) !!}
                    <button type="submit" class="btn btn-sm btn-default">Make Primary</button>
                    {!! Form::close() !!}
                @endif
            </td>

            <td>{!! link_to_route('edit_domain_path', 'Edit', [$site->id, $domain->id], ['class' => 'btn btn-primary btn-sm']) !!}</td>
            @if(count($domains) > 1)
            <td>
                {!! Form::open(['route' => ['destroy_domain_path', $site->id, $domain->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    {!! Form::hidden('id', $domain->id) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                {!! Form::close() !!}
            </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this domain?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection