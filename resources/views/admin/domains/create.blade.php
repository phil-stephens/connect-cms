@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')


    @include('admin.sites.partials.tabs', ['tab' => 'domains'])

        <h2 class="page-header">Add a Domain</h2>

        @include('admin.layouts.partials.errors')
        
        {!! Form::open() !!}

        {!! Form::hidden('site_id', $site->id) !!}

        <!-- Domain Form Input -->
        <div class="form-group">
            {!! Form::label('domain', 'Domain:') !!}
            {!! Form::text('domain', null, ['class' => 'form-control']) !!}
        </div>
        
        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Add Domain', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>
        
        {!! Form::close() !!}
@endsection