@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    @include('admin.sites.partials.tabs', ['tab' => 'domains'])


        <h2 class="page-header">Edit Domain</h2>

        @include('admin.layouts.partials.errors')
        
        {!! Form::model($domain) !!}

        {!! Form::hidden('id') !!}

        <!-- Domain Form Input -->
        <div class="form-group">
            {!! Form::label('domain', 'Domain:') !!}
            {!! Form::text('domain', null, ['class' => 'form-control']) !!}
        </div>
        
        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>
        
        {!! Form::close() !!}

        @if($site->domains()->count() > 1)
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Remove Domain</h3>
            </div>
            <div class="panel-body">
                <p>This cannot be undone.</p>

                {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_domain_path', $site->id, $domain->id], 'class' => 'destroy-form']) !!}

                {!! Form::hidden('id', $domain->id) !!}

                <!-- Submit field -->
                <div class="form-group">
                    {!! Form::submit('Remove Domain Now', ['class' => 'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @endif
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this domain?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection