<h1 class="page-header">Edit Compendium Index <small>{{ $index->name }}</small></h1>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'items') class="active"@endif>{!! link_to_route('compendium__index_items_path', 'Items', $index->uuid) !!}</li>
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('compendium__index_edit_path', 'Settings', $index->uuid) !!}</li>
</ul>