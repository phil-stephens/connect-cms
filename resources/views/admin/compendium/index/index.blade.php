@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.compendium.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Compendium Indices</h1>

        <a href="{{ route('compendium__index_create_path', $compendium->uuid) }}" class="btn btn-primary btn-lg pull-right">Create Index</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>

        <tbody>
        @foreach($indices as $index)
            <tr>
                <td>{{ $index->name }}</td>
                <td>{{ $index->description }}</td>
                <td>{!! link_to_route('compendium__index_items_path', 'Edit', $index->uuid, ['class' => 'btn btn-primary btn-sm']) !!}</td>
                <td>
                    {!! Form::open(['route' => ['compendium__index_destroy_path', $index->uuid], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection


@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this index?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection
