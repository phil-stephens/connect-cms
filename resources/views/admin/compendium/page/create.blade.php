@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.compendium.partials.side-nav')
@endsection

@section('content')

        <h1 class="page-header">Create Compendium Page</h1>

        @include('admin.layouts.partials.errors')

        {!! Form::open() !!}

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('content[title]', 'Page Title:') !!}
            {!! Form::text('content[title]', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Chapter Form Input -->
        <div class="form-group">
            {!! Form::label('chapter_id', 'Folder:') !!}
            {!! Form::select('chapter_id', $folders, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Template Form Input -->
        <div class="form-group">
            {!! Form::label('template', 'Template:') !!}
            {!! Form::select('template', $templates, 'page', ['class' => 'form-control']) !!}
        </div>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Create Page', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}
@endsection

{{--@include('admin.layouts.partials.sluggify')--}}