@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.compendium.partials.side-nav')
@endsection

@section('content')

    <h1 class="page-header">Edit Compendium Page</h1>

    @include('admin.layouts.partials.errors')

    {!! Form::model($model) !!}

            <!-- Name Form Input -->
    {{--<div class="form-group">--}}
        {{--{!! Form::label('name', 'Name:') !!}--}}
        {{--{!! Form::text('name', null, ['class' => 'form-control']) !!}--}}
    {{--</div>--}}

    {{--<!-- Slug Form Input -->--}}
    {{--<div class="form-group">--}}
    {{--{!! Form::label('slug', 'URL slug:') !!}--}}
    {{--{!! Form::text('slug', null, ['class' => 'form-control']) !!}--}}
    {{--</div>--}}

    <fieldset>
        <legend>Settings</legend>


        <!-- Chapter Form Input -->
        <div class="form-group">
            {!! Form::label('chapter_id', 'Folder:') !!}
            {!! Form::select('chapter_id', $folders, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Template Form Input -->
        <div class="form-group">
            {!! Form::label('template', 'Template:') !!}
            {!! Form::select('template', $templates, null, ['class' => 'form-control']) !!}
        </div>
    </fieldset>

    {{--<fieldset>--}}
    {{--<legend>Page Content</legend>--}}
    {{----}}
    {{--@include('admin.layouts.partials.form-content')--}}
    {{--</fieldset>--}}

    <fieldset>
        <legend>Content</legend>

        @include('admin.layouts.partials.form-content', ['include_custom' => false])
    </fieldset>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection

{{--@include('admin.layouts.partials.sluggify')--}}