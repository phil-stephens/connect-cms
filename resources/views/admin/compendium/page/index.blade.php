@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.compendium.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Compendium Pages</h1>

        <div class="pull-right">
            <a href="{{ route('compendium__page_create_path', $compendium->uuid) }}" class="btn btn-primary btn-lg">Create Page</a>
            <a href="{{ route('compendium__folder_create_path', $compendium->uuid) }}" class="btn btn-default btn-lg">Create Folder</a>
        </div>
    </div>

    @foreach($folders as $folder)

        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h2 class="panel-title pull-left">{{ $folder->name }} <a href="{!! route('compendium__folder_edit_path', $folder->uuid) !!}"><i class="fa fa-fw fa-cog"></i></a></h2>

                <a href="#" class="pull-right" onclick="$('#panel-{{ $folder->uuid }}').slideToggle(); return false;"><i class="fa fa-angle-down"></i></a>
            </div>

            <div class="panel-body" id="panel-{{ $folder->uuid }}">
                <table class="table table-striped" role="folder-table" data-folder-uuid="{{ $folder->uuid }}">

                </table>
            </div>
        </div>

    @endforeach

@endsection

@section('scripts')
    @parent

    <script src="{!! core_asset('js/spin.min.js') !!}"></script>
    <script>
        $(document).on('submit', '.destroy-form', function (e) {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this page?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });

        function initFolders()
        {
            $('[role="folder-table"]').each(function() {
                var uuid = $(this).data('folder-uuid');
                getPages('{{ route('compendium__folder_pages_path') }}/' + uuid, this);
            });
        }

        function getPages(path, table)
        {
            $(table).closest('.panel').spin();

            $.ajax({
                url: path,
                dataType: 'json',
            }).done(function (data) {
                $(table).html(data);
                $(table).closest('.panel').spin(false);
            }).fail(function () {
//                alert('Pages could not be loaded.');
            });
        }

        $(document).ready(function() {
            $(document).on('click', '.pagination a', function (e) {
                getPages($(this).attr('href'), $(this).closest('[role="folder-table"]'));
                e.preventDefault();
            });

            initFolders();
        });
    </script>
@endsection