@extends('admin.layouts.default')

@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
@include('admin.compendium.partials.side-nav')
@endsection

@section('content')

@include('admin.compendium.partials.tabs', ['tab' => 'general'])

@include('admin.layouts.partials.errors')

{!! Form::model($compendium) !!}

<!-- Theme Form Input -->
<div class="form-group">
    {!! Form::label('theme', 'Theme:') !!}
    {!! Form::text('theme', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('homepage_page_id', 'Homepage:') !!}
    {!! Form::hidden('homepage_type', 'Fastrack\Pages\Page') !!}
    {!! Form::select('homepage_page_id', $pages, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('primary_domain[domain]', 'Domain:') !!}
    {!! Form::text('primary_domain[domain]', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit field -->
<div class="form-group">
    {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
</div>



{!! Form::close() !!}
@endsection
