<thead>
<tr>
    <th>Page Name</th>
    <th>Page Title</th>
    <th class="button-column"></th>
    <th class="button-column"></th>
</tr>
</thead>
<tbody>
@foreach($pages as $page)
    @if( ! $page->internal)
        <tr data-id="{{ $page->id }}">
            <td>{!! link_to_route('compendium__page_edit_path', $page->name, $page->uuid) !!}</td>
            <td>{{ $page->getTitle() }}</td>
            <td>{!! link_to_route('compendium__page_edit_path', 'Edit', $page->uuid, ['class' => 'btn btn-primary btn-sm']) !!}</td>
            <td>
                {!! Form::open(['route' => ['compendium__page_destroy_path', $page->uuid ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                <button type="submit" class="btn btn-default btn-sm">Remove</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endif
@endforeach
</tbody>

<tfoot>
    <tr>
        <td class="text-center" colspan="5">{!! $pages->render() !!}</td>
    </tr>
</tfoot>