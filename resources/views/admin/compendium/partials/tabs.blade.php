<div class="page-header">
    <h1 class="pull-left">Compendium Settings</h1>
</div>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'general') class="active"@endif><a href="{{ route('compendium_path', $compendium->uuid) }}">General</a></li>
    <li role="presentation"@if($tab == 'theme') class="active"@endif><a href="{{ route('compendium_theme_path', $compendium->uuid) }}">Theme Settings</a></li>

    @if(Auth::user()->developer)
        <li role="presentation"@if($tab == 'export') class="active"@endif>{!! link_to_route('compendium_export_path', 'Export', $compendium->uuid) !!}</li>
    @endif
</ul>