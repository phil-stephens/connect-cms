@section('extra-nav-links')
    <ul class="nav navbar-nav">
        <li><a href="{!! route('compendium_path', $compendium->uuid) !!}"><i class="fa fa-fw fa-book"></i> {{ $compendium->property->name }}</a></li>
    </ul>
@endsection
<div class="col-sm-2 col-md-offset-2 col-sm-offset-3 sidebar secondary">
    <ul class="nav nav-sidebar">
        <li role="presentation"><a style="background: rgba(0,0,0,0.1);" href="{{ route('property_path', $compendium->property->id) }}"><i class="fa fa-fw fa-building"></i> Back to Property <i class="fa fa-angle-right"></i></a></li>
        <li role="presentation"><a href="{{ route('compendium_path', $compendium->uuid) }}"><i class="fa fa-fw fa-cog"></i> Settings</a></li>
        <li role="presentation"><a href="{{ route('compendium_pages_path', $compendium->uuid) }}"><i class="fa fa-fw fa-files-o"></i> Pages</a></li>
        <li role="presentation"><a href="{{ route('compendium_indices_path', $compendium->uuid) }}"><i class="fa fa-fw fa-sitemap"></i> Indices</a></li>
        <li role="presentation"><a href="{{ route('compendium_places_path', $compendium->uuid) }}"><i class="fa fa-fw fa-map-marker"></i> Places Feed</a></li>
    </ul>
</div>