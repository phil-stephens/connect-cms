@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.settings.partials.side-nav')
@endsection

@section('content')

        <h1 class="page-header">Create Feature</h1>

        @include('admin.layouts.partials.errors')

        {!! Form::open() !!}

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Class Form Input -->
        <div class="form-group">
            {!! Form::label('class', 'Custom Field:') !!}
            {!! Form::text('class', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('icon', 'Icon:') !!}

            @include('admin.layouts.partials.image-upload', ['field' => 'icon', 'showRemoveButton' => true])
        </div>



        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Create Feature', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}

        @include('admin.layouts.partials.media-modal')
@endsection