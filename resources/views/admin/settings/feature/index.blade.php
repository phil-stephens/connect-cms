@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.settings.partials.side-nav')
@endsection

@section('content')
    <div class="page-header">

        <h1 class="pull-left">System-wide Features List</h1>

        <a href="{{ route('account_amenity_create_path') }}" class="btn btn-primary btn-lg pull-right {{ ( ! configuration('system-wide-features', false)) ? 'disabled' : '' }}" role="new-feature-btn">Create New Feature</a>
    </div>

    <div class="alert alert-info">
        <div class="checkbox">
            <label>
                {!! Form::checkbox('configuration[system-wide-features]', 1, configuration('system-wide-features')) !!} Use system-wide feature list
            </label>
            <span class="help-block">By enabling the system-wide feature list you can share a common list of features and amenities across all properties</span>


        </div>
    </div>

    <div class="maskable" role="feature-table">

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Description</th>
                <th>Icon</th>
                <th class="button-column"></th>
                <th class="button-column"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($amenities as $amenity)
                <tr>
                    <td>{{ $amenity->name }}</td>
                    <td>
                        {!! $amenity->getImageTag(['w' => 50, 'h' => 50, 'fit' => 'crop'], ['class' => 'img-responsive']) !!}
                    </td>

                    <td>{!! link_to_route('account_amenity_edit_path', 'Edit', $amenity->uuid, ['class' => 'btn btn-primary btn-sm']) !!}</td>
                    <td>
                        {!! Form::open(['route' => ['account_amenity_destroy_path', $amenity->uuid ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                        <button type="submit" class="btn btn-default btn-sm">Remove</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>

        @if(( ! configuration('system-wide-features', false)))
        <div class="mask"></div>
        @endif
    </div>

@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this feature?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });

        $('[name="configuration[system-wide-features]"]').on('click', function(e) {
            if($(this).is(':checked'))
            {
                $('[role="new-feature-btn"]').removeClass('disabled');
                $('[role="feature-table"] .mask').remove();

                updateConfiguration(1);
            } else
            {
                $('[role="new-feature-btn"]').addClass('disabled');
                $('[role="feature-table"]').append('<div class="mask"></div>');

                updateConfiguration(0);
            }
        });

        function updateConfiguration(val)
        {
            $.ajax({
                type: "POST",
                url: '{{ route('configuration_update_path') }}',
                data: {_token: $('meta[name="csrf_token"]').attr('content'), key: 'system-wide-features', value: val},
                success: function(data)
                {
                    console.log(data);
                }
            });
        }
    </script>
@endsection