@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.settings.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Users</h1>

        <a href="{{ route('create_user_path') }}" class="btn btn-primary btn-lg pull-right">Create User</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th class="button-column"></th>
            <th>Name</th>
            <th>Last Logged In</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
            @if( ! $user->hidden)
                <tr>
                    <td>{!! $user->getImageTag(['w' => 50], ['class' => 'img-rounded']) !!}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->present()->lastLogin() }}</td>
                    <td>{!! link_to_route('edit_user_path', 'Edit', $user->id, ['class' => 'btn btn-primary btn-sm']) !!}</td>
                    <td>
                        {!! Form::open(['route' => ['destroy_user_path', $user->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                        {!! Form::hidden('id', $user->id) !!}
                        <button type="submit" class="btn btn-default btn-sm">Remove</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this user?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection