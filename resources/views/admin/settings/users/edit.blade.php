@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.settings.partials.side-nav')
@endsection

@section('content')

    <h1 class="page-header">Edit User <small>{{ $user->name }}</small></h1>

    @include('admin.layouts.partials.errors')

    {!! Form::model($user) !!}

    {!! Form::hidden('id') !!}

    <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Email Form Input -->
    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('avatar', 'Avatar:') !!}
        @include('admin.layouts.partials.image-upload', ['field' => 'avatar', 'model' => $user])
    </div>

    <!-- Biography Form Input -->
    <div class="form-group">
        {!! Form::label('biography', 'Biography:') !!}
        {!! Form::textarea('biography', null, ['class' => 'form-control', 'rows' => 3, 'role' => 'editor']) !!}
    </div>

    @if(Auth::user()->developer && Auth::id() != $user->id)
        <div class="checkbox">
            <label>
                {!! Form::checkbox('developer', 1, null) !!} Developer Account
            </label>
        </div>
    @else
        {!! Form::hidden('developer') !!}
    @endif

    <fieldset>
        <legend>Reset Password</legend>

        <!-- Password Form Input -->
        <div class="form-group">
            {!! Form::label('Password', 'Password:') !!}
            {!! Form::password('Password', ['class' => 'form-control']) !!}
        </div>

        <!--  Form Input -->
        <div class="form-group">
            {!! Form::label('password_confirmation', 'Confirm Password:') !!}
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
        </div>
    </fieldset>


    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}

    @if($user->id != \Auth::id())
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Remove User</h3>
        </div>
        <div class="panel-body">
            <p>This cannot be undone.</p>

            {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_user_path', $user->id], 'class' => 'destroy-form']) !!}

            {!! Form::hidden('id', $user->id) !!}

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Remove User Now', ['class' => 'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @endif

    @include('admin.layouts.partials.media-modal')

@endsection

@section('scripts')
    @parent

    @if(env('EDITOR_FORMAT', 'md') === 'html')
        {{--<script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>--}}
        <script src="/core/js/tinymce/tinymce.min.js"></script>
        <script>
            function tinymceInit()
            {
                tinymce.init({
                    selector: 'textarea[role="editor"]',
                    menubar : false,
                    content_css : '/core/css/editor.css',
                    statusbar : false,
                    plugins: "link code paste",
                    toolbar: "bold italic styleselect | bullist numlist | link code",
                    valid_elements : '+*[*]',
                    style_formats: [
                        {title: "Header 1", format: "h1"},
                        {title: "Header 2", format: "h2"},
                        {title: "Header 3", format: "h3"},
                        {title: "Header 4", format: "h4"},
                        {title: "Header 5", format: "h5"},
                        {title: "Header 6", format: "h6"},
                        {title: "Blockquote", format: "blockquote"}
                    ]
                });
            }

            tinymceInit();
        </script>
    @endif

@if($user->id != \Auth::id())
    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this user?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endif
@endsection
