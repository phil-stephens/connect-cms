@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.settings.partials.side-nav')
@endsection

@section('content')

    <h1 class="page-header">Create User</h1>

    @include('admin.layouts.partials.errors')

    {!! Form::open() !!}

    <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Email Form Input -->
    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>

    @if(Auth::user()->developer)
    <div class="checkbox">
        <label>
            {!! Form::checkbox('developer', 1, null) !!} Developer Account
        </label>
    </div>
    @endif

    <!-- Password Form Input -->
    <div class="form-group">
        {!! Form::label('password', 'Password:') !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>

    <!--  Form Input -->
    <div class="form-group">
        {!! Form::label('password_confirmation', 'Confirm Password:') !!}
        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create User', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection
