@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.settings.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Edit Integration</h1>
    </div>

    @include('admin.layouts.partials.errors')

    {!! Form::model($integration) !!}

            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Type Form Input -->
    <div class="form-group">
        {!! Form::label('type', 'Provider:') !!}
        {!! Form::select('type', $providers, null, ['class' => 'form-control', 'disabled']) !!}
    </div>

    <!-- Provider-specific info -->
    @include('admin.settings.integration.partials.' . class_basename($integration->type))

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>
    {!! Form::close() !!}
@endsection