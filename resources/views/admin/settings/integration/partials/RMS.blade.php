<!-- Username Form Input -->
<div class="form-group">
    {!! Form::label('RMS[username]', 'Username:') !!}
    {!! Form::text('RMS[username]', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Form Input -->
<div class="form-group">
    {!! Form::label('RMS[password]', 'Password:') !!}
    {!! Form::text('RMS[password]', null, ['class' => 'form-control']) !!}
</div>

<!-- Agent ID Form Input -->
<div class="form-group">
    {!! Form::label('RMS[custom1]', 'Agent ID:') !!}
    {!! Form::text('RMS[custom1]', null, ['class' => 'form-control']) !!}
</div>