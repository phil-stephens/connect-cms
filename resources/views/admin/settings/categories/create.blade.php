@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.settings.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Create Place Category</h1>

        <a href="{{ route('categories_path') }}" class="btn btn-default btn-lg pull-right">Back to Categories</a>
    </div>

    @include('admin.layouts.partials.errors')

    {!! Form::open() !!}

            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    <div class="form-group">
        {!! Form::label('slug', 'URL slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>

    <fieldset>
        <legend>Additional Content</legend>

        <div class="form-group">
            {!! Form::label('content[excerpt]', 'Excerpt:') !!}
            {!! Form::textarea('content[excerpt]', null, ['class' => 'form-control', 'rows' => 2]) !!}
        </div>

        <!-- Body Form Input -->
        <div class="form-group">
            {!! Form::label('content[body]', 'Body:') !!}
            {!! Form::textarea('content[body]', null, ['class' => 'form-control', 'rows' => 10, 'role' => 'editor' ]) !!}
        </div>

        @include('admin.layouts.partials.image-upload', ['field' => 'content[featured_image]', 'showRemoveButton' => true])

    </fieldset>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create Place Category', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection

@include('admin.layouts.partials.sluggify')

@section('modals')
    @parent
    @include('admin.layouts.partials.media-modal')
@endsection

@if(env('EDITOR_FORMAT', 'md') === 'html')
@section('scripts')
    @parent

    <script src="{!! core_asset('js/tinymce/tinymce.min.js') !!}"></script>
    <script>
        function tinymceInit()
        {
            tinymce.init({
                selector: 'textarea[role="editor"]',
                menubar : false,
                content_css : '/core/css/editor.css',
                statusbar : false,
                plugins: "link code paste",
                toolbar: "bold italic styleselect | bullist numlist | link code | cms_image",
                valid_elements : '+*[*]',
                convert_urls: false,
                style_formats: [
                    {title: "Header 1", format: "h1"},
                    {title: "Header 2", format: "h2"},
                    {title: "Header 3", format: "h3"},
                    {title: "Header 4", format: "h4"},
                    {title: "Header 5", format: "h5"},
                    {title: "Header 6", format: "h6"},
                    {title: "Blockquote", format: "blockquote"}
                ],
                setup : function(ed) {
                    // Add a custom button
                    ed.addButton('cms_image', {
                        title : 'Insert Image',
                        image: '{{ core_asset('img/tinymce-photo-icon.png') }}',
                        onclick : function() {
                            $('#media-browser').modal('show');
                            targetEditor = ed;
                        }
                    });
                }
            });
        }

        tinymceInit();
    </script>
@endsection
@endif