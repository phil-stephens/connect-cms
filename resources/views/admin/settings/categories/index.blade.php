@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.settings.partials.side-nav')
@endsection

@section('content')
    <div class="page-header">
        <h1 class="pull-left">Place Categories</h1>

        <p class="pull-right">
            <a href="{{ route('venues_path') }}" class="btn btn-default btn-lg">Manage Places</a>
            <a href="{{ route('create_category_path') }}" class="btn btn-primary btn-lg">Create Category</a>
        </p>
    </div>


    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{ $category->name }}</td>
                <td>{!! link_to_route('edit_category_path', 'Edit', $category->id, ['class' => 'btn btn-primary btn-sm']) !!}</td>
                <td>
                    {!! Form::open(['route' => ['destroy_category_path', $category->id], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    {!! Form::hidden('id', $category->id) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this category?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection