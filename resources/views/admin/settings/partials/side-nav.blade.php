@section('extra-nav-links')
    <ul class="nav navbar-nav">
        <li><a href="{{ route('settings_path') }}"><i class="fa fa-fw fa-cog"></i> Settings</a></li>
    </ul>
@endsection

<div class="col-sm-2 col-md-offset-2 col-sm-offset-3 sidebar secondary">
    <div class="collapse">
        <ul class="nav nav-sidebar">
            <li role="presentation"><a href="{{ route('users_path') }}"><i class="fa fa-fw fa-user"></i> Users</a></li>
            <li role="presentation"><a href="{{ route('integrations_path') }}"><i class="fa fa-fw fa-share-alt"></i> Integrations</a></li>
            <li role="presentation"><a href="{{ route('account_amenities_path') }}"><i class="fa fa-fw fa-check-square-o"></i> Features</a></li>
            <li role="presentation"><a href="{{ route('categories_path') }}"><i class="fa fa-fw fa-map-marker"></i> Place Categories</a></li>
        </ul>
    </div>
</div>