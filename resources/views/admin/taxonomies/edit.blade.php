@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')
    @include('admin.sites.partials.tabs', ['tab' => 'taxonomies'])


    <h2 class="page-header">Taxonomies</h2>


    {!! Form::open() !!}

    {!! Form::hidden('id', $site->id) !!}

    @foreach(['Page', 'Property', 'Room', 'Gallery', 'Venue', 'CalendarEvent', 'Article', 'Offer'] as $model)
        <fieldset>
            <legend>{{ $model }}</legend>

            @if( ! empty($taxonomies[$model]->id))
                {!! Form::hidden('taxonomy[' . $model . '][id]', $taxonomies[$model]->id) !!}
            @endif

            <!-- Title Form Input -->
            <div class="form-group">
                {!! Form::label('taxonomy[' . $model . '][title]', 'Meta Title:') !!}
                {!! Form::text('taxonomy[' . $model . '][title]', $taxonomies[$model]->title, ['class' => 'form-control']) !!}
            </div>

            <!-- Description Form Input -->
            <div class="form-group">
                {!! Form::label('taxonomy[' . $model . '][description]', 'Meta Description:') !!}
                {!! Form::textarea('taxonomy[' . $model . '][description]', $taxonomies[$model]->description, ['class' => 'form-control','rows' => 3]) !!}
            </div>
        </fieldset>

    @endforeach

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}

@endsection