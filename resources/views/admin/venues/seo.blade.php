@extends('admin.layouts.default')

@section('content')

        @include('admin.venues.partials.tabs', ['tab' => 'seo'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}

        {!! Form::hidden('id') !!}

        <div class="form-group">
                {!! Form::label('canonical_site_id', 'Canonical Site:') !!}
                {!! Form::select('canonical_site_id', $sites, null, ['class' => 'form-control']) !!}
        </div>

        @include('admin.layouts.partials.form-seo')

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}

@endsection