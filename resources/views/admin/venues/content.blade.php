@extends('admin.layouts.default')

@section('content')

        @include('admin.venues.partials.tabs', ['tab' => 'content'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}

        {!! Form::hidden('id') !!}

        @include('admin.layouts.partials.form-content')

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}
@endsection