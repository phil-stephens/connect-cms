@extends('admin.layouts.default')

@section('content')

    @include('admin.venues.partials.tabs', ['tab' => 'slideshow'])

    @include('admin.layouts.partials.form-slideshow')

@endsection