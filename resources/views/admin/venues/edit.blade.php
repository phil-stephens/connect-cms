@extends('admin.layouts.default')

@section('content')

        @include('admin.venues.partials.tabs', ['tab' => 'edit'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}

        {!! Form::hidden('id') !!}
        
        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Slug Form Input -->
        <div class="form-group">
            {!! Form::label('slug', 'URL slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Categories:</label>
            @foreach($categories as $category)
                <div class="checkbox">
                    <label>
                        {!! Form::checkbox('categories[]', $category->id, in_array($category->id, $venueCategories)) !!}
                        {{ $category->name }}
                    </label>
                </div>
            @endforeach
        </div>

        <fieldset>
            <legend>Contact Details</legend>
            {{--<!-- Address Form Input -->--}}
            {{--<div class="form-group">--}}
                {{--{!! Form::label('address', 'Address:') !!}--}}
                {{--{!! Form::text('address', null, ['class' => 'form-control']) !!}--}}
            {{--</div>--}}

            <!-- Website Form Input -->
            <div class="form-group">
                {!! Form::label('website', 'Website:') !!}
                {!! Form::text('website', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Phone Form Input -->
            <div class="form-group">
                {!! Form::label('phone', 'Phone:') !!}
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Email Form Input -->
            <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div>
        </fieldset>

        <div class="alert alert-success">
            <div class="checkbox">
                <label>
                    {!! Form::checkbox('active', 1, null) !!} Active
                </label>
                <span class="help-block">Only Places marked as 'active' will appear on public-facing websites</span>
            </div>
        </div>




<!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}


        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Remove Place</h3>
            </div>
            <div class="panel-body">
                <p>This cannot be undone.</p>

                {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_venue_path', $model->id], 'class' => 'destroy-form']) !!}

                {!! Form::hidden('id', $model->id) !!}

                <!-- Submit field -->
                <div class="form-group">
                    {!! Form::submit('Remove Place Now', ['class' => 'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this place?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection