@extends('admin.layouts.default')

@section('content')
    <div class="page-header">
        <h1 class="pull-left">Places</h1>

        <p class="pull-right">
            <a href="{{ route('categories_path') }}" class="btn btn-default btn-lg">Manage Categories</a>
            <a href="{{ route('create_venue_path') }}" class="btn btn-primary btn-lg">Create Place</a>
        </p>
    </div>


    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Category</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($venues as $venue)
            <tr>
                <td>{{ $venue->name }}</td>
                <td>{{ $venue->present()->addressString() }}</td>
                <td>{{ $venue->present()->categoryList() }}</td>
                <td>{!! link_to_route('edit_venue_path', 'Edit', $venue->id, ['class' => 'btn btn-primary btn-sm']) !!}</td>
                {{--<td><a href="/location/venue/{{ ltrim($venue->slug, '/') }}" target="_blank">Preview</a></td>--}}
                <td>
                    {!! Form::open(['route' => ['destroy_venue_path', $venue->id], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    {!! Form::hidden('id', $venue->id) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this venue?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection