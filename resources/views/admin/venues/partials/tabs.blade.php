<h1 class="page-header">Edit Place <small>{{ $model->name }}</small></h1>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('edit_venue_path', 'Settings', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'location') class="active"@endif>{!! link_to_route('location_venue_path', 'Location', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'content') class="active"@endif>{!! link_to_route('content_venue_path', 'Content', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'slideshow') class="active"@endif>{!! link_to_route('slideshow_venue_path', 'Place Images', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'seo') class="active"@endif>{!! link_to_route('seo_venue_path', 'SEO', $model->id) !!}</li>
</ul>