@extends('admin.layouts.default')

@section('content')

        <h1 class="page-header">Create Place</h1>

        @include('admin.layouts.partials.errors')

        {!! Form::open() !!}

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Slug Form Input -->
        <div class="form-group">
            {!! Form::label('slug', 'URL slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Categories:</label>
            @foreach($categories as $category)
                <div class="checkbox">
                    <label>
                        {!! Form::checkbox('categories[]', $category->id) !!}
                        {{ $category->name }}
                    </label>
                </div>
            @endforeach
        </div>

        <fieldset>
            <legend>Contact Details</legend>

            <!-- Address Form Input -->
            {{--<div class="form-group">--}}
                {{--{!! Form::label('address', 'Address:') !!}--}}
                {{--{!! Form::text('address', null, ['class' => 'form-control']) !!}--}}
            {{--</div>--}}

            <!-- Website Form Input -->
            <div class="form-group">
                {!! Form::label('website', 'Website:') !!}
                {!! Form::text('website', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Phone Form Input -->
            <div class="form-group">
                {!! Form::label('phone', 'Phone:') !!}
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Email Form Input -->
            <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div>

        </fieldset>

        <fieldset>
            <legend>Location</legend>

            @include('admin.layouts.partials.form-location')
        </fieldset>



        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Create Place', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}
@endsection

@include('admin.layouts.partials.sluggify')