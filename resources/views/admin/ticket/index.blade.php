@extends('admin.layouts.default')

@section('content')
    <div class="page-header">
        <h1 class="pull-left">Box Office <small>{{ $event->name }}</small></h1>

        <p class="pull-right">
            <a href="{{ route('boxoffice_create_path', $event->uuid) }}" class="btn btn-primary btn-lg">Manual Ticket Purchase</a>
        </p>
    </div>


    <?php foreach($event->ticket_types as $type) {

    $sales = 0;
    ?>

    <h3>{{ $type->name }}</h3>

    <table class="table table-striped">
        <thead>
        <tr>
            <th></th>
            <th>Attendee Name</th>
            <th>Attendee Email</th>
            <th>Attendee Company</th>
            <th class="btn-column"></th>
            <th class="btn-column"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 1;
        foreach($type->purchases as $purchase) {

        $sales += $purchase->price_tickets - $purchase->price_discount;

        foreach($purchase->tickets as $key => $ticket) {
        ?>
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $ticket->name }}</td>
            <td>{{ $ticket->email }}</td>
            <td>{{ $purchase->company }}</td>
            <td>
                @if($purchase->paid)
                <span class="label label-success">PAID</span>
                @else
                <span class="label label-danger">UNPAID</span>
                @endif
            </td>
            <td>
                @if($key == 0)
                <a href="#" class="btn btn-default btn-sm" data-uuid="{{ $purchase->uuid }}" data-email="{{ $purchase->email }}" data-toggle="modal" data-target="#email-modal"><i class="fa fa-envelope-o"></i></a>
                @endif
            </td>
        </tr>



        <?php
        $i++;
        }
        }?>
        </tbody>

        <tfoot>
        <tr class="text-right">
            <td colspan="5">Total Sales (exc GST and CC fees): ${{ $sales }}</td>
        </tr>
        </tfoot>
    </table>
    <?php } ?>


    <div class="modal fade" id="email-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Send Ticket Information</h4>
                </div>

                {!! Form::open(['route' => ['boxoffice_email_path', $event->uuid]]) !!}
                <div class="modal-body">
                    {!! Form::hidden('purchase_uuid', null, ['id' => 'purchase_uuid']) !!}

                    <!-- Email Form Input -->
                    <div class="form-group">
                        {!! Form::label('email_to', 'Send To Email:') !!}
                        {!! Form::email('email_to', null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('site_id', 'Sending Site:') !!}
                        {!! Form::select('site_id', $sites, null, ['class' => 'form-control', 'required']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Send Email</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    @parent

    <script>
        $('#email-modal').on('show.bs.modal', function (e) {
            var btn = $(e.relatedTarget) // Button that triggered the modal

            $('#purchase_uuid').val( btn.data('uuid') );
            $('#email_to').val( btn.data('email') );
        });
    </script>
@endsection

