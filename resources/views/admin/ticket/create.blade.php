@extends('admin.layouts.default')

@section('content')
    <div class="page-header">
        <h1 class="pull-left">Box Office <small>{{ $event->name }}</small></h1>

        <p class="pull-right">
            <a href="{{ route('boxoffice_path', $event->uuid) }}" class="btn btn-default btn-lg">Back To Attendee List</a>
        </p>
    </div>

    <h2 class="page-header">Manually Create Ticket Purchase</h2>

    {!! Form::open() !!}

    <fieldset>
        <legend>Ticket Type</legend>

        @foreach($event->ticket_types as $i => $ticket)
            <div class="form-group">
                    <div class="radio">
                        <label>
                            <input type="radio" name="ticket_type_id" value="{{ $ticket->id }}" data-price="{{ $ticket->price }}" data-tax="{{ $ticket->tax_rate }}" _onclick="updateAttendees();" checked="checked">
                           {{ $ticket->name }} - ${{ $ticket->price }} + <abbr title="Goods and Sales Tax" class="initialism">GST</abbr> per attendee
                        </label>
                    </div>
            </div>
        @endforeach
    </fieldset>

    <fieldset>
        <legend>Purchaser Details</legend>

        <!-- First_name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Job_title Form Input -->
        <div class="form-group">
            {!! Form::label('position', 'Job Title', ['class' => 'control-label']) !!}
            {!! Form::text('position', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Company Form Input -->
        <div class="form-group">
            {!! Form::label('company', 'Company Name', ['class' => 'control-label']) !!}
            {!! Form::text('company', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Telephone Form Input -->
        <div class="form-group">
            {!! Form::label('phone', 'Telephone No.', ['class' => 'control-label']) !!}
            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Email Form Input -->
        <div class="form-group">
            {!! Form::label('email', 'Email Address', ['class' => 'control-label']) !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Address1 Form Input -->
        <div class="form-group">
            {!! Form::label('address1', 'Address', ['class' => 'control-label']) !!}
            {!! Form::text('address1', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Address2 Form Input -->
        <div class="form-group">
            {!! Form::label('address2', 'Address2', ['class' => 'control-label sr-only']) !!}
            {!! Form::text('address2', null, ['class' => 'form-control']) !!}
        </div>

        <!-- City Form Input -->
        <div class="form-group">
            {!! Form::label('city', 'Town/City', ['class' => 'control-label']) !!}
            {!! Form::text('city', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Postcode Form Input -->
        <div class="form-group">
            {!! Form::label('postcode', 'Postcode', ['class' => 'control-label']) !!}
            {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Country Form Input -->
        <div class="form-group">
            {!! Form::label('country', 'Country', ['class' => 'control-label']) !!}
            {!! Form::select('country', config('countries.iso_countries'), null, ['class' => 'form-control']) !!}
        </div>
    </fieldset>

    <fieldset>
        <legend>Attendee Information
        </legend>

        <!-- Attendees Form Input -->
        <div class="form-group">
            {!! Form::label('attendees', 'Number of Attendees', ['class' => 'control-label']) !!}
            {!! Form::select('attendees', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15], null, ['id' => 'attendeeCount', 'class' => 'form-control', 'onchange' => 'updateAttendees();']) !!}
        </div>

        <div id="tickets"></div>

    </fieldset>

    <fieldset>
        <legend>Notes</legend>

        <!-- Notes Form Input -->
        <div class="form-group">
            {!! Form::label('notes', 'Notes:', ['class' => 'sr-only']) !!}
            {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
        </div>
    </fieldset>


    <fieldset>
        <legend>Price</legend>

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('discount_code', 'Discount Code', ['class' => 'control-label']) !!}
                        {!! Form::text('discount_code', null, ['class' => 'form-control']) !!}
                </div>

                <button class="btn btn-primary" id="check-discount-code">Apply</button>

            </div>
        </div>


        <table class="table">
            <tbody>
            <tr>
                <td>Ticket Price</td>
                <td style="width: 40%;">
                    <div class="input-group">
                        <div class="input-group-addon">$</div>
                        {!! Form::text('price_tickets', null, ['class' => 'form-control', 'onchange' => 'priceManuallyUpdated(this);']) !!}
                    </div>
                </td>
            </tr>

            <tr id="discount-row">
                <td>Discount <span id="discount-percent"></span>%</td>
                <td>-$<span id="price-discount"></span></td>
            </tr>

            <tr>
                <td><abbr title="Goods and Sales Tax" class="initialism">GST</abbr> (<span id="tax_rate"></span>%)</td>
                <td>$<span id="price-gst"></span></td>
            </tr>

            {{--<tr>--}}
                {{--<td>Credit Card Processing Fee (3%)</td>--}}
                {{--<td>$<span id="price-cc"></span></td>--}}
            {{--</tr>--}}

            <tr>
                <td><strong>Total Amount Due</strong></td>
                <td><strong>$<span id="total-price"></span></strong></td>
            </tr>
            </tbody>
        </table>

        {{--{!! Form::hidden('price_tickets') !!}--}}
        {!! Form::hidden('price_discount') !!}
        {!! Form::hidden('price_tax') !!}
{{--        {!! Form::hidden('price_cc') !!}--}}
        {!! Form::hidden('price') !!}

        {{--<div class="form-group">--}}
            {{--<div class="col-sm-9 col-sm-offset-3">--}}
                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--<input type="checkbox" name="agree" value="1">--}}
                        {{--By clicking 'SUBMIT AND PAY' below I acknowledge that I am purchasing tickets for <strong>{{ $event->name }}</strong>--}}
                        {{--and I accept the event <a href="/terms-and-conditions" target="_blank">Terms &amp; Conditions</a>--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="alert alert-success">
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="paid"" value="1">
                        Purchase has been paid
                    </label>
                </div>
            </div>

        </div>

    </fieldset>

    <!-- Site_id Form Input -->
    <div class="form-group">
        {!! Form::label('site_id', 'Sending Site:') !!}
        {!! Form::select('site_id', $sites, null, ['class' => 'form-control', 'required']) !!}
    </div>
    
    
    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create Ticket Purchase', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection

@section('scripts')
    @parent
    <script src="{!! core_asset('js/handlebars.js') !!}"></script>

    <script>
        var attendees = 1;
        var tax;
        var pricePer;
        var priceTickets;
        var priceDiscount;
        var priceGst;
//        var priceCC;
        var priceGrandTotal;
        var eWayAccessCode;
        var eWayConfig;
        var template;
        var source;
        var discount = 0;
        var manualPricePer;

        $('#check-discount-code').on('click', function(e)
        {
            e.preventDefault();

            var theButton = $(this);
            theButton.prop('disabled', true).html('Check &nbsp;<i class="fa fa-fw fa-spinner fa-spin"></i>');

            // Do the check
            $.ajax({
                type: "POST",
                url: '{{ route('check_discount_code_path') }}',
                data: {code: $('[name="discount_code"]').val(), _token: '{{ csrf_token() }}'},
                success: function (data) {
                    console.log(data);

                    var obj = JSON.parse(data);
                    discount = obj.discount;

                    theButton.prop('disabled', false).html('Check &nbsp;<i class="fa fa-fw fa-check"></i>');

                    updatePrice();
                },
                error: function(data) {
                    console.log(data);

                    discount = 0;

                    theButton.prop('disabled', false).html('Check &nbsp;<i class="fa fa-fw fa-times"></i>');

                    updatePrice();
                }
            });
        });

        function eWayResultCallback(result, transactionID, errors) {
            if (result == 'Complete') {
                window.location.href = '{{ $event->ticketUrl('process') }}?AccessCode=' + eWayAccessCode;
            } else if (result == 'Error') {
                alert("There was a problem completing the payment: " + errors);
                $('#submitBtn').prop('disabled', false).html('Submit and Pay');
            }
        }

        function priceManuallyUpdated(elem)
        {
            manualPricePer = $(elem).val() / attendees;

            tax = Number($('[name="ticket_type_id"]:checked').data('tax'));

            $('#tax_rate').html(tax);
            attendees = Number($('#attendeeCount').val()) + 1;

            priceTickets = (manualPricePer * attendees); // * ( 1 + ({{ setting('gst_percent', 10) }} / 100));
            //$('#price-tickets').html(priceTickets.toFixed(2));
            //$('[name="price_tickets"]').val(priceTickets.toFixed(2));

            if(discount > 0)
            {
                $('#discount-percent').html(discount);

                priceDiscount = priceTickets * (discount / 100);
                $('#price-discount').html(priceDiscount.toFixed(2));
                $('[name="price_disocunt"]').val(priceDiscount.toFixed(2));

                $('#discount-row').show();
            } else
            {
                priceDiscount = 0;
                $('#discount-row').hide();
            }

            priceGst = (priceTickets - priceDiscount) * (tax / 100);
            $('#price-gst').html(priceGst.toFixed(2));
            $('[name="price_tax"]').val(priceGst.toFixed(2));


//            priceCC = ((priceTickets - priceDiscount) + priceGst) * 0.03;
//            $('#price-cc').html(priceCC.toFixed(2));
//            $('[name="price_cc"]').val(priceCC.toFixed(2));


            priceGrandTotal = (priceTickets - priceDiscount) + priceGst; // + priceCC;
            $('#total-price').html(priceGrandTotal.toFixed(2));
            $('[name="price"]').val(priceGrandTotal.toFixed(2));
        }


        function updatePrice()
        {
            pricePer = Number($('[name="ticket_type_id"]:checked').data('price'));
            tax = Number($('[name="ticket_type_id"]:checked').data('tax'));

            $('#tax_rate').html(tax);
            attendees = Number($('#attendeeCount').val()) + 1;

            priceTickets = (pricePer * attendees); // * ( 1 + ({{ setting('gst_percent', 10) }} / 100));
            $('#price-tickets').html(priceTickets.toFixed(2));
            $('[name="price_tickets"]').val(priceTickets.toFixed(2));

            if(discount > 0)
            {
                $('#discount-percent').html(discount);

                priceDiscount = priceTickets * (discount / 100);
                $('#price-discount').html(priceDiscount.toFixed(2));
                $('[name="price_disocunt"]').val(priceDiscount.toFixed(2));

                $('#discount-row').show();
            } else
            {
                priceDiscount = 0;
                $('#discount-row').hide();
            }

            priceGst = (priceTickets - priceDiscount) * (tax / 100);
            $('#price-gst').html(priceGst.toFixed(2));
            $('[name="price_tax"]').val(priceGst.toFixed(2));


//            priceCC = ((priceTickets - priceDiscount) + priceGst) * 0.03;
//            $('#price-cc').html(priceCC.toFixed(2));
//            $('[name="price_cc"]').val(priceCC.toFixed(2));


            priceGrandTotal = (priceTickets - priceDiscount) + priceGst; // + priceCC;
            $('#total-price').html(priceGrandTotal.toFixed(2));
            $('[name="price"]').val(priceGrandTotal.toFixed(2));
        }

        function updateAttendees()
        {
            attendees = Number($('#attendeeCount').val()) + 1;

            $('#tickets').html('');

            for(var i = 1; i <= attendees; i++)
            {
                var html    = template({count: i });
                $('#tickets').append(html);
            }

            updatePrice();

            $('#attendeeAndSubmit').show();
        }

        $(document).ready(function() {
            source   = $("#attendee-template").html();
            template = Handlebars.compile(source);

            updateAttendees();

            {{--$('[role="order-form"]').on('submit', function (e) {--}}
                {{--e.preventDefault();--}}

                {{--if($('[name="agree"]:checked').length)--}}
                {{--{--}}
                    {{--$('#submitBtn').prop('disabled', true);--}}

                    {{--$('#submitBtn').append('&nbsp;<i class="fa fa-fw fa-spinner fa-spin"></i>');--}}

                    {{--$.ajax({--}}
                        {{--type: "POST",--}}
                        {{--url: '{{ route('eway_access_token_path') }}',--}}
                        {{--data: $(this).serialize(),--}}
                        {{--success: function (data) {--}}
                            {{--console.log(data);--}}

                            {{--var obj = JSON.parse(data);--}}

                            {{--eWayAccessCode = obj.AccessCode;--}}

                            {{--eWayConfig = {--}}
                                {{--sharedPaymentUrl: obj.SharedPaymentUrl--}}
                            {{--};--}}

                            {{--eCrypt.showModalPayment(eWayConfig, eWayResultCallback);--}}
                        {{--},--}}
                        {{--error: function(data) {--}}
                            {{--console.log(data);--}}
                        {{--}--}}
                    {{--});--}}
                {{--} else--}}
                {{--{--}}
                    {{--alert('You must agree to the Terms & Conditions');--}}
                {{--}--}}


            {{--});--}}
        });
    </script>

    <script id="attendee-template" type="text/x-handlebars-template">
        <fieldset class="attendee">
            <legend>Attendee @{{ count }}</legend>

            <div class="form-group">
                <label for="ticket[@{{ count }}][name]" class="control-label">Attendee Name</label>
                <input class="form-control" name="ticket[@{{ count }}][name]" type="text" id="ticket[@{{ count }}][name]">
            </div>

            <div class="form-group">
                <label for="ticket[@{{ count }}][email]" class="control-label">Attendee Email</label>
                <input class="form-control" name="ticket[@{{ count }}][email]" type="text" id="ticket[@{{ count }}][email]">
            </div>
        </fieldset>
    </script>
@endsection