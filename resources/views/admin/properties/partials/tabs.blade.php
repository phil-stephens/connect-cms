<div class="page-header">
    <h1 class="pull-left">Edit Property Settings</h1>

    @if(Auth::user()->developer)
    <a href="{!! route('create_property_path') !!}" class="btn btn-primary btn-lg pull-right">Create New Property</a>
    @endif
</div>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('edit_property_path', 'Settings', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'location') class="active"@endif>{!! link_to_route('edit_property_location_path', 'Location', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'amenities') class="active"@endif>{!! link_to_route('edit_property_amenities_path', 'General Features', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'statistics') class="active"@endif>{!! link_to_route('statistics_path', $model->present()->roomNoun(1) . ' Statistics', $model->uuid) !!}</li>
    <li role="presentation"@if($tab == 'content') class="active"@endif>{!! link_to_route('edit_property_content_path', 'Content', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'slideshow') class="active"@endif>{!! link_to_route('edit_property_slideshow_path', 'Property Images', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'seo') class="active"@endif>{!! link_to_route('edit_property_seo_path', 'SEO', $model->id) !!}</li>
    <li role="presentation"@if($tab == 'integrations') class="active"@endif>{!! link_to_route('property_integration_path', 'Integrations', $model->id) !!}</li>
</ul>
