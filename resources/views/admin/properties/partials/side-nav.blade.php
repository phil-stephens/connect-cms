@section('extra-nav-links')
    <ul class="nav navbar-nav">
        <li><a href="{!! route('property_path', $property->id) !!}"><i class="fa fa-fw fa-building"></i> {{ $property->name }}</a></li>
    </ul>
@endsection

<div class="col-sm-2 col-md-offset-2 col-sm-offset-3 sidebar secondary">

    <div class="collapse">


        <ul class="nav nav-sidebar">
            @if(env('HAS_COMPENDIUM', false) || Auth::user()->developer)
                <li role="presentation"><a style="background: rgba(0,0,0,0.1);" href="{{ route('compendium_path', $property->compendium->uuid) }}"><i class="fa fa-fw fa-book"></i> Compendium <i class="fa fa-angle-right"></i></a></li>
            @endif
            <li role="presentation"><a href="{{ route('edit_property_path', $property->id) }}"><i class="fa fa-fw fa-cog"></i> Settings</a></li>
            <li role="presentation"><a href="{{ route('amenities_path', $property->id) }}"><i class="fa fa-fw fa-check-square-o"></i> Features</a></li>
            <li role="presentation"><a href="{{ route('rooms_path', $property->id) }}"><i class="fa fa-fw fa-bed"></i> {{ $property->present()->roomNoun(2) }}</a></li>
            <li role="presentation"><a href="{{ route('offers_path', $property->id) }}"><i class="fa fa-fw fa-certificate"></i> Special Offers</a></li>
            <li role="presentation"><a href="{{ route('reviews_path', $property->uuid) }}"><i class="fa fa-fw fa-comment"></i> Reviews</a></li>
        </ul>
    </div>
</div>