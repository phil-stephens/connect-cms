@extends('admin.layouts.default')

@section('content')
<div class="page-header">
    <h1 class="pull-left">Properties</h1>

    @if(Auth::user()->developer)
    <a href="{!! route('create_property_path') !!}" class="btn btn-primary btn-lg pull-right">Create New Property</a>
    @endif
</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Property Name</th>
        <th>Accommodation</th>
        <th class="button-column"></th>
    </tr>
    </thead>
    @foreach($properties as $property)
        <tr>
            <td>{!! link_to_route('property_path', $property->name, $property->id) !!}</td>
            <td>{!! link_to_route('rooms_path', $property->present()->roomCount(), $property->id) !!}</td>
            <td>{!! link_to_route('property_path', 'Explore', $property->id, ['class' => 'btn btn-primary btn-sm']) !!}</td>
        </tr>
    @endforeach
</table>
@endsection