@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav', ['property' => $model])
@endsection

@section('content')

        @include('admin.properties.partials.tabs', ['tab' => 'seo'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}
            <!-- Canonical_site Form Input -->
            <div class="form-group">
                {!! Form::label('canonical_site_id', 'Canonical Site:') !!}
                {!! Form::select('canonical_site_id', $sites, null, ['class' => 'form-control']) !!}
            </div>

            @include('admin.layouts.partials.form-seo')

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
            </div>

        {!! Form::close() !!}
@endsection