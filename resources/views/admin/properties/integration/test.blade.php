@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav', ['property' => $model])
@endsection

@section('content')

    @include('admin.properties.partials.tabs', ['tab' => 'integrations'])


    <h2 class="page-header">
        Testing Property Integration
        <small>{{ $integration->name }}</small>
    </h2>

    <p>Available Room Types:</p>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Client ID</th>
                <th>Name</th>

            </tr>
        </thead>
        <tbody>
        @foreach($rooms as $room)
            <tr>
                <td>{{ $room['client_id'] }}</td>
                <td>{{ $room['name'] }}</td>
            </tr>
        @endforeach
        </tbody>


    </table>

@endsection