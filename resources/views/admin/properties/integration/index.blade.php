@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav', ['property' => $model])
@endsection

@section('content')

    @include('admin.properties.partials.tabs', ['tab' => 'integrations'])


    <div class="clearfix">
        <a href="{{ route('property_integration_add_path', $model->uuid) }}" class="btn btn-primary pull-right">Add an Integration</a>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Integration Name</th>
                <th>Type</th>
                <th>Client ID</th>
                <th class="button-column"></th>
                <th class="button-column"></th>
            </tr>
        </thead>
        <tbody>
        @foreach($model->integrations as $integration)
            <tr>

                <td>{{ $integration->name }}</td>

                <td>{{ config('integrations.providers.rates.' . class_basename($integration->type))['name'] }}</td>

                <td>{{ $integration->pivot->client_id }}</td>
                <td>
                    {!! link_to_route('property_integration_edit_path', 'Edit', [$model->uuid, $integration->uuid], ['class' => 'btn btn-sm btn-primary']) !!}
                </td>
                <td>
                    {!! link_to_route('property_integration_test_path', 'Test', [$model->uuid, $integration->uuid], ['class' => 'btn btn-sm btn-default']) !!}
                </td>
            </tr>
        @endforeach
        </tbody>


    </table>

@endsection