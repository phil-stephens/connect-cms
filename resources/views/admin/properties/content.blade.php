@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav', ['property' => $model])
@endsection

@section('content')

        @include('admin.properties.partials.tabs', ['tab' => 'content'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}

            @include('admin.layouts.partials.form-content')

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
            </div>

        {!! Form::close() !!}
@endsection