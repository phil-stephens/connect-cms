@extends('admin.layouts.default')

@section('content')
    <div class="container-fluid">
        <h1>Create Property</h1>

        @include('admin.layouts.partials.errors')

        {!! Form::open() !!}

            <!-- Name Form Input -->
            <div class="form-group">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Slug Form Input -->
            <div class="form-group">
                {!! Form::label('slug', 'URL Slug:') !!}
                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Room_noun Form Input -->
            <div class="form-group">
                {!! Form::label('room_noun', 'Room noun:') !!}
                {!! Form::text('room_noun', 'room', ['class' => 'form-control']) !!}
                <span class="help-block"><i class="fa fa-info-circle"></i> The word that describes your accommodation type i.e. room, apartment, villa etc</span>
            </div>

            <!-- Bookingbutton_channel Form Input -->
            {{--<div class="form-group">--}}
                {{--{!! Form::label('bookingbutton_channel', 'BookingButton Channel:') !!}--}}
                {{--{!! Form::text('bookingbutton_channel', null, ['class' => 'form-control']) !!}--}}
            {{--</div>--}}

            <fieldset>
                <legend>Location</legend>

                @include('admin.layouts.partials.form-location')
            </fieldset>

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Create Property', ['class' => 'btn btn-primary btn-lg']) !!}
            </div>

        {!! Form::close() !!}
    </div>
@endsection

@include('admin.layouts.partials.sluggify')