@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav', ['property' => $model])
@endsection

@section('content')

        @include('admin.properties.partials.tabs', ['tab' => 'slideshow'])

        @include('admin.layouts.partials.form-slideshow')


@endsection