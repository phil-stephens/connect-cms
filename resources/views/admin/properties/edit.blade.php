@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

        @include('admin.properties.partials.tabs', ['tab' => 'edit'])

        @include('admin.layouts.partials.errors')

        {!! Form::model($model) !!}

            <!-- Name Form Input -->
            <div class="form-group">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Slug Form Input -->
            <div class="form-group">
                {!! Form::label('slug', 'URL Slug:') !!}
                {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Room_noun Form Input -->
            <div class="form-group">
                {!! Form::label('room_noun', 'Room noun:') !!}
                {!! Form::text('room_noun', null, ['class' => 'form-control']) !!}
                <span class="help-block"><i class="fa fa-info-circle"></i> The word that describes your accommodation type i.e. room, apartment, villa etc</span>
            </div>

            {{--<!-- Bookingbutton_channel Form Input -->--}}
            {{--<div class="form-group">--}}
                {{--{!! Form::label('bookingbutton_channel', 'BookingButton Channel:') !!}--}}
                {{--{!! Form::text('bookingbutton_channel', null, ['class' => 'form-control']) !!}--}}
            {{--</div>--}}

            <!-- Star_rating Form Input -->
            {{--<div class="form-group">--}}
                {{--{!! Form::label('star_rating', 'Star Rating:') !!}--}}
                {{--{!! Form::text('star_rating', null, ['class' => 'form-control']) !!}--}}
            {{--</div>--}}

        <fieldset>
            <legend>Contact Details</legend>

            <!-- Phone Form Input -->
            <div class="form-group">
                {!! Form::label('phone', 'Phone:') !!}
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Email Form Input -->
            <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
            </div>
        </fieldset>

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
            </div>

        {!! Form::close() !!}

        @if(Auth::user()->developer)
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Remove Property</h3>
            </div>
            <div class="panel-body">
                <p>This cannot be undone.</p>

                {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_property_path', $model->id], 'class' => 'destroy-form']) !!}

                {!! Form::hidden('id', $model->id) !!}

                <!-- Submit field -->
                <div class="form-group">
                    {!! Form::submit('Remove Property Now', ['class' => 'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @endif

        <div class="alert alert-info">
            'Best Rate' JSON URL for this property:
            {!! link_to_route('rate_json_path', null, $model->uuid) !!}
        </div>
@endsection

@if(Auth::user()->developer)
@section('scripts')
    @parent
    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this property?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection
@endif