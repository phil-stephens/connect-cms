@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav', ['property' => $model])
@endsection

@section('content')

    @include('admin.properties.partials.tabs', ['tab' => 'amenities', 'property' => $model])

    @include('admin.layouts.partials.amenities-selector', ['modelAmenities' => $model->generalAmenities, 'destroyPath' => route('destroy_property_amenity_path', $model->id), 'addButtonLabel' => 'Add to Property\'s General Features'])

@endsection

