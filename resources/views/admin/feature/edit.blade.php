@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

        <h1 class="page-header">Edit Feature</h1>

        @include('admin.layouts.partials.errors')

        {!! Form::model($amenity) !!}

        {!! Form::hidden('property_id', $property->id) !!}

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Class Form Input -->
        <div class="form-group">
        {!! Form::label('class', 'Custom Field:') !!}
        {!! Form::text('class', null, ['class' => 'form-control']) !!}
        </div>



        <div class="form-group">
            {!! Form::label('icon', 'Icon:') !!}

            @include('admin.layouts.partials.image-upload', ['field' => 'icon', 'model' => $amenity, 'showRemoveButton' => true])
        </div>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

        {!! Form::close() !!}

        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Remove Feature</h3>
            </div>
            <div class="panel-body">
                <p>This cannot be undone.</p>

                {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_amenity_path', $property->id, $amenity->id], 'class' => 'destroy-form']) !!}

                {!! Form::hidden('id', $amenity->id) !!}

                <!-- Submit field -->
                <div class="form-group">
                    {!! Form::submit('Remove Feature Now', ['class' => 'btn btn-danger']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>


        @include('admin.layouts.partials.media-modal')
@endsection

@section('scripts')
    @parent

    <script>

        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this feature?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>

@endsection