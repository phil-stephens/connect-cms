@extends('admin.layouts.default')

@section('head')
    @parent
    {!! core_icons(['hotel', 'media']) !!}
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.feature.partials.tabs', ['tab' => 'groups'])

    <div class="clearfix">
        <a href="{{ route('create_amenity_path', $property->id) }}" class="btn btn-primary pull-right">Create New Feature Group</a>
    </div>


    <table class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Features</th>
                <th class="button-column"></th>
                <th class="button-column"></th>
            </tr>
        </thead>

        <tbody>
            @foreach($groups as $group)
                <tr>
                    <th>{{ $group->name }}</th>
                    <th>{{ $group->features()->count() }}</th>
                    <th></th>
                    <th></th>
                </tr>

            @endforeach
        </tbody>

    </table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this feature?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection