@extends('admin.layouts.default')

@section('head')
    @parent
    {!! core_icons(['hotel', 'media']) !!}
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')


    @include('admin.feature.partials.tabs', ['tab' => 'list'])

    @if(configuration('system-wide-features', false))
    <div class="alert alert-danger">
        <strong>Please note:</strong> This features list is for reference only and not used by properties.  Refer to your {!! link_to_route('account_amenities_path', 'System-wide Features') !!} for your master list.
    </div>
    @endif


    <div class="clearfix">
        <a href="{{ route('create_amenity_path', $property->id) }}" class="btn btn-primary pull-right {{ ( configuration('system-wide-features', false)) ? 'disabled' : '' }}">Create New Feature</a>
    </div>

    <div class="maskable" role="feature-table">

        <table class="table table-striped">
            <thead>
            <tr>
                <th>Description</th>
                <th>Icon</th>
                {{--<th colspan="2">CSS Icon Class</th>--}}
                <th class="button-column"></th>
                <th class="button-column"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($amenities as $amenity)
                <tr>
                    <td>{{ $amenity->name }}</td>
                    <td>
                        {!! $amenity->getImageTag(['w' => 50, 'h' => 50, 'fit' => 'crop'], ['class' => 'img-responsive']) !!}
                    </td>
                    {{--<td>--}}
                    {{--@if( $amenity->class)--}}
                    {{--<i class="{{ $amenity->class }}" style="font-size: 24px; margin: 0;"></i>--}}
                    {{--@endif--}}
                    {{--</td>--}}
                    {{--<td><code>{{ $amenity->class }}</code></td>--}}

                    <td>{!! link_to_route('edit_amenity_path', 'Edit', [$property->id, $amenity->id], ['class' => 'btn btn-primary btn-sm']) !!}</td>
                    <td>
                        {!! Form::open(['route' => ['destroy_amenity_path', $property->id, $amenity->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                        {!! Form::hidden('id', $amenity->id) !!}
                        <button type="submit" class="btn btn-default btn-sm">Remove</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        @if((configuration('system-wide-features', false)))
            <div class="mask"></div>
        @endif
    </div>

@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this feature?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection