@extends('admin.layouts.default')

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/pickadate/default.css"/>
    <link rel="stylesheet" href="/core/css/pickadate/default.date.css"/>
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.offers.partials.tabs', ['tab' => 'edit'])

    @include('admin.layouts.partials.errors')

    {!! Form::model($model) !!}

    <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    <div class="form-group">
        {!! Form::label('slug', 'Slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Headline Form Input -->
    <div class="form-group">
        {!! Form::label('headline', 'Headline:') !!}
        {!! Form::text('headline', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Link Form Input -->
    <div class="form-group">
        {!! Form::label('link', 'Direct Link:') !!}
        {!! Form::text('link', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Start_at Form Input -->
    <div class="form-group">
        {!! Form::label('start_at', 'Start at:') !!}
        {!! Form::text('start_at', $model->start_at->format('j F Y'), ['class' => 'form-control start_at']) !!}
    </div>

    <!-- Finish_at Form Input -->
    <div class="form-group">
        {!! Form::label('finish_at', 'Finish at:') !!}
        {!! Form::text('finish_at', $model->finish_at->format('j F Y'), ['class' => 'form-control finish_at']) !!}
    </div>

    <!-- Terms Form Input -->
    <div class="form-group">
        {!! Form::label('terms', 'Terms and Conditions:') !!}
        {!! Form::textarea('terms', null, ['class' => 'form-control', 'rows' => 7, 'role' => 'editor']) !!}
    </div>

    <div class="checkbox">
        <label>
            {!! Form::checkbox('active', 1) !!} Active
        </label>
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}

    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Remove Special Offer</h3>
        </div>
        <div class="panel-body">
            <p>This cannot be undone.</p>

            {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_offer_path', $property->id, $model->id], 'class' => 'destroy-form']) !!}

            {!! Form::hidden('id', $model->id) !!}

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Remove Special Offer Now', ['class' => 'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('scripts')
    @parent

    <script src="/core/js/pickadate/picker.js"></script>
    <script src="/core/js/pickadate/picker.date.js"></script>

    @if(env('EDITOR_FORMAT', 'md') === 'html')
        <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
        <script>
            function tinymceInit()
            {
                tinymce.init({
                    selector: 'textarea[role="editor"]',
                    menubar : false,
                    content_css : '/core/css/editor.css',
                    statusbar : false,
                    plugins: "link code paste",
                    toolbar: "bold italic | bullist numlist | link code",
                    valid_elements : '+*[*]'
                });
            }

            tinymceInit();
        </script>
    @endif

    <script>
        $(document).ready(function() {

            $('.destroy-form').on('submit', function(e)
            {
                e.preventDefault();

                var theForm = this;

                bootbox.confirm('Are you sure you want to remove this special offer?', function(result) {
                    if(result)
                    {
                        theForm.submit();
                    }
                });
            });

            var finish_at = $('.finish_at').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true
            });

            $('.start_at').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true,
                onSet: function(context) {
                    var finishPicker = finish_at.pickadate('picker');
                    var minDate = context.select;
                    var finishSelected = finishPicker.get('select');

                    if(finishSelected != null && finishSelected.pick < minDate)
                    {
                        finishPicker.set('select', minDate);
                    }

                    finishPicker.set('min', new Date(minDate));
                }
            });
        });
    </script>
@endsection