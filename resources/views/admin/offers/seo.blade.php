@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    @include('admin.offers.partials.tabs', ['tab' => 'seo'])
    
    @include('admin.layouts.partials.errors')

    {!! Form::model($model) !!}

    {!! Form::hidden('id') !!}

    @include('admin.layouts.partials.form-seo')

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}

@endsection