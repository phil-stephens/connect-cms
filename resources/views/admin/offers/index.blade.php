@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Special Offers</h1>

        <a href="{!! route('create_offer_path', $property->id) !!}" class="btn btn-primary btn-lg pull-right">Create Special Offer</a>
    </div>

    <table class="table table-striped sortable-table" id="sortable">
        <thead>
        <tr>
            <th></th>
            <th>Offer Name</th>
            <th>Offer Title</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>

        <tbody>
        @foreach($offers as $offer)
                <tr data-id="{{ $offer->id }}">
                    <td class="sort-handle"><i class="fa fa-fw fa-bars"></i></td>
                    <td>{!! link_to_route('edit_offer_path', $offer->name, [$property->id, $offer->id]) !!}</td>
                    <td>{{ $offer->getTitle() }}</td>
                    <td>{!! link_to_route('edit_offer_path', 'Edit', [$property->id, $offer->id], ['class' => 'btn btn-primary btn-sm']) !!}</td>
                    <td>
                        {!! Form::open(['route' => ['destroy_offer_path', $property->id, $offer->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                        {!! Form::hidden('id', $offer->id) !!}
                        <button type="submit" class="btn btn-default btn-sm">Remove</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    @parent

    <script src="/core/js/jquery.sortable.min.js"></script>

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this special offer?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });

        $('#sortable').sortable({
            group: 'offers',
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            handle: 'td.sort-handle',
            onDrop: function ($item, container, _super) {
                // Just loop through the table
                var i = 0;
                var offers = [];
                $('#sortable tbody tr').each(function() {

                    offers[ $(this).data('id') ] = i;
//                    $('.sort-order', this).val(i);

                    i++;
                });

                //console.log(JSON.stringify(pages));

                // Submit details via AJAX
                $.ajax({
                    type: "POST",
                    url: '{{ route('sort_offers_path', $property->id) }}',
                    data: {_token: $('meta[name="csrf_token"]').attr('content'), data: offers},
                    success: function(data)
                    {
                        console.log(data);
                    }
                });

                _super($item, container);
            }
        });
    </script>
@endsection