<h1 class="page-header">Edit Special Offer <small>{{ $model->name }}</small></h1>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('edit_offer_path', 'Settings', [$property->id, $model->id]) !!}</li>
    <li role="presentation"@if($tab == 'content') class="active"@endif>{!! link_to_route('content_offer_path', 'Content', [$property->id, $model->id]) !!}</li>
    {{--<li role="presentation"@if($tab == 'slideshow') class="active"@endif>{!! link_to_route('slideshow_offer_path', 'Slideshow', [$property->id, $model->id]) !!}</li>--}}
    <li role="presentation"@if($tab == 'seo') class="active"@endif>{!! link_to_route('seo_offer_path', 'SEO', [$property->id, $model->id]) !!}</li>
</ul>