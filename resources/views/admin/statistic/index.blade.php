@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav', ['property' => $model])
@endsection

@section('content')

    @include('admin.properties.partials.tabs', ['tab' => 'statistics', 'property' => $model])

    <div class="page-header">

        <h2 class="pull-left">{{ $model->present()->roomNoun() }} Statistics</h2>

        <a href="{{ route('statistic_create_path', $model->uuid) }}" class="btn btn-primary pull-right">Add a new {{ $model->present()->roomNoun() }} Statistic</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Title</th>
            <th>Key</th>
            <th>Custom</th>
            <th>Icon</th>
            <th class="button-column"></th>
            {{--<th class="button-column"></th>--}}
        </tr>
        </thead>
        <tbody>
        @foreach($statistics as $statistic)
            <tr>
                <td>{{ $statistic->title }}</td>
                <td>{{ $statistic->key }}</td>
                <td>{{ $statistic->custom }}</td>
                <td>
                    {!! $statistic->getImageTag(['w' => 50, 'h' => 50, 'fit' => 'crop'], ['class' => 'img-responsive']) !!}
                </td>

                <td>{!! link_to_route('statistic_edit_path', 'Edit', $statistic->uuid, ['class' => 'btn btn-primary btn-sm']) !!}</td>
                <td>
                    {!! Form::open(['route' => ['statistic_destroy_path', $model->uuid ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    {!! Form::hidden('uuid', $statistic->uuid) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this statistic?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection



