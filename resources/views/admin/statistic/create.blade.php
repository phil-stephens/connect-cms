@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav', ['property' => $model])
@endsection

@section('content')

    @include('admin.properties.partials.tabs', ['tab' => 'statistics', 'property' => $model])


    <div class="page-header">

        <h2 class="pull-left">Create {{ $model->present()->roomNoun() }} Statistic</h2>

        <a href="{!! route('statistics_path', $model->uuid) !!}" class="btn btn-default pull-right">Back to {{ $model->present()->roomNoun() }} Statistics</a>
    </div>

    {!! Form::open() !!}

            <!-- Title Form Input -->
    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Key Form Input -->
    <div class="form-group">
        {!! Form::label('key', 'Key:') !!}
        {!! Form::text('key', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Custom Form Input -->
    <div class="form-group">
        {!! Form::label('custom', 'Custom:') !!}
        {!! Form::text('custom', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('icon', 'Icon:') !!}

        @include('admin.layouts.partials.image-upload', ['field' => 'icon', 'model' => null, 'showRemoveButton' => true])
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create Room Statistic', ['class' => 'btn btn-primary']) !!}
    </div>

    {!! Form::close() !!}
@endsection

@section('modals')
    @include('admin.layouts.partials.media-modal')
@endsection



