@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.properties.partials.side-nav', ['property' => $model])
@endsection

@section('content')

    @include('admin.properties.partials.tabs', ['tab' => 'statistics', 'property' => $model])


    <div class="page-header">

        <h2 class="pull-left">Edit {{ $model->present()->roomNoun() }} Statistic</h2>

        <a href="{!! route('statistics_path', $model->uuid) !!}" class="btn btn-default pull-right">Back to {{ $model->present()->roomNoun() }} Statistics</a>
    </div>

    {!! Form::model($statistic) !!}

            <!-- Title Form Input -->
    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Key Form Input -->
    <div class="form-group">
        {!! Form::label('key', 'Key:') !!}
        {!! Form::text('key', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Custom Form Input -->
    <div class="form-group">
        {!! Form::label('custom', 'Custom:') !!}
        {!! Form::text('custom', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('icon', 'Icon:') !!}

        @include('admin.layouts.partials.image-upload', ['field' => 'icon', 'model' => $statistic,  'showRemoveButton' => true])
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}
    </div>

    {!! Form::close() !!}

    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Remove {{ $model->present()->roomNoun() }} Statistic</h3>
        </div>
        <div class="panel-body">
            <p>This cannot be undone.</p>

            {!! Form::open(['method' => 'DELETE', 'route' => ['statistic_destroy_path', $model->uuid], 'class' => 'destroy-form']) !!}

            {!! Form::hidden('uuid', $statistic->uuid) !!}

                    <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Remove ' . $model->present()->roomNoun() . ' Statistic', ['class' => 'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('modals')
    @parent
    @include('admin.layouts.partials.media-modal')
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this statistic?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection



