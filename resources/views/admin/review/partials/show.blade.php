<div class="modal-body">
    @if($review->stars)
    <p>{{ $review->stars }} <i class="fa fa-star"></i></p>
    @endif

    <blockquote>
        {!! $review->getMessage() !!}

        <footer>{{ $review->name }} <cite title="Source Title">{{ $review->email }}</cite></footer>
    </blockquote>

    @if($review->room)
        <p>{{ $review->property->present()->roomNoun() }}: {{ $review->room->name }}</p>
    @endif

    @if($review->date)
        <p>Approximate date of stay: {{ $review->date->format('l, j F Y') }}</p>
    @endif

    @if($review->source)
        <p>Source: {{ $review->source }}</p>
    @endif

    @if($review->url)
        <p>Source URL: {{ prepUrl($review->url) }}</p>
    @endif
</div>

<div class="modal-footer">
    {!! Form::open(['route' => ['review_edit_path', $review->uuid]]) !!}
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <a href="{{ route('review_edit_path', $review->uuid) }}" class="btn btn-primary">Edit</a>

    @if($review->published)
    {!! Form::submit('Unpublish', ['class' => 'btn btn-default']) !!}
    {{--<a href="{{ route('review_edit_path', $review->uuid) }}" class="btn btn-default">Unpublish</a>--}}
    @else
    {!! Form::hidden('published', true) !!}
    {!! Form::submit('Publish', ['class' => 'btn btn-default']) !!}
    {{--<a href="{{ route('review_edit_path', $review->uuid) }}" class="btn btn-default">Publish</a>--}}
    @endif
    {!! Form::close() !!}
</div>