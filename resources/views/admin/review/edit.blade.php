@extends('admin.layouts.default')

@section('head')
    @parent
    <link rel="stylesheet" href="{{ core_asset('css/pickadate/default.css') }}"/>
    <link rel="stylesheet" href="{{ core_asset('css/pickadate/default.date.css') }}"/>
@endsection

@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
@include('admin.properties.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Edit Review</h1>

        <a href="{!! route('reviews_path', $property->uuid) !!}" class="btn btn-default btn-lg pull-right">Back to Reviews</a>
    </div>

    {!! Form::model($review) !!}

    <fieldset>
        <legend>Reviewer Details</legend>

        <!-- Name Form Input -->
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
        </div>

        <!-- Email Form Input -->
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>
    </fieldset>

    <fieldset>
        <legend>Stay Information</legend>

        <!-- Date Form Input -->
        <div class="form-group">
            {!! Form::label('date', 'Approximate Date:') !!}
            {!! Form::text('date', $review->date->format('j F Y'), ['class' => 'form-control', 'role' => 'datepicker']) !!}
        </div>

        <!-- Room_id Form Input -->
        <div class="form-group">
            {!! Form::label('room_id', $property->present()->roomNoun(1) . ' Type:') !!}
            {!! Form::select('room_id', array_merge([0 => 'N/A'], $rooms), null, ['class' => 'form-control']) !!}
        </div>
    </fieldset>

    <fieldset>
        <legend>Review Details</legend>

        <!-- Message Form Input -->
        <div class="form-group">
            {!! Form::label('message', 'Message:') !!}
            {!! Form::textarea('message', null, ['class' => 'form-control', 'rows' => 4]) !!}
        </div>

        <!-- Stars Form Input -->
        <div class="form-group">
            {!! Form::label('stars', 'Stars:') !!}
            {!! Form::select('stars', ['0' => '0',
                                        '1' => '1',
                                        '1.5' => '1.5',
                                        '2' => '2',
                                        '2.5' => '2.5',
                                        '3' => '3',
                                        '3.5' => '3.5',
                                        '4' => '4',
                                        '4.5' => '4.5',
                                        '5' => '5'], null, ['class' => 'form-control']) !!}
        </div>
    </fieldset>

    <fieldset>
        <legend>Attribution</legend>

        <!-- Source Form Input -->
        <div class="form-group">
            {!! Form::label('source', 'Source:') !!}
            {!! Form::text('source', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Url Form Input -->
        <div class="form-group">
            {!! Form::label('url', 'URL of original review:') !!}
            {!! Form::text('url', null, ['class' => 'form-control']) !!}
        </div>
    </fieldset>


    <div class="alert alert-info">
        {{--<div class="checkbox">--}}
            {{--<label>--}}
                {{--{!! Form::checkbox('approved', 1, null) !!} Approved--}}
            {{--</label>--}}
            {{--<span class="help-block">This review has been moderated and approved to be published on sites</span>--}}
        {{--</div>--}}

        <div class="checkbox">
            <label>
                {!! Form::checkbox('published', 1, null) !!} Published
            </label>
            <span class="help-block">This review has been approved and can be displayed on sites</span>

        </div>
    </div>
    
    
    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}
@endsection

@section('scripts')
    @parent

    <script src="{{ core_asset('js/pickadate/picker.js') }}"></script>
    <script src="{{ core_asset('js/pickadate/picker.date.js') }}"></script>

    <script>
        $(document).ready(function() {

            $('[role="datepicker"]').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true,
                max: true
            });
        });
    </script>
@endsection