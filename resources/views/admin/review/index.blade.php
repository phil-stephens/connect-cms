@extends('admin.layouts.default')

@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
@include('admin.properties.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Reviews</h1>

        <a href="{!! route('review_create_path', $property->uuid) !!}" class="btn btn-primary btn-lg pull-right">Create New Review</a>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th width="15%"></th>
                <th></th>
                <th width="10%"></th>
                {{--<th class="button-column">Approved</th>--}}
                <th class="button-column">Published</th>
                <th class="button-column"></th>
                <th class="button-column"></th>
            </tr>
        </thead>

        <tbody>
            @foreach($reviews as $review)
                <tr>
                    <td>{{ $review->name }}</td>
                    <td class="text-preview-cell" data-toggle="modal" data-target="#preview-review" data-uuid="{{ $review->uuid }}">
                        <div class="text-preview-column">
                            {!! $review->message !!}
                        </div>
                    </td>
                    {{--<td class="text-center">--}}
                        {{--<i class="fa fa-{{ ($review->approved) ? 'check' : 'remove' }}"></i>--}}
                    {{--</td>--}}
                    <td>
                        {{ $review->created_at->format('d M Y') }}
                    </td>
                    <td class="text-center">
                        <i class="fa fa-{{ ($review->published) ? 'check' : 'remove' }}"></i>
                    </td>
                    <td>
                        {!! link_to_route('review_edit_path', 'Edit', $review->uuid, ['class' => 'btn btn-primary btn-sm']) !!}
                    </td>
                    <td>
                        {!! Form::open(['route' => ['review_destroy_path', $property->uuid ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                        {!! Form::hidden('uuid', $review->uuid) !!}
                        <button type="submit" class="btn btn-default btn-sm">Remove</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>

        <tfoot>
        <tr>
            <td class="text-center" colspan="6">{!! $reviews->render() !!}</td>
        </tr>
        </tfoot>
    </table>
@endsection

@section('modals')
    @parent

    <div class="modal fade" id="preview-review" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Review</h4>
                </div>
                <div id="preview-form">
                    <div class="modal-body">
                    </div>

                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    @parent
    <script src="{!! core_asset('js/spin.min.js') !!}"></script>

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this review?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });

        $('#preview-review').on('show.bs.modal', function (e) {
            var btn = $(e.relatedTarget) // Button that triggered the modal

            if(btn.data('uuid') != undefined)
            {
                $('#preview-form').spin();

                $('#preview-form').load('{!! route('review_path__ajax') !!}/' + btn.data('uuid'));
            } else
            {
                // close the modal?
            }

        });
    </script>
@endsection