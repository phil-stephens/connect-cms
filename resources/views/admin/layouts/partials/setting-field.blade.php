 <div class="form-group">
    {!! Form::hidden('setting[' . $setting->key . '][type]', $setting->type) !!}

    {!! Form::label('setting[' . $setting->key . '][value]', $setting->label . ':') !!}

    @if($existing = $model->settingRow($setting->key))
        {!! Form::hidden('setting[' . $setting->key . '][id]', $existing->id) !!}
        <?php $value = $existing->value; ?>
    @else
        <?php
        $value = ((isset($setting->default)) ? $setting->default : null);

        ?>
    @endif

    @if($setting->type == 'plain')
        @if(isset($setting->pre) || isset($setting->post))
            <div class="input-group">
                @endif

                @if(isset($setting->pre))
                    <span class="input-group-addon">{!! $setting->pre !!}</span>
                @endif

                {!! Form::text('setting[' . $setting->key . '][value]', $value, ['class' => 'form-control']) !!}

                @if(isset($setting->post))
                    <span class="input-group-addon">{!! $setting->post !!}</span>
                @endif

                @if(isset($setting->pre) || isset($setting->post))
            </div>
        @endif

    @elseif($setting->type == 'html')

        {!! Form::textarea('setting[' . $setting->key . '][value]', $value, ['class' => 'form-control', 'rows' => 5, 'role' => 'editor']) !!}

    @elseif($setting->type == 'image')

        @include('admin.layouts.partials.image-upload', ['field' => 'setting[' . $setting->key . '][uuid]', 'model' => $model->setting($setting->key), 'showRemoveButton' => true])
    @elseif($setting->type == 'colour')

        {!! Form::text('setting[' . $setting->key . '][value]', $value, ['class' => 'form-control color {hash:true}']) !!}

    @elseif($setting->type == 'file')

        <div role="file-dropzone" class="file-uploader placeholder">

            <div class="file-info">
                @if($existing && $file = $existing->file)
                    <i class="fa {{ mimeIconClass($file->mime_type) }} fa-3x"></i> {{ $file->original_name }} <small>{{ byte_format($file->size) }} |
                        <a href="{!! route('uuid_download_path', $file->uuid) !!}">Download Original</a>
                    </small>
                @endif
            </div>

            <div class="dropzone file-clickable">
                <div class="dropzone-previews"></div>
            </div>

            {!! Form::hidden('setting[' . $setting->key . '][uuid]', null, ['role' => 'uuid-field']) !!}
        </div>
    @endif


    @if( ! empty($setting->description))
        <span class="help-block"><i class="fa fa-info-circle"></i> {{ $setting->description }}</span>
    @endif
</div>

 @section('scripts')
     @parent

     <script>
         function fileDropzoneInit() {

             $('[role="file-dropzone"]').each(function()
             {
                 if(this.dropzone == undefined)
                 {
//                     var hash = $(this).data('hash');
//                     var targetId = $(this).data('target-id');
                     var clickable = $('.file-clickable', this);
                     var previews = $('.dropzone-previews', this);
                     var fileInfo = $('.file-info', this);
                     var uuid = $('[role="uuid-field"]', this);

                     $(this).dropzone( {
                         url: '{{ route('content_file_path') }}',
                         addRemoveLinks: true,
                         maxFiles: 1,
                         maxFilesize: {{ env('UPLOAD_LIMIT', 32) }},
                         parallelUploads: 1,
                         previewsContainer: previews[0],
                         clickable: clickable[0],
                         init: function() {

                             this.on('sending', function(file, xhr, formData) {
                                 formData.append('_token', $('meta[name="csrf_token"]').attr('content'));
                             });

                             this.on("success", function(file, responseText) {
                                 console.log(responseText);

                                 fileInfo.html('<i class="fa ' + responseText.mime_class + ' fa-3x"></i> ' + responseText.name + ' <small>' + responseText.size + '</small>');

                                 uuid.val(responseText.uuid)
                                 this.removeFile(file);
                             });
                         }
                     });
                 }
             })
         }

         fileDropzoneInit();
     </script>

 @endsection





