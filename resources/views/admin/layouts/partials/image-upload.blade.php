<?php $fieldHash = str_random(); ?>
<div class="image-preview" data-toggle="modal" data-target="#media-browser" data-target-field="{{ $field }}" id="{{ $fieldHash }}">
    <i class="fa fa-cloud-upload fa-fw upload-icon"></i>
    @if( ! empty($model))
        {!! $model->getImageTag(config('image.preview_params'), ['class' => 'img-responsive content-image']) !!}

        {!! Form::hidden($field, $model->getImageUuid()) !!}
    @else
        {!! Form::hidden($field) !!}
    @endif

</div>

<div class="btn-group" role="group" style="margin-bottom: 15px;">
    @unless(empty($showRemoveButton))
    <button class="btn btn-default btn-sm" role="remove-image" data-target-field="{{ $field }}" data-target-element="{{ $fieldHash }}">Remove</button>
    @endunless
    @if( ! empty($model) && ! empty($model->getImagePath()))
    <a href="{!! route('uuid_download_path', $model->getImageUuid()) !!}" role="button" class="btn btn-default btn-sm">Download Original</a>
    @endif
</div>