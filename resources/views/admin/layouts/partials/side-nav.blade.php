<div class="col-md-2 col-sm-4 sidebar">
    <div class="_sidebar">
        <ul class="nav nav-sidebar {{ (isset($_nav_collapsed)) ? $_nav_collapsed : '' }}">
            <li role="presentation"><a href="{{ route('sites_path') }}" title="Websites" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-code"></i><span class="nav-label">Websites</span></a></li>

            @if(env('HAS_PROPERTIES', true))
                <li role="presentation"><a href="{{ route('properties_path') }}" title="Properties" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-building-o"></i><span class="nav-label">Properties</span></a></li>
            @endif

            {{--@if(env('HAS_ACCOUNT_AMENITIES', false))--}}
                {{--<li role="presentation"><a href="{{ route('account_amenities_path') }}" title="Account-level Features" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-check-square-o"></i><span class="nav-label">Account-level Features</span></a></li>--}}
            {{--@endif--}}
            
            <li role="presentation"><a href="{{ route('feeds_path') }}" title="Content Feeds" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-newspaper-o"></i><span class="nav-label">Content Feeds</span></a></li>

            @if(env('HAS_VENUES', true))
                <li role="presentation"><a href="{{ route('venues_path') }}" title="Places" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-map-marker"></i><span class="nav-label">Places</span></a></li>
            @endif

            @if(env('HAS_EVENTS', true))
                <li role="presentation"><a href="{{ route('calendars_path') }}" title="Calendars" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-calendar"></i><span class="nav-label">Calendars</span></a></li>
            @endif

            <li role="presentation"><a href="{{ route('libraries_path') }}" title="Media" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-image"></i><span class="nav-label">Media</span></a></li>

            {{--<li role="presentation"><a href="{{ route('integrations_path') }}" title="Integrations" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-share-alt"></i><span class="nav-label">Integrations</span></a></li>--}}

            {{--<li role="presentation" class="divider"></li>--}}
            {{--<li role="presentation"><a href="{{ route('users_path') }}" title="Users" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-user"></i><span class="nav-label">Users</span></a></li>--}}

            {{--@if( ! Auth::user()->parent)--}}
            {{--<li role="presentation"><a href="#"><i class="fa fa-fw fa-users"></i> Clients</a></li>--}}
            {{--@endif--}}

            {{--<li role="presentation"><a href="{{ route('users_path') }}" title="General Settings" data-toggle="tooltip" data-placement="right"><i class="fa fa-fw fa-cog"></i><span class="nav-label">General Settings</span></a></li>--}}

        </ul>

        {{--<p class="text-center sidebar-footer">--}}
        {{--<a href="http://fastrackg.com" target="_blank"><strong>Fastrack</strong><em>Connect</em></a>--}}
        {{--</p>--}}
    </div>

    @yield('secondary-nav')

</div>