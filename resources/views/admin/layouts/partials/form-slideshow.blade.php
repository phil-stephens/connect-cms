@include('admin.layouts.partials.errors')

{{--<div id="image-upload" class="slideshow-image-uploader placeholder">--}}
    {{--<div class="dropzone" id="image-clickable">--}}
        {{--<div id="image-preview" class="dropzone-previews"></div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="alert alert-info">
    These images can be used as a gallery or slideshow, depending on how the page template is set up. They are not the same as the Featured Image.
</div>

<p>
    <button class="btn btn-primary btn-lg btn-block" role="add-slide">Add New Image</button>
</p>

{{--@if( $model->slideshow && $model->slideshow->slides()->count())--}}

    {!! Form::open(['route' => ['update_slides_path', $model->slideshow->id]]) !!}

    <!-- Submit field -->
    {{--<div class="form-group">--}}
        {{--{!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}--}}
    {{--</div>--}}

    <table class="table sortable-table" id="sortable">
        <tbody class="list-body">
        @foreach($model->slideshow->slides as $slide)
            <tr>
                <td class="sort-handle"><i class="fa fa-fw fa-bars"></i></td>
                <td class="image-column">
                    @include('admin.layouts.partials.image-upload', ['field' => "slide[{$slide->id}][image]", 'model' => $slide])
                </td>

                <td>
                    {!! Form::hidden("slide[{$slide->id}][id]", $slide->id) !!}
                    {!! Form::text("slide[{$slide->id}][headline]", $slide->headline, ['class' => 'form-control', 'placeholder' => 'Headline']) !!}

                    {!! Form::textarea("slide[{$slide->id}][excerpt]", $slide->excerpt, ['class' => 'form-control', 'placeholder' => 'Excerpt', 'rows' => 3]) !!}

                    {!! Form::text("slide[{$slide->id}][link]", $slide->link, ['class' => 'form-control', 'placeholder' => 'Link']) !!}

                    {!! Form::hidden("slide[{$slide->id}][order]", $slide->order, ['class' => 'sort-order']) !!}

                    <label>{!! Form::checkbox("slide[{$slide->id}][destroy]", true) !!} Delete image</label>
                </td>
                <td>
                    <label for="">Crop From:</label>
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                @foreach(['top,left', 'top', 'top,right'] as $position)
                                <td class="text-center">{!! Form::radio("slide[{$slide->id}][crop_from]", $position, (bool) ($slide->crop_from == $position)) !!}</td>
                                @endforeach
                            </tr>

                            <tr>
                                @foreach(['left', '', 'right'] as $position)
                                    <td class="text-center">{!! Form::radio("slide[{$slide->id}][crop_from]", $position, (bool) ($slide->crop_from == $position)) !!}</td>
                                @endforeach
                            </tr>

                            <tr>
                                @foreach(['bottom,left', 'bottom', 'bottom,right'] as $position)
                                    <td class="text-center">{!! Form::radio("slide[{$slide->id}][crop_from]", $position, (bool) ($slide->crop_from == $position)) !!}</td>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>

                    <label>
                        {!! Form::checkbox("slide[{$slide->id}][face_detection]", true, (bool) $slide->face_detection) !!}
                        Face detection
                    </label>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary']) !!}
    </div>

    {!! Form::close() !!}
{{--@endif--}}

@include('admin.layouts.partials.media-modal')



@section('scripts')
    @parent

    <script src="/core/js/jquery.sortable.min.js"></script>
    <script src="/core/js/handlebars.js"></script>

    <script>

        function initSorting()
        {
            if($('#sortable').length) {
                $('#sortable').sortable({
                    group: 'slides',
                    containerSelector: 'table',
                    itemPath: '> tbody',
                    itemSelector: 'tr',
                    placeholder: '<tr class="placeholder"/>',
                    handle: 'td.sort-handle',
                    onDrop: function ($item, container, _super) {
                        $item.removeClass(container.group.options.draggedClass).removeAttr('style');
                        $('body').removeClass(container.group.options.bodyClass);

                        doSorting();

                        console.log(container);
                        _super($item, container);
                    }
                });
            }
        }

        function doSorting()
        {
            var i = 0;
            $('#sortable tbody tr').each(function() {
                $('.sort-order', this).val(i);

                i++;
            });
        }

        initSorting();

        $('[role="add-slide"]').on('click', function(e)
        {
            e.preventDefault();
//            alert('foo');

            var fieldHash = Math.random().toString(36).slice(2);

            var source   = $("#slideshow-slide-template").html();
            var template = Handlebars.compile(source);
            var html    = template({hash: fieldHash });

            $('.list-body').append(html);

            doSorting();

            $('html, body').animate({
                scrollTop: $("#slide_" + fieldHash).offset().top
            }, 1000);
        });
    </script>

    @include('admin.layouts.partials.handlebars.slideshow-slide')

@endsection