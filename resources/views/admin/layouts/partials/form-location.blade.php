<!-- Address1 Form Input -->
<div class="form-group">
    {!! Form::label('address1', 'Street Address:') !!}
    {!! Form::text('address1', null, ['class' => 'form-control']) !!}
</div>

<!-- Address2 Form Input -->
<div class="form-group">
    {!! Form::text('address2', null, ['class' => 'form-control']) !!}
</div>

<!-- City Form Input -->
<div class="form-group">
    {!! Form::label('city', 'City/Town:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <!-- Postcode Form Input -->
        <div class="form-group">
            {!! Form::label('postcode', 'Postcode:') !!}
            {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <!-- State Form Input -->
        <div class="form-group">
            {!! Form::label('state', 'State/Region:') !!}
            {!! Form::text('state', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Country Form Input -->
<div class="form-group">
    {!! Form::label('country', 'Country:') !!}
    {!! Form::select('country', config('countries.iso_countries'), null, ['class' => 'form-control']) !!}
</div>

@if( isset($showMap) && $showMap )
<fieldset>
    <legend>Actual Location</legend>

    <p>Drag the map marker so that it is in the most accurate position.</p>

    <div id="map-canvas" style="width: 100%; height: 400px; background: #ccc;"></div>

    <input type="hidden" name="latitude" />
    <input type="hidden" name="longitude" />
</fieldset>

@section('scripts')
    @parent
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key={{ config('geo.google_api_key') }}&sensor=true&v=3.exp"></script>

    <script>
        function initialize() {
            var myLatlng = new google.maps.LatLng({{ $model->latitude }}, {{ $model->longitude }});

            var mapOptions = {
                center: myLatlng,
                zoom: 16,
                disableDefaultUI: true,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL
                },
                scrollwheel: false
            };

            var map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title:"{{ $model->name }}",
                draggable:true
            });

            google.maps.event.addListener(marker, 'dragend', function(e){
                $('input[name="latitude"]').val(e.latLng.lat());
                $('input[name="longitude"]').val(e.latLng.lng());
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection
@endif