<fieldset>
    <legend>Meta Tags</legend>

    <!-- Title Form Input -->
    <div class="form-group clearfix">
        {!! Form::label('seo[title]', 'Page Title:') !!}
        {!! Form::text('seo[title]', null, ['class' => 'form-control']) !!}
        <small class="pull-right" id="title-count"></small>
    </div>

    <!-- Description Form Input -->
    <div class="form-group clearfix">
        {!! Form::label('seo[description]', 'Description:') !!}
        {!! Form::textarea('seo[description]', null, ['class' => 'form-control', 'rows' => 3]) !!}
        <small class="pull-right" id="description-count"></small>
    </div>
</fieldset>

<fieldset>
    <legend>Facebook OpenGraph</legend>

    <!-- Og_title Form Input -->
    <div class="form-group">
        {!! Form::label('seo[og_title]', 'OpenGraph Title:') !!}
        {!! Form::text('seo[og_title]', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Og_description Form Input -->
    <div class="form-group">
        {!! Form::label('seo[og_description]', 'OpenGraph Description:') !!}
        {!! Form::textarea('seo[og_description]', null, ['class' => 'form-control', 'rows' => 3]) !!}
    </div>
</fieldset>

<fieldset>
    <legend>Twitter Card</legend>

    <!-- Twitter_site Form Input -->
    <div class="form-group">
        {!! Form::label('seo[twitter_site]', 'Twitter Site:') !!}
        <div class="input-group">
            <div class="input-group-addon">@</div>
            {!! Form::text('seo[twitter_site]', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Twitter_creator Form Input -->
    <div class="form-group">
        {!! Form::label('seo[twitter_creator]', 'Twitter Creator:') !!}
        <div class="input-group">
            <div class="input-group-addon">@</div>
            {!! Form::text('seo[twitter_creator]', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Twitter_description Form Input -->
    <div class="form-group">
        {!! Form::label('seo[twitter_description]', 'Twitter Description:') !!}
        {!! Form::textarea('seo[twitter_description]', null, ['class' => 'form-control', 'rows' => 3]) !!}
    </div>
</fieldset>

<fieldset>
    <legend>Google+</legend>

    <!-- Google_plus_publisher Form Input -->
    <div class="form-group">
        {!! Form::label('seo[google_plus_publisher]', 'Google+ Publisher:') !!}
        {!! Form::text('seo[google_plus_publisher]', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Google_plus_author Form Input -->
    <div class="form-group">
        {!! Form::label('seo[google_plus_author]', 'Google+ Author:') !!}
        {!! Form::text('seo[google_plus_author]', null, ['class' => 'form-control']) !!}
    </div>
</fieldset>

<fieldset>
    <legend>On-page Javascript</legend>

    <!-- Javascript_head Form Input -->
    <div class="form-group">
        {!! Form::label('seo[javascript_head]', 'Before closing &lt;head&gt; tag:') !!}
        {!! Form::hidden('seo[javascript_head]', null) !!}
        <div class="ace-editor" id="ace_editor_head">{{ $model->seo->javascript_head }}</div>
    </div>

    <!-- Javascript_body_top Form Input -->
    <div class="form-group">
        {!! Form::label('seo[javascript_body_top]', 'After the opening &lt;body&gt; tag:') !!}
        {!! Form::hidden('seo[javascript_body_top]', null) !!}
        <div class="ace-editor" id="ace_editor_body_top">{{ $model->seo->javascript_body_top }}</div>
    </div>

    <!-- Javascript_body_bottom Form Input -->
    <div class="form-group">
        {!! Form::label('seo[javascript_body_bottom]', 'Before the closing &lt;body&gt; tag:') !!}
        {!! Form::hidden('seo[javascript_body_bottom]', null) !!}
        <div class="ace-editor" id="ace_editor_body_bottom">{{ $model->seo->javascript_body_bottom }}</div>
    </div>
</fieldset>

@if( ! isset($include_hide_from_sitemap) || $include_hide_from_sitemap)
<div class="alert alert-danger">
    <div class="checkbox">
        <label>
            {!! Form::checkbox('seo[hide_on_sitemap]', 1, null) !!} Hide from <code>sitemap.xml</code>
        </label>
    </div>
</div>
@endif

@section('scripts')
    @parent
    <script src="{!! core_asset('js/ace/src-min-noconflict/ace.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            editor_head = ace.edit('ace_editor_head');
            editor_head.session.setMode("ace/mode/html");
            editor_head.setShowPrintMargin(false);

            editor_body_top = ace.edit('ace_editor_body_top');
            editor_body_top.session.setMode("ace/mode/html");
            editor_body_top.setShowPrintMargin(false);

            editor_body_bottom = ace.edit('ace_editor_body_bottom');
            editor_body_bottom.session.setMode("ace/mode/html");
            editor_body_bottom.setShowPrintMargin(false);

            $('form').on('submit', function(e) {
                e.preventDefault();

                var theForm = this;

                $('[name="seo[javascript_head]"]').val( editor_head.getSession().getValue() );
                $('[name="seo[javascript_body_top]"]').val( editor_body_top.getSession().getValue() );
                $('[name="seo[javascript_body_bottom]"]').val( editor_body_bottom.getSession().getValue() );

                theForm.submit();
            });

            updateTitleCount();
            updateDescriptionCount();

            $('input[name="seo[title]"]').keyup(function() {
                updateTitleCount();
            });
            $('textarea[name="seo[description]"]').keyup(function() {
                updateDescriptionCount();
            });

            function updateTitleCount()
            {
                var text_max = 55;
                var text_length = $('input[name="seo[title]"]').val().length;
                var text_remaining = text_max - text_length;

                if(text_remaining <= 0)
                {
                    $('#title-count').addClass('text-danger');
                } else
                {
                    $('#title-count').removeClass('text-danger');
                }

                $('#title-count').html(text_remaining + ' characters remaining');
            }

            function updateDescriptionCount()
            {
                var text_max = 155;
                var text_length = $('textarea[name="seo[description]"]').val().length;
                var text_remaining = text_max - text_length;

                if(text_remaining <= 0)
                {
                    $('#description-count').addClass('text-danger');
                } else
                {
                    $('#description-count').removeClass('text-danger');
                }

                $('#description-count').html(text_remaining + ' characters remaining');
            }
        });
    </script>

@endsection