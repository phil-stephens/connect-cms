@include('admin.layouts.partials.errors')

<div class="row">
    <div class="col-md-4">

        <div class="panel-group" id="source-accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="pages-heading">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#source-accordion" href="#collapse-pages" aria-expanded="true" aria-controls="collapseOne">
                            Master Features List
                        </a>
                    </h4>
                </div>

                <div id="collapse-pages" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="pages-heading">

                    {!! Form::open(['class' => 'source-form']) !!}
                    <div class="panel-body">

                        @foreach($amenities as $amenity)
                            <div class="checkbox"
                                 data-id="{{ $amenity->id }}"
                                 data-name="{{ $amenity->name }}">
                                <label>
                                    <input type="checkbox" name="amenity[{{ $amenity->id }}][add]" value="1" />
                                    {{ $amenity->name }}
                                </label>
                            </div>
                        @endforeach

                    </div>

                    <div class="panel-footer">
                        {!! Form::submit($addButtonLabel, ['class' => 'btn btn-primary btn-sm']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>

    {!! Form::open() !!}
    <div class="col-md-8">

        <div class="panel-group" id="target-list" role="tablist" aria-multiselectable="true">

            @foreach($modelAmenities as $amenity)
                <div class="panel panel-default" id="amenity-{{ $amenity->id }}">
                    <div class="panel-heading draggable" role="tab" id="amenity-{{ $amenity->id }}-heading">
                        @if($amenity->property_id != 0 and configuration('system-wide-features', false))
                            <small class="pull-right text-muted">DEPRECATED</small>
                        @endif

                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse-amenity-{{ $amenity->id }}" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fa fa-angle-down text-muted"></i> {{ $amenity->name }}

                            </a>
                        </h4>
                    </div>

                    <div id="collapse-amenity-{{ $amenity->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="amenity-{{ $amenity->id }}-heading">
                        <div class="panel-body">

                            {!! Form::hidden('amenity[' . $amenity->id . '][id]', $amenity->id, ['class' => 'amenity-id']) !!}
                            {!! Form::hidden('amenity[' . $amenity->id . '][order]', $amenity->pivot->order, ['class' => 'sort-order']) !!}

                                    <!-- Label Form Input -->
                            <div class="form-group">
                                {!! Form::label('amenity[' . $amenity->id . '][description]', 'Description (optional):') !!}
                                {!! Form::textarea('amenity[' . $amenity->id . '][description]', $amenity->pivot->description, ['class' => 'form-control', 'rows' => 2]) !!}
                            </div>
                        </div>

                        <div class="panel-footer clearfix">
                            <a href="#" class="btn btn-sm btn-default pull-right" role="remove-item" data-target="{{ $amenity->id }}">Remove</a>
                        </div>
                    </div>
                </div>
            @endforeach


        </div>

        <!-- Submit field -->
        <div class="form-group">
            {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
        </div>

    </div>

    {!! Form::close() !!}
</div>



@section('scripts')
    @parent

    <script src="/core/js/handlebars.js"></script>
    <script src="/core/js/jquery.sortable.min.js"></script>

    <script>
        $('.source-form').on('submit', function(e) {

            e.preventDefault();

            var theForm = this;

            $('.checkbox', theForm).each(function()
            {
                if($('[type="checkbox"]', this).is(':checked'))
                {
                    var source   = $("#amenity-item-template").html();
                    var template = Handlebars.compile(source);
                    var html    = template({
                        hash: Math.random().toString(36).slice(2),
                        name: $(this).data('name'),
                        id: $(this).data('id'),
                    });

                    $('#target-list').append(html);

                    $(this).remove();
                }
            });

            initSorting();
            doSorting();
        });

        function initSorting()
        {
            $('#target-list').sortable({
                group: 'amenity-items',
                itemSelector: 'div.panel',
                containerSelector: 'div.panel-group',
                placeholder: '<div class="panel-placeholder"></div>',
                onDrop: function ($item, container, _super) {
                    $item.removeClass(container.group.options.draggedClass).removeAttr('style');
                    $('body').removeClass(container.group.options.bodyClass);

                    doSorting();

                    _super($item, container);
                }
            });
        }

        initSorting();

        function doSorting()
        {
            var i = 0;
            $('#target-list .panel').each(function() {
                $('.sort-order', this).val(i);

                i++;
            });
        }

        $(document).on('click', '[role="remove-item"]', function(e) {
            e.preventDefault();

            var target = $('#amenity-' + $(this).data('target'));

            var amenityId = $('.amenity-id', target).val();

            if(amenityId != undefined)
            {
                console.log(amenityId);
                // Do something to remove it here
                $.ajax({
                    type: "POST",
                    url: '{{ $destroyPath }}',
                    data: {_method: 'DELETE', _token: $('meta[name="csrf_token"]').attr('content'), id: amenityId},
                    success: function(data)
                    {
                        console.log(data);
                    }
                });
            }


            $(target).remove();
        });
    </script>

    @include('admin.layouts.partials.handlebars.amenity-item')
@endsection