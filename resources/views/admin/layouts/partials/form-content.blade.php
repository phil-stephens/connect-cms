{{--<fieldset>--}}
    {{--<legend>Page Content</legend>--}}
    <!-- Title Form Input -->
    <div class="form-group">
        {!! Form::label('content[title]', 'Page Title:') !!}
        {!! Form::text('content[title]', null, ['class' => 'form-control']) !!}
        @if( isset($annotation))
            @if( ! $annotation->title)
                <span class="help-block"><i class="fa fa-exclamation-triangle"></i> The Page Title for this page is not used</span>
            @elseif( ! empty($annotation->title_note))
                <span class="help-block"><i class="fa fa-info-circle"></i> {{ $annotation->title_note }}</span>
            @endif
        @endif
    </div>

    <!-- Excerpt Form Input -->
    <div class="form-group">
        {!! Form::label('content[excerpt]', 'Excerpt:') !!}
        {!! Form::textarea('content[excerpt]', null, ['class' => 'form-control', 'rows' => 2]) !!}
        @if( isset($annotation))
            @if( ! empty($annotation->excerpt_character_limit))
            <small class="pull-right" id="excerpt-count"></small>
            @endif

            @if( ! $annotation->excerpt)
                <span class="help-block"><i class="fa fa-exclamation-triangle"></i> The Excerpt for this page is not used</span>
            @elseif( ! empty($annotation->excerpt_note))
                <span class="help-block"><i class="fa fa-info-circle"></i> {{ $annotation->excerpt_note }}</span>
            @endif

        @endif
    </div>


    <?php //$editorId = str_random(); ?>

    <!-- Body Form Input -->
    <div class="form-group">
        {!! Form::label('content[body]', 'Body:') !!}
        {!! Form::textarea('content[body]', null, ['class' => 'form-control', 'rows' => 10, 'role' => 'editor' ]) !!}
        @if( isset($annotation))
            @if( ! $annotation->body)
                <span class="help-block"><i class="fa fa-exclamation-triangle"></i> The Body for this page is not used</span>
            @elseif( ! empty($annotation->body_note))
                <span class="help-block"><i class="fa fa-info-circle"></i> {{ $annotation->body_note }}</span>
            @endif
        @endif
    </div>

@if(isset($model))

    @if(env('HAS_ATOMS', false) || Auth::user()->developer)
    <div class="form-group">
        <a href="{!! route('atom_edit_path', $model->content->uuid) !!}" class="btn btn-default btn-lg"><i class="fa fa-indent"></i> Block Editor</a>
    </div>
    @endif


    {{--<p><a data-toggle="modal" data-target="#media-browser" data-target-editor="{{ $editorId }}" class="btn btn-default btn-sm">Insert image</a></p>--}}



    <label>Featured Image:</label>

    @include('admin.layouts.partials.image-upload', ['field' => 'content[featured_image]', 'model' => $model, 'showRemoveButton' => true])



    @if( isset($annotation))
        @if( ! $annotation->image)
            <span class="help-block"><i class="fa fa-exclamation-triangle"></i> The Featured Image for this page is not used</span>
        @elseif( ! empty($annotation->image_note))
            <span class="help-block"><i class="fa fa-info-circle"></i> {{ $annotation->image_note }}</span>
        @endif
    @endif
{{--</fieldset>--}}



    @foreach($settings as $setting)
        @include('admin.layouts.partials.setting-field')
    @endforeach


    @if(env('HAS_CUSTOM', false))
    @unless(isset($include_custom) && ! $include_custom)
<fieldset>
    <legend>Custom Fields</legend>

    <div class="panel-group" id="custom-fields" role="tablist" aria-multiselectable="true">
        @if(isset($model))
        @foreach($model->content->customFields as $customField)
        <div class="panel panel-default" id="field_{{ $field_hash = str_random() }}">
            <div class="panel-heading draggable" role="tab" id="field_{{ $field_hash }}-heading">
                <small class="pull-right text-muted">{{ $customField->type }}</small>

                <h4 class="panel-title">
                    <a class="sort-handle"><i class="fa fa-bars"></i></a>

                    <a role="button" data-toggle="collapse" href="#collapse-field_{{ $field_hash }}" aria-expanded="true" aria-controls="collapseOne">
                        {{ $customField->key }}&nbsp;
                    </a>
                </h4>
            </div>

            <div id="collapse-field_{{ $field_hash }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="field_{{ $field_hash }}-heading">
                <div class="panel-body">

                    {!! Form::hidden('content[custom][' . $field_hash . '][id]', $customField->id) !!}
                    {!! Form::hidden('content[custom][' . $field_hash . '][order]', $customField->order, ['class' => 'sort-order']) !!}

                    <div class="form-group">
                        {!! Form::label('content[custom][' . $field_hash . '][key]', 'Field Key:') !!}
                        {!! Form::text('content[custom][' . $field_hash . '][key]', $customField->key, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('content[custom][' . $field_hash . '][type]', 'Field Type:') !!}
                        {!! Form::select('content[custom][' . $field_hash . '][type]', ['html' => 'Text (HTML)', 'plain' => 'Text (plain)', 'image' => 'Image', 'file' => 'File'], $customField->type, ['class' => 'form-control', 'data-id' => $field_hash, 'onchange' => 'customFieldSwitcher(this)']) !!}
                    </div>

                    <div class="form-group" id="html-{{ $field_hash }}"@if($customField->type != 'html') style="display: none;"@endif>
                        {!! Form::label('content[custom][' . $field_hash . '][value_html]', 'Value:') !!}
                        {!! Form::textarea('content[custom][' . $field_hash . '][value_html]', ($customField->type == 'html') ? $customField->value : null, ['class' => 'form-control', 'rows' => 5, 'role' => 'editor']) !!}
                    </div>

                    <div class="form-group" id="plain-{{ $field_hash }}"@if($customField->type != 'plain') style="display: none;"@endif>
                        {!! Form::label('content[custom][' . $field_hash . '][value_plain]', 'Value:') !!}
                        {!! Form::text('content[custom][' . $field_hash . '][value_plain]',  ($customField->type == 'plain') ? $customField->value : null, ['class' => 'form-control', 'rows' => 3]) !!}
                    </div>


                    <div id="image-{{ $field_hash }}" @if($customField->type != 'image') style="display: none;"@endif >
                        @include('admin.layouts.partials.image-upload', ['field' => 'content[custom][' . $field_hash . '][value_image]', 'model' => $customField->image])
                    </div>


                    <div id="file-{{ $field_hash }}" class="file-uploader placeholder"@if($customField->type != 'file') style="display: none;"@endif role="file-dropzone"
                         data-hash="cf-{{ $field_hash }}"
                         data-target-id="{{ $customField->id }}">

                        <div class="file-info">
                            @if($file = $customField->file)
                                <i class="fa {{ mimeIconClass($file->mime_type) }} fa-3x"></i> {{ $file->original_name }} <small>{{ byte_format($file->size) }}</small>
                            @endif
                        </div>

                        <div class="dropzone image-clickable">
                            <div class="dropzone-previews"></div>
                        </div>


                    </div>

                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('content[custom][' . $field_hash . '][remove]', 1) !!}
                            Remove Custom Field
                        </label>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @endif
    </div>


    <a href="#" class="btn btn-default custom-field-btn pull-right">Add New Custom Field</a>

    @endif
</fieldset>
@endunless
    @endif


@section('modals')
    @parent
    @if(empty($_media_modal))
        @include('admin.layouts.partials.media-modal')
    @endif
@endsection

@section('scripts')
    @parent

    <script src="/core/js/jscolor/jscolor.js"></script>

@endsection

@if( isset($annotation) && ! empty($annotation->excerpt_character_limit))
@section('scripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            updateExcerptCount();

            $('textarea[name="content[excerpt]"]').keyup(function() {
                updateExcerptCount();
            });

            function updateExcerptCount()
            {
                var text_max = {{ $annotation->excerpt_character_limit }};
                var text_length = $('textarea[name="content[excerpt]"]').val().length;
                var text_remaining = text_max - text_length;

                if(text_remaining <= 0)
                {
                    $('#excerpt-count').addClass('text-danger');
                } else
                {
                    $('#excerpt-count').removeClass('text-danger');
                }

                $('#excerpt-count').html(text_remaining + ' characters remaining');
            }
        });
    </script>
@endsection
@endif

@if(env('EDITOR_FORMAT', 'md') === 'html')
@section('scripts')
    @parent

    {{--<script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>--}}
    <script src="/core/js/tinymce/tinymce.min.js"></script>
    <script>
        function tinymceInit()
        {
            tinymce.init({
                selector: 'textarea[role="editor"]',
                menubar : false,
                content_css : '/core/css/editor.css',
                statusbar : false,
                plugins: "link code paste",
                toolbar: "bold italic styleselect | bullist numlist | link code | cms_image",
                valid_elements : '+*[*]',
                convert_urls: false,
                style_formats: [
                    {title: "Header 1", format: "h1"},
                    {title: "Header 2", format: "h2"},
                    {title: "Header 3", format: "h3"},
                    {title: "Header 4", format: "h4"},
                    {title: "Header 5", format: "h5"},
                    {title: "Header 6", format: "h6"},
                    {title: "Blockquote", format: "blockquote"}
                    ],
                setup : function(ed) {
                    // Add a custom button
                    ed.addButton('cms_image', {
                        title : 'Insert Image',
                        image: '{{ core_asset('img/tinymce-photo-icon.png') }}',
                        onclick : function() {
                            $('#media-browser').modal('show');
                            targetEditor = ed;
                        }
                    });
                }
            });
        }

        tinymceInit();
    </script>
@endsection
@endif

@if(isset($model))
@section('scripts')
    @parent
    <script src="/core/js/handlebars.js"></script>
    <script src="/core/js/jquery.sortable.min.js"></script>

    <script>

        function imageDropzoneInit() {

            $('[role="image-dropzone"]').each(function()
            {
                if(this.dropzone == undefined)
                {
                    var hash = $(this).data('hash');
                    var target = $(this).data('target');
                    var targetId = $(this).data('target-id');
                    var model = $(this).data('model');
                    var clickable = $('.image-clickable', this);
                    var previews = $('.dropzone-previews', this);
                    var toolbar = $('.toolbar', this);

                    if( ! $('.content-image', this).length)
                    {
                        $(this).append('<img class="img-responsive content-image" />');
                    }

                    var image = $('.content-image', this);

                    $(this).dropzone( {
                        url: '{{ route('content_image_path') }}/' + hash,
                        addRemoveLinks: true,

                        acceptedFiles: 'image/*',
                        maxFiles: 1,
                        maxFilesize: 32,
                        parallelUploads: 1,
                        previewsContainer: previews[0],
                        clickable: clickable[0],
                        init: function() {
                            this.on('addedfile', function(file) {
                                image.css('opacity', 0.3);
                            });

                            this.on('sending', function(file, xhr, formData) {
                                formData.append('_token', $('meta[name="csrf_token"]').attr('content'));
                                formData.append('url_params', '{!! serialize(['w' => 400, 'h' => 300, 'fit' => 'crop']) !!}');
                                if(target != undefined) formData.append('target', target);
                                if(model != undefined) formData.append('model', model);
                                if(model != targetId) formData.append('id', targetId);
                            });

                            this.on("success", function(file, responseText) {
                                console.log(responseText);

                                image.attr('src', responseText.path).css('opacity', 1);

                                image.attr('id', 'image-preview-' + responseText.id);

                                $('button', toolbar).each(function() {
                                    $(this).data('id', responseText.id);
                                })

                                toolbar.fadeIn();
                                this.removeFile(file);
                            });
                        }
                    });
                }
            })
        }

        {{--function fileDropzoneInit() {--}}

            {{--$('[role="file-dropzone"]').each(function()--}}
            {{--{--}}
                {{--if(this.dropzone == undefined)--}}
                {{--{--}}
                    {{--var hash = $(this).data('hash');--}}
                    {{--var targetId = $(this).data('target-id');--}}
                    {{--var clickable = $('.image-clickable', this);--}}
                    {{--var previews = $('.dropzone-previews', this);--}}
                    {{--var fileInfo = $('.file-info', this);--}}

                    {{--$(this).dropzone( {--}}
                        {{--url: '{{ route('content_file_path') }}/' + hash,--}}
                        {{--addRemoveLinks: true,--}}
                        {{--maxFiles: 1,--}}
                        {{--maxFilesize: 32,--}}
                        {{--parallelUploads: 1,--}}
                        {{--previewsContainer: previews[0],--}}
                        {{--clickable: clickable[0],--}}
                        {{--init: function() {--}}

                            {{--this.on('sending', function(file, xhr, formData) {--}}
                                {{--formData.append('_token', $('meta[name="csrf_token"]').attr('content'));--}}
                                {{--formData.append('id', targetId);--}}
                            {{--});--}}

                            {{--this.on("success", function(file, responseText) {--}}
                                {{--console.log(responseText);--}}

                                {{--fileInfo.html('<i class="fa ' + responseText.mime_class + ' fa-3x"></i> ' + responseText.name + ' <small>' + responseText.size + '</small>');--}}

                                {{--this.removeFile(file);--}}
                            {{--});--}}
                        {{--}--}}
                    {{--});--}}
                {{--}--}}
            {{--})--}}
        {{--}--}}

        imageDropzoneInit();
//        fileDropzoneInit();

        function customFieldSwitcher(elem)
        {
            var id = $(elem).data('id');

            var val = $(elem).val();

            $('#html-' + id).hide();
            $('#plain-' + id).hide();
            $('#image-' + id).hide();
            $('#file-' + id).hide();

            $('#' + val + '-' + id).show();
        }

        $('.custom-field-btn').on('click', function(e)
        {
            e.preventDefault();

            var fieldHash = Math.random().toString(36).slice(2);

            var source   = $("#custom-field-template").html();
            var template = Handlebars.compile(source);
            var html    = template({hash: fieldHash });

            $('#custom-fields').append(html);

            @if(env('EDITOR_FORMAT', 'md') === 'html')
            tinymceInit();
            @endif

            imageDropzoneInit();
            fileDropzoneInit();
            initSorting();
            doSorting();
        });

        function initSorting()
        {
            $('#custom-fields').sortable({
                group: 'custom-fields',
                itemSelector: 'div.panel',
                handle: 'a.sort-handle',
                containerSelector: 'div.panel-group',
                placeholder: '<div class="panel-placeholder"></div>',
                onDrop: function ($item, container, _super) {
                    $item.removeClass(container.group.options.draggedClass).removeAttr('style');
                    $('body').removeClass(container.group.options.bodyClass);

                    doSorting();

                    _super($item, container);
                }
            });
        }

        initSorting();

        function doSorting()
        {
            var i = 0;
            $('#custom-fields .panel').each(function() {
                $('.sort-order', this).val(i);

                i++;
            });
        }

        {{--// Image/file toolbar buttons--}}
        {{--$('[role="download"]').on('click', function() {--}}
            {{--if($(this).data('id') != undefined)--}}
            {{--{--}}
                {{--document.location.href = '/.admin/download/' + $(this).data('type') + '/' + $(this).data('id');--}}
            {{--}--}}
        {{--});--}}

        {{--$('[role="trash"]').on('click', function() {--}}
            {{--// Remove image from the content--}}
            {{--var toolbar = $(this).parent('.toolbar');--}}
            {{--var preview = $('#' + $(this).data('type') + '-preview-' + $(this).data('id'));--}}

            {{--// Do it using AJAX--}}
            {{--$.ajax({--}}
                {{--type: "POST",--}}
                {{--url: '{{ route('destroy_upload_path') }}',--}}
                {{--data: {_method: 'DELETE',--}}
                    {{--_token: $('meta[name="csrf_token"]').attr('content'),--}}
                    {{--id: $(this).data('id'),--}}
                    {{--type: $(this).data('type'),--}}
                    {{--target: $(this).data('target'),--}}
                    {{--target_id: $(this).data('target-id'),--}}
                    {{--model: $(this).data('model'),--}}
                    {{--model_id: $(this).data('model-id')},--}}
                {{--success: function(data)--}}
                {{--{--}}
                    {{--console.log(data);--}}

                    {{--preview.attr('src', '');--}}

                    {{--toolbar.fadeOut();--}}
                {{--}--}}
            {{--});--}}

        {{--});--}}
    </script>

    @include('admin.layouts.partials.handlebars.custom-field')
@endsection


@endif