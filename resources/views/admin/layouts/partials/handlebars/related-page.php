<script id="related-page-template" type="text/x-handlebars-template">
    <div class="panel panel-default" id="page-{{ hash }}">
        <div class="panel-heading draggable" role="tab" id="page-{{ hash }}-heading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#target-accordion" href="#collapse-page-{{ hash }}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="fa fa-angle-down text-muted"></i> {{{ name }}}
                </a>
            </h4>
        </div>

        <div id="collapse-page-{{ hash }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="page-{{ hash }}-heading">
            <div class="panel-body">
                <input type="hidden" name="page[{{ hash }}][order]" class="sort-order" />
                <input type="hidden" name="page[{{ hash }}][id]" value="{{ id }}" />

                <div class="form-group">
                    <label for="page[{{ hash }}][key]">Key:</label>
                    <input type="text" class="form-control" id="page[{{ hash }}][key]" name="page[{{ hash }}][key]">
                </div>
            </div>

            <div class="panel-footer clearfix">
                <a href="#" class="btn btn-sm btn-default pull-right" role="remove-page" data-target="{{ hash }}">Remove</a>
            </div>
        </div>
    </div>
</script>