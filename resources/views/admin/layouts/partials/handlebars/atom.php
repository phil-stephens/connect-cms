<script id="atom-<?= $template->key ?>-template" type="text/x-handlebars-template">
    <div class="panel panel-default" id="field_{{ hash }}">
        <div class="panel-heading draggable" role="tab" id="field_{{ hash }}-heading">
            <h4 class="panel-title">
                <a class="sort-handle"><i class="fa fa-bars"></i></a>
                <a role="button" data-toggle="collapse" href="#collapse-field_{{ hash }}" aria-expanded="true" aria-controls="collapseOne">
                    <?= $template->name ?>
                </a>
            </h4>
        </div>

        <div id="collapse-field_{{ hash }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="field_{{ hash }}-heading">
            <div class="panel-body">
                <?php foreach($template->fields as $field) { ?>
                    <input type="hidden" name="atom[{{ hash }}][field][<?= $field->key ?>][type]" value="<?= $field->type ?>" class="form-control">
                    <input type="hidden" name="atom[{{ hash }}][field][<?= $field->key ?>][id]" value="{{ fields.<?= $field->key ?>.id }}" class="form-control">

                    <div class="form-group">
                    <label for=""><?= $field->label ?>:</label>


                    <?php if($field->type == 'plain') { ?>
                        <?php if(isset($field->pre) || isset($field->post)) { ?>
                        <div class="input-group">
                        <?php } ?>

                            <?php if(isset($field->pre)) { ?>
                            <span class="input-group-addon"><?= $field->pre ?></span>
                            <?php } ?>


                            <input type="text" name="atom[{{ hash }}][field][<?= $field->key ?>][value]" value="{{ fields.<?= $field->key ?>.value }}" class="form-control">

                            <?php if(isset($field->post)) { ?>
                            <span class="input-group-addon"><?= $field->post ?></span>
                            <?php } ?>

                        <?php if(isset($field->pre) || isset($field->post)) { ?>
                        </div>
                        <?php } ?>

                    <?php } elseif($field->type == 'html') { ?>

                        <textarea name="atom[{{ hash }}][field][<?= $field->key ?>][value]" role="editor" rows="5" class="form-control">
                            {{{ fields.<?= $field->key ?>.value }}}
                        </textarea>

                    <?php } elseif($field->type == 'image') { ?>

                    <div class="image-preview" data-toggle="modal" data-target="#media-browser"
                         data-target-field="atom[{{ hash }}][field][<?= $field->key ?>][uuid]">
                        {{{ fields.<?= $field->key ?>.img }}}
                        <i class="fa fa-cloud-upload fa-fw upload-icon"></i>
                        <input name="atom[{{ hash }}][field][<?= $field->key ?>][uuid]" type="hidden" value="{{ fields.<?= $field->key ?>.uuid }}">
                    </div>

                    <?php } elseif($field->type == 'colour') { ?>

                        <input type="text" name="atom[{{ hash }}][field][<?= $field->key ?>][value]" class="form-control color {hash:true}" value="{{ fields.<?= $field->key ?>.value }}">

                    <?php } ?>

                    <?php if( ! empty($field->description)) { ?>
                    <span class="help-block"><i class="fa fa-info-circle"></i> <?= $field->description ?></span>
                    <?php } ?>
                </div>
                <?php } ?>

                <div class="checkbox">
                    <label>
                        <input name="atom[{{ hash }}][remove]" type="checkbox" value="1">
                        Remove Block
                    </label>

                    <input type="hidden" name="atom[{{ hash }}][order]" class="sort-order" value="{{ order }}">
                    <input type="hidden" name="atom[{{ hash }}][id]" value="{{ id }}">
                    <input type="hidden" name="atom[{{ hash }}][key]" value="<?= $template->key ?>">
                    <input type="hidden" name="atom[{{ hash }}][theme]" value="<?= $template->theme ?>">
                    <input type="hidden" name="atom[{{ hash }}][template]" value="<?= $template->template ?>">
                </div>
            </div>
        </div>
    </div>
</script>