<script id="slideshow-slide-template" type="text/x-handlebars-template">
<tr id="slide_{{ hash }}">
    <td class="sort-handle"><i class="fa fa-fw fa-bars"></i></td>
    <td>
        <!--<img src="{{ path }}" />-->
        <div class="image-preview" data-toggle="modal" data-target="#media-browser" data-target-field="slide[{{ hash }}][image]">
            <i class="fa fa-cloud-upload fa-fw upload-icon"></i>
            <input name="slide[{{ hash }}][image]" type="hidden">
        </div>
    </td>

    <td>
        <input class="form-control" placeholder="Headline" name="slide[{{ hash }}][headline]" type="text">

        <textarea class="form-control" placeholder="Excerpt" rows="3" name="slide[{{ hash }}][excerpt]" cols="50"></textarea>

        <input class="form-control" placeholder="Link" name="slide[{{ hash }}][link]" type="text">

        <input class="sort-order" name="slide[{{ hash }}][order]" type="hidden" value="0">

        <label><input name="slide[{{ hash }}][destroy]" type="checkbox" value="1"> Delete image</label>
    </td>

    <td>
        <label for="">Crop From:</label>
        <table class="table table-striped">
            <tbody>
            <tr>
                <td class="text-center"><input name="slide[{{ hash }}][crop_from]" type="radio" value="top,left"></td>
                <td class="text-center"><input name="slide[{{ hash }}][crop_from]" type="radio" value="top"></td>
                <td class="text-center"><input name="slide[{{ hash }}][crop_from]" type="radio" value="top,right"></td>
            </tr>

            <tr>
                <td class="text-center"><input name="slide[{{ hash }}][crop_from]" type="radio" value="left"></td>
                <td class="text-center"><input checked="checked" name="slide[{{ hash }}][crop_from]" type="radio" value=""></td>
                <td class="text-center"><input name="slide[{{ hash }}][crop_from]" type="radio" value="right"></td>
            </tr>

            <tr>
                <td class="text-center"><input name="slide[{{ hash }}][crop_from]" type="radio" value="bottom,left"></td>
                <td class="text-center"><input name="slide[{{ hash }}][crop_from]" type="radio" value="bottom"></td>
                <td class="text-center"><input name="slide[{{ hash }}][crop_from]" type="radio" value="bottom,right"></td>
            </tr>
            </tbody>
        </table>

        <label class="text-muted">
            <input name="slide[{{ hash }}][face_detection]" type="checkbox" value="1">
            Face detection
        </label>

    </td>
</tr>
</script>