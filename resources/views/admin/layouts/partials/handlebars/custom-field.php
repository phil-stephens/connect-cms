<script id="custom-field-template" type="text/x-handlebars-template">
<div class="panel panel-default" id="field_{{ hash }}">
    <div class="panel-heading draggable" role="tab" id="field_{{ hash }}-heading">

        <h4 class="panel-title">
            <a class="sort-handle"><i class="fa fa-bars"></i></a>
            <a role="button" data-toggle="collapse" href="#collapse-field_{{ hash }}" aria-expanded="true" aria-controls="collapseOne">
                New Custom Field
            </a>
        </h4>
    </div>

    <div id="collapse-field_{{ hash }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="field_{{ hash }}-heading">
        <div class="panel-body">

            <input type="hidden" name="content[custom][{{ hash }}][order]" class="sort-order">

            <div class="form-group">
                <label for="content[custom][{{ hash }}][key]">Field Key:</label>
                <input class="form-control" name="content[custom][{{ hash }}][key]" type="text" id="content[custom][{{ hash }}][key]">
            </div>

            <div class="form-group">
                <label for="content[custom][{{ hash }}][type]">Field Type:</label>
                <select class="form-control" data-id="{{ hash }}" onchange="customFieldSwitcher(this)" id="content[custom][{{ hash }}][type]" name="content[custom][{{ hash }}][type]"><option value="html">Text (HTML)</option><option value="plain">Text (plain)</option><option value="image">Image</option>
                    <!--<option value="file">File</option>-->
                </select>
            </div>

            <div class="form-group" id="html-{{ hash }}">
                <label for="content[custom][{{ hash }}][value_html]">Value:</label>
                <textarea class="form-control" rows="5" role="editor" name="content[custom][{{ hash }}][value_html]" cols="50" id="content[custom][{{ hash }}][value_html]"></textarea>
            </div>

            <div class="form-group" id="plain-{{ hash }}" style="display: none;">
                <label for="content[custom][{{ hash }}][value_plain]">Value:</label>
                <input class="form-control" name="content[custom][{{ hash }}][value_plain]" type="text" id="content[custom][{{ hash }}][value_plain]">
            </div>

            <div id="image-{{ hash }}" style="display: none;">
                <div class="image-preview" data-toggle="modal" data-target="#media-browser" data-target-field="content[custom][{{ hash }}][value_image]">
                    <i class="fa fa-cloud-upload fa-fw upload-icon"></i>
                    <input name="content[custom][{{ hash }}][value_image]" type="hidden">
                </div>
            </div>

            <div id="file-{{ hash }}" class="file-uploader placeholder" style="display: none;" role="file-dropzone"
                 data-hash="cf-{{ hash }}">
                <div class="file-info"></div>
                <div class="dropzone image-clickable">
                    <div class="dropzone-previews"></div>
                </div>
            </div>

            <div class="checkbox">
                <label>
                    <input name="content[custom][{{ hash }}][remove]" type="checkbox" value="1">
                    Remove Custom Field
                </label>
            </div>
        </div>
    </div>
</div>
</script>