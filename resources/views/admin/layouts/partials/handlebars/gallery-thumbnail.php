<script id="gallery-thumbnail-template" type="text/x-handlebars-template">
<div class="gallery-item" data-id="{{ image_id }}">
    <div class="thumbnail">
        <img src="{{ path }}" />
    </div>

    <div class="overlay">
        <form method="POST" action="http://cms.lo/.admin/site/{{ site_id }}/gallery/{{ gallery_id }}/image/{{ image_id }}" accept-charset="UTF-8" class="destroy-form">
            <input name="gallery_id" type="hidden" value="{{ gallery_id }}">
            <input name="image_id" type="hidden" value="{{ image_id }}">
            <button type="submit" class="btn btn-default btn-sm">Remove</button>
        </form>
    </div>
</div>
</script>