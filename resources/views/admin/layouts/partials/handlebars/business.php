<script id="business-template" type="text/x-handlebars-template">
    <div class="panel panel-default" id="business-{{ hash }}">
        <div class="panel-heading draggable" role="tab" id="business-{{ hash }}-heading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#target-accordion" href="#collapse-business-{{ hash }}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="fa fa-angle-down text-muted"></i> {{{ name }}}
                </a>
            </h4>
        </div>

        <div id="collapse-business-{{ hash }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="business-{{ hash }}-heading">
            <div class="panel-body">
                <input type="hidden" name="business[{{ hash }}][order]" class="sort-order" />
                <input type="hidden" name="business[{{ hash }}][id]" value="{{ id }}" />

            </div>

            <div class="panel-footer clearfix">
                <a href="#" class="btn btn-sm btn-default pull-right" role="remove-business" data-target="{{ hash }}">Remove</a>
            </div>
        </div>
    </div>
</script>