<script id="related-room-template" type="text/x-handlebars-template">
    <div class="panel panel-default" id="room-{{ hash }}">
        <div class="panel-heading draggable" role="tab" id="room-{{ hash }}-heading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#target-accordion" href="#collapse-room-{{ hash }}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="fa fa-angle-down text-muted"></i> {{{ name }}}
                </a>
            </h4>
        </div>

        <div id="collapse-room-{{ hash }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="room-{{ hash }}-heading">
            <div class="panel-body">
                <input type="hidden" name="room[{{ hash }}][id]" value="{{ id }}" />

                <div class="form-group">
                    <label for="room[{{ hash }}][key]">Key:</label>
                    <input type="text" class="form-control" id="room[{{ hash }}][key]" name="room[{{ hash }}][key]">
                </div>
            </div>

            <div class="panel-footer clearfix">
                <a href="#" class="btn btn-sm btn-default pull-right" role="remove-room" data-target="{{ hash }}">Remove</a>
            </div>
        </div>
    </div>
</script>