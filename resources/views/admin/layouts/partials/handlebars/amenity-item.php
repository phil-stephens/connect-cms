<script id="amenity-item-template" type="text/x-handlebars-template">
    <div class="panel panel-default" id="amenity-{{ hash }}">
        <div class="panel-heading draggable" role="tab" id="amenity-{{ hash }}-heading">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#target-accordion" href="#collapse-amenity-{{ hash }}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="fa fa-angle-down text-muted"></i> {{ name }}
                </a>
            </h4>
        </div>

        <div id="collapse-amenity-{{ hash }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="amenity-{{ hash }}-heading">
            <div class="panel-body">
                <input type="hidden" name="amenity[{{ hash }}][id]" value="{{ id }}" />
                <input type="hidden" name="amenity[{{ hash }}][order]" class="sort-order" />

                <div class="form-group">
                    <label for="amenity[{{ hash }}][description]">Description:</label>
                    <textarea class="form-control" id="amenity[{{ hash }}][description]" name="amenity[{{ hash }}][description]" rows="2"></textarea>
                </div>
            </div>

            <div class="panel-footer clearfix">
                <a href="#" class="btn btn-sm btn-default pull-right" role="remove-item" data-target="{{ hash }}">Remove</a>
            </div>
        </div>
    </div>
</script>