<script id="navigation-item-template" type="text/x-handlebars-template">
    <div class="panel panel-default" id="item-{{ hash }}">
        <div class="panel-heading draggable" role="tab" id="item-{{ hash }}-heading">
            <small class="pull-right text-muted">{{ class_basename }}</small>

            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#target-accordion" href="#collapse-item-{{ hash }}" aria-expanded="true" aria-controls="collapseOne">
                    <i class="fa fa-angle-down text-muted"></i> {{{ label }}}
                </a>
            </h4>
        </div>

        <div id="collapse-item-{{ hash }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="item-{{ hash }}-heading">
            <div class="panel-body">
                <input type="hidden" name="item[{{ hash }}][order]" class="sort-order" />
                <input type="hidden" name="item[{{ hash }}][class]" value="{{ class }}" />
                <input type="hidden" name="item[{{ hash }}][class_id]" value="{{ class_id }}" />

                <div class="form-group">
                    <label for="item[{{ hash }}][label]">Label:</label>
                    <input type="text" class="form-control" id="item[{{ hash }}][label]" name="item[{{ hash }}][label]" value="{{ name }}">
                </div>
            </div>

            <div class="panel-footer clearfix">
                <a href="#" class="btn btn-sm btn-default pull-right" role="remove-item" data-target="{{ hash }}">Remove</a>
            </div>
        </div>
    </div>
</script>