<script id="room-statistic-row-template" type="text/x-handlebars-template">
<tr id="stat_{{ hash }}">
    <td class="sort-handle"><i class="fa fa-fw fa-bars"></i></td>

    <td>
        <input type="text" class="form-control" name="stats[{{ hash }}][key]"/>
    </td>

    <td>
        <input type="text" class="form-control" name="stats[{{ hash }}][title]"/>
    </td>

    <td>
        <input type="text" class="form-control" name="stats[{{ hash }}][value]"/>
    </td>

    <td>
        <input type="checkbox" name="stats[{{ hash }}][delete]" value="1"/>
        <input type="hidden" name="stats[{{ hash }}][order]" class="sort-order" />
    </td>
</tr>
</script>