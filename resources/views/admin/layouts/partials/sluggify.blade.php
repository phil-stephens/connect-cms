@section('scripts')
    @parent

    <script src="/core/js/speakingurl.min.js"></script>

    <script>
        $(document).ready(function() {
            $('input[name="name"]').on('blur', function (e) {
                var slug = $('input[name="slug"]');

                if (slug.val() == '') {
                    slug.val(getSlug($(this).val(), {separator: '{!! env('URL_SEPARATOR', '-') !!}'}));
                }
            });
        });
    </script>
@endsection