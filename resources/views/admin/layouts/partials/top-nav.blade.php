<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        @section('nav-header')
            <div class="navbar-header std-navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>


{{--                <a class="navbar-brand site-title" href="{{ route('sites_path') }}">{{ ( ! Auth::user()->parent ) ? 'FastrackConnect' : Auth::user()->parent->name }}</a>--}}
                <a class="navbar-brand site-title" href="{{ route('sites_path') }}"><i class="fa fa-fw fa-home"></i></a>
            </div>
        @show

        <div class="collapse navbar-collapse" id="navbar-collapse">
            @yield('extra-nav-links')

            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>{!! link_to_route('settings_path', 'Settings') !!}</li>

                            <li class="divider"></li>

                            <li>{!! link_to_route('edit_me_path', 'Edit Profile') !!}</li>

                            <li>{!! link_to_route('logout_path', 'Logout') !!}</li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>