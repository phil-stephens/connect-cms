<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="/core/img/favicon.png" />

    <title>FastrackConnect</title>

    @section('head')
        <link rel="stylesheet" href="/core/css/core.css?ck=connect"/>

        <link href='//fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
    @show
</head>
<body>

@yield('content')

@section('scripts')
    <script src="/core/js/jquery.min.js"></script>
    <script src="/core/js/bootstrap.min.js"></script>
@show
</body>
</html>