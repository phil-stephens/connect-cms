<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="/core/img/favicon.png" />

    <title>FastrackConnect</title>

    @section('head')
    <link rel="stylesheet" href="/core/css/core.css?ck=connect"/>

    {!! fontawesome() !!}

    <link href='//fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>
    @show
</head>
<body>
<header class="header">
    @section('main-navigation')
        @include('admin.layouts.partials.top-nav')
    @show
</header>

<div class="container-fluid">
    <div class="row">
        @include('admin.layouts.partials.side-nav')

        <div class="content-pane {{ (isset($_body_class)) ? $_body_class : '' }}">
            @include('flash::message')

            @yield('content')
        </div>
    </div>
</div>

@yield('modals')

@section('scripts')
    <script src="/core/js/jquery.min.js"></script>
    <script src="/core/js/bootstrap.min.js"></script>
    <script src="/core/js/bootbox.js"></script>
    <script src="/core/js/connect.js"></script>
@show
</body>
</html>