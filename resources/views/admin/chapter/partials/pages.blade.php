<thead>
<tr>
    {{--<th></th>--}}
    <th>Page Name</th>
    <th>Page Title</th>
    <th class="button-column"></th>
    <th class="button-column"></th>
</tr>
</thead>
<tbody>
@foreach($pages as $page)
    @if( ! $page->internal)
        <tr data-id="{{ $page->id }}">
            {{--<td class="sort-handle"><i class="fa fa-fw fa-bars"></i></td>--}}
            <td>{!! link_to_route('edit_page_path', $page->name, [$site->id, $page->id]) !!}</td>
            <td>{{ $page->getTitle() }}</td>
            <td>{!! link_to_route('edit_page_path', 'Edit', [$site->id, $page->id], ['class' => 'btn btn-primary btn-sm']) !!}</td>
            <td>
                {!! Form::open(['route' => ['destroy_page_path', $site->id, $page->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                {!! Form::hidden('id', $page->id) !!}
                <button type="submit" class="btn btn-default btn-sm">Remove</button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endif
@endforeach
</tbody>

<tfoot>
    <tr>
        <td class="text-center" colspan="5">{!! $pages->render() !!}</td>
    </tr>
</tfoot>