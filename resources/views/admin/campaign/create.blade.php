@extends('admin.layouts.default')

@section('head')
    @parent
    <link rel="stylesheet" href="/core/css/pickadate/default.css"/>
    <link rel="stylesheet" href="/core/css/pickadate/default.date.css"/>
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Create Campaign</h1>

        <div class="pull-right">
            <a href="{{ route('campaigns_path', $site->uuid) }}" class="btn btn-primary btn-lg">Back to Campaigns</a>
        </div>
    </div>

    {!! Form::open() !!}
            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Start_at Form Input -->
    <div class="form-group">
        {!! Form::label('start_at', 'Start at:') !!}
        {!! Form::text('start_at', \Carbon\Carbon::today()->format('j F Y'), ['class' => 'form-control start_at']) !!}
    </div>

    <!-- Finish_at Form Input -->
    <div class="form-group">
        {!! Form::label('finish_at', 'Finish at:') !!}
        {!! Form::text('finish_at', \Carbon\Carbon::today()->addMonth()->format('j F Y'), ['class' => 'form-control finish_at']) !!}
    </div>
    
    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create Campaign', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    @parent

    <script src="/core/js/pickadate/picker.js"></script>
    <script src="/core/js/pickadate/picker.date.js"></script>

    <script>
        $(document).ready(function() {

            var finish_at = $('.finish_at').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true
            });

            $('.start_at').pickadate({
                format: 'd mmmm yyyy',
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenName: true,
                onSet: function(context) {
                    var finishPicker = finish_at.pickadate('picker');
                    var minDate = context.select;
                    var finishSelected = finishPicker.get('select');

                    if(finishSelected != null && finishSelected.pick < minDate)
                    {
                        finishPicker.set('select', minDate);
                    }

                    finishPicker.set('min', new Date(minDate));
                }
            });
        });
    </script>
@endsection