@extends('admin.layouts.default')


@section('secondary-nav')
<?php
$_body_class = 'padded';
$_nav_collapsed = 'collapsed';
?>
@include('admin.sites.partials.side-nav')
@endsection

@section('content')

@include('admin.campaign.partials.tabs', ['tab' => 'variations'])

<a href="{{ route('variation_create_path', $campaign->uuid) }}" class="btn btn-primary btn-lg">Create Variation</a>

    <table class="table table-striped">
        <thead>
            <th>Name</th>
            <th>Visits</th>
            <th class="btn-column"></th>
        </thead>

        <tbody>
            @foreach($campaign->variations as $variation)
                <tr>
                    <th>
                        {!! link_to_route('variation_edit_path', $variation->name, $variation->uuid) !!}
                    </th>
                    <th>{{ $variation->visits }}</th>
                    <th>
                        {!! link_to_route('variation_edit_path', 'Edit', $variation->uuid, ['class' => 'btn btn-default btn-sm']) !!}
                    </th>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
