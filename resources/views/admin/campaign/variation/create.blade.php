@extends('admin.layouts.default')


@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    @include('admin.campaign.partials.tabs', ['tab' => 'variations'])

    <h2 class="page-header">Create Variation</h2>

    {!! Form::open() !!}
            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Key Form Input -->
    <div class="form-group">
        {!! Form::label('key', 'Key:') !!}
        {!! Form::text('key', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Payload Form Input -->
    <div class="form-group">
        {!! Form::label('payload', 'Payload:') !!}
        {!! Form::textarea('payload', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create Variation', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>
    {!! Form::close() !!}

@endsection
