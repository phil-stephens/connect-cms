<div class="page-header">
    <h1 class="pull-left">Edit Campaign <small>{{ $campaign->name }}</small></h1>

    <a href="{{ route('campaigns_path', $campaign->site->uuid) }}" class="btn btn-primary btn-lg pull-right">Back to Campaigns</a>
</div>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('campaign_edit_path', 'Settings', $campaign->uuid) !!}</li>
    <li role="presentation"@if($tab == 'variations') class="active"@endif>{!! link_to_route('variations_path', 'Variations', $campaign->uuid) !!}</li>
</ul>