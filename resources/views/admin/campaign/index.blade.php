@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Campaigns</h1>

        <div class="pull-right">
            <a href="{{ route('campaign_create_path', $site->uuid) }}" class="btn btn-primary btn-lg">Create Campaign</a>
        </div>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Campaign Name</th>
                <th>Variations</th>
                <th>Start</th>
                <th>Finish</th>
                <th>UUID</th>
                <th class="btn-column"></th>
            </tr>
        </thead>

        <tbody>
            @foreach($campaigns as $campaign)
                <tr>
                    <td>{!! link_to_route('campaign_edit_path', $campaign->name, $campaign->uuid) !!}</td>
                    <td>{!! link_to_route('variations_path', $campaign->variations()->count(), $campaign->uuid) !!}</td>
                    <td>{{ $campaign->start_at->format('j F Y') }}</td>
                    <td>{{ $campaign->finish_at->format('j F Y') }}</td>
                    <td>{{ $campaign->uuid }}</td>
                    <td>{!! link_to_route('campaign_edit_path', 'Edit', $campaign->uuid, ['class' => 'btn btn-primary btn-sm']) !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>


@endsection