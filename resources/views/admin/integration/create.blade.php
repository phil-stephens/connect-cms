@extends('admin.layouts.default')

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Create Integration</h1>
    </div>

    @include('admin.layouts.partials.errors')

    {!! Form::open() !!}

            <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Type Form Input -->
    <div class="form-group">
        {!! Form::label('type', 'Provider:') !!}
        {!! Form::select('type', $providers, null, ['class' => 'form-control', 'onchange' => 'showProviderForm(this);']) !!}
    </div>


    <!-- Provider-specific info -->
    <div id="providers">
    @foreach($providers as $type => $provider)
        <div id="{{ class_basename($type) }}" style="display: none;" role="provider-form">
            @include('admin.integration.partials.' . class_basename($type), ['prefix' => class_basename($type)])
        </div>
        @endforeach
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Create Integration', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
    @parent

    <script>
        function showProviderForm(select)
        {
            var providerClass = $(select).val();
            var provider = providerClass.split(/[\\/]/).pop();

            $('#providers [role="provider-form"]').hide();

            $('#' + provider).show();
        }
    </script>
@endsection