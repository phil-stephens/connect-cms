@extends('admin.layouts.default')

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Integrations</h1>

        {!! link_to_route('integration_create_path', 'Create Integration', [], ['class' => 'btn btn-lg pull-right btn-primary']) !!}
    </div>


    <table class="table table-striped">

        <tbody>
        @foreach($integrations as $integration)
            <tr>
                <td>{{ $integration->name }}</td>

                <td>
                    {{ config('integrations.providers.rates.' . class_basename($integration->type))['name'] }}
                </td>

                <td>
                    {!! link_to_route('integration_edit_path', 'Edit', $integration->uuid, ['class' => 'btn btn-primary btn-sm']) !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection