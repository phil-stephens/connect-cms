
@extends('admin.layouts.default')

@section('head')
    @parent
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    @include('admin.navigations.partials.tabs', ['tab' => 'items'])

    @include('admin.layouts.partials.errors')

    <div class="row">
        <div class="col-md-4">

            <div class="panel-group" id="source-accordion" role="tablist" aria-multiselectable="true">
                @foreach($items as $index => $group)
                    @if($group->count())
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="{{ $index }}-heading">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#source-accordion" href="#collapse-{{ $index }}" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="fa fa-angle-down text-muted"></i> {{ ucfirst($index) }}
                                    </a>
                                </h4>
                            </div>



                            <div id="collapse-{{ $index }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{ $index }}-heading">

                                {!! Form::open(['class' => 'source-form']) !!}
                                <div class="panel-body">

                                    @foreach($group as $item)
                                        @if( empty($item->internal) )
                                        <?php
                                        $name = $label = $item->name;

                                        if(in_array(class_basename($item), ['Room', 'Offer'] )) $label .= ' <small>(' . $item->property->name . ')</small>';
                                        ?>

                                        <div class="checkbox"
                                             data-id="{{ $item->id }}"
                                             data-class="{{ get_class($item) }}"
                                             data-class_basename="{{ class_basename($item) }}"
                                             data-name="{{ $name }}"
                                             data-label="{!! $label !!}">
                                            <label>
                                                <input type="checkbox" name="item[{{ $item->id }}][add]" value="1" />
                                                {!! $label !!}
                                            </label>
                                        </div>
                                        @endif
                                    @endforeach

                                </div>

                                <div class="panel-footer">
                                    {!! Form::submit('Add to Navigation Group', ['class' => 'btn btn-primary btn-sm']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>


                        </div>
                    @endif
                @endforeach
            </div>

        </div>

        {!! Form::open() !!}
        <div class="col-md-8">

            <div class="panel-group" id="target-list" role="tablist" aria-multiselectable="true">

                @foreach($navigation->items as $item)
                <div class="panel panel-default" id="item-{{ $item->id }}">
                    <div class="panel-heading draggable" role="tab" id="item-{{ $item->id }}-heading">
                        <small class="pull-right text-muted">{{ class_basename($item->link) }}</small>

                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse-item-{{ $item->id }}" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fa fa-angle-down text-muted"></i> {{ $item->link->name }}
                                @if(in_array(class_basename($item->link), ['Room', 'Offer']))
                                    <small>({{ $item->link->property->name }})</small>
                                @endif
                            </a>
                        </h4>
                    </div>

                    <div id="collapse-item-{{ $item->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="item-{{ $item->id }}-heading">
                        <div class="panel-body">

                            {!! Form::hidden('item[' . $item->id . '][id]', $item->id, ['class' => 'item-id']) !!}
                            {!! Form::hidden('item[' . $item->id . '][order]', $item->order, ['class' => 'sort-order']) !!}

                            <!-- Label Form Input -->
                            <div class="form-group">
                                {!! Form::label('item[' . $item->id . '][label]', 'Label:') !!}
                                {!! Form::text('item[' . $item->id . '][label]', $item->label, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="panel-footer clearfix">
                            <a href="#" class="btn btn-sm btn-default pull-right" role="remove-item" data-target="{{ $item->id }}">Remove</a>
                        </div>
                    </div>
                </div>
                @endforeach


            </div>

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
            </div>

        </div>

        {!! Form::close() !!}
    </div>


@endsection

@section('scripts')
    @parent

    <script src="/core/js/handlebars.js"></script>
    <script src="/core/js/jquery.sortable.min.js"></script>

    <script>
        $('.source-form').on('submit', function(e) {

            e.preventDefault();

            var theForm = this;

            $('.checkbox', theForm).each(function()
            {
                if($('[type="checkbox"]', this).is(':checked'))
                {
                    var source   = $("#navigation-item-template").html();
                    var template = Handlebars.compile(source);
                    var html    = template({
                        hash: Math.random().toString(36).slice(2),
                        name: $(this).data('name'),
                        label: $(this).data('label'),
                        class_id: $(this).data('id'),
                        class: $(this).data('class'),
                        class_basename: $(this).data('class_basename')
                    });

                    $('#target-list').append(html);

                    $(this).remove();
                }
            });

            initSorting();
            doSorting();
        });

        function initSorting()
        {
            $('#target-list').sortable({
                group: 'navigation-items',
                itemSelector: 'div.panel',
                containerSelector: 'div.panel-group',
                placeholder: '<div class="panel-placeholder"></div>',
                onDrop: function ($item, container, _super) {
                    $item.removeClass(container.group.options.draggedClass).removeAttr('style');
                    $('body').removeClass(container.group.options.bodyClass);

                    doSorting();

                    _super($item, container);
                }
            });
        }

        initSorting();

        function doSorting()
        {
            var i = 0;
            $('#target-list .panel').each(function() {
                $('.sort-order', this).val(i);

                i++;
            });
        }

        $(document).on('click', '[role="remove-item"]', function(e) {
            e.preventDefault();

            var target = $('#item-' + $(this).data('target'));

            var itemId = $('.item-id', target).val();

            if(itemId != undefined)
            {
                // Do something to remove it here
                $.ajax({
                    type: "POST",
                    url: '{{ route('destroy_navigation_item_path', [$site->id, $navigation->id]) }}',
                    data: {_method: 'DELETE', _token: $('meta[name="csrf_token"]').attr('content'), id: itemId},
                    success: function(data)
                    {
                        console.log(data);
                    }
                });
            }


            $(target).remove();
        });
    </script>

    @include('admin.layouts.partials.handlebars.navigation-item')
@endsection