<h1 class="page-header">Edit Navigation Group <small>{{ $navigation->name }}</small></h1>

<ul class="nav nav-tabs">
    <li role="presentation"@if($tab == 'items') class="active"@endif>{!! link_to_route('navigation_items_path', 'Items', [$site->id, $navigation->id]) !!}</li>
    <li role="presentation"@if($tab == 'edit') class="active"@endif>{!! link_to_route('edit_navigation_path', 'Settings', [$site->id, $navigation->id]) !!}</li>
</ul>