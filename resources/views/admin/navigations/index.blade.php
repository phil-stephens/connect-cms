@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    <div class="page-header">
        <h1 class="pull-left">Navigation Groups</h1>

        <a href="{{ route('create_navigation_path', $site->id) }}" class="btn btn-primary btn-lg pull-right">Create Navigation Group</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th class="button-column"></th>
            <th class="button-column"></th>
        </tr>
        </thead>

        <tbody>
        @foreach($navigations as $navigation)
            <tr>
                <td>{{ $navigation->name }}</td>
                <td>{{ $navigation->description }}</td>
                <td>{!! link_to_route('navigation_items_path', 'Edit', [$site->id, $navigation->id], ['class' => 'btn btn-primary btn-sm']) !!}</td>
                <td>
                    {!! Form::open(['route' => ['destroy_navigation_path', $site->id, $navigation->id ], 'method' => 'DELETE', 'class' => 'destroy-form']) !!}
                    {!! Form::hidden('id', $navigation->id) !!}
                    <button type="submit" class="btn btn-default btn-sm">Remove</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this navigation group?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection