@extends('admin.layouts.default')

@section('secondary-nav')
    <?php
    $_body_class = 'padded';
    $_nav_collapsed = 'collapsed';
    ?>
    @include('admin.sites.partials.side-nav')
@endsection

@section('content')

    @include('admin.navigations.partials.tabs', ['tab' => 'edit'])

    @include('admin.layouts.partials.errors')

    {!! Form::model($navigation) !!}

    <!-- Name Form Input -->
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Slug Form Input -->
    <div class="form-group">
        {!! Form::label('slug', 'Slug:') !!}
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Description Form Input -->
    <div class="form-group">
        {!! Form::label('description', 'Description:') !!}
        {!! Form::text('description', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit field -->
    <div class="form-group">
        {!! Form::submit('Save Changes', ['class' => 'btn btn-primary btn-lg']) !!}
    </div>

    {!! Form::close() !!}


    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Remove Navigation Group</h3>
        </div>
        <div class="panel-body">
            <p>This cannot be undone.</p>

            {!! Form::open(['method' => 'DELETE', 'route' => ['destroy_navigation_path', $site->id, $navigation->id], 'class' => 'destroy-form']) !!}

            {!! Form::hidden('id', $navigation->id) !!}

            <!-- Submit field -->
            <div class="form-group">
                {!! Form::submit('Remove Navigation Group Now', ['class' => 'btn btn-danger']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    @parent

    <script>
        $('.destroy-form').on('submit', function(e)
        {
            e.preventDefault();

            var theForm = this;

            bootbox.confirm('Are you sure you want to remove this navigation group?', function(result) {
                if(result)
                {
                    theForm.submit();
                }
            });
        });
    </script>
@endsection