var ftgRates = {
    load : function(id) {
        var xhr;
        var url = 'http://rates.fastrackg.com/.getrates/' + id;

        if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
        else {
            var versions = ["MSXML2.XmlHttp.5.0",
                "MSXML2.XmlHttp.4.0",
                "MSXML2.XmlHttp.3.0",
                "MSXML2.XmlHttp.2.0",
                "Microsoft.XmlHttp"]

            for(var i = 0, len = versions.length; i < len; i++) {
                try {
                    xhr = new ActiveXObject(versions[i]);
                    break;
                }
                catch(e){}
            }
        }

        xhr.onreadystatechange = ensureReadiness;

        function ensureReadiness() {
            if(xhr.readyState < 4) {
                return;
            }

            if(xhr.status !== 200) {
                return;
            }

            if(xhr.readyState === 4) {
                ftgRates.insert(JSON.parse(xhr.responseText));
            }
        }

        xhr.open('GET', url, true);
        xhr.send('');
    },

    insert : function(data) {
        console.log(data);

        var a = document.querySelectorAll('[data-rate-id]');

        for (var i in a) if (a.hasOwnProperty(i)) {

            var id = a[i].getAttribute('data-rate-id');

            if(data[id] != undefined)
            {
                // prefix
                var prefix = a[i].getAttribute('data-rate-prefix');

                // suffix
                var suffix = a[i].getAttribute('data-rate-suffix');

                var output = '';

                if(prefix != undefined)
                {
                    output += prefix;
                }

                output += data[id].rate;

                if(suffix != undefined)
                {
                    output += suffix;
                }

                a[i].innerHTML = output;
            }

        }
    }
}
