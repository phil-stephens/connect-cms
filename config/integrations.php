<?php

return [

    'providers' => [

        'rates' => [
            'BookingButton' => [
                        'name'  => 'The BookingButton (Siteminder)',
                        'type'  => 'Fastrack\Integrations\Providers\Siteminder\BookingButton'
                    ],

            'RMS' => [
                        'name'  => 'RMS',
                        'type'  => 'Fastrack\Integrations\Providers\RMS\RMS'
                    ],
            'Legacy' => [
                'name'  => 'FastrackBeds (Legacy Version)',
                'type'  => 'Fastrack\Integrations\Providers\FastrackBeds\Legacy'
            ]
        ]
    ]

];
