<?php

return [
    // Thunderstorms
    200 => 'wi-storm-showers',  // thunderstorm with light rain
    201 => 'wi-thunderstorm',   // thunderstorm with rain
    202 => 'wi-thunderstorm',   // thunderstorm with heavy rain
    210 => 'wi-storm-showers',  // light thunderstorm
    211 => 'wi-thunderstorm',   // thunderstorm
    212 => 'wi-thunderstorm',   // heavy thunderstorm
    221 => 'wi-thunderstorm',   // ragged thunderstorm
    230 => 'wi-storm-showers',  // thunderstorm with light drizzle
    231 => 'wi-storm-showers',  // thunderstorm with drizzle
    232 => 'wi-storm-showers',  // thunderstorm with heavy drizzle

    // Drizzle
    300 => 'wi-sprinkle',       // light intensity drizzle
    301 => 'wi-sprinkle',       // drizzle
    302 => 'wi-sprinkle',       // heavy intensity drizzle
    310 => 'wi-sprinkle',       // light intensity drizzle rain
    311 => 'wi-rain-mix',       // drizzle rain
    312 => 'wi-rain-mix',       // heavy intensity drizzle rain
    313 => 'wi-rain-mix',       // shower rain and drizzle
    314 => 'wi-rain-mix',       // heavy shower rain and drizzle
    321 => 'wi-rain-mix',       // shower drizzle

    // Rain
    500 => 'wi-rain-mix',       // light rain
    501 => 'wi-rain',           // moderate rain
    502 => 'wi-rain',           // heavy intensity rain
    503 => 'wi-rain',           // very heavy rain
    504 => 'wi-rain',           // extreme rain
    511 => 'wi-rain',           // freezing rain
    520 => 'wi-showers',        // light intensity shower rain
    521 => 'wi-showers',        // shower rain
    522 => 'wi-rain-mix',       // heavy intensity shower rain
    531 => 'wi-rain-mix',       // ragged shower rain

    // Snow
    600 => 'wi-snow',           // light snow
    601 => 'wi-snow',           // snow
    602 => 'wi-snow',           // heavy snow
    611 => 'wi-sleet',          // sleet
    612 => 'wi-sleet',          // shower sleet
    615 => 'wi-snow',           // light rain and snow
    616 => 'wi-snow',           // rain and snow
    620 => 'wi-snow',           // light shower snow
    621 => 'wi-snow',           // shower snow
    622 => 'wi-snow',           // heavy shower snow

    //  Atmosphere
    701 => 'wi-day-fog',        // mist
    711 => 'wi-smoke',          // smoke
    721 => 'wi-day-haze',       // haze
    731 => 'wi-dust',           // sand, dust whirls
    741 => 'wi-day-fog',        // fog
    751 => 'wi-dust',           // sand
    761 => 'wi-dust',           // dust
    762 => 'wi-dust',           // volcanic ash
    771 => 'wi-strong-wind',    // squalls
    781 => 'wi-tornado',        // tornado

    // Clouds
    800 => 'wi-day-sunny',      // clear sky
    801 => 'wi-day-cloudy',     // few clouds
    802 => 'wi-cloud',          // scattered clouds
    803 => 'wi-day-cloudy',     // broken clouds
    804 => 'wi-cloudy',         // overcast clouds

    // Extreme
    900 => 'wi-tornado',        // tornado
    901 => 'wi-thunderstorm',   // tropical storm
    902 => 'wi-hurricane',      // hurricane
    903 => 'wi-snowflake-cold', // cold
    904 => 'wi-hot',            // hot
    905 => 'wi-windy',          // windy
    906 => 'wi-hail',           // hail

    // Additional
    951 => 'wi-day-sunny',      // calm
    952 => 'wi-windy',          // light breeze
    953 => 'wi-windy',          // gentle breeze
    954 => 'wi-windy',          // moderate breeze
    955 => 'wi-windy',          // fresh breeze
    956 => 'wi-windy',          // strong breeze
    957 => 'wi-strong-wind',    // high wind, near gale
    958 => 'wi-strong-wind',    // gale
    959 => 'wi-strong-wind',    // severe gale
    960 => 'wi-thunderstorm',   // storm
    961 => 'wi-thunderstorm',   // violent storm
    962 => 'wi-hurricane',      // hurricane
];

















