<?php

Route::get('{siteUuid}/campaigns', [
    'as'    => 'campaigns_path',
    'uses'  => 'CampaignController@index'
]);

Route::get('{siteUuid}/campaign/create', [
    'as'    => 'campaign_create_path',
    'uses'  => 'CampaignController@create'
]);

Route::post('{siteUuid}/campaign/create', [
    'as'    => 'campaign_create_path',
    'uses'  => 'CampaignController@store'
]);

//Route::get('campaign/{campaignUuid}', [
//    'as'    => 'campaign_path',
//    'uses'  => 'CampaignController@show'
//]);

Route::get('campaign/{campaignUuid}/edit', [
    'as'    => 'campaign_edit_path',
    'uses'  => 'CampaignController@edit'
]);

Route::post('campaign/{campaignUuid}/edit', [
    'as'    => 'campaign_edit_path',
    'uses'  => 'CampaignController@update'
]);

// Variations
Route::get('campaign/{campaignUuid}/variations', [
    'as'    => 'variations_path',
    'uses'  => 'VariationController@index'
]);

Route::get('campaign/{campaignUuid}/variation/create', [
    'as'    => 'variation_create_path',
    'uses'  => 'VariationController@create'
]);

Route::post('campaign/{campaignUuid}/variation/create', [
    'as'    => 'variation_create_path',
    'uses'  => 'VariationController@store'
]);

Route::get('campaign/variation/{variationUuid}/edit', [
    'as'    => 'variation_edit_path',
    'uses'  => 'VariationController@edit'
]);

Route::post('campaign/variation/{variationUuid}/edit', [
    'as'    => 'variation_edit_path',
    'uses'  => 'VariationController@update'
]);