<?php

//Route::get('atoms/{uuid}', [
//    'as'    => 'atom_edit_path',
//    'uses'  => 'AtomController@edit'
//]);
//
//
//Route::post('atoms/{uuid}', [
//    'as'    => 'atom_edit_path',
//    'uses'  => 'AtomController@update'
//]);

Route::get('block/{uuid}', [
    'as'    => 'atom_edit_path',
    'uses'  => 'AtomController@edit'
]);


Route::post('block/{uuid}', [
    'as'    => 'atom_edit_path',
    'uses'  => 'AtomController@update'
]);