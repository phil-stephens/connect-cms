<?php


Route::get('{propertyId}/dashboard', [
    'as'   => 'property_path',
    'uses' => 'DashboardController@property'
]);

Route::get('create', [
    'as'   => 'create_property_path',
    'uses' => 'PropertiesController@create'
]);

Route::post('create', [
    'as'   => 'create_property_path',
    'uses' => 'PropertiesController@store'
]);

Route::get('{propertyId}/edit', [
    'as'   => 'edit_property_path',
    'uses' => 'PropertiesController@edit'
]);

Route::post('{propertyId}/edit', [
    'as'   => 'edit_property_path',
    'uses' => 'PropertiesController@update'
]);

Route::get('{propertyId}/content', [
    'as'   => 'edit_property_content_path',
    'uses' => 'PropertiesController@content'
]);

Route::post('{propertyId}/content', [
    'as'   => 'edit_property_content_path',
    'uses' => 'PropertiesController@updateContent'
]);

Route::get('{propertyId}/location', [
    'as'   => 'edit_property_location_path',
    'uses' => 'PropertiesController@location'
]);

Route::post('{propertyId}/location', [
    'as'   => 'edit_property_location_path',
    'uses' => 'PropertiesController@updateLocation'
]);

Route::get('{propertyId}/slideshow', [
    'as'   => 'edit_property_slideshow_path',
    'uses' => 'PropertiesController@slideshow'
]);

//Route::post('{propertyId}/slideshow', [
//    'as'    => 'edit_property_slideshow_path',
//    'uses'  => 'PropertiesController@updateSlideshow'
//]);


Route::get('{propertyId}/general-features', [
    'as'   => 'edit_property_amenities_path',
    'uses' => 'PropertiesController@amenities'
]);

Route::post('{propertyId}/general-features', [
    'as'   => 'edit_property_amenities_path',
    'uses' => 'PropertiesController@updateAmenities'
]);

Route::delete('{propertyId}/general-features', [
    'as'   => 'destroy_property_amenity_path',
    'uses' => 'PropertiesController@removeAmenity'
]);

Route::get('{propertyId}/seo', [
    'as'   => 'edit_property_seo_path',
    'uses' => 'PropertiesController@seo'
]);

Route::post('{propertyId}/seo', [
    'as'   => 'edit_property_seo_path',
    'uses' => 'PropertiesController@updateSeo'
]);


Route::get('{propertyId}/integrations', [
    'as'   => 'property_integration_path',
    'uses' => 'PropertiesController@integrations'
]);

Route::get('{propertyId}/integration/add', [
    'as'   => 'property_integration_add_path',
    'uses' => 'PropertiesController@addIntegration'
]);

Route::post('{propertyId}/integration/add', [
    'as'   => 'property_integration_add_path',
    'uses' => 'PropertiesController@storeIntegration'
]);

Route::get('{propertyId}/integration/{integrationId}/edit', [
    'as'   => 'property_integration_edit_path',
    'uses' => 'PropertiesController@editIntegration'
]);

Route::post('{propertyId}/integration/{integrationId}/edit', [
    'as'   => 'property_integration_edit_path',
    'uses' => 'PropertiesController@updateIntegration'
]);


Route::get('{propertyId}/integration/{integrationId}/test', [
    'as'   => 'property_integration_test_path',
    'uses' => 'PropertiesController@testIntegration'
]);


Route::delete('{propertyId}', [
    'as'   => 'destroy_property_path',
    'uses' => 'PropertiesController@destroy'
]);

// Rooms
Route::get('{propertyId}/rooms', [
    'as'   => 'rooms_path',
    'uses' => 'RoomsController@index'
]);

Route::post('{propertyId}/rooms/sort', [
    'as'   => 'sort_rooms_path',
    'uses' => 'RoomsController@updateSorting'
]);

Route::get('{propertyId}/room/create', [
    'as'   => 'create_room_path',
    'uses' => 'RoomsController@create'
]);

Route::post('{propertyId}/room/create', [
    'as'   => 'create_room_path',
    'uses' => 'RoomsController@store'
]);

Route::get('{propertyId}/room/{roomId}/edit', [
    'as'   => 'edit_room_path',
    'uses' => 'RoomsController@edit'
]);

Route::post('{propertyId}/room/{roomId}/edit', [
    'as'   => 'edit_room_path',
    'uses' => 'RoomsController@update'
]);

Route::get('{propertyId}/room/{roomId}/content', [
    'as'   => 'content_room_path',
    'uses' => 'RoomsController@content'
]);

Route::post('{propertyId}/room/{roomId}/content', [
    'as'   => 'content_room_path',
    'uses' => 'RoomsController@updateContent'
]);

Route::get('{propertyId}/room/{roomId}/slideshow', [
    'as'   => 'slideshow_room_path',
    'uses' => 'RoomsController@slideshow'
]);

Route::get('{propertyId}/room/{roomId}/features', [
    'as'   => 'room_amenities_path',
    'uses' => 'RoomsController@amenities'
]);

Route::post('{propertyId}/room/{roomId}/features', [
    'as'   => 'room_amenities_path',
    'uses' => 'RoomsController@updateAmenities'
]);

Route::delete('{propertyId}/room/{roomId}/features', [
    'as'   => 'destroy_room_amenity_path',
    'uses' => 'RoomsController@removeAmenity'
]);

Route::get('{propertyId}/room/{roomId}/seo', [
    'as'   => 'seo_room_path',
    'uses' => 'RoomsController@seo'
]);

Route::post('{propertyId}/room/{roomId}/seo', [
    'as'   => 'seo_room_path',
    'uses' => 'RoomsController@updateSeo'
]);


Route::get('room/{roomUuid}/related', [
    'as'   => 'room_related_path',
    'uses' => 'RoomsController@related'
]);

Route::post('room/{roomUuid}/related', [
    'as'   => 'room_related_path',
    'uses' => 'RoomsController@updateRelated'
]);

Route::delete('room/{roomUuid}/related/destroy', [
    'as' => 'room_destroy_related_path',
    'uses'  => 'RoomsController@destroyRelated'
]);

// Sub-types
Route::get('room/{roomId}/subtypes', [
   'as' => 'subtypes_path',
    'uses'  => 'SubTypeController@index'
]);

//Route::any('room/{roomId}/sort/subtypes', function() {
//    return 'foo';
//});
Route::any('room/{roomId}/subtypes/sort', [
    'as' => 'subtype_sort_path',
    'uses'  => 'SubTypeController@updateSorting'
]);

Route::get('room/{roomId}/subtype/create', [
    'as' => 'subtype_create_path',
    'uses'  => 'SubTypeController@create'
]);

Route::post('room/{roomId}/subtype/create', [
    'as' => 'subtype_create_path',
    'uses'  => 'SubTypeController@store'
]);

Route::get('room/subtype/{subtypeId}/edit', [
    'as' => 'subtype_edit_path',
    'uses'  => 'SubTypeController@edit'
]);

Route::post('room/subtype/{subtypeId}/edit', [
    'as' => 'subtype_edit_path',
    'uses'  => 'SubTypeController@update'
]);

Route::get('room/subtype/{subtypeId}/content', [
    'as' => 'subtype_content_path',
    'uses'  => 'SubTypeController@content'
]);

Route::post('room/subtype/{subtypeId}/content', [
    'as' => 'subtype_content_path',
    'uses'  => 'SubTypeController@updateContent'
]);

Route::get('room/subtype/{subtypeId}/slideshow', [
    'as' => 'subtype_slideshow_path',
    'uses'  => 'SubTypeController@slideshow'
]);

Route::get('room/subtype/{subtypeId}/seo', [
    'as' => 'subtype_seo_path',
    'uses'  => 'SubTypeController@seo'
]);

Route::post('room/subtype/{subtypeId}/seo', [
    'as' => 'subtype_seo_path',
    'uses'  => 'SubTypeController@updateSeo'
]);

Route::delete('room/subtype/{subtypeId}', [
    'as' => 'subtype_destroy_path',
    'uses'  => 'SubTypeController@destroy'
]);



Route::get('{propertyId}/room/{roomId}/integrations', [
    'as'   => 'room_integration_path',
    'uses' => 'RoomsController@integrations'
]);

Route::get('{propertyId}/room/{roomId}/integration/add', [
    'as'   => 'room_integration_add_path',
    'uses' => 'RoomsController@addIntegration'
]);

Route::post('{propertyId}/room/{roomId}/integration/add', [
    'as'   => 'room_integration_add_path',
    'uses' => 'RoomsController@storeIntegration'
]);

Route::get('{propertyId}/room/{roomId}/integration/{integrationId}/edit', [
    'as'   => 'room_integration_edit_path',
    'uses' => 'RoomsController@editIntegration'
]);

Route::post('{propertyId}/room/{roomId}/integration/{integrationId}/edit', [
    'as'   => 'room_integration_edit_path',
    'uses' => 'RoomsController@updateIntegration'
]);


//Route::get('{propertyId}/room/{roomId}/integration/{integrationId}/test', [
//    'as'   => 'room_integration_test_path',
//    'uses' => 'RoomsController@testIntegration'
//]);



Route::delete('{propertyId}/room/{roomId}', [
    'as'   => 'destroy_room_path',
    'uses' => 'RoomsController@destroy'
]);

// Amenities
Route::get('{propertyId}/features', [
    'as'   => 'amenities_path',
    'uses' => 'AmenitiesController@index'
]);

Route::get('{propertyId}/feature/create', [
    'as'   => 'create_amenity_path',
    'uses' => 'AmenitiesController@create'
]);

Route::post('{propertyId}/feature/create', [
    'as'   => 'create_amenity_path',
    'uses' => 'AmenitiesController@store'
]);

Route::get('{propertyId}/feature/{amenityId}/edit', [
    'as'   => 'edit_amenity_path',
    'uses' => 'AmenitiesController@edit'
]);

Route::post('{propertyId}/feature/{amenityId}/edit', [
    'as'   => 'edit_amenity_path',
    'uses' => 'AmenitiesController@update'
]);

Route::delete('{propertyId}/feature/{amenityId}', [
    'as'   => 'destroy_amenity_path',
    'uses' => 'AmenitiesController@destroy'
]);

Route::get('{propertyUuid}/reviews', [
   'as'     => 'reviews_path',
    'uses'  => 'ReviewController@index'
]);

Route::get('review/{reviewUuid?}', [
    'as'     => 'review_path__ajax',
    'uses'  => 'ReviewController@show'
]);

//Route::get('review/{reviewUuid}/publish', [
//    'as'     => 'review_publish_path',
//    'uses'  => 'ReviewController@publish'
//]);

Route::get('{propertyUuid}/review/create', [
    'as'     => 'review_create_path',
    'uses'  => 'ReviewController@create'
]);

Route::post('{propertyUuid}/review/create', [
    'as'     => 'review_create_path',
    'uses'  => 'ReviewController@store'
]);

Route::get('review/{reviewUuid}/edit', [
    'as'     => 'review_edit_path',
    'uses'  => 'ReviewController@edit'
]);

Route::post('review/{reviewUuid}/edit', [
    'as'     => 'review_edit_path',
    'uses'  => 'ReviewController@update'
]);

Route::delete('{propertyUuid}/review', [
    'as'     => 'review_destroy_path',
    'uses'  => 'ReviewController@destroy'
]);


Route::get('{propertyUuid}/statistics', [
    'as'   => 'statistics_path',
    'uses' => 'StatisticController@index'
]);

Route::get('{propertyUuid}/statistic/create', [
    'as'   => 'statistic_create_path',
    'uses' => 'StatisticController@create'
]);


Route::post('{propertyUuid}/statistic/create', [
    'as'   => 'statistic_create_path',
    'uses' => 'StatisticController@store'
]);

Route::delete('statistic/{statisticUuid}', [
    'as'   => 'statistic_destroy_path',
    'uses' => 'StatisticController@destroy'
]);

Route::get('statistic/{statisticUuid}', [
    'as'   => 'statistic_edit_path',
    'uses' => 'StatisticController@edit'
]);

Route::post('statistic/{statisticUuid}', [
    'as'   => 'statistic_edit_path',
    'uses' => 'StatisticController@update'
]);





Route::get('{propertyUuid}/feature/groups', [
    'as'    => 'feature_groups_path',
    'uses'  => 'FeatureGroupController@index'
]);