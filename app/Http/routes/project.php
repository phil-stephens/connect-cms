<?php

// Sanctuary Stays
Route::any('ss/search', 'Projects\SanctuaryStays\SearchController@doSearch');
Route::any('ss/basic-search', 'Projects\SanctuaryStays\SearchController@doBasicSearch');
Route::get('ss/search/results/{uuid}', 'Projects\SanctuaryStays\SearchController@show');