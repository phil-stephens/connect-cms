<?php


Route::get('{siteId}/chapter/create', [
    'as'   => 'chapter_create_path',
    'uses' => 'ChapterController@create'
]);

Route::post('{siteId}/chapter/create', [
    'as'   => 'chapter_create_path',
    'uses' => 'ChapterController@store'
]);

Route::get('{siteId}/chapter/{chapterId?}', [
    'as'   => 'chapter_pages_path',
    'uses' => 'ChapterController@pages'
]);

Route::get('{siteId}/chapter/{chapterId}/edit', [
    'as'   => 'chapter_edit_path',
    'uses' => 'ChapterController@edit'
]);

Route::post('{siteId}/chapter/{chapterId}/edit', [
    'as'   => 'chapter_edit_path',
    'uses' => 'ChapterController@update'
]);




Route::get('{siteId}/pages', [
    'as'   => 'pages_path',
    'uses' => 'PagesController@index'
]);

Route::post('{siteId}/pages/sort', [
    'as'   => 'sort_pages_path',
    'uses' => 'PagesController@updateSorting'
]);

Route::get('{siteId}/page/create', [
    'as'   => 'create_page_path',
    'uses' => 'PagesController@create'
]);

Route::post('{siteId}/page/create', [
    'as'   => 'create_page_path',
    'uses' => 'PagesController@store'
]);

Route::get('{siteId}/page/{pageId}/edit', [
    'as'   => 'edit_page_path',
    'uses' => 'PagesController@edit'
]);

Route::post('{siteId}/page/{pageId}/edit', [
    'as'   => 'edit_page_path',
    'uses' => 'PagesController@update'
]);

Route::get('{siteId}/page/{pageId}/settings', [
    'as'   => 'page_settings_path',
    'uses' => 'PagesController@settings'
]);

Route::post('{siteId}/page/{pageId}/settings', [
    'as'   => 'page_settings_path',
    'uses' => 'PagesController@updateSettings'
]);

Route::get('{siteId}/page/{pageId}/content', [
    'as'   => 'content_page_path',
    'uses' => 'PagesController@content'
]);

Route::post('{siteId}/page/{pageId}/content', [
    'as'   => 'content_page_path',
    'uses' => 'PagesController@updateContent'
]);

Route::get('{siteId}/page/{pageId}/seo', [
    'as'   => 'seo_page_path',
    'uses' => 'PagesController@seo'
]);

Route::post('{siteId}/page/{pageId}/seo', [
    'as'   => 'seo_page_path',
    'uses' => 'PagesController@updateSeo'
]);

Route::get('{siteId}/page/{pageId}/slideshow', [
    'as'   => 'slideshow_page_path',
    'uses' => 'PagesController@slideshow'
]);

Route::get('{siteId}/page/{pageId}/annotations', [
    'as'   => 'page_annotations_path',
    'uses' => 'PagesController@annotations'
]);

Route::post('{siteId}/page/{pageId}/annotations', [
    'as'   => 'page_annotations_path',
    'uses' => 'PagesController@updateAnnotations'
]);

Route::delete('{siteId}/page/{pageId}', [
    'as'   => 'destroy_page_path',
    'uses' => 'PagesController@destroy'
]);


// Related content
Route::get('page/{pageId}/related', [
    'as' => 'page_related_path',
    'uses'  => 'PagesController@related'
]);

Route::post('page/{pageId}/related', [
    'as' => 'page_related_path',
    'uses'  => 'PagesController@updateRelated'
]);

Route::delete('page/{pageId}/related/destroy', [
    'as' => 'page_destroy_related_path',
    'uses'  => 'PagesController@destroyRelated'
]);