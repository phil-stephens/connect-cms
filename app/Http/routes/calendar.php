<?php


// calendars
Route::get('calendars', [
    'as'   => 'calendars_path',
    'uses' => 'CalendarsController@index'
]);

Route::group(['prefix' => 'calendar'], function() {
    Route::get('create', [
        'as'   => 'create_calendar_path',
        'uses' => 'CalendarsController@create'
    ]);

    Route::post('create', [
        'as'   => 'create_calendar_path',
        'uses' => 'CalendarsController@store'
    ]);

    Route::get('{calendarId}/edit', [
        'as'   => 'edit_calendar_path',
        'uses' => 'CalendarsController@edit'
    ]);

    Route::post('{calendarId}/edit', [
        'as'   => 'edit_calendar_path',
        'uses' => 'CalendarsController@update'
    ]);

    Route::delete('{calendarId}', [
        'as'   => 'destroy_calendar_path',
        'uses' => 'CalendarsController@destroy'
    ]);

    // Events
    Route::get('{calendarId}/events', [
        'as'   => 'events_path',
        'uses' => 'EventsController@index'
    ]);

    Route::get('{calendarId}/event/create', [
        'as'   => 'create_event_path',
        'uses' => 'EventsController@create'
    ]);

    Route::post('{calendarId}/event/create', [
        'as'   => 'create_event_path',
        'uses' => 'EventsController@store'
    ]);

    Route::get('{calendarId}/event/{eventId}/edit', [
        'as'   => 'edit_event_path',
        'uses' => 'EventsController@edit'
    ]);

    Route::post('{calendarId}/event/{eventId}/edit', [
        'as'   => 'edit_event_path',
        'uses' => 'EventsController@update'
    ]);

    Route::get('{calendarId}/event/{eventId}/content', [
        'as'   => 'content_event_path',
        'uses' => 'EventsController@content'
    ]);

    Route::post('{calendarId}/event/{eventId}/content', [
        'as'   => 'content_event_path',
        'uses' => 'EventsController@updateContent'
    ]);

    Route::get('{calendarId}/event/{eventId}/slideshow', [
        'as'   => 'slideshow_event_path',
        'uses' => 'EventsController@slideshow'
    ]);

    Route::get('{calendarId}/event/{eventId}/seo', [
        'as'   => 'seo_event_path',
        'uses' => 'EventsController@seo'
    ]);

    Route::post('{calendarId}/event/{eventId}/seo', [
        'as'   => 'seo_event_path',
        'uses' => 'EventsController@updateSeo'
    ]);

    Route::delete('{calendarId}/event/{eventId}', [
        'as'   => 'destroy_event_path',
        'uses' => 'EventsController@destroy'
    ]);
});

// Tickets/Box Office
Route::get('boxoffice/{eventId}', [
    'as'    => 'boxoffice_path',
    'uses'  => 'TicketController@index'
]);

Route::get('boxoffice/{eventId}/create', [
    'as'    => 'boxoffice_create_path',
    'uses'  => 'TicketController@create'
]);

Route::post('boxoffice/{eventId}/create', [
    'as'    => 'boxoffice_create_path',
    'uses'  => 'TicketController@store'
]);

Route::post('boxoffice/{eventId}/email', [
    'as'    => 'boxoffice_email_path',
    'uses'  => 'TicketController@email'
]);

