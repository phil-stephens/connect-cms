<?php

Route::group(['namespace' => 'Settings', 'prefix' => 'settings'], function() {

    Route::get('/', [
       'as' => 'settings_path',
        'uses'  => 'SettingController@index'
    ]);

    // Amenities
    Route::get('features', [
        'as'    => 'account_amenities_path',
        'uses'  => 'FeatureController@index'
    ]);

    Route::get('feaute/create', [
        'as'    => 'account_amenity_create_path',
        'uses'  => 'FeatureController@create'
    ]);

    Route::post('feature/create', [
        'as'    => 'account_amenitiy_create_path',
        'uses'  => 'FeatureController@store'
    ]);

    Route::get('feature/{uuid}/edit', [
        'as'    => 'account_amenity_edit_path',
        'uses'  => 'FeatureController@edit'
    ]);

    Route::post('feature/{uuid}/edit', [
        'as'    => 'account_amenitiy_edit_path',
        'uses'  => 'FeatureController@update'
    ]);

    Route::delete('feature/{uuid}', [
        'as'    => 'account_amenity_destroy_path',
        'uses'  => 'FeatureController@destroy'
    ]);


    Route::get('users', [
        'as'   => 'users_path',
        'uses' => 'UsersController@index'
    ]);

    Route::group(['prefix' => 'user'], function() {

        Route::get('create', [
            'as'   => 'create_user_path',
            'uses' => 'UsersController@create'
        ]);

        Route::post('create', [
            'as'   => 'create_user_path',
            'uses' => 'UsersController@store'
        ]);

        Route::get('me', [
            'as'   => 'edit_me_path',
            'uses' => 'UsersController@editMe'
        ]);

        Route::post('me', [
            'as'   => 'edit_me_path',
            'uses' => 'UsersController@updateMe'
        ]);

        Route::get('{userId}/edit', [
            'as'   => 'edit_user_path',
            'uses' => 'UsersController@edit'
        ]);

        Route::post('{userId}/edit', [
            'as'   => 'edit_user_path',
            'uses' => 'UsersController@update'
        ]);

        Route::delete('{userId}', [
            'as'   => 'destroy_user_path',
            'uses' => 'UsersController@destroy'
        ]);
    });


    Route::get('integrations', [
        'as'     => 'integrations_path',
        'uses'  => 'IntegrationController@index'
    ]);

    Route::get('integration/create', [
        'as'     => 'integration_create_path',
        'uses'  => 'IntegrationController@create'
    ]);

    Route::post('integration/create', [
        'as'     => 'integration_create_path',
        'uses'  => 'IntegrationController@store'
    ]);

    Route::get('integration/{integrationId}/edit', [
        'as'     => 'integration_edit_path',
        'uses'  => 'IntegrationController@edit'
    ]);



    Route::get('categories', [
        'as'   => 'categories_path',
        'uses' => 'CategoryController@index'
    ]);

    Route::get('category/create', [
        'as'   => 'create_category_path',
        'uses' => 'CategoryController@create'
    ]);

    Route::post('category/create', [
        'as'   => 'create_category_path',
        'uses' => 'CategoryController@store'
    ]);

    Route::get('category/{categoryId}/edit', [
        'as'   => 'edit_category_path',
        'uses' => 'CategoryController@edit'
    ]);

    Route::post('category/{categoryId}/edit', [
        'as'   => 'edit_category_path',
        'uses' => 'CategoryController@update'
    ]);

    Route::delete('category/{categoryId}', [
        'as'   => 'destroy_category_path',
        'uses' => 'CategoryController@destroy'
    ]);



    Route::post('configuration/update', [
        'as'    => 'configuration_update_path',
        'uses'  => 'ConfigurationController@update'
    ]);
});