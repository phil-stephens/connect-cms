<?php

Route::group(['namespace' => 'Compendium'], function() {

    Route::get('compendium/{uuid}', [
        'as'     => 'compendium_path',
        'uses'  => 'SettingController@edit'
    ]);

    Route::post('compendium/{uuid}', [
        'as'     => 'compendium_path',
        'uses'  => 'SettingController@update'
    ]);

    Route::get('compendium/{uuid}/theme', [
        'as'     => 'compendium_theme_path',
        'uses'  => 'SettingController@theme'
    ]);

    Route::post('compendium/{uuid}/theme', [
        'as'     => 'compendium_theme_path',
        'uses'  => 'SettingController@updateTheme'
    ]);

    Route::get('compendium/{uuid}/places', [
        'as'     => 'compendium_places_path',
        'uses'  => 'SettingController@places'
    ]);


    Route::get('compendium/{uuid}/pages', [
        'as'     => 'compendium_pages_path',
        'uses'  => 'PageController@index'
    ]);

    Route::get('compendium/folder/{uuid?}', [
        'as'     => 'compendium__folder_pages_path',
        'uses'  => 'FolderController@pages'
    ]);

    Route::get('compendium/{uuid}/folder/create', [
        'as'     => 'compendium__folder_create_path',
        'uses'  => 'FolderController@create'
    ]);

    Route::post('compendium/{uuid}/folder/create', [
        'as'     => 'compendium__folder_store_path',
        'uses'  => 'FolderController@store'
    ]);

    Route::get('compendium/folder/{uuid}/edit', [
        'as'     => 'compendium__folder_edit_path',
        'uses'  => 'FolderController@edit'
    ]);

    Route::post('compendium/folder/{uuid}/edit', [
        'as'     => 'compendium__folder_edit_path',
        'uses'  => 'FolderController@update'
    ]);

    Route::get('compendium/{uuid}/page/create', [
        'as'     => 'compendium__page_create_path',
        'uses'  => 'PageController@create'
    ]);

    Route::post('compendium/{uuid}/page/create', [
        'as'     => 'compendium__page_create_path',
        'uses'  => 'PageController@store'
    ]);

    Route::get('compendium/page/{uuid}/edit', [
        'as'     => 'compendium__page_edit_path',
        'uses'  => 'PageController@edit'
    ]);

    Route::post('compendium/page/{uuid}/edit', [
        'as'     => 'compendium__page_edit_path',
        'uses'  => 'PageController@update'
    ]);

    Route::delete('compendium/page/{uuid}/destroy', [
        'as'     => 'compendium__page_destroy_path',
        'uses'  => 'PageController@destroy'
    ]);




    Route::get('compendium/{uuid}/indices', [
        'as'     => 'compendium_indices_path',
        'uses'  => 'IndexController@index'
    ]);

    Route::get('compendium/{uuid}/index/create', [
        'as'     => 'compendium__index_create_path',
        'uses'  => 'IndexController@create'
    ]);

    Route::post('compendium/{uuid}/index/create', [
        'as'     => 'compendium__index_create_path',
        'uses'  => 'IndexController@store'
    ]);

    Route::get('compendium/index/{uuid}/edit', [
        'as'     => 'compendium__index_edit_path',
        'uses'  => 'IndexController@edit'
    ]);

    Route::post('compendium/index/{uuid}/edit', [
        'as'     => 'compendium__index_edit_path',
        'uses'  => 'IndexController@update'
    ]);

    Route::get('compendium/index/{uuid}/items', [
        'as'     => 'compendium__index_items_path',
        'uses'  => 'IndexController@items'
    ]);

    Route::post('compendium/index/{uuid}/items', [
        'as'     => 'compendium__index_items_path',
        'uses'  => 'IndexController@updateItems'
    ]);

    Route::delete('compendium/index/{uuid}', [
        'as'     => 'compendium__index_destroy_path',
        'uses'  => 'IndexController@destroy'
    ]);

});
