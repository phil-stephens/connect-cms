<?php

Route::get('export/site/{uuid}', [
   'as'     => 'export_site_path',
    'uses'  => 'ExportController@site'
]);

Route::post('export/site/{uuid}', [
    'as'     => 'export_site_path',
    'uses'  => 'ExportController@exportSite'
]);

Route::post('import/site/{uuid}', [
    'as'     => 'import_site_path',
    'uses'  => 'ExportController@importSite'
]);




Route::get('export/compendium/{uuid}', [
    'as'     => 'compendium_export_path',
    'uses'  => 'ExportController@compendium'
]);

Route::post('export/compendium/{uuid}', [
    'as'     => 'compendium_export_path',
    'uses'  => 'ExportController@exportSite'
]);
