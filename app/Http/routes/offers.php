<?php

Route::get('{propertyId}/offers', [
    'as'   => 'offers_path',
    'uses' => 'OffersController@index'
]);

Route::get('{propertyId}/offer/create', [
    'as'   => 'create_offer_path',
    'uses' => 'OffersController@create'
]);

Route::post('{propertyId}/offer/create', [
    'as'   => 'create_offer_path',
    'uses' => 'OffersController@store'
]);

Route::get('{propertyId}/offer/{offerId}/edit', [
    'as'   => 'edit_offer_path',
    'uses' => 'OffersController@edit'
]);

Route::post('{propertyId}/offer/{offerId}/edit', [
    'as'   => 'edit_offer_path',
    'uses' => 'OffersController@update'
]);

Route::get('{propertyId}/offer/{offerId}/content', [
    'as'   => 'content_offer_path',
    'uses' => 'OffersController@content'
]);

Route::post('{propertyId}/offer/{offerId}/content', [
    'as'   => 'content_offer_path',
    'uses' => 'OffersController@updateContent'
]);

Route::get('{propertyId}/offer/{offerId}/slideshow', [
    'as'   => 'slideshow_offer_path',
    'uses' => 'OffersController@slideshow'
]);

Route::get('{propertyId}/offer/{offerId}/seo', [
    'as'   => 'seo_offer_path',
    'uses' => 'OffersController@seo'
]);

Route::post('{propertyId}/offer/{offerId}/seo', [
    'as'   => 'seo_offer_path',
    'uses' => 'OffersController@updateSeo'
]);

Route::post('{propertyId}/offers/sort', [
    'as'   => 'sort_offers_path',
    'uses' => 'OffersController@updateSorting'
]);

Route::delete('{propertyId}/offer/{offerId}', [
    'as'   => 'destroy_offer_path',
    'uses' => 'OffersController@destroy'
]);