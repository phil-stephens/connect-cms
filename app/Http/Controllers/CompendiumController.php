<?php

namespace Fastrack\Http\Controllers;

use Fastrack\Calendars\EventRepository;
use Fastrack\Locations\VenueRepository;
use Fastrack\Pages\PageRepository;
use Fastrack\Properties\RoomRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class CompendiumController extends Controller
{

    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var VenueRepository
     */
    private $venueRepository;
    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var RoomRepository
     */
    private $roomRepository;

    public function __construct(PageRepository $pageRepository,
                                VenueRepository $venueRepository,
                                EventRepository $eventRepository,
                                RoomRepository $roomRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->venueRepository = $venueRepository;
        $this->eventRepository = $eventRepository;
        $this->roomRepository = $roomRepository;
    }

    public function page($uuid)
    {
        $content = $this->pageRepository->get($uuid);

        // If this is the homepage, then bail
        if (class_basename(site()->homepage) == 'Page' && site()->homepage->id == $content->id) {
            return redirect('/', 301);
        }

        if ($content->template == '_raw') {
            return response($content->getExcerpt(false), 200, ['Content-Type' => 'text/plain']);
        }

        return theme($content->template, compact('content'));
    }

    public function place($uuid)
    {
        $content = $this->venueRepository->get($uuid);

        return theme('venue', compact('content'));
    }

    public function event($uuid)
    {
        $content = $this->eventRepository->get($uuid);

        return theme('calendar_event', compact('content'));
    }

    public function property($uuid)
    {

    }

    public function room($uuid)
    {
        $content = $this->roomRepository->get($uuid);

        return theme('page', compact('content'));
    }

    public function article($uuid)
    {

    }
}
