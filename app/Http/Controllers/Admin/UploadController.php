<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Commands\ExtractColour;
use Fastrack\Content\CustomField;
use Fastrack\Files\File;
use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Images\Image;
use Fastrack\Media\Library;
use Fastrack\Properties\Amenity;
use Fastrack\Sites\SettingRepository;
use Fastrack\Slideshows\SlideRepository;
use Fastrack\Slideshows\SlideshowRepository;
use Illuminate\Http\Request;
use Img;
use Storage;

/**
 * Class UploadController
 * @package Fastrack\Http\Controllers\Admin
 */
class UploadController extends Controller
{

    /**
     * @var SlideshowRepository
     */
    private $slideshowRepository;
    /**
     * @var SlideRepository
     */
    private $slideRepository;

    /**
     * @var SettingRepository
     */
    private $settingRepository;


    function __construct(
        SlideshowRepository $slideshowRepository,
        SlideRepository $slideRepository,
        SettingRepository $settingRepository
    ) {
        $this->slideshowRepository = $slideshowRepository;
        $this->slideRepository = $slideRepository;
        $this->settingRepository = $settingRepository;
    }

    public function storeMedia(Request $request)
    {
        if ($file = $request->file('file')) {
            $folder = md5(date('Ymd'));

            $file_name = uniqid('', true) . '.' . $file->getClientOriginalExtension();

            try {
                Storage::put($folder . '/' . $file_name, file_get_contents($file->getRealPath()));

                // Get image meta
                $img = Img::make($file->getRealPath())->orientate();

                // Find the default image library
                if($libraryId = $request->get('library'))
                {
                    $library = Library::whereUuid($libraryId)->first();
                } else
                {
                    $library = Library::whereDefault(true)->first();
                }

                $image = Image::create([
                    'library_id'    => $library->id,
                    'original_name' => $file->getClientOriginalName(),
                    'file_name'     => $file_name,
                    'path'          => $folder,
                    'mime_type'     => $file->getMimeType(),
                    'size'          => $file->getSize(),
                    'width'         => $img->width(),
                    'height'        => $img->height()
                ]);

                if ($exif = $img->exif()) {
                    $exif['FileName'] = $file->getClientOriginalName();
                    $image->exif = serialize($exif);
                    $image->save();
                }

                $returnData = ['uuid' => (string) $image->uuid, 'library' => $library->toArray()];

                if ($params = $request->url_params) {
                    $returnData['path'] = $image->getPath(unserialize($params));
                }

                $this->dispatch(
                    new ExtractColour($image)
                );

                if ($request->ajax()) {
                    return response()->json($returnData, 200);
                }

            } catch (\Exception $e) {
                die($e->getMessage());
            }

            return redirect()->back();
        }
    }

    public function storeFile(Request $request)
    {
        if ($file = $request->file('file')) {
            $folder = md5(date('Ymd'));

            $file_name = uniqid('', true) . '.' . $file->getClientOriginalExtension();

            try {
                Storage::put($folder . '/' . $file_name, file_get_contents($file->getRealPath()));

                $file = File::create([
                    'original_name' => $file->getClientOriginalName(),
                    'file_name'     => $file_name,
                    'path'          => $folder,
                    'mime_type'     => $file->getMimeType(),
                    'size'          => $file->getSize()
                ]);

                // if this is being added to an existing custom field
                //try {
                //    $customField = CustomField::find($request->get('id'));
                //
                //    $customField->type = 'file';
                //    $customField->value = null;
                //    $customField->file_id = $file->id;
                //    $customField->save();
                //} catch (\Exception $exception) {
                //    // just continue
                //}

                $returnData = [
                    'uuid'         => (string) $file->uuid,
                    'name'       => $file->original_name,
                    'mime_class' => mimeIconClass($file->mime_type),
                    'size'       => byte_format($file->size)
                ];

                if ($request->ajax()) {
                    return response()->json($returnData, 200);
                }

            } catch (\Exception $e) {
                die($e->getMessage());
            }

            return redirect()->back();
        }
    }



    public function generateTag(Request $request)
    {
        if($uuid = $request->get('uuid'))
        {
            $image = Image::whereUuid($uuid)->firstOrFail();

            $params = [];

            if($w = $request->get('width')) $params['w'] = $w;
            if($h = $request->get('height')) $params['h'] = $h;

            $params['fit'] = $request->get('fit', 'fill');

            $additional = parse_str($request->get('additional'));

            if( ! empty($additional)) $params = array_merge($additional, $params);

            // do something with the additional fields...
            return $image->getTag($params);
        }
    }




    /**
     * @param Request $request
     * @param $uploadHash
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    //public function storeImage(Request $request, $uploadHash)
    //{
    //    if ($file = $request->file('file')) {
    //        $folder = md5(date('Ymd'));
    //
    //        $file_name = uniqid('', true) . '.' . $file->getClientOriginalExtension();
    //
    //        try {
    //            Storage::put($folder . '/' . $file_name, file_get_contents($file->getRealPath()));
    //
    //            // Get image meta
    //            $img = Img::make($file->getRealPath())->orientate();
    //
    //            $image = Image::create([
    //                'original_name' => $file->getClientOriginalName(),
    //                'file_name'     => $file_name,
    //                'path'          => $folder,
    //                'mime_type'     => $file->getMimeType(),
    //                'size'          => $file->getSize(),
    //                'width'         => $img->width(),
    //                'height'        => $img->height(),
    //                'upload_hash'   => $uploadHash
    //            ]);
    //
    //            if ($exif = $img->exif()) {
    //                $exif['FileName'] = $file->getClientOriginalName();
    //                $image->exif = serialize($exif);
    //                $image->save();
    //            }
    //
    //            $returnData = ['id' => $image->id];
    //
    //            if ($params = $request->url_params) {
    //                $returnData['path'] = $image->getPath(unserialize($params));
    //            }
    //
    //            if ($target = $request->get('target')) {
    //                if($target == 'amenity')
    //                {
    //                    $amenity = Amenity::find($request->get('id'));
    //
    //                    $amenity->image_id = $image->id;
    //
    //                    $amenity->save();
    //                } elseif ($target == 'setting' && $key = $request->get('targetKey')) {
    //                    $model = $request->get('model');
    //                    $m = $model::findOrFail($request->get('modelId'));
    //                    $this->settingRepository->updateImageSetting($m, $key, $image->id);
    //                } elseif ($model = $request->get('model')) {
    //                    $m = $model::findOrFail($request->get('id'));
    //
    //                    if ($target == 'content') {
    //                        $m->$target->image_id = $image->id;
    //                        $m->$target->save();
    //                    } elseif ($target == 'gallery') {
    //                        $this->galleryRepository->addImage($m->id, $m->site_id, $image->id);
    //                    } elseif ($target == 'slideshow') {
    //                        if ( ! $m->slideshow()->count()) {
    //                            $slideshow = $this->slideshowRepository->create($m->name);
    //                            $m->slideshow()->associate($slideshow);
    //                            $m->save();
    //                        } else {
    //                            $slideshow = $m->slideshow;
    //                        }
    //
    //                        // Create the slide
    //                        $slide = $this->slideRepository->create($image, $slideshow);
    //
    //                        $returnData['slide_id'] = $slide->id;
    //                    }
    //                }
    //
    //                if ($target == 'customField' && $id = $request->get('id')) {
    //                    // Get the custom field
    //                    // access target_id
    //                    try {
    //                        $customField = CustomField::find($id);
    //
    //                        $customField->type = 'image';
    //                        $customField->value = null;
    //                        $customField->image_id = $image->id;
    //                        $customField->save();
    //                    } catch (\Exception $exception) {
    //                        // just continue
    //                    }
    //                }
    //            }
    //
    //            // Raise an event to extract the colour from the image
    //            // -------
    //            //
    //            $this->dispatch(
    //                new ExtractColour($image)
    //            );
    //
    //            if ($request->ajax()) {
    //                return response()->json($returnData, 200);
    //            }
    //
    //        } catch (\Exception $e) {
    //            die($e->getMessage());
    //        }
    //
    //        return redirect()->back();
    //    }
    //}

    //public function storeFile(Request $request, $uploadHash)
    //{
    //    if ($file = $request->file('file')) {
    //        $folder = md5(date('Ymd'));
    //
    //        $file_name = uniqid('', true) . '.' . $file->getClientOriginalExtension();
    //
    //        try {
    //            Storage::put($folder . '/' . $file_name, file_get_contents($file->getRealPath()));
    //
    //            $file = File::create([
    //                'original_name' => $file->getClientOriginalName(),
    //                'file_name'     => $file_name,
    //                'path'          => $folder,
    //                'mime_type'     => $file->getMimeType(),
    //                'size'          => $file->getSize(),
    //                'upload_hash'   => $uploadHash
    //            ]);
    //
    //            // if this is being added to an existing custom field
    //            try {
    //                $customField = CustomField::find($request->get('id'));
    //
    //                $customField->type = 'file';
    //                $customField->value = null;
    //                $customField->file_id = $file->id;
    //                $customField->save();
    //            } catch (\Exception $exception) {
    //                // just continue
    //            }
    //
    //            $returnData = [
    //                'id'         => $file->id,
    //                'name'       => $file->original_name,
    //                'mime_class' => mimeIconClass($file->mime_type),
    //                'size'       => byte_format($file->size)
    //            ];
    //
    //            if ($request->ajax()) {
    //                return response()->json($returnData, 200);
    //            }
    //
    //        } catch (\Exception $e) {
    //            die($e->getMessage());
    //        }
    //
    //        return redirect()->back();
    //    }
    //}

    public function destroy(Request $request)
    {

        if ($request->get('target') == 'setting') {
            $model = $request->get('model');
            $m = $model::findOrFail($request->get('model_id'));

            if ($request->get('type') == 'image') {
                $this->settingRepository->updateImageSetting($m, $request->get('target_key'), 0);
            }
        } elseif ($request->get('id')) {
            $field = $request->get('type') . '_id';


            if ($target = $request->get('target')) {

                if ($model = $request->get('model')) {
                    $m = $model::findOrFail($request->get('model_id'));
                }

                if ($target == 'content') {
                    $m->$target->$field = 0;
                    $m->$target->save();
                } elseif ($target == 'gallery') {
                    //$this->galleryRepository->addImage($m->id, $m->site_id, $image->id);
                } elseif ($target == 'slideshow') {
                    //if( ! $m->slideshow()->count() )
                    //{
                    //    $slideshow = $this->slideshowRepository->create($m->name);
                    //    $m->slideshow()->associate($slideshow);
                    //    $m->save();
                    //} else
                    //{
                    //    $slideshow = $m->slideshow;
                    //}


                } elseif ($target == 'customField' && $targetId = $request->get('target_id')) {
                    // Get the custom field
                    // access target_id
                    try {
                        $customField = CustomField::find($targetId);

                        $customField->$field = 0;
                        $customField->save();
                    } catch (\Exception $exception) {
                        // just continue
                    }
                }
            }
        }
    }

}
