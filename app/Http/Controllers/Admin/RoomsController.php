<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreateRoomRequest;
use Fastrack\Http\Requests\UpdateRoomRequest;
use Fastrack\Integrations\IntegrationRepository;
use Fastrack\Properties\PropertyRepository;
use Fastrack\Properties\RoomRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Slideshows\SlideRepository;
use Illuminate\Http\Request;
use Fastrack\Slideshows\Slideshow;

/**
 * Class RoomsController
 * @package Fastrack\Http\Controllers\Admin
 */
class RoomsController extends Controller
{

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var RoomRepository
     */
    private $roomRepository;
    /**
     * @var SlideRepository
     */
    private $slideRepository;
    /**
     * @var IntegrationRepository
     */
    private $integrationRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    function __construct(
        PropertyRepository $propertyRepository,
        RoomRepository $roomRepository,
        SlideRepository $slideRepository,
        IntegrationRepository $integrationRepository,
        SettingRepository $settingRepository
    ) {
        $this->propertyRepository = $propertyRepository;
        $this->roomRepository = $roomRepository;
        $this->slideRepository = $slideRepository;
        $this->integrationRepository = $integrationRepository;
        $this->settingRepository = $settingRepository;
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function index($propertyId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $rooms = $property->rooms;

        return view('admin.rooms.index', compact('property', 'rooms'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     */
    public function updateSorting(Request $request, $propertyId)
    {
        // Check validity of site
        $property = $this->propertyRepository->getById($propertyId);

        $this->roomRepository->sortRooms($request->all(), $propertyId);
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function create($propertyId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        return view('admin.rooms.create', compact('property', 'uploadHash'));
    }

    /**
     * @param CreateRoomRequest $request
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRoomRequest $request, $propertyId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $room = $this->roomRepository->create($request->all(), $propertyId);

        flash()->success($property->present()->roomNoun() . ' \'' . $room->name . '\' created!');

        return redirect()->route('rooms_path', $propertyId);
    }

    /**
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\View\View
     */
    public function edit($propertyId, $roomId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $model = $this->roomRepository->getById($roomId, $propertyId);

        return view('admin.rooms.edit', compact('property', 'model'));
    }

    /**
     * @param UpdateRoomRequest $request
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRoomRequest $request, $propertyId, $roomId)
    {
        $formData = $request->all();

        $formData['active'] = $request->has('active');

        $this->roomRepository->update($roomId, $formData, $propertyId);

        $property = $this->propertyRepository->getById($propertyId);

        flash()->success($property->present()->roomNoun() . ' updated!');

        return redirect()->back();
    }

    /**
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\View\View
     */
    public function content($propertyId, $roomId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $model = $this->roomRepository->getById($roomId, $propertyId);
        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        $settings = $this->settingRepository->gatherSettings('rooms');

        return view('admin.rooms.content', compact('property', 'model', 'uploadHash', 'settings'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateContent(Request $request, $propertyId, $roomId)
    {
        $this->roomRepository->update($roomId, $request->all(), $propertyId);

        $this->settingRepository->updateAll($this->roomRepository->getById($roomId, $propertyId),
                                            $request->get('setting'));

        $property = $this->propertyRepository->getById($propertyId);

        flash()->success($property->present()->roomNoun() . ' updated!');

        return redirect()->back();
    }

    /**
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\View\View
     */
    public function slideshow($propertyId, $roomId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $model = $this->roomRepository->getById($roomId, $propertyId);
        //$uploadHash = md5(uniqid('', true)); // Move this to a view composer?


        if( empty($model->slideshow_id))
        {
            $slideshow = Slideshow::create(['name' => $model->name]);

            $model->slideshow_id = $slideshow->id;

            $model->save();
        }

        return view('admin.rooms.slideshow', compact('property', 'model'));
    }

    //public function updateSlideshow(Request $request, $propertyId, $roomId)
    //{
    //    $this->slideRepository->updateMany($request->all());
    //
    //    flash()->success('Slides updated!');
    //
    //    return redirect()->back();
    //}

    /**
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\View\View
     */
    public function amenities($propertyId, $roomId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $model = $this->roomRepository->getById($roomId, $propertyId);
        //$amenities = $property->amenities;

        $amenities = $this->roomRepository->getAvailableAmenities($roomId, $propertyId);

        return view('admin.rooms.amenities', compact('property', 'model', 'amenities'));
    }


    /*
     * public function items($siteId, $navigationId)
    {
        $site = $this->siteRepository->getById($siteId);

        $navigation = $this->navigationRepository->getById($navigationId, $siteId);

        // Get all of the usable items
        $pages = $this->navigationRepository->getAvailablePages($navigationId, $siteId);

        return view('admin.navigations.items', compact('site', 'navigation', 'pages'));
    }
     */

    /**
     * @param Request $request
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAmenities(Request $request, $propertyId, $roomId)
    {
        $this->roomRepository->updateAmenities($roomId, $request->all(), $propertyId);

        flash()->success('Features updated');

        return redirect()->back();
    }

    public function removeAmenity(Request $request, $propertyId, $roomId)
    {
        $this->roomRepository->removeAmenity($roomId, $request->get('id'), $propertyId);

        if ($request->ajax()) {
            return response()->json(true, 200);
        }

        return redirect()->back();
    }

    /**
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\View\View
     */
    public function seo($propertyId, $roomId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $model = $this->roomRepository->getById($roomId, $propertyId);

        return view('admin.rooms.seo', compact('property', 'model'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSeo(Request $request, $propertyId, $roomId)
    {
        $room = $this->roomRepository->getById($roomId, $propertyId);
        $this->roomRepository->updateSeo($room, $request->all());

        $property = $this->propertyRepository->getById($propertyId);

        flash()->success($property->present()->roomNoun() . ' SEO updated!');

        return redirect()->back();
    }


    public function related($roomUuid)
    {
        $model = $this->roomRepository->get($roomUuid);
        $property = $model->property;

        $rooms = $this->roomRepository->getUnrelatedRooms($model);

        return view('admin.rooms.related', compact('property', 'model', 'rooms'));
    }

    public function updateRelated(Request $request, $roomUuid)
    {
        $this->roomRepository->updateRelated($roomUuid, $request->all());

        flash()->success('Related rooms updated!');

        return redirect()->back();
    }

    public function destroyRelated(Request $request, $roomUuid)
    {
        return $this->roomRepository->unrelateRoom($roomUuid, $request->get('id'));
    }


    public function integrations($propertyId, $roomId)
    {
        $property = $this->propertyRepository->get($propertyId);
        $model = $this->roomRepository->get($roomId);

        return view('admin.rooms.integration.index', compact('property', 'model'));
    }

    public function addIntegration($propertyId, $roomId)
    {
        $property = $this->propertyRepository->get($propertyId);
        $model = $this->roomRepository->get($roomId);
        $integrations = $this->integrationRepository->getAll();

        return view('admin.rooms.integration.add', compact('property', 'model', 'integrations'));
    }

    public function storeIntegration(Request $request, $propertyId, $roomId)
    {
        $room = $this->roomRepository->get($roomId);
        $this->integrationRepository->attach($room, $request->all());

        flash()->success('Integration added');

        return redirect()->route('room_integration_path', [$propertyId, $roomId]);
    }

    public function editIntegration($propertyId, $roomId, $integrationId)
    {
        $property = $this->propertyRepository->get($propertyId);
        $model = $this->roomRepository->get($roomId);

        foreach($model->integrations as $integration)
        {
            if($integration->uuid == $integrationId)
            {
                return view('admin.rooms.integration.edit', compact('property', 'model', 'integration'));
            };
        }
    }

    public function updateIntegration(Request $request, $propertyId, $roomId, $integrationId)
    {
        $room = $this->roomRepository->get($roomId);
        $this->integrationRepository->updateAttachment($integrationId, $room, $request->all());

        flash()->success('Integration updated');

        return redirect()->back();
    }



    /**
     * @param Request $request
     * @param $propertyId
     * @param $roomId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $propertyId, $roomId)
    {
        $this->roomRepository->destroy($roomId, $propertyId);

        $property = $this->propertyRepository->getById($propertyId);

        flash()->success($property->present()->roomNoun() . ' removed');

        return redirect()->route('rooms_path', $propertyId);
    }
}
