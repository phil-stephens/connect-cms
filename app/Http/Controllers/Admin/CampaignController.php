<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Campaigns\CampaignRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class CampaignController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * CampaignController constructor.
     */
    public function __construct(SiteRepository $siteRepository, CampaignRepository $campaignRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->campaignRepository = $campaignRepository;
    }

    public function index($siteUuid)
    {
        $site = $this->siteRepository->get($siteUuid);
        $campaigns = $site->campaigns;

        return view('admin.campaign.index', compact('site', 'campaigns'));
    }

    public function create($siteUuid)
    {
        $site = $this->siteRepository->get($siteUuid);

        return view('admin.campaign.create', compact('site'));
    }

    public function store(Request $request, $siteUuid)
    {
        $this->campaignRepository->create($request->all(), $siteUuid);

        flash()->success('Campaign Created');

        return redirect()->route('campaigns_path', $siteUuid);
    }

    public function edit($campaignUuid)
    {
        $campaign = $this->campaignRepository->get($campaignUuid);
        $site = $campaign->site;

        return view('admin.campaign.edit', compact('site', 'campaign'));
    }

    public function update(Request $request, $campaignUuid)
    {
        $this->campaignRepository->update($campaignUuid, $request->all());

        flash()->success('Campaign Updated');

        return redirect()->back();
    }
}
