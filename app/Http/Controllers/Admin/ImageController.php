<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Images\ImageRepository;
use Fastrack\Media\LibraryRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class ImageController extends Controller
{

    /**
     * @var ImageRepository
     */
    private $imageRepository;
    /**
     * @var LibraryRepository
     */
    private $libraryRepository;

    /**
     * ImageController constructor.
     * @param ImageRepository $imageRepository
     */
    public function __construct(ImageRepository $imageRepository, LibraryRepository $libraryRepository)
    {
        $this->imageRepository = $imageRepository;
        $this->libraryRepository = $libraryRepository;
    }

    public function edit($uuid)
    {
        $image = $this->imageRepository->get($uuid);
        $libraries = $this->libraryRepository->getAll();

        return view('admin.image.edit', compact('image', 'libraries'));
    }

    public function update(Request $request, $uuid)
    {
        $this->imageRepository->update($uuid, $request->all());

        flash()->success('Image updated');

        return redirect()->back();
    }
}
