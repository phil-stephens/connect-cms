<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Calendars\EventRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;
use Fastrack\Calendars\TicketPurchase;
use Fastrack\Calendars\Ticket;

class TicketController extends Controller
{

    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var SiteRepository
     */
    private $siteRepository;

    /**
     * TicketController constructor.
     */
    public function __construct(EventRepository $eventRepository, SiteRepository $siteRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->siteRepository = $siteRepository;
    }

    public function index($eventId)
    {
        $event = $this->eventRepository->get($eventId);

        $sites = $this->siteRepository->getAll()->lists('name', 'id')->all();

        return view('admin.ticket.index', compact('event', 'sites'));
    }

    public function email(Request $request, $eventUuid)
    {
        $purchase = TicketPurchase::whereUuid($request->get('purchase_uuid'))->firstOrFail();

        $event = $this->eventRepository->get($eventUuid);

        $this->sendEmail($purchase, $event, $request);

        flash()->success('Purchase information sent');

        return redirect()->back();
    }

    public function create($eventId)
    {
        $event = $this->eventRepository->get($eventId);

        $sites = $this->siteRepository->getAll()->lists('name', 'id')->all();

        return view('admin.ticket.create', compact('event', 'sites'));
    }

    public function store(Request $request, $eventId)
    {
        $event = $this->eventRepository->get($eventId);

        $purchase = new TicketPurchase();

        $purchase->fill($request->all());

        //$purchase->transaction_id = str_random();

        $purchase->save();

        foreach ($request->get('ticket') as $ticket) {
            $t = new Ticket();

            $t->fill($ticket);

            $t->ticket_purchase_id = $purchase->id;

            $t->save();

            // Add details to mailing list (if not the same as the purchaser details)
        }

        //if($request->get('site_id') != site('id'))
        //{
        //    $currentSite = site('id');
        //
        //    \App::singleton('site', function () use ($request) {
        //        try {
        //            return $this->siteRepository->getById($request->get('site_id'));
        //        } catch (\Exception $e) {
        //            return false;
        //        }
        //    });
        //}
        //
        //\Mail::send('emails.registered', compact('purchase', 'event'), function ($message) use ($purchase) {
        //    $message->to($purchase->email,
        //        $purchase->name)->subject('Thank you for registering with Maximum Occupancy 2015');
        //});
        //
        //if( isset($currentSite))
        //{
        //    \App::singleton('site', function () use ($currentSite) {
        //        try {
        //            return $this->siteRepository->getById($currentSite);
        //        } catch (\Exception $e) {
        //            return false;
        //        }
        //    });
        //}

        $this->sendEmail($purchase, $event, $request);

        flash()->success('Ticket Purchase created');

        return redirect()->route('boxoffice_path', $event->uuid);
    }

    private function sendEmail($purchase, $event, $request)
    {



        if($request->get('site_id') != site('id'))
        {
            $currentSite = site('id');

            \App::singleton('site', function () use ($request) {
                try {
                    return $this->siteRepository->getById($request->get('site_id'));
                } catch (\Exception $e) {
                    return false;
                }
            });
        }

        \Mail::send('emails.registered', compact('purchase', 'event'), function ($message) use ($purchase, $request) {
            $to = ($request->has('email_to')) ? $request->get('email_to') : $purchase->email;

            $message->to($to,
                $purchase->name)->subject('Thank you for registering with Maximum Occupancy 2015');
        });

        if( isset($currentSite))
        {
            \App::singleton('site', function () use ($currentSite) {
                try {
                    return $this->siteRepository->getById($currentSite);
                } catch (\Exception $e) {
                    return false;
                }
            });
        }
    }
}
