<?php

namespace Fastrack\Http\Controllers\Admin\Settings;

use Fastrack\Locations\CategoryRepository;
use Illuminate\Http\Request;

use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class CategoryController extends Controller
{
    // Categories - move this into it's own repo/controller
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        //$categories = $this->venueRepository->getCategories();
        $categories = $this->categoryRepository->getAll();

        return view('admin.settings.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.settings.categories.create');
    }

    public function store(Request $request)
    {
        //$this->venueRepository->createCategory($request->all());

        $this->categoryRepository->create($request->all());

        flash()->success('Category created!');

        return redirect()->route('venue_categories_path');
    }

    public function edit($categoryId)
    {
        $category = $this->categoryRepository->getById($categoryId);
        $category->load('content');
        //$category = $this->venueRepository->getCategory($categoryId);

        return view('admin.settings.categories.edit', compact('category'));
    }

    public function update(Request $request, $categoryId)
    {
        // Bit hacky...
        $category = $this->categoryRepository->getById($categoryId);

        $this->categoryRepository->update($category->uuid, $request->all());
        //$this->venueRepository->updateCategory($categoryId, $request->all());

        flash()->success('Category updated!');

        return redirect()->back();
    }

    public function destroy(Request $request, $categoryId)
    {
        $category = $this->categoryRepository->getById($categoryId);

        $this->categoryRepository->destroy($category->uuid);

        //$this->venueRepository->destroyCategory($categoryId, $request->all());

        flash()->success('Category removed!');

        return redirect()->route('venue_categories_path');
    }
}
