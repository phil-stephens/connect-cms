<?php

namespace Fastrack\Http\Controllers\Admin\Settings;

use Illuminate\Http\Request;

use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
        return redirect()->route('users_path');
    }
}
