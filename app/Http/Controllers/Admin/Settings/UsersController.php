<?php namespace Fastrack\Http\Controllers\Admin\Settings;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreateUserRequest;
use Fastrack\Http\Requests\UpdateUserRequest;
use Fastrack\Users\UserRepository;
use Illuminate\Http\Request;

/**
 * Class UsersController
 * @package Fastrack\Http\Controllers\Admin
 */
class UsersController extends Controller
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $users = $this->userRepository->getAll();

        return view('admin.settings.users.index', compact('users'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.settings.users.create');
    }

    /**
     * @param CreateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateUserRequest $request)
    {
        $this->userRepository->create($request->all());

        flash()->success('User created');

        return redirect()->route('users_path');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function editMe()
    {
        return $this->edit(\Auth::id());
    }

    /**
     * @param UpdateUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateMe(UpdateUserRequest $request)
    {
        $this->userRepository->update(\Auth::id(), $request->all());

        flash()->success('Profile updated');

        return redirect()->route('edit_me_path');
    }

    /**
     * @param $userId
     * @return \Illuminate\View\View
     */
    public function edit($userId)
    {
        $user = $this->userRepository->getById($userId);

        return view('admin.settings.users.edit', compact('user'));
    }

    /**
     * @param UpdateUserRequest $request
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $request, $userId)
    {
        $this->userRepository->update($userId, $request->all());

        flash()->success('User updated');

        return redirect()->route('edit_user_path', $userId);
    }

    /**
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $userId)
    {
        if ($userId != \Auth::id()) {
            $this->userRepository->destroy($userId);

            flash()->success('User removed');

            return redirect()->route('users_path');
        }

        return redirect()->back();
    }

}
