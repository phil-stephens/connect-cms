<?php

namespace Fastrack\Http\Controllers\Admin\Settings;

use Fastrack\Configurations\ConfigurationRepository;
use Illuminate\Http\Request;

use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class ConfigurationController extends Controller
{

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;



    public function __construct(ConfigurationRepository $configurationRepository)
    {
        $this->configurationRepository = $configurationRepository;
    }

    public function update(Request $request)
    {
        // this is for a single update via AJAX
        return $this->configurationRepository->updateSingle($request->get('key'), $request->get('value'));

    }
}
