<?php

namespace Fastrack\Http\Controllers\Admin\Settings;

use Fastrack\Integrations\IntegrationRepository;
use Fastrack\Jobs\CheckRates;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class IntegrationController extends Controller
{
    //
    /**
     * @var IntegrationRepository
     */
    private $integrationRepository;

    /**
     * IntegrationController constructor.
     */
    public function __construct(IntegrationRepository $integrationRepository)
    {
        $this->integrationRepository = $integrationRepository;
    }

    public function index()
    {
        $integrations = $this->integrationRepository->getAll();
        //
        return view('admin.settings.integration.index', compact('integrations'));

        //foreach($integrations as $i)
        //{
        //    $job = (new CheckRates($i));
        //
        //    $this->dispatch($job);
        //}

    }

    public function create()
    {
        $providers = [];

        $config = config('integrations.providers.rates');

        foreach($config as $provider)
        {
            $providers[$provider['type']] = $provider['name'];
        }

        return view('admin.settings.integration.create', compact('providers'));
    }

    public function store(Request $request)
    {
        $integration = $this->integrationRepository->create($request->all());

        flash()->success('Integration created');

        return redirect()->route('integrations_path');
    }

    public function edit($integrationId)
    {
        $integration = $this->integrationRepository->get($integrationId);

        $providers = [];

        $config = config('integrations.providers.rates');

        foreach($config as $provider)
        {
            $providers[$provider['type']] = $provider['name'];
        }

        return view('admin.settings.integration.edit', compact('integration', 'providers'));
    }

}
