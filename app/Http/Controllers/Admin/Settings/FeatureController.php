<?php namespace Fastrack\Http\Controllers\Admin\Settings;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreateAmenityRequest;
use Fastrack\Properties\AmenityRepository;
use Fastrack\Properties\PropertyRepository;
use Illuminate\Http\Request;


class FeatureController extends Controller
{

    /**
     * @var AmenityRepository
     */
    private $amenityRepository;

    /**
     * @param AmenityRepository $amenityRepository
     */
    function __construct(AmenityRepository $amenityRepository)
    {
        $this->amenityRepository = $amenityRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $amenities = $this->amenityRepository->getAccount();

        return view('admin.settings.feature.index', compact('amenities'));
    }

    public function create()
    {
        return view('admin.settings.feature.create');
    }

    public function store(Request $request)
    {
        $this->amenityRepository->create($request->all());

        flash()->success('New feature created');

        return redirect()->route('account_amenities_path');
    }

    public function edit($uuid)
    {
        $amenity = $this->amenityRepository->get($uuid);

        return view('admin.settings.feature.edit', compact('amenity'));

    }

    public function update(Request $request, $uuid)
    {
        $this->amenityRepository->updateByUuid($uuid, $request->all());

        flash()->success('Feature updated');

        return redirect()->back();
    }

    public function destroy(Request $request, $uuid)
    {
        $this->amenityRepository->destroyByUuid($uuid);

        flash()->success('Feature removed');

        return redirect()->route('account_amenities_path');
    }
}
