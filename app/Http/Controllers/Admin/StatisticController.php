<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Properties\PropertyRepository;
use Fastrack\Properties\StatisticRepository;
use Illuminate\Http\Request;

use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class StatisticController extends Controller
{

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var StatisticRepository
     */
    private $statisticRepository;

    public function __construct(PropertyRepository $propertyRepository, StatisticRepository $statisticRepository)
    {
        $this->propertyRepository = $propertyRepository;
        $this->statisticRepository = $statisticRepository;
    }

    public function index($uuid)
    {
        $model = $this->propertyRepository->get($uuid);
        $statistics = $model->statistics;

        return view('admin.statistic.index', compact('model', 'statistics'));

    }

    public function create($uuid)
    {
        $model = $this->propertyRepository->get($uuid);

        return view('admin.statistic.create', compact('model'));
    }

    public function store(Request $request, $uuid)
    {
        $this->statisticRepository->create($request->all(), $uuid);

        flash()->success('Statistic created');

        return redirect()->route('statistics_path', $uuid);
    }


    public function edit($uuid)
    {
        $statistic = $this->statisticRepository->get($uuid);
        $model = $statistic->property;


        return view('admin.statistic.edit', compact('model', 'statistic'));

    }

    public function update(Request $request, $uuid)
    {
        $this->statisticRepository->update($uuid, $request->all());

        flash()->success('Statistic updated');

        return redirect()->back();
    }

    public function destroy(Request $request, $uuid)
    {
        //$property = $this->statisticRepository->destroy($uuid);
        $this->statisticRepository->destroy($request->get('uuid'));


        flash()->success('Statistic removed');

        //return redirect()->route('statistics_path', $property->uuid);
        return redirect()->route('statistics_path', $uuid);
    }
}
