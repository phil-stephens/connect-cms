<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Atoms\AtomRepository;
use Fastrack\Sites\Site;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class AtomController extends Controller
{

    /**
     * @var AtomRepository
     */
    private $atomRepository;

    /**
     * AtomController constructor.
     */
    public function __construct(AtomRepository $atomRepository)
    {

        $this->atomRepository = $atomRepository;
    }

    public function edit($uuid)
    {
        // $uuid is the content UUID
        $content = $this->atomRepository->getContent($uuid);

        // Assemble the atom list from the manifest...
        $templates = $this->atomRepository->gatherTemplates();

        $select = [];

        foreach($templates as $template)
        {
            $select[$template->key] = $template->name;
        }

        return view('admin.atom.edit', compact('content', 'templates', 'select'));
    }

    public function update(Request $request, $uuid)
    {
        if($request->has('atom'))
        {

            //$content = $this->atomRepository->getContent($uuid);

            $this->atomRepository->update($uuid, $request->get('atom'));
            //dd($request->get('atom'));

            flash()->success('Atoms updated');
        }

        return redirect()->back();
    }
}
