<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreatePropertyRequest;
use Fastrack\Http\Requests\UpdatePropertyRequest;
use Fastrack\Integrations\IntegrationRepository;
use Fastrack\Properties\PropertyRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Sites\Site;
use Fastrack\Slideshows\SlideRepository;
use Illuminate\Http\Request;
use Fastrack\Slideshows\Slideshow;

/**
 * Class PropertiesController
 * @package Fastrack\Http\Controllers\Admin
 */
class PropertiesController extends Controller
{

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var SlideRepository
     */
    private $slideRepository;
    /**
     * @var IntegrationRepository
     */
    private $integrationRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @param PropertyRepository $propertyRepository
     * @param SlideRepository $slideRepository
     */
    function __construct(PropertyRepository $propertyRepository,
                        SlideRepository $slideRepository,
                        IntegrationRepository $integrationRepository,
                        SettingRepository $settingRepository)
    {
        $this->propertyRepository = $propertyRepository;
        $this->slideRepository = $slideRepository;
        $this->integrationRepository = $integrationRepository;
        $this->settingRepository = $settingRepository;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $properties = $this->propertyRepository->getAll();

        if (count($properties) == 1) {
            \Session::reflash();

            return redirect()->route('property_path', $properties[0]->id);
        }

        return view('admin.properties.index', compact('properties'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        return view('admin.properties.create', compact('uploadHash'));
    }

    /**
     * @param CreatePropertyRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePropertyRequest $request)
    {
        $property = $this->propertyRepository->create($request->all());

        flash()->success('Property \'' . $property->name . '\' create!');

        return redirect()->route('properties_path');
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function edit($propertyId)
    {
        $property = $model = $this->propertyRepository->getById($propertyId);

        return view('admin.properties.edit', compact('property', 'model'));
    }

    /**
     * @param UpdatePropertyRequest $request
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePropertyRequest $request, $propertyId)
    {
        $this->propertyRepository->update($propertyId, $request->all());

        flash()->success('Property updated!');

        return redirect()->back();
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function content($propertyId)
    {
        $model = $this->propertyRepository->getById($propertyId);
        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        $settings = $this->settingRepository->gatherSettings('properties');

        return view('admin.properties.content', compact('model', 'uploadHash', 'settings'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateContent(Request $request, $propertyId)
    {
        $this->propertyRepository->update($propertyId, $request->all());

        $this->settingRepository->updateAll($this->propertyRepository->getById($propertyId),
                                            $request->get('setting'));

        flash()->success('Property updated!');

        return redirect()->back();
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function location($propertyId)
    {
        $model = $this->propertyRepository->getById($propertyId);

        return view('admin.properties.location', compact('model'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateLocation(Request $request, $propertyId)
    {
        $this->propertyRepository->update($propertyId, $request->all());

        flash()->success('Property updated!');

        return redirect()->back();
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function slideshow($propertyId)
    {
        $model = $this->propertyRepository->getById($propertyId);

        if( empty($model->slideshow_id))
        {
            $slideshow = Slideshow::create(['name' => $model->name]);

            $model->slideshow_id = $slideshow->id;

            $model->save();
        }

        return view('admin.properties.slideshow', compact('model'));
    }

    public function amenities($propertyId)
    {
        $model = $this->propertyRepository->getById($propertyId);

        $amenities = $this->propertyRepository->getAvailableAmenities($propertyId);

        return view('admin.properties.amenities', compact('model', 'amenities'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateAmenities(Request $request, $propertyId)
    {
        $this->propertyRepository->updateAmenities($propertyId, $request->all());

        flash()->success('Features updated');

        return redirect()->back();
    }

    public function removeAmenity(Request $request, $propertyId)
    {
        $this->propertyRepository->removeAmenity($propertyId, $request->get('id'));

        if ($request->ajax()) {
            return response()->json(true, 200);
        }

        return redirect()->back();
    }

    public function statistics($propertyUuid)
    {
        $model = $this->propertyRepository->get($propertyUuid);

        return view('admin.properties.statistics', compact('model'));

    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function seo($propertyId)
    {
        $model = $this->propertyRepository->getById($propertyId);

        $sites = Site::get()->lists('name', 'id')->all();


        return view('admin.properties.seo', compact('model', 'sites'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSeo(Request $request, $propertyId)
    {
        $property = $this->propertyRepository->getById($propertyId);;
        $this->propertyRepository->updateSeo($property, $request->all());

        flash()->success('Property SEO updated!');

        return redirect()->back();
    }



    public function integrations($propertyId)
    {
        $model = $this->propertyRepository->getById($propertyId);

        return view('admin.properties.integration.index', compact('model'));
    }

    public function addIntegration($propertyId)
    {
        $model = $this->propertyRepository->get($propertyId);
        $integrations = $this->integrationRepository->getAll();

        return view('admin.properties.integration.add', compact('model', 'integrations'));
    }

    public function storeIntegration(Request $request, $propertyId)
    {
        $property = $this->propertyRepository->get($propertyId);
        $this->integrationRepository->attach($property, $request->all());

        flash()->success('Integration added');

        return redirect()->route('property_integration_path', $property->id);
    }

    public function editIntegration($propertyId, $integrationId)
    {
        $model = $this->propertyRepository->get($propertyId);

        foreach($model->integrations as $integration)
        {
            if($integration->uuid == $integrationId)
            {
                return view('admin.properties.integration.edit', compact('model', 'integration'));
            };
        }
    }

    public function updateIntegration(Request $request, $propertyId, $integrationId)
    {
        $property = $this->propertyRepository->get($propertyId);
        $this->integrationRepository->updateAttachment($integrationId, $property, $request->all());

        flash()->success('Integration updated');

        return redirect()->back();
    }

    public function testIntegration($propertyId, $integrationId)
    {
        $model = $this->propertyRepository->get($propertyId);

        foreach($model->integrations as $integration)
        {
            if($integration->uuid == $integrationId)
            {
                //dd($integration);
                $type = $integration->type;

                $provider = new $type($integration);

                $rooms = $provider->getRoomIds($integration->pivot->client_id);

                return view('admin.properties.integration.test', compact('model', 'rooms', 'integration'));
            };
        }
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $propertyId)
    {
        $this->propertyRepository->destroy($propertyId);

        flash()->success('Property removed');

        return redirect()->route('properties_path');
    }

}
