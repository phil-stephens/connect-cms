<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Properties\PropertyRepository;
use Fastrack\Sites\SiteRepository;

/**
 * Class DashboardController
 * @package Fastrack\Http\Controllers\Admin
 */
class DashboardController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;

    /**
     * @param SiteRepository $siteRepository
     * @param PropertyRepository $propertyRepository
     */
    function __construct(SiteRepository $siteRepository, PropertyRepository $propertyRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $sites = $this->siteRepository->getAll();

        return view('admin.dashboard.index', compact('sites'));
    }

    /**
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function site($siteId)
    {
        return redirect()->route('edit_site_path', $siteId);

        $site = $this->siteRepository->getById($siteId);

        return view('admin.dashboard.site', compact('site'));
    }

    /**
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function property($propertyId)
    {
        return redirect()->route('edit_property_path', $propertyId);

        $property = $this->propertyRepository->getById($propertyId);

        return view('admin.dashboard.property', compact('property'));
    }
}
