<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Sites\SiteRepository;
use Fastrack\Sites\TaxonomyRepository;
use Illuminate\Http\Request;

/**
 * Class TaxonomyController
 * @package Fastrack\Http\Controllers\Admin
 */
class TaxonomyController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var TaxonomyRepository
     */
    private $taxonomyRepository;

    /**
     * @param SiteRepository $siteRepository
     * @param TaxonomyRepository $taxonomyRepository
     */
    function __construct(SiteRepository $siteRepository, TaxonomyRepository $taxonomyRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->taxonomyRepository = $taxonomyRepository;
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function edit($siteId)
    {
        $site = $this->siteRepository->getById($siteId);
        $taxonomies = $this->taxonomyRepository->getAllForSite($siteId);

        return view('admin.taxonomies.edit', compact('site', 'taxonomies'));
    }

    /**
     * @param Request $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $siteId)
    {
        $this->taxonomyRepository->update($request->all(), $siteId);

        flash()->success('Taxonomy settings updated');

        return redirect()->back();
    }

}
