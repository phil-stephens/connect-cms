<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\News\ArticleRepository;
use Fastrack\News\FeedRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Slideshows\SlideRepository;
use Fastrack\Users\UserRepository;
use Illuminate\Http\Request;
use Fastrack\Slideshows\Slideshow;

/**
 * Class ArticlesController
 * @package Fastrack\Http\Controllers\Admin
 */
class ArticlesController extends Controller
{


    /**
     * @var ArticleRepository
     */
    private $articleRepository;
    /**
     * @var FeedRepository
     */
    private $feedRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var SlideRepository
     */
    private $slideRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;


    function __construct(
        ArticleRepository $articleRepository,
        FeedRepository $feedRepository,
        UserRepository $userRepository,
        SlideRepository $slideRepository,
        SettingRepository $settingRepository
    ) {
        $this->articleRepository = $articleRepository;
        $this->feedRepository = $feedRepository;
        $this->userRepository = $userRepository;
        $this->slideRepository = $slideRepository;
        $this->settingRepository = $settingRepository;
    }

    /**
     * @param $feedId
     * @return \Illuminate\View\View
     */
    public function index($feedId)
    {
        $feed = $this->feedRepository->getById($feedId);
        $articles = $feed->articles;

        return view('admin.news.articles.index', compact('feed', 'articles'));
    }

    /**
     * @param $feedId
     * @return \Illuminate\View\View
     */
    public function create($feedId)
    {
        $feed = $this->feedRepository->getById($feedId);
        $users = $this->userRepository->getAll();
        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        return view('admin.news.articles.create', compact('feed', 'users', 'uploadHash'));
    }

    /**
     * @param Request $request
     * @param $feedId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $feedId)
    {
        $this->articleRepository->create($request->all(), $feedId);

        flash()->success('Article created!');

        return redirect()->route('feed_path', $feedId);
    }

    /**
     * @param $feedId
     * @param $articleId
     * @return \Illuminate\View\View
     */
    public function edit($feedId, $articleId)
    {
        $feed = $this->feedRepository->getById($feedId);
        $model = $this->articleRepository->getById($articleId, $feedId);
        $users = $this->userRepository->getAll();

        return view('admin.news.articles.edit', compact('feed', 'users', 'model'));
    }

    /**
     * @param Request $request
     * @param $feedId
     * @param $articleId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $feedId, $articleId)
    {
        $formData = $request->all();

        $formData['draft'] = (isset($formData['draft']));

        $this->articleRepository->update($articleId, $formData, $feedId);

        flash()->success('Article updated!');

        return redirect()->back();
    }

    // @TODO
    // Needs Work
    // Won't work as expected since articles are outside of the scope of sites and themes
    //
    //public function settings($feedId, $articleId)
    //{
    //    //$site = $this->siteRepository->getById($siteId);
    //    //$model = $this->pageRepository->getById($pageId, $siteId);
    //
    //    $feed = $this->feedRepository->getById($feedId);
    //    $model = $this->articleRepository->getById($articleId, $feedId);
    //
    //    $settings = [];
    //
    //    try
    //    {
    //        // Get the settings skeleton from the manifest file
    //
    //        $manifest = file_get_contents(public_path('themes/'  . $site->theme . '/manifest.json'));
    //
    //        $manifest = json_decode($manifest)->templates;
    //
    //
    //        if(isset($manifest->settings))
    //        {
    //            $settings = array_merge($settings, $manifest->settings);
    //        }
    //
    //        $template = 'article'; //$model->template;
    //
    //        if(isset($manifest->files->$template))
    //        {
    //
    //            $settings = array_merge($settings, $manifest->files->$template);
    //        }
    //
    //    } catch( \Exception $e)
    //    {
    //
    //    }
    //
    //    return view('admin.news.articles.settings', compact('feed', 'model', 'settings'));
    //}
    //
    //public function updateSettings(Request $request, $feedId, $articleId)
    //{
    //    $article = $this->articleRepository->getById($articleId, $feedId);
    //
    //    $this->settingRepository->updateAll($article, $request->get('setting'));
    //
    //    flash()->success('Article settings updated');
    //
    //    return redirect()->back();
    //}

    /**
     * @param $feedId
     * @param $articleId
     * @return \Illuminate\View\View
     */
    public function content($feedId, $articleId)
    {
        $feed = $this->feedRepository->getById($feedId);
        $model = $this->articleRepository->getById($articleId, $feedId);
        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?
        $settings = $this->settingRepository->gatherSettings('articles');

        return view('admin.news.articles.content', compact('feed', 'model', 'uploadHash', 'settings'));
    }

    /**
     * @param Request $request
     * @param $feedId
     * @param $articleId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateContent(Request $request, $feedId, $articleId)
    {
        $this->articleRepository->update($articleId, $request->all(), $feedId);

        $this->settingRepository->updateAll($this->articleRepository->getById($articleId, $feedId),
                                            $request->get('setting'));

        flash()->success('Article updated!');

        return redirect()->back();
    }

    /**
     * @param $feedId
     * @param $articleId
     * @return \Illuminate\View\View
     */
    public function slideshow($feedId, $articleId)
    {
        $feed = $this->feedRepository->getById($feedId);
        $model = $this->articleRepository->getById($articleId, $feedId);
        //$uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        if( empty($model->slideshow_id))
        {
            $slideshow = Slideshow::create(['name' => $model->name]);

            $model->slideshow_id = $slideshow->id;

            $model->save();
        }

        return view('admin.news.articles.slideshow', compact('feed', 'model'));
    }

    //public function updateSlideshow(Request $request, $feedId, $articleId)
    //{
    //    $this->slideRepository->updateMany($request->all());
    //
    //    flash()->success('Slides updated!');
    //
    //    return redirect()->back();
    //}

    /**
     * @param $feedId
     * @param $articleId
     * @return \Illuminate\View\View
     */
    public function seo($feedId, $articleId)
    {
        $feed = $this->feedRepository->getById($feedId);
        $model = $this->articleRepository->getById($articleId, $feedId);

        return view('admin.news.articles.seo', compact('feed', 'model'));
    }

    /**
     * @param Request $request
     * @param $feedId
     * @param $articleId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSeo(Request $request, $feedId, $articleId)
    {
        $this->articleRepository->update($articleId, $request->all(), $feedId);

        flash()->success('Article updated!');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $feedId
     * @param $articleId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $feedId, $articleId)
    {
        $this->articleRepository->destroy($request->get('id'), $feedId);

        flash()->success('Article removed');

        return redirect()->route('feed_path', $feedId);

    }
}
