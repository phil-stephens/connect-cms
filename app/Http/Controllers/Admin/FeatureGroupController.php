<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Properties\PropertyRepository;
use Illuminate\Http\Request;

use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class FeatureGroupController extends Controller
{

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;

    /**
     * FeatureGroupController constructor.
     */
    public function __construct(PropertyRepository $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;
    }

    public function index($propertyUuid)
    {
        $property = $this->propertyRepository->get($propertyUuid);
        $groups = $property->featureGroups;

        return view('admin.feature.group.index', compact('property', 'groups'));

    }
}
