<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Offers\OfferRepository;
use Fastrack\Properties\PropertyRepository;
use Fastrack\Sites\SettingRepository;
use Illuminate\Http\Request;
use Fastrack\Slideshows\Slideshow;

/**
 * Class OffersController
 * @package Fastrack\Http\Controllers\Admin
 */
class OffersController extends Controller
{

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var OfferRepository
     */
    private $offerRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @param PropertyRepository $propertyRepository
     * @param OfferRepository $offerRepository
     */
    function __construct(PropertyRepository $propertyRepository, OfferRepository $offerRepository, SettingRepository $settingRepository)
    {
        $this->propertyRepository = $propertyRepository;
        $this->offerRepository = $offerRepository;
        $this->settingRepository = $settingRepository;
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function index($propertyId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $offers = $property->offers;

        return view('admin.offers.index', compact('property', 'offers'));
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function create($propertyId)
    {
        $property = $this->propertyRepository->getById($propertyId);

        return view('admin.offers.create', compact('property'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $propertyId)
    {
        $this->offerRepository->create($request->all(), $propertyId);

        flash()->success('Special Offer created!');

        return redirect()->route('offers_path', $propertyId);
    }

    /**
     * @param $propertyId
     * @param $offerId
     * @return \Illuminate\View\View
     */
    public function edit($propertyId, $offerId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $model = $this->offerRepository->getById($offerId, $propertyId);

        return view('admin.offers.edit', compact('property', 'model'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @param $offerId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $propertyId, $offerId)
    {
        $formData = $request->all();

        $formData['active'] = (isset($formData['active']));

        $this->offerRepository->update($offerId, $formData, $propertyId);

        flash()->success('Offer updated!');

        return redirect()->back();
    }

    /**
     * @param $propertyId
     * @param $offerId
     * @return \Illuminate\View\View
     */
    public function content($propertyId, $offerId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $model = $this->offerRepository->getById($offerId, $propertyId);

        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        $settings = $this->settingRepository->gatherSettings('offers');

        return view('admin.offers.content', compact('property', 'model', 'uploadHash', 'offers', 'settings'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @param $offerId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateContent(Request $request, $propertyId, $offerId)
    {
        $offer = $this->offerRepository->getById($offerId, $propertyId);
        $this->offerRepository->updateContent($offer, $request->all());

        $this->settingRepository->updateAll($offer,
                                            $request->get('setting'));

        flash()->success('Offer updated!');

        return redirect()->back();
    }

    /**
     * @param $propertyId
     * @param $offerId
     * @return \Illuminate\View\View
     */
    public function slideshow($propertyId, $offerId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $model = $this->offerRepository->getById($offerId, $propertyId);

        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?


        // Quick hack to keep things moving
        if( empty($model->slideshow_id))
        {
            $slideshow = Slideshow::create(['name' => $model->name]);

            $model->slideshow_id = $slideshow->id;

            $model->save();
        }

        return view('admin.offers.slideshow', compact('property', 'model', 'uploadHash'));
    }

    /**
     * @param $propertyId
     * @param $offerId
     * @return \Illuminate\View\View
     */
    public function seo($propertyId, $offerId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $model = $this->offerRepository->getById($offerId, $propertyId);


        return view('admin.offers.seo', compact('property', 'model'));
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @param $offerId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSeo(Request $request, $propertyId, $offerId)
    {
        $offer = $this->offerRepository->getById($offerId, $propertyId);
        $this->offerRepository->updateSeo($offer, $request->all());

        flash()->success('Offer updated!');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $propertyId
     */
    public function updateSorting(Request $request, $propertyId)
    {
        // Check validity of property
        $property = $this->propertyRepository->getById($propertyId);

        $this->offerRepository->sortOffers($request->all(), $propertyId);
    }

    /**
     * @param Request $request
     * @param $propertyId
     * @param $offerId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $propertyId, $offerId)
    {
        $this->offerRepository->destroy($offerId, $propertyId);

        flash()->success('Offer removed');

        return redirect()->route('offers_path', $propertyId);
    }

}
