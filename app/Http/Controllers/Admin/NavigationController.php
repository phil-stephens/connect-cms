<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Navigations\NavigationRepository;
use Fastrack\Pages\PageRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;

/**
 * Class NavigationController
 * @package Fastrack\Http\Controllers\Admin
 */
class NavigationController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var NavigationRepository
     */
    private $navigationRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * @param SiteRepository $siteRepository
     * @param NavigationRepository $navigationRepository
     * @param PageRepository $pageRepository
     */
    function __construct(
        SiteRepository $siteRepository,
        NavigationRepository $navigationRepository,
        PageRepository $pageRepository
    ) {
        $this->siteRepository = $siteRepository;
        $this->navigationRepository = $navigationRepository;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function index($siteId)
    {
        $site = $this->siteRepository->getById($siteId);
        $navigations = $site->navigations;

        return view('admin.navigations.index', compact('site', 'navigations'));
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function create($siteId)
    {
        $site = $this->siteRepository->getById($siteId);

        return view('admin.navigations.create', compact('site'));
    }

    /**
     * @param Request $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $siteId)
    {
        //$site = $this->siteRepository->getById($siteId);

        $navigation = $this->navigationRepository->create($request->all(), $siteId);

        flash()->success('Navigation Group created!');

        return redirect()->route('navigation_items_path', [$siteId, $navigation->id]);
    }

    /**
     * @param $siteId
     * @param $navigationId
     * @return \Illuminate\View\View
     */
    public function edit($siteId, $navigationId)
    {
        $site = $this->siteRepository->getById($siteId);

        $navigation = $this->navigationRepository->getById($navigationId, $siteId);

        return view('admin.navigations.edit', compact('site', 'navigation'));
    }

    /**
     * @param Request $request
     * @param $siteId
     * @param $navigationId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $siteId, $navigationId)
    {
        $this->navigationRepository->update($navigationId, $request->all(), $siteId);

        flash()->success('Navigation Group updated');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $siteId
     * @param $navigationId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $siteId, $navigationId)
    {
        $this->navigationRepository->destroy($request->get('id'), $siteId);

        flash()->success('Navigation Group removed');

        return redirect()->route('navigations_path', $siteId);
    }

    /**
     * @param $siteId
     * @param $navigationId
     * @return \Illuminate\View\View
     */
    public function items($siteId, $navigationId)
    {
        $site = $this->siteRepository->getById($siteId);

        $navigation = $this->navigationRepository->getById($navigationId, $siteId);

        // Get all of the usable items
        $items = $this->navigationRepository->getAvailableItems($navigationId, $siteId);

        return view('admin.navigations.items', compact('site', 'navigation', 'items'));
    }

    /**
     * @param Request $request
     * @param $siteId
     * @param $navigationId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateItems(Request $request, $siteId, $navigationId)
    {
        $this->navigationRepository->updateItems($navigationId, $request->all(), $siteId);

        flash()->success('Navigation Items updated!');

        return redirect()->route('navigation_items_path', [$siteId, $navigationId]);
    }

    /**
     * @param Request $request
     * @param $siteId
     * @param $navigationId
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function destroyItem(Request $request, $siteId, $navigationId)
    {
        $this->navigationRepository->destroyItem($request->get('id'), $navigationId);

        if ($request->ajax()) {
            return response()->json(true, 200);
        }

        return redirect()->back();
    }
}
