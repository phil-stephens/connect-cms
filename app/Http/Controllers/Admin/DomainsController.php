<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreateDomainRequest;
use Fastrack\Http\Requests\UpdateDomainRequest;
use Fastrack\Sites\DomainRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;

/**
 * Class DomainsController
 * @package Fastrack\Http\Controllers\Admin
 */
class DomainsController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var DomainRepository
     */
    private $domainRepository;

    /**
     * @param SiteRepository $siteRepository
     * @param DomainRepository $domainRepository
     */
    function __construct(SiteRepository $siteRepository, DomainRepository $domainRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->domainRepository = $domainRepository;
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function index($siteId)
    {
        $site = $this->siteRepository->getById($siteId);
        $domains = $site->domains;

        return view('admin.domains.index', compact('site', 'domains'));
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function create($siteId)
    {
        $site = $this->siteRepository->getById($siteId);

        return view('admin.domains.create', compact('site'));
    }

    /**
     * @param CreateDomainRequest $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateDomainRequest $request, $siteId)
    {
        $this->domainRepository->create($request->all(), $siteId);

        flash()->success('Domain successfully added');

        return redirect()->route('domains_path', $siteId);
    }

    /**
     * @param $siteId
     * @param $domainId
     * @return \Illuminate\View\View
     */
    public function edit($siteId, $domainId)
    {
        $site = $this->siteRepository->getById($siteId);
        $domain = $this->domainRepository->getById($domainId, $siteId);

        return view('admin.domains.edit', compact('site', 'domain'));
    }

    /**
     * @param UpdateDomainRequest $request
     * @param $siteId
     * @param $domainId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateDomainRequest $request, $siteId, $domainId)
    {
        $this->domainRepository->update($domainId, $request->all(), $siteId);

        flash()->success('Domain update');

        return redirect()->back();

    }

    /**
     * @param Request $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setPrimary(Request $request, $siteId)
    {
        $this->domainRepository->setPrimary($request->all(), $siteId);

        flash()->success('Primary domain set');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $siteId
     * @param $domainId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $siteId, $domainId)
    {
        $this->domainRepository->destroy($domainId, $siteId);

        flash()->success('Domain removed');

        return redirect()->route('domains_path', $siteId);
    }
}
