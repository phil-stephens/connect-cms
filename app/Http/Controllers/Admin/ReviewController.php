<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Properties\PropertyRepository;
use Fastrack\Reviews\ReviewRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class ReviewController extends Controller
{

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var ReviewRepository
     */
    private $reviewRepository;


    public function __construct(ReviewRepository $reviewRepository,
                                PropertyRepository $propertyRepository)
    {
        $this->reviewRepository = $reviewRepository;
        $this->propertyRepository = $propertyRepository;
    }

    public function index($propertyUuid)
    {
        $property = $this->propertyRepository->get($propertyUuid);
        $reviews = $this->reviewRepository->getPaginated($propertyUuid);

        return view('admin.review.index', compact('property', 'reviews'));
    }

    public function show($reviewUuid)
    {
        $review = $this->reviewRepository->get($reviewUuid);

        return view('admin.review.partials.show', compact('review'));
    }

    public function create($propertyUuid)
    {
        $property = $this->propertyRepository->get($propertyUuid);

        $rooms = $property->rooms->lists('name', 'id')->all();

        return view('admin.review.create', compact('property', 'rooms'));
    }

    public function store(Request $request, $propertyUuid)
    {
        $formData = $request->all();
        //$formData['approved'] = $request->has('approved');
        $formData['published'] = $request->has('published');

        $this->reviewRepository->create($propertyUuid, $formData);

        flash()->success('Review Created');

        return redirect()->route('reviews_path', $propertyUuid);
    }

    public function edit($reviewUuid)
    {
        $review = $this->reviewRepository->get($reviewUuid);
        $property = $review->property;
        $rooms = $property->rooms->lists('name', 'id')->all();

        return view('admin.review.edit', compact('property', 'review', 'rooms'));
    }

    public function update(Request $request, $reviewUuid)
    {
        $formData = $request->all();
        //$formData['approved'] = $request->has('approved');
        $formData['published'] = $request->has('published');

        $this->reviewRepository->update($reviewUuid, $formData);

        flash()->success('Review Updated');

        return redirect()->back();
    }

    public function destroy(Request $request, $propertyUuid)
    {
        $this->reviewRepository->destroy($request->get('uuid'));

        flash()->success('Review removed');

        return redirect()->route('reviews_path', $propertyUuid);
    }
}
