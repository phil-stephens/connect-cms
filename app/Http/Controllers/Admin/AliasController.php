<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreateAliasRequest;
use Fastrack\Sites\AliasRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;
use League\Csv\Reader;

/**
 * Class AliasController
 * @package Fastrack\Http\Controllers\Admin
 */
class AliasController extends Controller
{


    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var AliasRepository
     */
    private $aliasRepository;

    /**
     * @param SiteRepository $siteRepository
     * @param AliasRepository $aliasRepository
     */
    function __construct(SiteRepository $siteRepository, AliasRepository $aliasRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->aliasRepository = $aliasRepository;
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function index($siteId)
    {
        $site = $this->siteRepository->getById($siteId);
        $aliases = $site->aliases;

        return view('admin.aliases.index', compact('site', 'aliases'));
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function create($siteId)
    {
        $site = $this->siteRepository->getById($siteId);

        return view('admin.aliases.create', compact('site'));
    }

    /**
     * @param CreateAliasRequest $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateAliasRequest $request, $siteId)
    {
        $this->aliasRepository->create($request->all(), $siteId);

        flash()->success('Alias successfully added');

        return redirect()->route('aliases_path', $siteId);
    }

    public function storeMultiple(Request $request, $siteId)
    {
        $count = $this->aliasRepository->createMultiple($request->all(), $siteId);

        flash()->success($count . ' ' . str_plural('Alias', $count) . ' successfully added');

        return redirect()->route('aliases_path', $siteId);
    }


    public function upload(Request $request, $siteId)
    {
        $site = $this->siteRepository->getById($siteId);

        // We'll do it inline first...
        if ($file = $request->file('file')) {

            $csv = Reader::createFromPath($file->getRealPath());

            $headers = $csv->fetchOne();

            $data = $csv->setOffset(1)->fetchAssoc($headers);

            //dd($data);
            $aliases = [];

            foreach ($data as $key => $row) {
                $url = parse_url($row['URL']);
                //dd($url);
                if ( ! empty($url['path'])) {

                    $from = $url['path'];

                    if( $request->has('use_url_fragment')) $from .= '#' . $url['fragment'];

                    $aliases[$from] = [
                        'from' => ltrim($from, '/'),
                        'to'   => ( ! empty($row['Redirect'])) ? $row['Redirect'] : $request->get('to'),
                        'type' => ( ! empty($row['Type'])) ? $row['Type'] : $request->get('type')
                    ];
                }

                //$data[$key]['URL'] = parse_url($row['URL']);
            }

            return view('admin.aliases.upload', compact('site', 'aliases'));
        } else {
            abort(404);
        }
    }

    /**
     * @param $siteId
     * @param $aliasId
     * @return \Illuminate\View\View
     */
    public function edit($siteId, $aliasId)
    {
        $site = $this->siteRepository->getById($siteId);
        $alias = $this->aliasRepository->getById($aliasId, $siteId);

        return view('admin.aliases.edit', compact('site', 'alias'));
    }

    /**
     * @param CreateAliasRequest $request
     * @param $siteId
     * @param $aliasId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CreateAliasRequest $request, $siteId, $aliasId)
    {
        $this->aliasRepository->update($aliasId, $request->all(), $siteId);

        flash()->success('Alias successfully updated');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $siteId
     * @param $aliasId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $siteId, $aliasId)
    {
        $this->aliasRepository->destroy($aliasId, $siteId);

        flash()->success('Alias removed');

        return redirect()->route('aliases_path', $siteId);
    }
}
