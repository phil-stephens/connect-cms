<?php

namespace Fastrack\Http\Controllers\Admin\Compendium;

use Fastrack\Navigations\NavigationRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class IndexController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var NavigationRepository
     */
    private $navigationRepository;


    /**
     * IndexController constructor.
     */
    public function __construct(SiteRepository $siteRepository,
                                NavigationRepository $navigationRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->navigationRepository = $navigationRepository;
    }

    public function index($uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        $indices = $compendium->navigations;

        return view('admin.compendium.index.index', compact('compendium', 'indices'));
    }

    public function create($uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        return view('admin.compendium.index.create', compact('compendium'));

    }

    public function store(Request $request, $uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        $index = $this->navigationRepository->create($request->all(), $compendium->id);

        flash()->success('Index created!');

        return redirect()->route('compendium__index_items_path', $index->uuid);
    }


    public function edit($uuid)
    {
        $index = $this->navigationRepository->get($uuid);

        $compendium = $index->site;


        return view('admin.compendium.index.edit', compact('compendium', 'index'));

    }

    public function update(Request $request, $uuid)
    {
        $index = $this->navigationRepository->get($uuid);
        $this->navigationRepository->update($index->id, $request->all(), $index->site_id);

        flash()->success('Index updated');

        return redirect()->back();
    }

    public function items($uuid)
    {
        $index = $this->navigationRepository->get($uuid);

        $compendium = $index->site;

        $items = $this->navigationRepository->getAvailableItems($index->id, $compendium->id);

        return view('admin.compendium.index.items', compact('compendium', 'index', 'items'));

    }

    public function updateItems(Request $request, $uuid)
    {
        $index = $this->navigationRepository->get($uuid);

        $this->navigationRepository->updateItems($index->id, $request->all(), $index->site_id);

        flash()->success('Items updated');

        return redirect()->route('compendium__index_items_path', $uuid);
    }

    public function destroy(Request $request, $uuid)
    {
        $index = $this->navigationRepository->get($uuid);

        $compendium_uuid = $index->site->uuid;

        $this->navigationRepository->destroy($index->id, $index->site_id);

        flash()->success('Index removed');

        return redirect()->route('compendium_indices_path', $compendium_uuid);
    }

}
