<?php

namespace Fastrack\Http\Controllers\Admin\Compendium;

use Fastrack\Pages\AnnotationRepository;
use Fastrack\Pages\ChapterRepository;
use Fastrack\Pages\PageRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class PageController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var ChapterRepository
     */
    private $chapterRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var AnnotationRepository
     */
    private $annotationRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * PageController constructor.
     */
    public function __construct(SiteRepository $siteRepository,
                                ChapterRepository $chapterRepository,
                                PageRepository $pageRepository,
                                AnnotationRepository $annotationRepository,
                                SettingRepository $settingRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->chapterRepository = $chapterRepository;
        $this->pageRepository = $pageRepository;
        $this->annotationRepository = $annotationRepository;
        $this->settingRepository = $settingRepository;
    }

    public function index($uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        $folders = $this->chapterRepository->getAll($compendium->id);

        return view('admin.compendium.page.index', compact('compendium', 'folders'));
    }

    public function create($uuid)
    {
        $compendium = $this->siteRepository->get($uuid);
        $folders = $this->chapterRepository->getAll($compendium->id)->lists('name', 'id');

        // List available templates
        $templates = [];

        if ($compendium->theme != 'default') {
            $dir = scandir(public_path('themes/' . $compendium->theme . '/views'));
        } else {
            $dir = scandir(base_path('resources/views/default'));
        }

        foreach ($dir as $file) {
            if (strpos($file, '.blade.php') !== false) {
                $shortname = str_replace('.blade.php', '', $file);
                $templates[$shortname] = $shortname;
            }
        }

        $templates['_raw'] = 'Raw (outputs page excerpt only)';

        return view('admin.compendium.page.create', compact('compendium', 'folders', 'templates'));

    }

    public function store(Request $request, $uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        $formData = $request->all();

        $formData['name'] = $formData['content']['title'];
        $formData['slug'] = str_slug($formData['content']['title']);

        $page = $this->pageRepository->create($formData, $compendium->id);

        flash()->success('Page created!');

        return redirect()->route('compendium__page_edit_path', $page->uuid);

    }

    public function edit($uuid)
    {
        $model = $this->pageRepository->get($uuid);

        $model->load('content');

        $compendium = $model->site;
        $folders = $this->chapterRepository->getAll($compendium->id)->lists('name', 'id');


        $annotation = $this->annotationRepository->getForPage($model->id);

        $settings = $this->settingRepository->getPageSettings($compendium, $model);


        // List available templates
        $templates = [];

        if ($compendium->theme != 'default') {
            $dir = scandir(public_path('themes/' . $compendium->theme . '/views'));
        } else {
            $dir = scandir(base_path('resources/views/default'));
        }

        foreach ($dir as $file) {
            if (strpos($file, '.blade.php') !== false) {
                $shortname = str_replace('.blade.php', '', $file);
                $templates[$shortname] = $shortname;
            }
        }

        $templates['_raw'] = 'Raw (outputs page excerpt only)';

        return view('admin.compendium.page.edit', compact('model', 'compendium', 'folders', 'templates', 'annotations', 'settings'));
    }

    public function update(Request $request, $uuid)
    {
        $formData = $request->all();

        $formData['name'] = $formData['content']['title'];

        $this->pageRepository->updateByUuid($uuid, $formData);

        $this->settingRepository->updateAll($this->pageRepository->get($uuid),
            $request->get('setting'));

        flash()->success('Page updated!');

        return redirect()->back();
    }

    public function destroy(Request $request, $uuid)
    {
        $siteUuid = $this->pageRepository->destroyByUuid($uuid);

        flash()->success('Page removed');

        return redirect()->route('compendium_pages_path', $siteUuid);
    }


}
