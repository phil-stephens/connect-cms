<?php

namespace Fastrack\Http\Controllers\Admin\Compendium;

use Fastrack\Locations\VenueRepository;
use Fastrack\Pages\PageRepository;
use Fastrack\Sites\DomainRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class SettingController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var VenueRepository
     */
    private $venueRepository;
    /**
     * @var DomainRepository
     */
    private $domainRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;


    /**
     * SettingController constructor.
     */
    public function __construct(SiteRepository $siteRepository,
                                PageRepository $pageRepository,
                                VenueRepository $venueRepository,
                                DomainRepository $domainRepository,
                                SettingRepository $settingRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->pageRepository = $pageRepository;
        $this->venueRepository = $venueRepository;
        $this->domainRepository = $domainRepository;
        $this->settingRepository = $settingRepository;
    }

    public function edit($uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        $compendium->load('primary_domain');

        $pages = $this->pageRepository->getAllDropdown($compendium->id);


        return view('admin.compendium.edit', compact('compendium', 'pages'));
    }

    public function update(Request $request, $uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        // Do the domain...
        //$formData = $request->all();

        //if( ! isset($formData['in_development'])) $formData['in_development'] = false;
        $formData = $request->all();

        $this->siteRepository->update($compendium, $formData);

        $this->domainRepository->update($compendium->primary_domain_id, $formData['primary_domain'], $compendium->id);

        flash()->success('Compendium updated');

        return redirect()->back();
    }

    public function theme($uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        $settings = [];

        try {
            // Get the settings skeleton from the manifest file
            $manifest = file_get_contents(public_path('themes/' . $compendium->theme . '/manifest.json'));

            $settings = json_decode($manifest)->settings;

        } catch (\Exception $e) {

        }

        return view('admin.compendium.theme', compact('compendium', 'settings'));
    }

    public function updateTheme(Request $request, $uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        $this->settingRepository->updateAll($compendium, $request->get('setting'));

        flash()->success('Theme settings updated');

        return redirect()->back();
    }

    public function places($uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        $businesses = $this->venueRepository->getUnrelated($compendium->id);

        return view('admin.compendium.places', compact('compendium', 'businesses'));
    }
}
