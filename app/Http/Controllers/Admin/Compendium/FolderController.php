<?php

namespace Fastrack\Http\Controllers\Admin\Compendium;

use Fastrack\Pages\ChapterRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class FolderController extends Controller
{

    /**
     * @var ChapterRepository
     */
    private $chapterRepository;
    /**
     * @var SiteRepository
     */
    private $siteRepository;

    /**
     * FolderController constructor.
     */
    public function __construct(ChapterRepository $chapterRepository,
                                SiteRepository $siteRepository)
    {
        $this->chapterRepository = $chapterRepository;
        $this->siteRepository = $siteRepository;
    }

    public function pages(Request $request, $uuid)
    {
        $pages = $this->chapterRepository->getPages($uuid);

        return response()->json(\View::make('admin.compendium.partials.pages', compact('pages'))->render());
    }

    public function create($uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        return view('admin.compendium.folder.create', compact('compendium'));
    }

    public function store(Request $request, $uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        $formData = $request->all();
        $formData['slug'] = str_slug($formData['name']);

        $this->chapterRepository->create($formData, $compendium->id);

        flash()->success('Chapter created!');

        return redirect()->route('compendium_pages_path', $uuid);
    }

    public function edit($uuid)
    {
        $model = $this->chapterRepository->get($uuid);

        $compendium = $model->site;

        return view('admin.compendium.folder.edit', compact('compendium', 'model'));
    }

    public function update(Request $request, $uuid)
    {
        $this->chapterRepository->update($request->all(), $uuid);

        flash()->success('Folder updated');

        return redirect()->back();
    }


}
