<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\News\FeedRepository;
use Fastrack\Sites\Site;
use Illuminate\Http\Request;

/**
 * Class FeedsController
 * @package Fastrack\Http\Controllers\Admin
 */
class FeedsController extends Controller
{

    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * @param FeedRepository $feedRepository
     */
    function __construct(FeedRepository $feedRepository)
    {
        $this->feedRepository = $feedRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $feeds = $this->feedRepository->getAll();

        return view('admin.news.feeds.index', compact('feeds'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $sites = Site::get()->lists('name', 'id')->all();

        return view('admin.news.feeds.create', compact('sites'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $feed = $this->feedRepository->create($request->all());

        flash()->success('Content Feed created!');

        return redirect()->route('feed_path', $feed->id);
    }

    /**
     * @param $feedId
     * @return \Illuminate\View\View
     */
    public function edit($feedId)
    {
        $feed = $this->feedRepository->getById($feedId);
        $sites = Site::get()->lists('name', 'id')->all();

        return view('admin.news.feeds.edit', compact('feed', 'sites'));
    }

    /**
     * @param Request $request
     * @param $feedId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $feedId)
    {
        $this->feedRepository->update($feedId, $request->all());

        flash()->success('Content Feed updated!');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $feedId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $feedId)
    {
        $this->feedRepository->destroy($request->get('id'));

        flash()->success('Content Feed removed!');

        return redirect()->route('feeds_path');
    }

}
