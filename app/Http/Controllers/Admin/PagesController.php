<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreatePageRequest;
use Fastrack\Http\Requests\UpdatePageRequest;
use Fastrack\Pages\AnnotationRepository;
use Fastrack\Pages\ChapterRepository;
use Fastrack\Pages\PageRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Sites\SiteRepository;
use Fastrack\Slideshows\SlideRepository;
use Fastrack\Slideshows\Slideshow;
use Illuminate\Http\Request;

/**
 * Class PagesController
 * @package Fastrack\Http\Controllers\Admin
 */
class PagesController extends Controller
{


    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var SlideRepository
     */
    private $slideRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;
    /**
     * @var ChapterRepository
     */
    private $chapterRepository;
    /**
     * @var AnnotationRepository
     */
    private $annotationRepository;


    function __construct(
        SiteRepository $siteRepository,
        PageRepository $pageRepository,
        SlideRepository $slideRepository,
        SettingRepository $settingRepository,
        ChapterRepository $chapterRepository,
        AnnotationRepository $annotationRepository
    ) {
        $this->siteRepository = $siteRepository;
        $this->pageRepository = $pageRepository;
        $this->slideRepository = $slideRepository;
        $this->settingRepository = $settingRepository;
        $this->chapterRepository = $chapterRepository;
        $this->annotationRepository = $annotationRepository;
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function index($siteId)
    {
        $site = $this->siteRepository->getById($siteId);
        //$pages = $site->pages;

        $chapters = $this->chapterRepository->getAll($siteId);

        // This will need to change to 'chapters'
        $pages = $this->pageRepository->getAllPaginated($siteId);

        return view('admin.pages.index', compact('site', 'pages', 'chapters'));
    }

    public function listPages($siteId, $chapterId = 0)
    {
        // Only accessible via AJAX

        // Get the paginated list from a chapter repo
        $pages = $this->pageRepository->getAllPaginated($siteId, $chapterId);

        // return the listing partial

    }

    /**
     * @param Request $request
     * @param $siteId
     */
    public function updateSorting(Request $request, $siteId)
    {
        // Check validity of site
        $site = $this->siteRepository->getById($siteId);

        $this->pageRepository->sortPages($request->all(), $siteId);
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function create($siteId)
    {
        $site = $this->siteRepository->getById($siteId);
        $chapters = $this->chapterRepository->getAll($siteId)->lists('name', 'id');
        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        // List available templates
        $templates = [];

        if ($site->theme != 'default') {
            $dir = scandir(public_path('themes/' . $site->theme . '/views'));
        } else {
            $dir = scandir(base_path('resources/views/default'));
        }

        foreach ($dir as $file) {
            if (strpos($file, '.blade.php') !== false) {
                $shortname = str_replace('.blade.php', '', $file);
                $templates[$shortname] = $shortname;
            }
        }

        $templates['_raw'] = 'Raw (outputs page excerpt only)';

        return view('admin.pages.create', compact('site', 'chapters', 'uploadHash', 'templates'));
    }

    /**
     * @param CreatePageRequest $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePageRequest $request, $siteId)
    {
        $page = $this->pageRepository->create($request->all(), $siteId);

        flash()->success('Page \'' . $page->name . '\' create!');

        return redirect()->route('pages_path', $siteId);
    }

    /**
     * @param $siteId
     * @param $pageId
     * @return \Illuminate\View\View
     */
    public function edit($siteId, $pageId)
    {
        $site = $this->siteRepository->getById($siteId);
        $model = $this->pageRepository->getById($pageId, $siteId);
        $chapters = $this->chapterRepository->getAll($siteId)->lists('name', 'id');

        // List available templates
        $templates = [];

        if ($site->theme != 'default') {
            $dir = scandir(public_path('themes/' . $site->theme . '/views'));
        } else {
            $dir = scandir(base_path('resources/views/default'));
        }

        foreach ($dir as $file) {
            if (strpos($file, '.blade.php') !== false) {
                $shortname = str_replace('.blade.php', '', $file);
                $templates[$shortname] = $shortname;
            }
        }

        $templates['_raw'] = 'Raw (outputs page excerpt only)';

        return view('admin.pages.edit', compact('site', 'model', 'templates', 'chapters'));
    }

    /**
     * @param UpdatePageRequest $request
     * @param $siteId
     * @param $pageId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePageRequest $request, $siteId, $pageId)
    {
        $this->pageRepository->update($pageId, $request->all(), $siteId);

        flash()->success('Page updated!');

        return redirect()->back();
    }

    //public function settings($siteId, $pageId)
    //{
    //    $site = $this->siteRepository->getById($siteId);
    //    $model = $this->pageRepository->getById($pageId, $siteId);
    //
    //    $settings = [];
    //
    //    try {
    //        // Get the settings skeleton from the manifest file
    //
    //        $manifest = file_get_contents(public_path('themes/' . $site->theme . '/manifest.json'));
    //
    //        $manifest = json_decode($manifest)->templates;
    //
    //
    //        if (isset($manifest->settings)) {
    //            $settings = array_merge($settings, $manifest->settings);
    //        }
    //
    //        $template = $model->template;
    //
    //        if (isset($manifest->files->$template)) {
    //
    //            $settings = array_merge($settings, $manifest->files->$template);
    //        }
    //
    //    } catch (\Exception $e) {
    //
    //    }
    //
    //    return view('admin.pages.settings', compact('site', 'model', 'settings'));
    //}
    //
    //
    //public function updateSettings(Request $request, $siteId, $pageId)
    //{
    //    $page = $this->pageRepository->getById($pageId, $siteId);
    //
    //    $this->settingRepository->updateAll($page, $request->get('setting'));
    //
    //    flash()->success('Page settings updated');
    //
    //    return redirect()->back();
    //}

    /**
     * @param $siteId
     * @param $pageId
     * @return \Illuminate\View\View
     */
    public function content($siteId, $pageId)
    {
        $site = $this->siteRepository->getById($siteId);
        $model = $this->pageRepository->getById($pageId, $siteId);
        $annotation = $this->annotationRepository->getForPage($pageId);

        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        $settings = $this->settingRepository->getPageSettings($site, $model);

        return view('admin.pages.content', compact('site', 'model', 'annotation', 'uploadHash', 'settings'));
    }

    /**
     * @param Request $request
     * @param $siteId
     * @param $pageId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateContent(Request $request, $siteId, $pageId)
    {
        $this->pageRepository->update($pageId, $request->all(), $siteId);

        //$page = $this->pageRepository->getById($pageId, $siteId);
        $this->settingRepository->updateAll($this->pageRepository->getById($pageId, $siteId),
                                            $request->get('setting'));

        flash()->success('Page updated!');

        return redirect()->back();
    }

    /**
     * @param $siteId
     * @param $pageId
     * @return \Illuminate\View\View
     */
    public function seo($siteId, $pageId)
    {
        $site = $this->siteRepository->getById($siteId);
        $model = $this->pageRepository->getById($pageId, $siteId);

        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        return view('admin.pages.seo', compact('site', 'model', 'uploadHash'));
    }

    /**
     * @param Request $request
     * @param $siteId
     * @param $pageId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSeo(Request $request, $siteId, $pageId)
    {
        $page = $this->pageRepository->getById($pageId, $siteId);
        $this->pageRepository->updateSeo($page, $request->all('seo'));

        flash()->success('Page SEO updated!');

        return redirect()->back();
    }

    /**
     * @param $siteId
     * @param $pageId
     * @return \Illuminate\View\View
     */
    public function slideshow($siteId, $pageId)
    {
        $site = $this->siteRepository->getById($siteId);
        $model = $this->pageRepository->getById($pageId, $siteId);

        // Quick hack to keep things moving
        if( empty($model->slideshow_id))
        {
            $slideshow = Slideshow::create(['name' => $model->name]);

            $model->slideshow_id = $slideshow->id;

            $model->save();
        }


        //$uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        return view('admin.pages.slideshow', compact('site', 'model'));
    }

    public function annotations($siteId, $pageId)
    {
        $site = $this->siteRepository->getById($siteId);
        $model = $this->pageRepository->getById($pageId, $siteId);
        $annotation = $this->annotationRepository->getForPage($pageId);

        return view('admin.pages.annotations', compact('site', 'model', 'annotation'));
    }

    public function updateAnnotations(Request $request, $siteId, $pageId)
    {
        $model = $this->pageRepository->getById($pageId, $siteId);
        $this->annotationRepository->update($request->all(), $pageId);

        flash()->success('Page Annotations updated!');

        return redirect()->back();
    }

    public function related($pageId)
    {
        $model = $this->pageRepository->get($pageId);
        $site = $model->site;

        $chapters = $this->pageRepository->getUnrelatedPages($model);

        return view('admin.pages.related', compact('site', 'model', 'chapters'));
    }

    public function updateRelated(Request $request, $pageId)
    {
        $this->pageRepository->updateRelated($pageId, $request->all());

        flash()->success('Related pages updated!');

        return redirect()->route('page_related_path', $pageId);
    }

    public function destroyRelated(Request $request, $pageId)
    {
        return $this->pageRepository->unRelatePage($pageId, $request->get('id'));

        //return $request->get('id');
    }

    /**
     * @param Request $request
     * @param $siteId
     * @param $pageId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $siteId, $pageId)
    {
        $this->pageRepository->destroy($pageId, $siteId);

        flash()->success('Page removed');

        return redirect()->route('pages_path', $siteId);
    }
}
