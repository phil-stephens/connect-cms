<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Slideshows\SlideRepository;
use Illuminate\Http\Request;

/**
 * Class SlideshowController
 * @package Fastrack\Http\Controllers\Admin
 */
class SlideshowController extends Controller
{

    /**
     * @var SlideRepository
     */
    private $slideRepository;

    /**
     * @param SlideRepository $slideRepository
     */
    function __construct(SlideRepository $slideRepository)
    {
        $this->slideRepository = $slideRepository;
    }

    /**
     * @param Request $request
     * @param $slideshowId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSlides(Request $request, $slideshowId)
    {
        $this->slideRepository->updateMany($request->all(), $slideshowId);

        flash()->success('Slides updated!');

        return redirect()->back();
    }

}
