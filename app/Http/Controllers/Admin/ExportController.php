<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Navigations\Navigation;
use Fastrack\Navigations\NavigationRepository;
use Fastrack\Pages\Chapter;
use Fastrack\Pages\PageRepository;
use Fastrack\Sites\Domain;
use Fastrack\Sites\Site;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class ExportController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var NavigationRepository
     */
    private $navigationRepository;


    public function __construct(SiteRepository $siteRepository,
                                PageRepository $pageRepository,
                                NavigationRepository $navigationRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->pageRepository = $pageRepository;
        $this->navigationRepository = $navigationRepository;
    }

    public function site($uuid)
    {
        $site = $this->siteRepository->get($uuid);

        return view('admin.export.site', compact('site'));
    }

    public function compendium($uuid)
    {
        $compendium = $this->siteRepository->get($uuid);

        return view('admin.export.compendium', compact('compendium'));
    }


    public function exportSite(Request $request, $uuid)
    {
        $site = $this->siteRepository->get($uuid);

        $site->load('chapters', 'navigations');

        foreach($site->chapters as $chapter)
        {
            $chapter->load('pages');

            foreach($chapter->pages as $page)
            {
                $page->load('content');
            }
        }

        foreach($site->navigations as $navigation)
        {
            $navigation->load('items');
        }

        //dd(json_decode( $site->toJson() ));
        //echo serialize($site);

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/plain");
        header("Content-Disposition: attachment; filename=export.txt");
        header("Expires: 0");
        header("Pragma: public");

        echo serialize($site);
    }

    public function importSite(Request $request, $uuid)
    {
        $site = $this->siteRepository->get($uuid);

        if ($file = $request->file('file')) {

            $obj = file_get_contents($file->getRealPath());

            $pageArray = [];

            $imported = unserialize($obj);

            foreach($imported->chapters as $c)
            {
                // create the folder
                $folder = new Chapter([
                    'name'  => $c->name,
                    'slug'  => $c->slug
                ]);

                $site->chapters()->save($folder);

                // then loop through the pages
                foreach($c->pages as $p)
                {
                    $page = $this->pageRepository->create([
                        'chapter_id'    => $folder->id,
                        'name'          => $p->name,
                        'slug'          => $p->slug,
                        'template'      => $p->template,
                        'internal'      => $p->internal,
                        'content'       => [
                            'title'     => $p->content->title,
                            'excerpt'   => $p->content->excerpt,
                            'body'      => $p->content->body
                        ]
                    ], $site->id);

                    $pageArray[$p->id] = $page->id;
                }


            }

            foreach($imported->navigations as $n)
            {
                $index = new Navigation([
                    'name'  => $n->name,
                    'slug'  => $n->slug,
                    'description'   => $n->description
                ]);

                $site->navigations()->save($index);

                $items = [];

                foreach($n->items as $i)
                {
                    $items[] = [
                        'class' => $i->link_type,
                        'class_id'  => ($i->link_type == 'Fastrack\Pages\Page') ? $pageArray[$i->link_id] : $i->link_id,
                        'label'     => $i->label,
                        'order'     => $i->order
                    ];
                }

                $this->navigationRepository->updateItems($index->id, ['item' => $items], $site->id);

            }
            //// Create the site
            //$site = new Site();
            //
            //$site->name = $imported->name;
            //$site->wix_redirects = $imported->wix_redirects;
            //$site->theme = $imported->theme;
            //$site->business_feed = $imported->business_feed;
            //
            //$site->save();
            //
            //// Now do the domains
            //foreach($imported->domains as $d)
            //{
            //    // check to see if domain already exists first
            //    $domain = new Domain([
            //        'domain'    => $d->domain
            //    ]);
            //
            //    $site->domains()->save($domain);
            //
            //    if($d->id == $imported->primary_domain_id)
            //    {
            //        $site->primary_domain_id = $domain->id;
            //        $site->save();
            //    }
            //}

            // Do the SEO

            dd( $imported );
        }
    }


}
