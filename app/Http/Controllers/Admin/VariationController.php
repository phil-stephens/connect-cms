<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Campaigns\CampaignRepository;
use Fastrack\Campaigns\VariationRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class VariationController extends Controller
{

    /**
     * @var CampaignRepository
     */
    private $campaignRepository;
    /**
     * @var VariationRepository
     */
    private $variationRepository;

    /**
     * VariationController constructor.
     */
    public function __construct(CampaignRepository $campaignRepository, VariationRepository $variationRepository)
    {
        $this->campaignRepository = $campaignRepository;
        $this->variationRepository = $variationRepository;
    }

    public function index($campaignUuid)
    {
        $campaign = $this->campaignRepository->get($campaignUuid);
        $site = $campaign->site;

        return view('admin.campaign.variation.index', compact('site', 'campaign'));

    }

    public function create($campaignUuid)
    {
        $campaign = $this->campaignRepository->get($campaignUuid);
        $site = $campaign->site;

        return view('admin.campaign.variation.create', compact('site', 'campaign'));
    }

    public function store(Request $request, $campaignUuid)
    {
        $this->variationRepository->create($request->all(), $campaignUuid);

        flash()->success('Variation Created');

        return redirect()->route('variations_path', $campaignUuid);
    }

    public function edit($variationUuid)
    {
        $variation = $this->variationRepository->get($variationUuid);
        $campaign = $variation->campaign;
        $site = $campaign->site;

        return view('admin.campaign.variation.edit', compact('site', 'campaign', 'variation'));
    }

    public function update(Request $request, $variationUuid)
    {
        $this->variationRepository->update($variationUuid, $request->all());

        flash()->success('Variation Updated');

        return redirect()->back();
    }

    public function destroy(Request $request, $variationUuid)
    {

    }
}
