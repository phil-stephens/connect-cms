<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Pages\ChapterRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class ChapterController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var ChapterRepository
     */
    private $chapterRepository;

    /**
     * ChapterController constructor.
     */
    public function __construct(SiteRepository $siteRepository, ChapterRepository $chapterRepository)
    {
        $this->siteRepository = $siteRepository;
        $this->chapterRepository = $chapterRepository;
    }

    public function create($siteId)
    {
        $site = $this->siteRepository->getById($siteId);

        return view('admin.chapter.create', compact('site'));
    }

    public function store(Request $request, $siteId)
    {
        $chapter = $this->chapterRepository->create($request->all(), $siteId);

        flash()->success('Chapter created!');

        return redirect()->route('pages_path', $siteId);
    }

    public function pages(Request $request, $siteId, $chapterId)
    {
        $site = $this->siteRepository->getById($siteId);
        $pages = $this->chapterRepository->getPages($chapterId);

        //dd($pages);
        return response()->json(\View::make('admin.chapter.partials.pages', compact('site', 'pages'))->render());
    }

    public function edit($siteId, $chapterId)
    {
        $site = $this->siteRepository->getById($siteId);
        $model = $this->chapterRepository->get($chapterId);

        return view('admin.chapter.edit', compact('site', 'model'));
    }

    public function update(Request $request, $siteId, $chapterId)
    {
        $this->chapterRepository->update($request->all(), $chapterId);

        flash()->success('Chapter updated');

        return redirect()->route('chapter_edit_path', [$siteId, $chapterId]);
    }
}
