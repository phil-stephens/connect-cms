<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Images\Image;
use Fastrack\Images\ImageRepository;
use Fastrack\Media\LibraryRepository;
use Illuminate\Http\Request;

class MediaController extends Controller
{

    /**
     * @var LibraryRepository
     */
    private $libraryRepository;
    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * MediaController constructor.
     */
    public function __construct(LibraryRepository $libraryRepository, ImageRepository $imageRepository)
    {
        $this->libraryRepository = $libraryRepository;
        $this->imageRepository = $imageRepository;
    }

    public function index()
    {
        $libraries = $this->libraryRepository->getAll();

        return view('admin.media.index', compact('libraries'));
    }

    public function create()
    {
        return view('admin.media.create');
    }

    public function store(Request $request)
    {
        $library = $this->libraryRepository->create($request->all());

        flash()->success('Library created');

        return redirect()->route('libraries_path');
    }

    public function show($libraryId)
    {
        $library = $this->libraryRepository->get($libraryId);
        $images = $this->libraryRepository->getImages($libraryId);

        return view('admin.media.show', compact('library', 'images'));
    }

    public function grid($libraryId)
    {
        $images = $this->libraryRepository->getImages($libraryId);

        return response()->json(\View::make('admin.media.partials.grid', compact('images'))->render());
    }

    public function edit($libraryId)
    {
        $library = $this->libraryRepository->get($libraryId);

        return view('admin.media.edit', compact('library'));
    }

    public function update(Request $request, $libraryId)
    {
        $this->libraryRepository->update($libraryId, $request->all());

        flash()->success('Media Library updated');

        return redirect()->back();
    }

    public function destroy($libraryId)
    {

    }

    public function destroyImage($imageUuid)
    {
        $this->imageRepository->destroy($imageUuid);

        flash()->success('Image permanently deleted');

        return redirect()->back();
    }
}
