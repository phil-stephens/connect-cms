<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreateAmenityRequest;
use Fastrack\Properties\AmenityRepository;
use Fastrack\Properties\PropertyRepository;

/**
 * Class AmenitiesController
 * @package Fastrack\Http\Controllers\Admin
 */
class AmenitiesController extends Controller
{

    /**
     * @var AmenityRepository
     */
    private $amenityRepository;
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;

    /**
     * @param AmenityRepository $amenityRepository
     * @param PropertyRepository $propertyRepository
     */
    function __construct(AmenityRepository $amenityRepository, PropertyRepository $propertyRepository)
    {
        $this->amenityRepository = $amenityRepository;
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function index($propertyId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $amenities = $property->amenities;

        return view('admin.feature.index', compact('property', 'amenities'));
    }

    /**
     * @param $propertyId
     * @return \Illuminate\View\View
     */
    public function create($propertyId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $uploadHash = md5(uniqid('', true));

        return view('admin.feature.create', compact('property', 'uploadHash'));


    }

    /**
     * @param CreateAmenityRequest $request
     * @param $propertyId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateAmenityRequest $request, $propertyId)
    {
        $amenity = $this->amenityRepository->create($request->all(), $propertyId);

        flash()->success('New feature created');

        return redirect()->route('amenities_path', $propertyId);
    }

    /**
     * @param $propertyId
     * @param $amenityId
     * @return \Illuminate\View\View
     */
    public function edit($propertyId, $amenityId)
    {
        $property = $this->propertyRepository->getById($propertyId);
        $amenity = $this->amenityRepository->getById($amenityId);
        $uploadHash = md5(uniqid('', true));

        return view('admin.feature.edit', compact('property', 'amenity', 'uploadHash'));
    }

    /**
     * @param CreateAmenityRequest $request
     * @param $propertyId
     * @param $amenityId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CreateAmenityRequest $request, $propertyId, $amenityId)
    {
        $this->amenityRepository->update($amenityId, $request->all());

        flash()->success('Feature updated');

        return redirect()->back();
    }

    /**
     * @param $propertyId
     * @param $amenityId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($propertyId, $amenityId)
    {
        $this->amenityRepository->destroy($amenityId);

        flash()->success('Feature removed');

        return redirect()->route('amenities_path', $propertyId);
    }

}
