<?php

namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Properties\PropertyRepository;
use Fastrack\Properties\RoomRepository;
use Fastrack\Properties\SubTypeRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Slideshows\Slideshow;
use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;

class SubTypeController extends Controller
{

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var RoomRepository
     */
    private $roomRepository;
    /**
     * @var SubTypeRepository
     */
    private $subTypeRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    public function __construct(PropertyRepository $propertyRepository,
                                RoomRepository $roomRepository,
                                SubTypeRepository $subTypeRepository,
                                SettingRepository $settingRepository)
    {
        $this->propertyRepository = $propertyRepository;
        $this->roomRepository = $roomRepository;
        $this->subTypeRepository = $subTypeRepository;
        $this->settingRepository = $settingRepository;
    }

    public function index($roomId)
    {
        $model = $this->roomRepository->get($roomId);
        $property = $model->property;

        return view('admin.rooms.subtype.index', compact('property', 'model'));
    }

    public function updateSorting(Request $request, $roomId)
    {
        //return $request->all();
        $this->subTypeRepository->sortTypes($request->all());
    }


    public function create($roomId)
    {
        $model = $this->roomRepository->get($roomId);
        $property = $model->property;

        return view('admin.rooms.subtype.create', compact('property', 'model'));
    }

    public function store(Request $request, $roomId)
    {
        $type = $this->subTypeRepository->create($request->all(), $roomId);

        flash()->success($type->room->property->present()->roomNoun() . ' Sub-type created');

        return redirect()->route('subtypes_path', [$roomId]);
    }

    public function edit($subTypeId)
    {
        $type = $this->subTypeRepository->get($subTypeId);
        $model = $type->room;
        $property = $model->property;

        return view('admin.rooms.subtype.edit', compact('property', 'model', 'type'));
    }

    public function update(Request $request, $subTypeId)
    {
        $this->subTypeRepository->update($subTypeId, $request->all());

        flash()->success('Sub-type updated');

        return redirect()->back();
    }

    public function content($subTypeId)
    {
        $type = $this->subTypeRepository->get($subTypeId);
        $model = $type->room;
        $property = $model->property;

        $type->load('content');

        $settings = $this->settingRepository->gatherSettings('rooms');

        return view('admin.rooms.subtype.content', compact('property', 'model', 'type', 'settings'));
    }

    public function updateContent(Request $request, $subTypeId)
    {
        $type = $this->subTypeRepository->get($subTypeId);
        $this->subTypeRepository->updateContent($type, $request->all('content'));

        $this->settingRepository->updateAll($type,
                                            $request->get('setting'));

        flash()->success('Sub-type content updated');

        return redirect()->back();
    }

    public function slideshow($subTypeId)
    {
        $type = $this->subTypeRepository->get($subTypeId);
        $model = $type->room;
        $property = $model->property;

        if( empty($type->slideshow_id))
        {
            $slideshow = Slideshow::create(['name' => $type->name]);

            $type->slideshow_id = $slideshow->id;

            $type->save();
        }

        return view('admin.rooms.subtype.slideshow', compact('property', 'model', 'type'));
    }

    public function seo($subTypeId)
    {
        $type = $this->subTypeRepository->get($subTypeId);
        $model = $type->room;
        $property = $model->property;

        $type->load('seo');

        return view('admin.rooms.subtype.seo', compact('property', 'model', 'type'));
    }

    public function updateSeo(Request $request, $subTypeId)
    {
        $type = $this->subTypeRepository->get($subTypeId);
        $this->subTypeRepository->updateSeo($type, $request->all('seo'));

        flash()->success('Sub-type SEO updated');

        return redirect()->back();
    }

    public function destroy($subtypeId)
    {

        $type = $this->subTypeRepository->get($subtypeId);

        $room  = $type->room;

        $this->subTypeRepository->destroy($subtypeId);

        flash()->success('Sub-type removed');

        return redirect()->route('subtypes_path', $room->uuid);
    }
}
