<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Calendars\CalendarRepository;
use Fastrack\Calendars\EventRepository;
use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreateEventRequest;
use Fastrack\Http\Requests\UpdateEventRequest;
use Fastrack\Sites\SettingRepository;
use Fastrack\Slideshows\Slideshow;
use Illuminate\Http\Request;

class EventsController extends Controller
{


    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var CalendarRepository
     */
    private $calendarRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    function __construct(EventRepository $eventRepository, CalendarRepository $calendarRepository, SettingRepository $settingRepository)
    {
        $this->eventRepository = $eventRepository;
        $this->calendarRepository = $calendarRepository;
        $this->settingRepository = $settingRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index($calendarId)
    {
        $calendar = $this->calendarRepository->getById($calendarId);
        $events = $this->eventRepository->getAll($calendarId);

        return view('admin.calendar.events.index', compact('calendar', 'events'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create($calendarId)
    {
        $calendar = $this->calendarRepository->getById($calendarId);

        //$uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        return view('admin.calendar.events.create', compact('calendar'));
    }

    /**
     * @param CreateEventRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateEventRequest $request, $calendarId)
    {
        $this->eventRepository->create($request->all(), $calendarId);

        flash()->success('Event created!');

        return redirect()->route('events_path', $calendarId);
    }

    /**
     * @param $eventId
     * @return \Illuminate\View\View
     */
    public function edit($calendarId, $eventId)
    {
        $calendar = $this->calendarRepository->getById($calendarId);
        $model = $this->eventRepository->getById($eventId, $calendarId);

        return view('admin.calendar.events.edit', compact('calendar', 'model'));
    }

    /**
     * @param UpdateEventRequest $request
     * @param $eventId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateEventRequest $request, $calendarId, $eventId)
    {
        $this->eventRepository->update($eventId, $request->all(), $calendarId);

        flash()->success('Event updated!');

        return redirect()->back();
    }

    /**
     * @param $eventId
     * @return \Illuminate\View\View
     */
    public function content($calendarId, $eventId)
    {
        $calendar = $this->calendarRepository->getById($calendarId);

        $model = $this->eventRepository->getById($eventId, $calendarId);
        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        $settings = $this->settingRepository->gatherSettings('events');

        return view('admin.calendar.events.content', compact('calendar', 'model', 'uploadHash', 'settings'));
    }

    /**
     * @param Request $request
     * @param $eventId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateContent(Request $request, $calendarId, $eventId)
    {
        $this->eventRepository->update($eventId, $request->all(), $calendarId);

        $this->settingRepository->updateAll($this->eventRepository->getById($eventId, $calendarId),
                                            $request->get('setting'));

        flash()->success('Event updated!');

        return redirect()->back();
    }

    /**
     * @param $eventId
     * @return \Illuminate\View\View
     */
    public function slideshow($calendarId, $eventId)
    {
        $calendar = $this->calendarRepository->getById($calendarId);

        $model = $this->eventRepository->getById($eventId, $calendarId);
        //$uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        if( empty($model->slideshow_id))
        {
            $slideshow = Slideshow::create(['name' => $model->name]);

            $model->slideshow_id = $slideshow->id;

            $model->save();
        }

        return view('admin.calendar.events.slideshow', compact('calendar', 'model'));

    }

    //public function updateSlideshow(Request $request, $eventId)
    //{
    //    $this->slideRepository->updateMany($request->all());
    //
    //    flash()->success('Slides updated!');
    //
    //    return redirect()->back();
    //}

    /**
     * @param $eventId
     * @return \Illuminate\View\View
     */
    public function seo($calendarId, $eventId)
    {
        $calendar = $this->calendarRepository->getById($calendarId);

        $model = $this->eventRepository->getById($eventId, $calendarId);

        return view('admin.calendar.events.seo', compact('calendar', 'model'));
    }

    /**
     * @param Request $request
     * @param $eventId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSeo(Request $request, $calendarId, $eventId)
    {
        $event = $this->eventRepository->getById($eventId, $calendarId);

        $this->eventRepository->updateSeo($event, $request->all());

        flash()->success('Event SEO updated!');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $eventId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $calendarId, $eventId)
    {
        $this->eventRepository->destroy($eventId, $calendarId);

        flash()->success('Event removed');

        return redirect()->route('events_path', $calendarId);
    }

}
