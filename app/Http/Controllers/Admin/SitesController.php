<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreateSiteRequest;
use Fastrack\Http\Requests\UpdateSiteRequest;
use Fastrack\Pages\PageRepository;
use Fastrack\Properties\PropertyRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Sites\SiteRepository;
use Illuminate\Http\Request;

/**
 * Class SitesController
 * @package Fastrack\Http\Controllers\Admin
 */
class SitesController extends Controller
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;


    function __construct(
        SiteRepository $siteRepository,
        PropertyRepository $propertyRepository,
        PageRepository $pageRepository,
        SettingRepository $settingRepository
    ) {
        $this->siteRepository = $siteRepository;
        $this->propertyRepository = $propertyRepository;
        $this->pageRepository = $pageRepository;
        $this->settingRepository = $settingRepository;
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $sites = $this->siteRepository->getAll();

        if (count($sites) == 1) {
            \Session::reflash();

            return redirect()->route('site_path', $sites[0]->id);
        }

        return view('admin.sites.index', compact('sites'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.sites.create');
    }

    /**
     * @param CreateSiteRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateSiteRequest $request)
    {
        $site = $this->siteRepository->create($request->all());

        flash()->success('Site \'' . $site->name . '\' create!');

        return redirect()->route('site_path', $site->id);
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function edit($siteId)
    {
        $site = $this->siteRepository->getById($siteId);

        $pages = $this->pageRepository->getAllDropdown($siteId);

        $properties = $this->propertyRepository->getAllDropdown($siteId);

        return view('admin.sites.edit', compact('site', 'pages', 'properties'));
    }

    /**
     * @param UpdateSiteRequest $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateSiteRequest $request, $siteId)
    {
        $formData = $request->all();

        if( ! isset($formData['in_development'])) $formData['in_development'] = false;
        if( ! isset($formData['secure'])) $formData['secure'] = false;

        $this->siteRepository->update($siteId, $formData);

        flash()->success('Site updated');

        return redirect()->back();
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function seo($siteId)
    {
        $site = $this->siteRepository->getById($siteId);

        return view('admin.sites.seo', compact('site'));
    }

    /**
     * @param Request $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSeo(Request $request, $siteId)
    {
        $this->siteRepository->updateSeo($siteId, $request->all('seo'));

        flash()->success('Site SEO updated');

        return redirect()->back();
    }

    /**
     * @param $siteId
     * @return \Illuminate\View\View
     */
    public function properties($siteId)
    {
        $site = $this->siteRepository->getById($siteId);
        $properties = $this->propertyRepository->getAll();

        return view('admin.sites.properties', compact('site', 'properties'));

    }

    /**
     * @param Request $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProperties(Request $request, $siteId)
    {
        $this->siteRepository->updateProperties($siteId, $request->all());

        flash()->success('Site properties updated');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $siteId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $siteId)
    {
        $this->siteRepository->destroy($request->get('id'));

        flash()->success('Site removed');

        return redirect()->route('sites_path');
    }


    // Custom Settings
    public function settings($siteId)
    {
        $site = $this->siteRepository->getById($siteId);

        $settings = [];

        try {
            // Get the settings skeleton from the manifest file
            $manifest = file_get_contents(public_path('themes/' . $site->theme . '/manifest.json'));

            $settings = json_decode($manifest)->settings;

        } catch (\Exception $e) {

        }

        return view('admin.sites.settings', compact('site', 'settings'));
    }

    public function updateSettings(Request $request, $siteId)
    {
        $site = $this->siteRepository->getById($siteId);

        $this->settingRepository->updateAll($site, $request->get('setting'));

        flash()->success('Site settings updated');

        return redirect()->back();
    }







}
