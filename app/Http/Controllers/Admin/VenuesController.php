<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Http\Requests\CreateVenueRequest;
use Fastrack\Http\Requests\UpdateVenueRequest;
use Fastrack\Locations\CategoryRepository;
use Fastrack\Locations\VenueRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Sites\Site;
use Fastrack\Sites\SiteRepository;
use Fastrack\Slideshows\SlideRepository;
use Fastrack\Slideshows\Slideshow;
use Illuminate\Http\Request;

/**
 * Class VenuesController
 * @package Fastrack\Http\Controllers\Admin
 */
class VenuesController extends Controller
{


    /**
     * @var VenueRepository
     */
    private $venueRepository;
    /**
     * @var SlideRepository
     */
    private $slideRepository;
    /**
     * @var SiteRepository
     */
    private $siteRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @param VenueRepository $venueRepository
     * @param SlideRepository $slideRepository
     */
    function __construct(VenueRepository $venueRepository,
                        SlideRepository $slideRepository,
                        SiteRepository $siteRepository,
                        SettingRepository $settingRepository,
                        CategoryRepository $categoryRepository)
    {
        $this->venueRepository = $venueRepository;
        $this->slideRepository = $slideRepository;
        $this->siteRepository = $siteRepository;
        $this->settingRepository = $settingRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $venues = $this->venueRepository->getAll();

        return view('admin.venues.index', compact('venues'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->venueRepository->getCategories();
        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        return view('admin.venues.create', compact('categories', 'uploadHash'));
    }

    /**
     * @param CreateVenueRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateVenueRequest $request)
    {
        $venue = $this->venueRepository->create($request->all());

        flash()->success('Venue \'' . $venue->name . '\' created!');

        return redirect()->route('venues_path');
    }

    /**
     * @param $venueId
     * @return \Illuminate\View\View
     */
    public function edit($venueId)
    {
        $model = $this->venueRepository->getById($venueId);
        $categories = $this->venueRepository->getCategories();

        $venueCategories = $model->categories()->lists('id')->all();

        return view('admin.venues.edit', compact('model', 'categories', 'venueCategories'));

    }

    /**
     * @param UpdateVenueRequest $request
     * @param $venueId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateVenueRequest $request, $venueId)
    {
        $formData = $request->all();

        if( ! isset($formData['active'])) $formData['active'] = false;

        $this->venueRepository->update($venueId, $formData);

        flash()->success('Venue updated!');

        return redirect()->back();
    }

    /**
     * @param $venueId
     * @return \Illuminate\View\View
     */
    public function content($venueId)
    {
        $model = $this->venueRepository->getById($venueId);

        $uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        $settings = $this->settingRepository->gatherSettings('places');

        return view('admin.venues.content', compact('model', 'uploadHash', 'settings'));
    }

    /**
     * @param Request $request
     * @param $venueId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateContent(Request $request, $venueId)
    {
        $this->venueRepository->update($venueId, $request->all());

        $this->settingRepository->updateAll($this->venueRepository->getById($venueId),
                                            $request->get('setting'));

        flash()->success('Venue updated!');

        return redirect()->back();
    }

    /**
     * @param $venueId
     * @return \Illuminate\View\View
     */
    public function location($venueId)
    {
        $model = $this->venueRepository->getById($venueId);

        return view('admin.venues.location', compact('model'));
    }

    /**
     * @param Request $request
     * @param $venueId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateLocation(Request $request, $venueId)
    {
        $this->venueRepository->update($venueId, $request->all());

        flash()->success('Venue updated!');

        return redirect()->back();
    }

    /**
     * @param $venueId
     * @return \Illuminate\View\View
     */
    public function slideshow($venueId)
    {
        $model = $this->venueRepository->getById($venueId);
        //$uploadHash = md5(uniqid('', true)); // Move this to a view composer?

        if( empty($model->slideshow_id))
        {
            $slideshow = Slideshow::create(['name' => $model->name]);

            $model->slideshow_id = $slideshow->id;

            $model->save();
        }

        return view('admin.venues.slideshow', compact('model'));

    }

    //public function updateSlideshow(Request $request, $venueId)
    //{
    //    $this->slideRepository->updateMany($request->all());
    //
    //    flash()->success('Slides updated!');
    //
    //    return redirect()->back();
    //}

    /**
     * @param $venueId
     * @return \Illuminate\View\View
     */
    public function seo($venueId)
    {
        $model = $this->venueRepository->getById($venueId);
        $sites = Site::get()->lists('name', 'id')->all();

        return view('admin.venues.seo', compact('model', 'sites'));

    }

    /**
     * @param Request $request
     * @param $venueId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSeo(Request $request, $venueId)
    {
        $venue = $this->venueRepository->getById($venueId);
        $this->venueRepository->updateSeo($venue, $request->all());

        flash()->success('Venue SEO updated!');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $venueId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $venueId)
    {
        $this->venueRepository->destroy($venueId);

        flash()->success('Venue removed');

        return redirect()->route('venues_path');
    }


    //// Categories - move this into it's own repo/controller
    //public function categories()
    //{
    //    $categories = $this->venueRepository->getCategories();
    //
    //    return view('admin.venues.categories.index', compact('categories'));
    //}
    //
    //public function createCategory()
    //{
    //    return view('admin.venues.categories.create');
    //}
    //
    //public function storeCategory(Request $request)
    //{
    //    //$this->venueRepository->createCategory($request->all());
    //
    //    $this->categoryRepository->create($request->all());
    //
    //    flash()->success('Category created!');
    //
    //    return redirect()->route('venue_categories_path');
    //}
    //
    //public function editCategory($categoryId)
    //{
    //    $category = $this->categoryRepository->getById($categoryId);
    //    $category->load('content');
    //    //$category = $this->venueRepository->getCategory($categoryId);
    //
    //    return view('admin.venues.categories.edit', compact('category'));
    //}
    //
    //public function updateCategory(Request $request, $categoryId)
    //{
    //    // Bit hacky...
    //    $category = $this->categoryRepository->getById($categoryId);
    //
    //    $this->categoryRepository->update($category->uuid, $request->all());
    //    //$this->venueRepository->updateCategory($categoryId, $request->all());
    //
    //    flash()->success('Category updated!');
    //
    //    return redirect()->back();
    //}
    //
    //public function destroyCategory(Request $request, $categoryId)
    //{
    //    $category = $this->categoryRepository->getById($categoryId);
    //
    //    $this->categoryRepository->destroy($category->uuid);
    //
    //    //$this->venueRepository->destroyCategory($categoryId, $request->all());
    //
    //    flash()->success('Category removed!');
    //
    //    return redirect()->route('venue_categories_path');
    //}

    // Site-specific stuff
    public function site($uuid)
    {
        $site = $this->siteRepository->get($uuid);

        //$businesses = $this->venueRepository->getUnrelated(site('id'));
        $businesses = $this->venueRepository->getUnrelated($site->id);

        return view('admin.sites.business', compact('site', 'businesses'));
    }

    public function updateSite(Request $request, $uuid)
    {

        $site = $this->siteRepository->get($uuid);

        // First update the site settings...
        $this->siteRepository->update($site->id, $request->all());

        if($request->get('business_feed') == 'manual' && $request->has('business'))
        {
            $this->venueRepository->updateSite($site->id, $request->get('business'));
        }

        //$this->roomRepository->updateAmenities($roomId, $request->all(), $propertyId);

        flash()->success('Places Feed updated!');

        return redirect()->back();
    }

    public function removeFromSite(Request $request, $uuid)
    {
        $site = $this->siteRepository->get($uuid);

        return $this->venueRepository->removeFromSite($site->id, $request->get('id'));
    }

}
