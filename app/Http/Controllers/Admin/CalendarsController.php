<?php namespace Fastrack\Http\Controllers\Admin;

use Fastrack\Calendars\CalendarRepository;
use Fastrack\Http\Controllers\Controller;
use Fastrack\Http\Requests;
use Fastrack\Sites\Site;
use Illuminate\Http\Request;

/**
 * Class CalendarsController
 * @package Fastrack\Http\Controllers\Admin
 */
class CalendarsController extends Controller
{

    /**
     * @var CalendarRepository
     */
    private $calendarRepository;

    /**
     * @param CalendarRepository $calendarRepository
     */
    function __construct(CalendarRepository $calendarRepository)
    {
        $this->calendarRepository = $calendarRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $calendars = $this->calendarRepository->getAll();

        return view('admin.calendar.index', compact('calendars'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $sites = Site::get()->lists('name', 'id')->all();

        return view('admin.calendar.create', compact('sites'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->calendarRepository->create($request->all());

        flash()->success('Calendar created!');

        return redirect()->route('calendars_path');
    }

    /**
     * @param $calendarId
     * @return \Illuminate\View\View
     */
    public function edit($calendarId)
    {
        $model = $this->calendarRepository->getById($calendarId);
        $sites = Site::get()->lists('name', 'id')->all();

        return view('admin.calendar.edit', compact('model', 'sites'));
    }

    /**
     * @param Request $request
     * @param $calendarId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $calendarId)
    {
        $this->calendarRepository->update($calendarId, $request->all());

        flash()->success('Calendar updated!');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $calendarId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $calendarId)
    {
        $this->calendarRepository->destroy($calendarId);

        flash()->success('Calendar removed');

        return redirect()->route('calendars_path');
    }
}
