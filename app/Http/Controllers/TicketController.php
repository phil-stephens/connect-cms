<?php namespace Fastrack\Http\Controllers;

use Fastrack\Calendars\EventRepository;
use Fastrack\Calendars\Temp;
use Fastrack\Calendars\Ticket;
use Fastrack\Calendars\TicketPurchase;
use Fastrack\Calendars\TicketRepository;
use Fastrack\Http\Requests;
use Illuminate\Http\Request;

class TicketController extends Controller
{


    /**
     * @var EventRepository
     */
    private $eventRepository;
    /**
     * @var TicketRepository
     */
    private $ticketRepository;


    public function __construct(
        EventRepository $eventRepository,
        TicketRepository $ticketRepository
    ) {
        $this->eventRepository = $eventRepository;
        $this->ticketRepository = $ticketRepository;
    }

    public function details($calendarSlug, $eventSlug)
    {
        $content = getPage('ticket-details');
        $event = $this->eventRepository->getBySlug($eventSlug, $calendarSlug);
        $tickets = $this->ticketRepository->getTypes($event->id);

        return theme('ticket-details', compact('content', 'event', 'tickets'));
    }

    // process
    public function process(Request $request, $calendarSlug, $eventSlug)
    {
        $event = $this->eventRepository->getBySlug($eventSlug, $calendarSlug);

        if ($key = $request->get('AccessCode')) {
            $order = Temp::whereKey($key)->firstOrFail();

            $details = unserialize($order->payload);

            $purchase = new TicketPurchase();

            $purchase->fill($details);

            $purchase->transaction_id = $key;

            $purchase->paid = true;

            $purchase->save();

            // Add details to mailing list...

            // Now do the tickets
            foreach ($details['ticket'] as $ticket) {
                $t = new Ticket();

                $t->fill($ticket);

                $t->ticket_purchase_id = $purchase->id;

                $t->save();

                // Add details to mailing list (if not the same as the purchaser details)
            }

            // Send confirmation email
            \Mail::send('emails.registered', compact('purchase', 'event'), function ($message) use ($purchase) {
                $message->to($purchase->email,
                    $purchase->name)->subject('Thank you for registering with Maximum Occupancy 2015');
            });

            // Send internal confirmation
            if ( ! empty(setting('internal_registration_email'))) {
                \Mail::send('emails.registered-internal', compact('purchase', 'event'),
                    function ($message) use ($purchase, $event) {
                        $message->to(setting('internal_registration_email'))->subject('New registration for ' . $event->name);
                    });
            }


            \Session::flash('purchase_id', $purchase->id);

            Temp::whereKey($key)->delete();

            return redirect()->to($event->ticketUrl('confirmed'));
        }
    }

    // Confirmation
    public function confirmed($calendarSlug, $eventSlug)
    {
        \Session::reflash();

        $content = getPage('ticket-confirmed');
        $event = $this->eventRepository->getBySlug($eventSlug, $calendarSlug);
        $purchase = $this->ticketRepository->getPurchase(\Session::get('purchase_id'));

        return theme('ticket-confirmed', compact('content', 'event', 'purchase'));
    }

    // Failure
    public function failure($calendarSlug, $eventSlug)
    {

    }
}
