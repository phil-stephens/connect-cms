<?php namespace Fastrack\Http\Controllers;

use Request as R;

/**
 * Class FrontController
 * @package Fastrack\Http\Controllers
 */
abstract class FrontController extends Controller
{

    /**
     * @return bool
     */
    protected function loadPageContent()
    {
        $path = R::path();

        try {
            return getPage($path);
        } catch (\Exception $e) {
            return false;
        }

    }

}
