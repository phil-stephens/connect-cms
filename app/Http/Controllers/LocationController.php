<?php namespace Fastrack\Http\Controllers;

use Fastrack\Http\Requests;
use Fastrack\Locations\VenueRepository;
use Fastrack\Pages\PageRepository;
use Illuminate\Http\Request;

/**
 * Class LocationController
 * @package Fastrack\Http\Controllers
 */
class LocationController extends Controller
{

    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var VenueRepository
     */
    private $venueRepository;

    /**
     * @param PageRepository $pageRepository
     * @param VenueRepository $venueRepository
     */
    function __construct(PageRepository $pageRepository, VenueRepository $venueRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->venueRepository = $venueRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $content = $this->pageRepository->getBySlug('location');
        $categories = $this->venueRepository->getCategories();
        $venues = $this->venueRepository->getByCategory($request->get('category')); // Site ID

        return theme('location', compact('content', 'categories', 'venues'));
    }

    /**
     * @param $venueSlug
     * @return \Illuminate\View\View
     */
    public function venue($venueSlug)
    {
        $content = $this->venueRepository->getBySlug($venueSlug);

        return theme('venue', compact('content'));
    }

}
