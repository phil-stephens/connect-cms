<?php namespace Fastrack\Http\Controllers;

use Fastrack\Calendars\EventRepository;
use Fastrack\Http\Requests;
use Fastrack\Locations\VenueRepository;
use Fastrack\News\FeedRepository;
use Fastrack\Pages\PageRepository;

/**
 * Class SitemapController
 * @package Fastrack\Http\Controllers
 */
class SitemapController extends Controller
{

    /**
     * @var
     */
    protected $domain;

    /**
     * @var
     */
    protected $xml;

    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * @var VenueRepository
     */

    /**
     * @var FeedRepository
     */
    private $feedRepository;
    /**
     * @var EventRepository
     */
    private $eventRepository;


    function __construct(
        PageRepository $pageRepository,
        VenueRepository $venueRepository,
        FeedRepository $feedRepository,
        EventRepository $eventRepository
    ) {
        $this->pageRepository = $pageRepository;
        $this->venueRepository = $venueRepository;
        $this->feedRepository = $feedRepository;
        $this->eventRepository = $eventRepository;
    }

    public function robots()
    {
        $robots = "User-agent: *\n";

        if(site('in_development'))
        {
            $robots .= "Disallow: /";
        } else
        {
            $robots .= "Disallow: /.admin/\nSitemap: " . asset('sitemap.xml');
        }

        //$robots = "User-agent: *\nDisallow: /.admin/\nSitemap: " . asset('sitemap.xml');

        return response($robots, 200, ['Content-Type' => 'text/plain']);
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function show()
    {
        if(site('in_development')) abort(404);

        $domain = site()->primary_domain;

        $prefix = (site()->secure) ? 'https://' : 'http://';

        $this->domain = $prefix . $domain->domain; // . '/';

        $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');


        // Homepage
        $this->addURLNode($this->domain, $this->updatedAt(site()->homepage), 'monthly', 1);

        // Get all of the static pages
        $pages = $this->pageRepository->getBySite(site('id'));

        foreach ($pages as $page) {
            if ( ! $this->isHomepage($page) && ! $page->internal && ! $page->seo->hide_on_sitemap) {
                $this->addURLNode($page->getUrl(), $this->updatedAt($page), 'monthly', 0.75);
            }
        }

        // Get the property/ies - then the rooms
        $properties = site()->properties;

        foreach ($properties as $property) {
            foreach ($property->rooms as $room) {
                //$this->addURLNode($property->room_noun . '/' . $room->slug , $this->updatedAt($room), 'weekly', 0.5);
                if ( ! $room->seo->hide_on_sitemap) {
                    $this->addURLNode($room->getUrl(), $this->updatedAt($room), 'weekly', 0.5);
                }
            }
        }

        // offers
        $offers = offers(site('id'));

        foreach ($offers as $offer) {
            if ( ! $offer->seo->hide_on_sitemap) {
                $this->addURLNode($offer->getUrl(), $this->updatedAt($offer), 'monthly', 0.5);
            }
        }


        // venues
        $venues = $this->venueRepository->getAll();

        foreach ($venues as $venue) {
            //$this->addURLNode('location/venue/' . $venue->slug, $this->updatedAt($venue), 'monthly', 0.5);
            if ( ! $venue->seo->hide_on_sitemap) {
                $this->addURLNode($venue->getUrl(), $this->updatedAt($venue), 'monthly', 0.5);
            }
        }


        // events
        $events = $this->eventRepository->getAllUpcoming(site('id'));

        foreach ($events as $event) {
            //$this->addURLNode('calendar/event/' . $event->slug, $this->updatedAt($event), 'weekly', 0.5);
            if ( ! $event->seo->hide_on_sitemap) {
                $this->addURLNode($event->getUrl(), $this->updatedAt($event), 'weekly', 0.5);
            }
        }

        // articles
        $articles = $this->feedRepository->getFeedArticles(null, site('id'));

        foreach ($articles as $article) {
            if ( ! $article->seo->hide_on_sitemap) {
                $this->addURLNode($article->getUrl(), $this->updatedAt($article), 'monthly', 0.5);
            }
        }

        return response($this->xml->asXML(), 200, ['Content-Type' => 'application/xml']);
    }

    /**
     * @param $page
     * @return bool
     */
    private function isHomepage($page)
    {
        if (class_basename(site()->homepage) == 'Page' && site()->homepage->id == $page->id) {
            return true;
        }

        return false;
    }

    /**
     * @param $model
     * @return mixed
     */
    private function updatedAt($model)
    {
        $first = $model->updated_at;
        $second = $model->content->updated_at;

        return $first->gt($second) ? $first : $second;
    }

    /**
     * @param $path
     * @param $updated_at
     * @param $frequency
     * @param $priority
     */
    private function addURLNode($path, $updated_at, $frequency, $priority)
    {
        $url = $this->xml->addChild('url');
        $url->addChild('loc', $path);
        $url->addChild('lastmod', $updated_at->format('Y-m-d'));
        $url->addChild('changefreq', $frequency);
        $url->addChild('priority', $priority);
    }

}
