<?php namespace Fastrack\Http\Controllers;

use Fastrack\Http\Requests;
use Fastrack\Mail\Vision6;
use Fastrack\Messages\Message;
use Fastrack\Pages\PageRepository;
use Fastrack\Subscribers\SubscriberRepository;
use Illuminate\Http\Request;

/**
 * Class FormController
 * @package Fastrack\Http\Controllers
 */
class FormController extends Controller
{

    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var SubscriberRepository
     */
    private $subscriberRepository;

    /**
     * @param PageRepository $pageRepository
     * @param SubscriberRepository $subscriberRepository
     */
    function __construct(PageRepository $pageRepository, SubscriberRepository $subscriberRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->subscriberRepository = $subscriberRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function wildcard(Request $request)
    {
        // Do something with the $request->path()
        // maybe some form-specific settings?

        // Go ahead and handle the form submission
        if ($request->get('target')) {

            if($this->filterAutomaticSubmissions($request))
            {
                $this->handleFormSubmission($request);

                flash()->success('Thank you for your enquiry.');
            }

        }

        return redirect()->back();
    }

    private function filterAutomaticSubmissions(Request $request)
    {
        if($field = $request->get('uri')) return false;

        if($timestamp = $request->get( md5( $request->get('target') ) ))
        {
            return time() > ($timestamp + env('ANTISPAM_TIME_LIMIT', 2));
        }

        return true;
    }

    /**
     * @param Request $request
     */
    private function handleFormSubmission(Request $request)
    {
        $to = base64_decode($request->get('target'));

        $m = new Message(['payload' => serialize($request->except(['_token', 'target', 'uri', md5($to)]))]);

        site()->messages()->save($m);

        $replyTo = $request->get('email');

        $content = "";

        foreach ($request->all() as $key => $value) {
            if ( ! in_array($key, ['_token', 'target', 'uri', md5($to)])) {
                $content .= "{$key}: {$value}\n";
            }
        }

        \Mail::raw($content, function ($message) use ($to, $replyTo) {
            $message->from('noreply@fastrackcms.com', site('name') . ' Website');

            if ( ! empty($replyTo)) {
                $message->replyTo($replyTo);
            }

            $message->subject('Enquiry from ' . site('name') . ' website');

            $message->to($to);

        });


    }

    // Need to add some validation on the request
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleMailingList(Request $request)
    {
        $subscriber = $this->subscriberRepository->create($request->all(), site());

        // Need to do some conditional stuff here to integrate with multiple providers...

        if (config('services.vision6.endpoint')) {
            try {
                $api = new Vision6(config('services.vision6.endpoint'), config('services.vision6.api_key'), '3.0');

                // Enable request debugging
                $api->setDebug(true);

                $contact_details = [
                    env('CLIENT_VISION6_EMAIL_FIELD', 'Email')           => $subscriber->email,
                    env('CLIENT_VISION6_FIRST_NAME_FIELD', 'First Name') => $subscriber->first_name,
                    env('CLIENT_VISION6_LAST_NAME_FIELD', 'Surname')     => $subscriber->last_name
                ];

                $listId = setting('vision6_list_id', config('services.vision6.list_id'));

                $contact_id = $api->invokeMethod('subscribeContact', $listId, $contact_details);

                $subscriber->external_id = $contact_id;
                $subscriber->save();
            } catch (\Exception $e) {

            }


        }

        if ($request->ajax()) {
            return response()->json(['success' => 'Thank you for your interest.'], 200);
        }

        flash()->success('Thank you for your interest.');

        return redirect()->back();
    }
}
