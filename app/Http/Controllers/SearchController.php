<?php namespace Fastrack\Http\Controllers;

use Carbon\Carbon;
use Fastrack\Http\Requests;
use Fastrack\Integrations\Integration;
use Fastrack\Integrations\IntegrationRepository;
use Fastrack\Properties\PropertyRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SearchController extends Controller
{

    /**
     * @var IntegrationRepository
     */
    private $integrationRepository;
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;


    public function __construct(IntegrationRepository $integrationRepository, PropertyRepository $propertyRepository)
    {
        $this->integrationRepository = $integrationRepository;
        $this->propertyRepository = $propertyRepository;
    }

    public function show(Request $request, $uuid = null)
    {
        if( ! $request->has('checkin_date')) return redirect()->to('/');

        $params = $request->all();

        // Allow for optional man-in-the-middle...
        if(site()->properties()->count() == 1)
        {

        }

        $content = getPage('search');

        $group = false;

        return theme('search', compact('content', 'params', 'uuid', 'group'));
    }

    public function group(Request $request)
    {
        if( ! $request->has('checkin_date')) return redirect()->to('/');

        $params = $request->all();

        // What if there is no group?
        if(site()->properties()->count() == 1)
        {
            return redirect()->route('search_path')->withInput();
        }

        $content = getPage('search');

        $group = true;
        $uuid = null;

        return theme('search', compact('content', 'params', 'uuid', 'group'));
    }

    public function property(Request $request, $propertyUuid, $uuid = null)
    {
        // first, last

        if( ! $request->has('checkin_date')) return redirect()->to('/');

        $params = $request->all();

        $property = $this->propertyRepository->get($propertyUuid);

        $content = getPage('search');

        //$uuid = null;


        return theme('search', compact('content', 'params', 'property', 'uuid'));
    }

    public function process(Request $request)
    {
        $uuid = $request->get('uuid');
        //dd($request->all());

        //$start = $request->get('checkin_date');

        $first = new Carbon($request->get('checkin_date'));

        if($request->has('checkout_date'))
        {
            $last = new Carbon($request->get('checkout_date'));

            if( ! $request->has('no_checkout_offset')) $last->subDay();
        } else
        {
            $last = new Carbon($request->get('checkin_date'));

            $last->addDays( $request->get('nights') -  1);
        }


        $excess = env('SEARCH_DISPLAY_NIGHTS', 13) - $request->get('nights');

        $before = floor($excess / 2);

        $after = ceil($excess / 2);

        // Do better filtering of dates here
        $today = Carbon::today();

        $diff = $today->diffInDays($first);


        if($diff <= $before)
        {
            $after += ($before - $diff);
            $before = $diff;
        }

        $searchStart = new Carbon($request->get('checkin_date'));

        $searchStart->subDays($before)->toDateString();

        $searchFinish = new Carbon($request->get('checkin_date'));

        $searchFinish->addDays($request->get('nights') + $after)->toDateString();

        // Do pre-filtering here...
        if($request->has('property'))
        {
            $property = $this->propertyRepository->get($request->get('property'));

            $properties = [
                $property->id
            ];
        } else
        {
            $properties = site()->properties->lists('id')->all();
        }

        $integrations = $this->integrationRepository->getSelected($properties);

        //dd($integrations);
        $results = new Collection([]);

        foreach($integrations as $integration)
        {
            if($integration->active)
            {
                $type = $integration->type;

                $provider = new $type($integration);

                $results = $results->merge( $provider->search($searchStart, $searchFinish) );
            }
        }

        $results = $this->postFilter($results, $properties, $uuid, $combineProperties = (bool) $request->get('group'));

        if((bool) $request->get('group'))
        {
            $template = 'layouts.search-results-group';
        } else
        {
            $template = 'layouts.search-results';

        }

        return view($template, compact('results', 'first', 'last', 'uuid'));
    }

    private function postFilter($results, $properties, $uuid, $combine_properties = false)
    {
        // remove unwanted properties
        foreach($results as $key => $result)
        {
            if( ! in_array($result->room->property_id, $properties))
            {
                $results->forget($key);
            }
        }

        if($combine_properties)
        {
            $properties = [];
            $dates = [];

            foreach($results as $result)
            {
                // Aggregation logic goes here...
                if( ! isset($properties[$result->room->property->uuid]))
                {
                    $properties[$result->room->property->uuid] = new \stdClass();

                    $properties[$result->room->property->uuid]->property = $result->room->property;

                    foreach($result->dates as $key => $date)
                    {
                        $dates[$result->room->property->uuid][$key] = [
                            'date'      => $date['date'],
                            'available' => $date['available'],
                            'rate'      => $date['rate']
                        ];
                    }


                } else
                {
                    foreach($result->dates as $key => $date)
                    {
                        $dates[$result->room->property->uuid][$key]['available'] += $date['available'];

                        if($date['rate'] < $dates[$result->room->property->uuid][$key]['rate'])
                        {
                            $dates[$result->room->property->uuid][$key]['rate'] = $date['rate'];
                        }
                    }
                }
            }

            foreach($dates as $key => $value)
            {
                $properties[$key]->dates = collect($value);
            }

            $results = collect($properties);

            //dd($results);

        } elseif($uuid)
        {
            foreach($results as $key => $result)
            {
                if($result->room->uuid == $uuid)
                {
                    $results->forget($key);

                    $results->prepend($result);

                    break;
                }
            }
        }

        return $results;
    }

}
