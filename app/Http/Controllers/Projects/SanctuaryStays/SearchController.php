<?php

namespace Fastrack\Http\Controllers\Projects\SanctuaryStays;

use Illuminate\Http\Request;
use Fastrack\Http\Requests;
use Fastrack\Http\Controllers\Controller;


use DB;

use Fastrack\Searches\Search;
use Fastrack\Properties\Room;

class SearchController extends Controller
{
    public function doSearch(Request $request)
    {
        // get the request variables
        $type = $request->get('apartment_type');
        $price1 = $request->get('price1');
        $price2 = $request->get('price2');
        $people = intval($request->get('people'));

        // Find the type of $type and run the search


        // Sets accommodation type to 'All', if none selected
        if (is_null($type)) {
            $type = array('All');
        }

        if (in_array("All", $type)) {
            $results = DB::table('rooms')
                ->whereBetween('price', [$price1, $price2])
                ->where('people', '>=', $people)
                ->lists('id');

        } else {
            $results = DB::table('rooms')
                ->whereBetween('price', [$price1, $price2])
                ->whereIn('type', $type)
                ->where('people', '>=', $people)
                ->lists('id');
        }


        $search = Search::create(['payload' => serialize($results)]);

        return redirect('ss/search/results/' . $search->uuid);
    }





    public function show($uuid)
    {

        $search = Search::where('uuid', $uuid)->firstOrFail();

        $list = unserialize( $search->payload);

        $results = Room::whereIn('id', $list )->get();

        $content = getPage('search-results');

        $roomIds = '';
        foreach ($list as $id) {
            $roomIds .= $id;
        }

        $test = urlencode(serialize($list));

        \Session::put('current_search', $list);
        \Session::put('search_results', $uuid);


        // return $rooms;
        return theme('search_results', compact('content', 'results', 'test', 'uuid'));
    }

    public function doBasicSearch(Request $request) {

        $type = $request->get('apartment_type');
        $price_range = $request->get('price_range');
        // $price1;
        // $price2;
        $people = intval($request->get('people'));


        if ($price_range == '1') {
            $price1 = 0;
            $price2 = 100;
        } elseif ($price_range == '2') {
            $price1 = 101;
            $price2 = 250;
        } elseif ($price_range == '3') {
            $price1 = 251;
            $price2 = 500;
        } elseif ($price_range == '4') {
            $price1 = 501;
            $price2 = 1000;
        } elseif ($price_range == '5') {
            $price1 = 1000;
            $price2 = 10000;
        } else {
            $price1 = 0;
            $price2 = 10000;
        }

        if ($type == 'All') {
            $results = DB::table('rooms')
                ->whereBetween('price', [$price1, $price2])
                ->where('people', '>=', $people)
                ->lists('id');
        } else {
            $results = DB::table('rooms')
                ->whereBetween('price', [$price1, $price2])
                ->where('type', $type)
                ->where('people', '>=', $people)
                ->lists('id');
        }

        $search = Search::create(['payload' => serialize($results)]);

        return redirect('ss/search/results/' . $search->uuid);
    }
}
