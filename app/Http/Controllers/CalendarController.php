<?php namespace Fastrack\Http\Controllers;

use Fastrack\Calendars\EventRepository;
use Fastrack\Http\Requests;
use Fastrack\Pages\PageRepository;

/**
 * Class CalendarController
 * @package Fastrack\Http\Controllers
 */
class CalendarController extends Controller
{


    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var EventRepository
     */
    private $eventRepository;


    function __construct(PageRepository $pageRepository, EventRepository $eventRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->eventRepository = $eventRepository;
    }

    public function index($calendarSlug = null)
    {
        $content = $this->pageRepository->getBySlug('calendar', site('id'));

        $events = $this->eventRepository->getAllUpcoming(site('id'), $calendarSlug);

        return theme('calendar', compact('content', 'events'));
    }

    public function show($calendarSlug, $eventSlug)
    {
        $content = $this->eventRepository->getBySlug($eventSlug, $calendarSlug);

        return theme('calendar_event', compact('content'));
    }

}
