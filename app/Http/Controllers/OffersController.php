<?php namespace Fastrack\Http\Controllers;

use Fastrack\Http\Requests;
use Fastrack\Offers\OfferRepository;

/**
 * Class OffersController
 * @package Fastrack\Http\Controllers
 */
class OffersController extends Controller
{

    /**
     * @var OfferRepository
     */
    private $offerRepository;

    /**
     * @param OfferRepository $offerRepository
     */
    public function __construct(OfferRepository $offerRepository)
    {
        $this->offerRepository = $offerRepository;
    }

    public function index()
    {
        $offers = $this->offerRepository->getAllForSite(site('id'));
        $content = getPage('offers', site('id'));

        return theme('offers', compact('content', 'offers'));
    }

    /**
     * @param $propertyId
     * @param $offerSlug
     * @return \Illuminate\View\View
     */
    public function show($propertyId, $offerSlug)
    {
        $content = $this->offerRepository->getBySlug($offerSlug, $propertyId);

        return theme('offer', compact('content'));
    }

}
