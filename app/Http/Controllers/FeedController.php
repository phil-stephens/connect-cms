<?php namespace Fastrack\Http\Controllers;

use Fastrack\Http\Requests;
use Fastrack\News\ArticleRepository;
use Fastrack\News\FeedRepository;

/**
 * Class FeedController
 * @package Fastrack\Http\Controllers
 */
class FeedController extends FrontController
{

    /**
     * @var FeedRepository
     */
    private $feedRepository;
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * @param FeedRepository $feedRepository
     * @param ArticleRepository $articleRepository
     */
    function __construct(FeedRepository $feedRepository, ArticleRepository $articleRepository)
    {
        $this->feedRepository = $feedRepository;
        $this->articleRepository = $articleRepository;
    }

    /**
     * @param null $feedSlug
     * @return \Illuminate\View\View
     */
    public function index($feedSlug = null)
    {
        $content = getPage('feed');
        $articles = $this->feedRepository->getFeedArticlesPaginated($feedSlug, site('id'), 'simple');

        return theme('feed', compact('content', 'articles'));
    }

    /**
     * @param $feedSlug
     * @param $articleSlug
     * @return \Illuminate\View\View
     */
    public function article($feedSlug, $articleSlug)
    {
        $feed = $this->feedRepository->getBySlug($feedSlug);
        $content = $this->articleRepository->getBySlug($articleSlug, $feed->id);

        return theme('article', compact('feed', 'content'));
    }

}
