<?php namespace Fastrack\Http\Controllers;

use Fastrack\Calendars\Temp;
use Fastrack\Http\Requests;
use Fastrack\Tickets\DiscountCode;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Validator;

//use GuzzleHttp\Psr7\Request as GuzzleRequest;

class CommerceController extends Controller
{


    public function eWayAccessToken(Request $request)
    {

        // Request the access token from eWay

        // This will all need to be inserted dynamically
        $endpoint = setting('eway_api_endpoint', 'https://api.sandbox.ewaypayments.com/AccessCodesShared');
        $apiKey = setting('eway_api_key', 'C3AB9CBoejSm5+sxMJ/G1P0hb3IKUEEx7fKFcJgECh9INXqBID8y+C8Koi8Tc5VszKgSK5');
        $apiPassword = setting('eway_api_password', 'Fastrack21');


        $headers = [
            'Content-Type' => 'application/json',
        ];


        $parts = explode(' ', $request->get('name'));

        if (count($parts) > 1) {
            $last_name = array_pop($parts);
            $first_name = implode(' ', $parts);
        } else {
            $last_name = '';
            $first_name = $request->get('name');
        }

        $reference = str_random();

        $body = [
            'Payment'          => [
                'TotalAmount'  => $request->get('price') * 100,
                'CurrencyCode' => setting('currency_code', 'AUD'),
                //"InvoiceReference" => $reference
            ],
            'RedirectUrl'      => route('eway_redirect_path'),
            'CancelUrl'        => route('eway_cancel_path'),
            'Method'           => 'ProcessPayment',
            'TransactionType'  => 'Purchase',
            // Add all the other fields later...
            'Customer'         => [
                'Reference'      => $reference,
                "FirstName"      => $first_name,
                "LastName"       => $last_name,
                "CompanyName"    => $request->get('company'),
                "JobDescription" => $request->get('position'),
                "Street1"        => $request->get('address1'),
                "Street2"        => $request->get('address2'),
                "City"           => $request->get('city'),
                "PostalCode"     => $request->get('postcode'),
                "Country"        => strtolower($request->get('country')),
                "Email"          => $request->get('email'),
                "Phone"          => $request->get('phone')
            ],
            //"LogoUrl": "https://mysite.com/images/logo4eway.jpg",
            "HeaderText"       => "Maximum Occupancy",
            "Language"         => "EN",
            "CustomerReadOnly" => true
        ];


        $client = new Client();

        $response = $client->post($endpoint, [
            'headers' => $headers,
            'body'    => json_encode($body),
            'auth'    => [$apiKey, $apiPassword]
        ]);

        $payload = $response->getBody();

        $credentials = json_decode($payload);

        // Dump the request payload
        Temp::create([
            'key'     => $credentials->AccessCode,
            'payload' => serialize($request->all())
        ]);

        // Send back the entire JSON string
        return $payload;
    }

    public function checkDiscount(Request $request)
    {
        //return $request->all();

        $code = DiscountCode::whereCode($request->get('code'))->firstOrFail();

        return $code->toJson();
    }

}
