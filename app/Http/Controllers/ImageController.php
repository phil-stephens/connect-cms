<?php namespace Fastrack\Http\Controllers;

use Fastrack\Http\Requests;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\Glide\Api\Api;
use League\Glide\Server;

//use Fastrack\Glide\Server;

/**
 * Class ImageController
 * @package Fastrack\Http\Controllers
 */
class ImageController extends Controller
{

    /**
     * @param Request $request
     */
    public function cache(Request $request)
    {
        // Need to add signature checking...
        //try {
        //    SignatureFactory::create( env('GLIDE_SIGNATURE') )->validateRequest($request);
        //} catch (SignatureException $e) {
        //    // Handle error
        //    abort(404);
        //}

        $path = '/uploads' . env('FILESYSTEM_PREFIX', '');

        // Set image source
        $source = new Filesystem(new Local(storage_path() . $path));

        // Set image cache
        $cache = new Filesystem(new Local(storage_path() . $path . '/.cache'));

        // Set image manager
        $imageManager = new ImageManager([
            'driver' => env('INTERVENTION_DRIVER', 'gd'),
        ]);

        /*
         * [
            'driver' => env('INTERVENTION_DRIVER', 'gd'),
        ]
         */

        // Set manipulators
        $manipulators = [
            new \League\Glide\Api\Manipulator\Orientation(),
            new \League\Glide\Api\Manipulator\Rectangle(),
            new \League\Glide\Api\Manipulator\Size(2000 * 2000),
            new \League\Glide\Api\Manipulator\Brightness(),
            new \League\Glide\Api\Manipulator\Contrast(),
            new \League\Glide\Api\Manipulator\Gamma(),
            new \League\Glide\Api\Manipulator\Sharpen(),
            new \League\Glide\Api\Manipulator\Filter(),
            new \League\Glide\Api\Manipulator\Blur(),
            new \League\Glide\Api\Manipulator\Pixelate(),
            new \League\Glide\Api\Manipulator\Output(),
        ];

        // Set API
        $api = new Api($imageManager, $manipulators);

        // Setup Glide server
        $server = new Server($source, $cache, $api);

        // Set the Base Url
        $server->setBaseUrl('/img/');

        // Make it so!
        $server->outputImage($request);
    }
}
