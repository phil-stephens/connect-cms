<?php namespace Fastrack\Http\Controllers;

use Fastrack\Http\Requests;
use Fastrack\Pages\PageRepository;
use Fastrack\Sites\AliasRepository;
use Illuminate\Http\Request;

/**
 * Class PagesController
 * @package Fastrack\Http\Controllers
 */
class PagesController extends Controller
{


    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var AliasRepository
     */
    private $aliasRepository;

    /**
     * @param PageRepository $pageRepository
     */
    function __construct(PageRepository $pageRepository, AliasRepository $aliasRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->aliasRepository = $aliasRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function homepage(Request $request)
    {
        // Check for Wix redirects
        if(site('wix_redirects') && $request->has('_escaped_fragment_'))
        {
            // get the URL fragment
            if($alias = $this->aliasRepository->getByHashbang(site('id'), $request->get('_escaped_fragment_')))
            {
                return redirect($alias->to, $alias->type);
            }
        }

        $content = site()->homepage;

        return theme(settings('homepage_template', 'homepage'), compact('content'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function wildcard(Request $request)
    {
        $content = $this->pageRepository->getBySlug($request->path());

        if(isSame($content, site()->homepage))
        {
        // If this is the homepage, then bail
        //if (class_basename(site()->homepage) == 'Page' && site()->homepage->id == $content->id) {
            return redirect('/', 301);
        }

        if ($content->template == '_raw') {
            return response($content->getExcerpt(false), 200, ['Content-Type' => 'text/plain']);
        }

        return theme($content->template, compact('content'));
    }
}
