<?php namespace Fastrack\Http\Controllers;

use Crypt;
use Fastrack\Files\FileRepository;
use Fastrack\Http\Requests;
use Fastrack\Images\ImageRepository;
use Storage;

class DownloadController extends Controller
{

    /**
     * @var FileRepository
     */
    private $fileRepository;
    /**
     * @var ImageRepository
     */
    private $imageRepository;

    /**
     * DownloadController constructor.
     */
    public function __construct(FileRepository $fileRepository, ImageRepository $imageRepository)
    {
        $this->fileRepository = $fileRepository;
        $this->imageRepository = $imageRepository;
    }

    public function get($crypt)
    {
        $details = json_decode(Crypt::decrypt($crypt));

        switch ($details->type) {
            case 'File':
                $download = $this->fileRepository->getById($details->id);
                break;

            case 'Image':
                $download = $this->imageRepository->getById($details->id);
                break;
        }

        if (Storage::exists($download->path . '/' . $download->file_name)) {
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Description: File Transfer');
            header("Content-type: {$download->mime_type}");
            header("Content-Disposition: attachment; filename={$download->original_name}");
            header("Expires: 0");
            header("Pragma: public");

            echo Storage::get($download->path . '/' . $download->file_name);
        }
    }

}
