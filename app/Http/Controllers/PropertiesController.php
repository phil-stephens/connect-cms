<?php

namespace Fastrack\Http\Controllers;

use Crypt;
use Fastrack\Http\Requests;
use Fastrack\Pages\PageRepository;
use Fastrack\Properties\PropertyRepository;
use Fastrack\Properties\RoomRepository;

/**
 * Class PropertiesController
 * @package Fastrack\Http\Controllers
 */
class PropertiesController extends Controller
{

    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var RoomRepository
     */
    private $roomRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;

    function __construct(
        PropertyRepository $propertyRepository,
        RoomRepository $roomRepository,
        PageRepository $pageRepository
    ) {
        $this->propertyRepository = $propertyRepository;
        $this->roomRepository = $roomRepository;
        $this->pageRepository = $pageRepository;
    }

    /**
     *
     */
    public function index()
    {
        $content = $this->pageRepository->getBySlug('accommodation');

        // Get the property and output...
        $properties = properties();

        return theme('properties', compact('content', 'properties'));
    }

    /**
     * @param $propertySlug
     * @return \Illuminate\View\View
     */
    public function property($propertySlug)
    {
        // Get the property and output...
        $content = $this->propertyRepository->getBySlug($propertySlug, site('id'));

        return theme('property', compact('content'));
    }

    /**
     * @param null $propertySlug
     * @return \Illuminate\View\View
     */
    public function rooms($propertySlug = null)
    {
        if (empty($propertySlug)) {
            $property = site()->properties->first();

            try {
                $content = getPage(str_plural($property->room_noun, 2), site('id'));
            } catch (\Exception $e) {
                $content = $property;
            }
        } else {
            $property = $this->propertyRepository->getBySlug($propertySlug, site('id'));
            $content = $property;
        }

        return theme('rooms', compact('property', 'content'));
    }

    /**
     * @param $slug1
     * @param null $slug2
     * @return \Illuminate\View\View
     */
    public function room($slug1, $slug2 = null)
    {
        if ( ! empty($slug2)) {
            $property = $this->propertyRepository->getBySlug($slug1, site('id'));
            $content = $this->roomRepository->getBySlug($slug2, $property->id);
        } else {
            $content = $this->roomRepository->getBySlug($slug1, site()->properties->first()->id);
        }

        return theme('room', compact('content'));

    }

    public function subTypes($slug1, $slug2 = null)
    {
        if ( ! empty($slug2)) {
            $property = $this->propertyRepository->getBySlug($slug1, site('id'));
            $content = $this->roomRepository->getBySlug($slug2, $property->id);
        } else {
            $content = $this->roomRepository->getBySlug($slug1, site()->properties->first()->id);
        }

        return theme('subtypes', compact('content'));
    }

    public function subType($slug1, $slug2, $slug3 = null)
    {
        // not really sure what's going on here?!
        return 'Subtype!';
        if ( ! empty($slug3)) {
            $property = $this->propertyRepository->getBySlug($slug1, site('id'));
            $room = $this->roomRepository->getBySlug($slug2, $property->id);

        } else {
            $content = $this->roomRepository->getBySlug($slug1, site()->properties->first()->id);
        }

        return theme('subtype', compact('content'));
    }

    public function rateJSON($uuid)
    {
        try {
            // Find the property...
            $property = $this->propertyRepository->get($uuid);

            $room = $this->propertyRepository->getCheapestRoom($property->id);
        } catch (\Exception $e) {
            try {
                // Find the room...
                $room = $this->roomRepository->get($uuid);

            } catch (\Exception $e) {

            }
        }

        if ( ! empty($room) && ! empty($room->dynamic_rate_data)) {

            $dynamic_data = (is_string($room->dynamic_rate_data)) ? unserialize($room->dynamic_rate_data) : $room->dynamic_rate_data;

            $data = [
                'property' => $room->property->name,
                'name'     => $room->name,
                'price'    => $room->price,
                'date'     => $dynamic_data['date']->format('U')
            ];

            return response()->json($data, 200);
        }

        return abort(404);
    }

    // A quick function to output the best rates for all rooms in a property
    public function getRates($uuid)
    {
        $property = $this->propertyRepository->get($uuid);

        $data = [];

        foreach($property->rooms as $room)
        {
            if(! empty($room->dynamic_rate_data))
            {
                $dynamic_data = (is_string($room->dynamic_rate_data)) ? unserialize($room->dynamic_rate_data) : $room->dynamic_rate_data;

                $data[$room->id] = [
                    'name'    => $room->name,
                    'rate'    => $room->price,
                    'date'    => $dynamic_data['date']->format('U')
                ];
            }

        }

        return response()->json($data, 200);
    }
}
