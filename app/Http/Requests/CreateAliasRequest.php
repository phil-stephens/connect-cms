<?php namespace Fastrack\Http\Requests;

class CreateAliasRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'site_id' => 'required',
            'from'    => 'required',
            'to'      => 'required'
        ];
    }

}
