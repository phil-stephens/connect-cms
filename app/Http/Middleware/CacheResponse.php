<?php namespace Fastrack\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Storage;

class CacheResponse
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (env('USE_PAGE_CACHE', true)) {

            // Check to see whether the response should be cached
            $parameters = $request->route()->getAction();

            if ( ! empty($parameters['cache'])) {
                // Perform action
                $hashName = md5($request->getHost() . '/' . trim($request->path(), '/'));

                //dd($response->getContent());
                Storage::put('.cache/' . $hashName, $response->getContent());
                //file_put_contents( storage_path('page_cache/' . $hashName), $response->getContent());
            }
        }

        return $response;
    }

}
