<?php namespace Fastrack\Http\Middleware;

use App;
use Closure;
use Fastrack\Sites\Site;
use Fastrack\Sites\SiteRepository;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Support\Facades\View;

class CheckDomain implements Middleware
{

    /**
     * @var SiteRepository
     */
    private $siteRepository;

    function __construct(SiteRepository $siteRepository)
    {
        $this->siteRepository = $siteRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        App::singleton('site', function () use ($request) {
            try {
                return $this->siteRepository->getByDomain($request->getHost());
            } catch (\Exception $e) {
                return false;
            }
        });

        if ( ! app('site')) {
            $default = Site::first();

            return redirect()->to('http://' . $default->primary_domain->domain, 301);
        } elseif (env('ONLY_PRIMARY_DOMAIN', false) && $request->getHost() != app('site')->primary_domain->domain) {
            return redirect()->to('http://' . app('site')->primary_domain->domain . '/' . ltrim($request->path(), '/'),
                301);
        }

        // Add the site theme to the view tree
        if (site('theme') != 'default') {
            View::addLocation(public_path('themes/' . $request->get('theme', site('theme')) . '/views'));
        }

        return $next($request);
    }

}
