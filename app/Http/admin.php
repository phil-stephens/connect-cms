<?php

/**
 * Admin Routes
 */

Route::get('/', function () {
    return redirect()->route('sites_path');
});

Route::get('sites', [
    'as'   => 'sites_path',
    'uses' => 'SitesController@index'
]);

Route::get('/bookingbuttonrefresh', function () {
    $properties = \Fastrack\Properties\Property::get();

    foreach ($properties as $property) {
        \Bus::dispatch(
            new \Fastrack\Commands\CheckBookingButton($property)
        );
    }
});

Route::get('/weather', function () {
    $properties = \Fastrack\Properties\Property::get();

    foreach ($properties as $property) {
        \Bus::dispatch(
            new \Fastrack\Commands\CheckWeather($property)
        );
    }
});

Route::get('download/by/uuid/{uuid}', [
    'as'   => 'uuid_download_path',
    'uses' => 'DownloadController@getUuid'
]);

Route::get('download/{type}/{id}', [
    'as'   => 'admin_download_path',
    'uses' => 'DownloadController@get'
]);

Route::group(['prefix' => 'site'], function() {


    Route::get('{siteId}/dashboard', [
        'as'   => 'site_path',
        'uses' => 'DashboardController@site'
    ]);

    Route::get('create', [
        'as'   => 'create_site_path',
        'uses' => 'SitesController@create'
    ]);

    Route::post('create', [
        'as'   => 'create_site_path',
        'uses' => 'SitesController@store'
    ]);

    Route::get('{siteId}/edit', [
        'as'   => 'edit_site_path',
        'uses' => 'SitesController@edit'
    ]);

    Route::post('{siteId}/edit', [
        'as'   => 'edit_site_path',
        'uses' => 'SitesController@update'
    ]);


    Route::get('{siteId}/settings', [
        'as'   => 'site_custom_settings_path',
        'uses' => 'SitesController@settings'
    ]);

    Route::post('{siteId}/settings', [
        'as'   => 'site_custom_settings_path',
        'uses' => 'SitesController@updateSettings'
    ]);


    Route::get('{siteId}/seo', [
        'as'   => 'seo_site_path',
        'uses' => 'SitesController@seo'
    ]);

    Route::post('{siteId}/seo', [
        'as'   => 'seo_site_path',
        'uses' => 'SitesController@updateSeo'
    ]);

    Route::get('{siteId}/properties', [
        'as'   => 'site_properties_path',
        'uses' => 'SitesController@properties'
    ]);

    Route::post('{siteId}/properties', [
        'as'   => 'site_properties_path',
        'uses' => 'SitesController@updateProperties'
    ]);

    Route::delete('{siteId}', [
        'as'   => 'destroy_site_path',
        'uses' => 'SitesController@destroy'
    ]);

    // Domains
    Route::get('{siteId}/domains', [
        'as'   => 'domains_path',
        'uses' => 'DomainsController@index'
    ]);

    Route::get('{siteId}/domain/create', [
        'as'   => 'create_domain_path',
        'uses' => 'DomainsController@create'
    ]);

    Route::post('{siteId}/domain/create', [
        'as'   => 'create_domain_path',
        'uses' => 'DomainsController@store'
    ]);

    Route::get('{siteId}/domain/{domainId}/edit', [
        'as'   => 'edit_domain_path',
        'uses' => 'DomainsController@edit'
    ]);

    Route::post('{siteId}/domain/{domainId}/edit', [
        'as'   => 'edit_domain_path',
        'uses' => 'DomainsController@update'
    ]);

    Route::delete('{siteId}/domain/{domainId}', [
        'as'   => 'destroy_domain_path',
        'uses' => 'DomainsController@destroy'
    ]);

    Route::post('{siteId}/domain/primary', [
        'as'   => 'set_primary_domain_path',
        'uses' => 'DomainsController@setPrimary'
    ]);

    // Aliases
    Route::get('{siteId}/aliases', [
        'as'   => 'aliases_path',
        'uses' => 'AliasController@index'
    ]);

    Route::get('{siteId}/alias/create', [
        'as'   => 'create_alias_path',
        'uses' => 'AliasController@create'
    ]);

    Route::post('{siteId}/alias/create', [
        'as'   => 'create_alias_path',
        'uses' => 'AliasController@store'
    ]);

    Route::post('{siteId}/alias/upload', [
        'as'   => 'upload_alias_path',
        'uses' => 'AliasController@upload'
    ]);

    Route::post('{siteId}/alias/create/multiple', [
        'as'   => 'create_multiple_alias_path',
        'uses' => 'AliasController@storeMultiple'
    ]);

    Route::get('{siteId}/alias/{aliasId}/edit', [
        'as'   => 'edit_alias_path',
        'uses' => 'AliasController@edit'
    ]);

    Route::post('{siteId}/alias/{aliasId}/edit', [
        'as'   => 'edit_alias_path',
        'uses' => 'AliasController@update'
    ]);

    Route::delete('{siteId}/alias/{aliasId}', [
        'as'   => 'destroy_alias_path',
        'uses' => 'AliasController@destroy'
    ]);


    // Taxonomies
    Route::get('{siteId}/taxonomies', [
        'as'   => 'taxonomies_path',
        'uses' => 'TaxonomyController@edit'
    ]);

    Route::post('{siteId}/taxonomies', [
        'as'   => 'taxonomies_path',
        'uses' => 'TaxonomyController@update'
    ]);

    // Navigation Groups
    Route::get('{siteId}/navigations', [
        'as'   => 'navigations_path',
        'uses' => 'NavigationController@index'
    ]);

    Route::get('{siteId}/navigation/create', [
        'as'   => 'create_navigation_path',
        'uses' => 'NavigationController@create'
    ]);

    Route::post('{siteId}/navigation/create', [
        'as'   => 'create_navigation_path',
        'uses' => 'NavigationController@store'
    ]);

    Route::get('{siteId}/navigation/{navigationId}/edit', [
        'as'   => 'edit_navigation_path',
        'uses' => 'NavigationController@edit'
    ]);

    Route::post('{siteId}/navigation/{navigationId}/edit', [
        'as'   => 'edit_navigation_path',
        'uses' => 'NavigationController@update'
    ]);

    Route::get('{siteId}/navigation/{navigationId}/items', [
        'as'   => 'navigation_items_path',
        'uses' => 'NavigationController@items'
    ]);

    Route::post('{siteId}/navigation/{navigationId}/items', [
        'as'   => 'navigation_items_path',
        'uses' => 'NavigationController@updateItems'
    ]);

    Route::delete('{siteId}/navigation/{navigationId}/item', [
        'as'   => 'destroy_navigation_item_path',
        'uses' => 'NavigationController@destroyItem'
    ]);

    Route::delete('{siteId}/navigation/{navigationId}', [
        'as'   => 'destroy_navigation_path',
        'uses' => 'NavigationController@destroy'
    ]);

    // Businesses/POI
    Route::get('{siteUuid}/businesses', [
        'as'    => 'site_business_path',
        'uses'  => 'VenuesController@site'
    ]);

    Route::post('{siteUuid}/businesses', [
        'as'    => 'site_business_path',
        'uses'  => 'VenuesController@updateSite'
    ]);

    Route::delete('{siteUuid}/businesses', [
        'as'    => 'site_remove_business_path',
        'uses'  => 'VenuesController@removeFromSite'
    ]);


    // Pages
    require_once 'routes/pages.php';


    // Campaigns

    require_once 'routes/campaigns.php';
});



Route::post('slideshow/{slideshowId}/slides', [
    'as'   => 'update_slides_path',
    'uses' => 'SlideshowController@updateSlides'
]);




// Properties
Route::get('properties', [
    'as'   => 'properties_path',
    'uses' => 'PropertiesController@index'
]);

Route::group(['prefix' => 'property'], function () {
    require_once 'routes/properties.php';
    require_once 'routes/offers.php';
});


// Venues
Route::get('venues', [
    'as'   => 'venues_path',
    'uses' => 'VenuesController@index'
]);



Route::get('venue/create', [
    'as'   => 'create_venue_path',
    'uses' => 'VenuesController@create'
]);

Route::post('venue/create', [
    'as'   => 'create_venue_path',
    'uses' => 'VenuesController@store'
]);

Route::get('venue/{venueId}/edit', [
    'as'   => 'edit_venue_path',
    'uses' => 'VenuesController@edit'
]);

Route::post('venue/{venueId}/edit', [
    'as'   => 'edit_venue_path',
    'uses' => 'VenuesController@update'
]);

Route::get('venue/{venueId}/content', [
    'as'   => 'content_venue_path',
    'uses' => 'VenuesController@content'
]);

Route::post('venue/{venueId}/content', [
    'as'   => 'content_venue_path',
    'uses' => 'VenuesController@updateContent'
]);

Route::get('venue/{venueId}/location', [
    'as'   => 'location_venue_path',
    'uses' => 'VenuesController@location'
]);

Route::post('venue/{venueId}/location', [
    'as'   => 'location_venue_path',
    'uses' => 'VenuesController@updateLocation'
]);

Route::get('venue/{venueId}/slideshow', [
    'as'   => 'slideshow_venue_path',
    'uses' => 'VenuesController@slideshow'
]);

//Route::post('venue/{venueId}/slideshow', [
//    'as'    => 'slideshow_venue_path',
//    'uses'  => 'VenuesController@updateSlideshow'
//]);

Route::get('venue/{venueId}/seo', [
    'as'   => 'seo_venue_path',
    'uses' => 'VenuesController@seo'
]);

Route::post('venue/{venueId}/seo', [
    'as'   => 'seo_venue_path',
    'uses' => 'VenuesController@updateSeo'
]);

Route::delete('venue/{pageId}', [
    'as'   => 'destroy_venue_path',
    'uses' => 'VenuesController@destroy'
]);

require_once 'routes/calendar.php';


// News Feeds
Route::get('news/feeds', [
    'as'   => 'feeds_path',
    'uses' => 'FeedsController@index'
]);

Route::get('news/feed/create', [
    'as'   => 'create_feed_path',
    'uses' => 'FeedsController@create'
]);

Route::post('news/feed/create', [
    'as'   => 'create_feed_path',
    'uses' => 'FeedsController@store'
]);

Route::get('news/feed/{feedId}/edit', [
    'as'   => 'edit_feed_path',
    'uses' => 'FeedsController@edit'
]);

Route::post('news/feed/{feedId}/edit', [
    'as'   => 'edit_feed_path',
    'uses' => 'FeedsController@update'
]);

Route::delete('news/feed/{feedId}', [
    'as'   => 'destroy_feed_path',
    'uses' => 'FeedsController@destroy'
]);

Route::get('news/feed/{feedId}', [
    'as'   => 'feed_path',
    'uses' => 'ArticlesController@index'
]);

Route::get('news/feed/{feedId}/article/create', [
    'as'   => 'create_article_path',
    'uses' => 'ArticlesController@create'
]);

Route::post('news/feed/{feedId}/article/create', [
    'as'   => 'create_article_path',
    'uses' => 'ArticlesController@store'
]);

Route::get('news/feed/{feedId}/article/{articleId}/edit', [
    'as'   => 'edit_article_path',
    'uses' => 'ArticlesController@edit'
]);

Route::post('news/feed/{feedId}/article/{articleId}/edit', [
    'as'   => 'edit_article_path',
    'uses' => 'ArticlesController@update'
]);

Route::get('news/feed/{feedId}/article/{articleId}/settings', [
    'as'   => 'article_settings_path',
    'uses' => 'ArticlesController@settings'
]);

Route::post('news/feed/{feedId}/article/{articleId}/settings', [
    'as'   => 'article_settings_path',
    'uses' => 'ArticlesController@updateSettings'
]);

Route::get('news/feed/{feedId}/article/{articleId}/content', [
    'as'   => 'content_article_path',
    'uses' => 'ArticlesController@content'
]);

Route::post('news/feed/{feedId}/article/{articleId}/content', [
    'as'   => 'content_article_path',
    'uses' => 'ArticlesController@updateContent'
]);

Route::get('news/feed/{feedId}/article/{articleId}/slideshow', [
    'as'   => 'slideshow_article_path',
    'uses' => 'ArticlesController@slideshow'
]);
//
//Route::post('news/feed/{feedId}/article/{articleId}/slideshow', [
//    'as'    => 'slideshow_article_path',
//    'uses'  => 'ArticlesController@updateSlideshow'
//]);

Route::get('news/feed/{feedId}/article/{articleId}/seo', [
    'as'   => 'seo_article_path',
    'uses' => 'ArticlesController@seo'
]);

Route::post('news/feed/{feedId}/article/{articleId}/seo', [
    'as'   => 'seo_article_path',
    'uses' => 'ArticlesController@updateSeo'
]);

Route::delete('news/feed/{feedId}/article/{articleId}', [
    'as'   => 'destroy_article_path',
    'uses' => 'ArticlesController@destroy'
]);



Route::group(['prefix' => 'media'], function() {

    Route::get('/', [
        'as'   => 'media_path',
        'uses' => 'MediaController@index'
    ]);
});

// Image uploads via Dropzone

Route::post('image/{uploadHash?}', [
    'as'   => 'content_image_path',
    'uses' => 'UploadController@storeImage'
]);

Route::post('upload/media', [
    'as'   => 'content_media_path',
    'uses' => 'UploadController@storeMedia'
]);

Route::post('media/image/tag', [
   'as' => 'image_tag_path',
    'uses'  => 'UploadController@generateTag'
]);

// File uploads via Dropzone

Route::post('file/{uploadHash?}', [
    'as'   => 'content_file_path',
    'uses' => 'UploadController@storeFile'
]);

Route::delete('upload', [
    'as'   => 'destroy_upload_path',
    'uses' => 'UploadController@destroy'
]);




Route::get('media/grid/{libraryId?}', [
    'as' => 'media_grid_path',
    'uses'  => 'MediaController@grid'
]);

Route::get('media/libraries', [
    'as'     => 'libraries_path',
    'uses'  => 'MediaController@index'
]);

Route::get('media/library/create', [
    'as'     => 'library_create_path',
    'uses'  => 'MediaController@create'
]);

Route::post('media/library/create', [
    'as'     => 'library_create_path',
    'uses'  => 'MediaController@store'
]);

Route::get('media/library/{libraryId}', [
    'as'     => 'library_path',
    'uses'  => 'MediaController@show'
]);

Route::get('media/library/{libraryId}/edit', [
    'as'     => 'library_edit_path',
    'uses'  => 'MediaController@edit'
]);

Route::post('media/library/{libraryId}/edit', [
    'as'     => 'library_edit_path',
    'uses'  => 'MediaController@update'
]);


Route::delete('media/{imageUuid}', [
    'as'    => 'image_destroy_path',
    'uses'  => 'MediaController@destroyImage'
]);

Route::get('image/edit/{imageUuid?}', [
    'as'     => 'image_edit_path__ajax',
    'uses'  => 'ImageController@edit'
]);

Route::post('image/edit/{imageUuid}', [
    'as'     => 'image_edit_path',
    'uses'  => 'ImageController@update'
]);



require_once 'routes/compendium.php';
require_once 'routes/export.php';
require_once 'routes/atoms.php';
require_once 'routes/settings.php';