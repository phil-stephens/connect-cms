<?php

Route::get('sitemap.xml', 'SitemapController@show');
Route::get('robots.txt', 'SitemapController@robots');

Route::get('/', 'PagesController@homepage');

Route::get('.c/.pa/{uuid}', 'CompendiumController@page');
Route::get('.c/.ve/{uuid}', 'CompendiumController@place');
Route::get('.c/.ca/{uuid}', 'CompendiumController@event');
Route::get('.c/.pr/{uuid}', 'CompendiumController@property');
Route::get('.c/.ro/{uuid}', 'CompendiumController@room');
Route::get('.c/.ar/{uuid}', 'CompendiumController@article');


Route::post('.search/process', [
    'as'   => 'search_process_path',
    'uses' => 'SearchController@process'
]);


Route::any('.search/group', [
    'as'   => 'search_group_path',
    'uses' => 'SearchController@group'
]);

Route::any('.search/{uuid?}', [
    'as'   => 'search_path',
    'uses' => 'SearchController@show'
]);

Route::any('.search/property/{uuid}/{roomUuid?}', [
    'as'   => 'search_property_path',
    'uses' => 'SearchController@property'
]);

// Project-specific custom routes - will be refactored one day
require_once 'routes/project.php';




Route::get('.test/bugsnag', function() {
    Bugsnag::notifyError('ErrorType', 'Test Error');
});

Route::get('.test/understand', function() {
    \Log::info('Understand.io test');
});


Route::get('calendar/{calendarSlug?}', 'CalendarController@index');
Route::get('calendar/{calendarSlug}/event/{eventSlug}', 'CalendarController@show');
Route::get('calendar/{calendarSlug}/event/{eventSlug}/tickets/details', 'TicketController@details');
Route::any('.com/eway/request-access-token', [
    'as'   => 'eway_access_token_path',
    'uses' => 'CommerceController@eWayAccessToken'
]);

Route::any('.com/eway/redirect', [
    'as'   => 'eway_redirect_path',
    'uses' => 'CommerceController@eWayRedirect'
]);

Route::any('.com/eway/cancel', [
    'as'   => 'eway_cancel_path',
    'uses' => 'CommerceController@eWayCancel'
]);

Route::any('.com/discount/check', [
    'as'   => 'check_discount_code_path',
    'uses' => 'CommerceController@checkDiscount'
]);


Route::get('calendar/{calendarSlug}/event/{eventSlug}/tickets/process', [
    'as'   => 'ticket_processed_path',
    'uses' => 'TicketController@process'
]);

Route::get('calendar/{calendarSlug}/event/{eventSlug}/tickets/confirmed', 'TicketController@confirmed');


Route::get('location', 'LocationController@index');
Route::get('location/venue/{venueSlug}', 'LocationController@venue');
//Route::get('galleries', 'GalleryController@index');
//Route::get('gallery/{gallerySlug}', 'GalleryController@show');

Route::get('feed/{feedSlug?}', 'FeedController@index');
Route::get('feed/{feedSlug}/{articleSlug}', 'FeedController@article');

Route::get('offers', 'OffersController@index');
Route::get('offer/{propertyId}/{offerSlug}', 'OffersController@show');


Route::post('mailinglist', [
    'as'   => 'mailing_list_path',
    'uses' => 'FormController@handleMailingList'
]);

Route::get('.download/{crypt}', [
    'as'   => 'download_file_path',
    'uses' => 'DownloadController@get'
]);

Route::get('.rates/{uuid}', [
    'as'   => 'rate_json_path',
    'uses' => 'PropertiesController@rateJSON'
]);


Route::get('auth/login', [
    'as'    => 'login_path',
    'uses'  => 'Auth\AuthController@getLogin'
]);

Route::post('auth/login', [
    'as'    => 'login_path',
    'uses'  => 'Auth\AuthController@postLogin'
]);


Route::get('auth/logout', [
    'as'    => 'logout_path',
    'uses'  => 'Auth\AuthController@getLogout'
]);

// Password reset link request routes...
Route::get('auth/password/email', 'Auth\PasswordController@getEmail');
Route::post('auth/password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('auth/password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('auth/password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => 'cors'], function() {
    Route::get('.getrates/{uuid}', 'PropertiesController@getRates');
});

Route::get('.checkrates', function () {

    $integrations = \Fastrack\Integrations\Integration::all(); // this will need to change...

    foreach($integrations as $i)
    {
        \Bus::dispatch(
            new \Fastrack\Jobs\CheckRates($i)
        );
    }
});

Route::get('.cache/availability', function () {

    $integrations = \Fastrack\Integrations\Integration::all(); // this will need to change...

    foreach($integrations as $i)
    {
        \Bus::dispatch(
            new \Fastrack\Jobs\CacheAvailability($i)
        );
    }
});

Route::get('.weather', function () {
    $properties = \Fastrack\Properties\Property::get();

    foreach ($properties as $property) {
        \Bus::dispatch(
            new \Fastrack\Commands\CheckWeather($property)
        );
    }
});