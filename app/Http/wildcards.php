<?php

// On-demand image server
Route::get('img/{path}', [
    'as'   => 'image_path',
    'uses' => 'ImageController@cache'
])->where('path', '.*');

// Wildcard routes at the very end - refers to 'static' pages
Route::get('{path}', [
    'uses' => 'PagesController@wildcard'
])->where('path', '.*');

// Form submissions
Route::post('{path}', [
    'uses' => 'FormController@wildcard'
])->where('path', '.*');