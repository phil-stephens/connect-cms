<?php namespace Fastrack\Images;

use Imgix\UrlBuilder as ImgixUrlBuilder;
use League\Glide\Http\UrlBuilderFactory as GlideUrlBuilder;

/**
 * Class ProcessorTrait
 * @package Fastrack\Images
 */
trait ProcessorTrait
{

    /**
     * @param $file_path
     * @param $parameters
     * @param null $crop_from
     * @param null $face_detection
     * @return string
     */
    private function glideCachePath($file_path, $parameters, $crop_from = null, $face_detection = null)
    {
        $base = url('/img');

        if(site()->secure)
        {
            $base = secure_url('/img');
        }

        $urlBuilder = GlideUrlBuilder::create($base);

        if ( ! empty($crop_from)) {
            $parameters['crop'] = str_replace(',', '-', $crop_from);
        }

        return $urlBuilder->getUrl($file_path, $parameters);

    }

    /**
     * @param $file_path
     * @param $parameters
     * @param null $crop_from
     * @param null $face_detection
     * @return string
     */
    private function imgixCachePath($file_path, $parameters, $crop_from = null, $face_detection = null)
    {
        $urlBuilder = new ImgixUrlBuilder(env('IMGIX_SERVER'));

        if(site()->secure) $urlBuilder->setUseHttps(true);

        $urlBuilder->setSignKey(env('IMGIX_SIGNATURE'));

        $crop = [];

        if ( ! empty($face_detection)) {
            $crop[] = 'face';
        }

        if ( ! empty($crop_from)) {
            $crop[] = $crop_from;
        }

        if ( ! empty($crop)) {
            $parameters['crop'] = implode(',', $crop);
        }

        return $urlBuilder->createURL($file_path, $parameters);
    }
}