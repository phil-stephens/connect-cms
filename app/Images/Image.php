<?php namespace Fastrack\Images;

use Fastrack\Files\DownloadableTrait;
use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package Fastrack\Images
 */
class Image extends Model
{

    use ProcessorTrait, DownloadableTrait, TaggableTrait, HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['library_id', 'original_name', 'file_name', 'path', 'mime_type', 'size', 'width', 'height', 'title', 'alt', 'crop_from', 'face_detection'];


    public function library()
    {
        return $this->belongsTo('Fastrack\Media\Library');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contents()
    {
        return $this->hasMany('Fastrack\Content\Content');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ogImages()
    {
        return $this->hasMany('Fastrack\Content\Seo', 'og_image_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function slides()
    {
        return $this->hasMany('Fastrack\Slideshows\Slide');
    }

    /**
     * @return $this
     */
    public function galleries()
    {
        return $this->belongsToMany('Fastrack\Images\Image')->withPivot('order');
    }

    /**
     * @param array $parameters
     * @param null $crop_from
     * @param null $face_detection
     * @return mixed
     */
    public function getPath($parameters = [], $crop_from = null, $face_detection = null)
    {
        // $crop_from fallbacks to native crop from
        if(empty($crop_from)) $crop_from = $this->crop_from;
        if(is_null($face_detection)) $face_detection = $this->face_detection;

        $method = env('IMAGE_PROCESSOR', 'glide') . 'CachePath';

        return $this->$method($this->path . '/' . $this->file_name, $parameters, $crop_from, $face_detection);
    }

    // Legacy method call
    public function getImagePath($parameters = [], $crop_from = null, $face_detection = null)
    {
        return $this->getPath($parameters, $crop_from, $face_detection);
    }

    /**
     * @param array $parameters
     * @param array $tagParameters
     * @param null $crop_from
     * @param null $face_detection
     * @return string
     */
    public function getTag($parameters = [], $tagParameters = [], $crop_from = null, $face_detection = null)
    {
        // decode the $parameters to figure out if we need to transform the image dimensions

        $tag = '<img src="' . $this->getPath($parameters, $crop_from, $face_detection) . '" ';

        $titleParameters = [
            'title' => $this->title,
            'alt'   => $this->alt
        ];

        $attributes = array_merge($titleParameters, $tagParameters);

        foreach ($attributes as $key => $value) {
            $tag .= "{$key}=\"{$value}\" ";
        }

        return $tag . '/>';
    }

    public function getImageTag($parameters = [], $tagParameters = [], $crop_from = null, $face_detection = null)
    {
        //// $crop_from fallbacks to native crop from
        //if(empty($crop_from)) $crop_from = $this->crop_from;
        //if(is_null($face_detection)) $face_detection = $this->face_detection;

        return $this->getTag($parameters, $tagParameters, $crop_from, $face_detection);
    }

    public function getImageUuid()
    {
        return $this->uuid;
    }

    public function getBackgroundAttribute($parameters = [], $otherStyles = null, $crop_from = null, $face_detection = null)
    {
        //// $crop_from fallbacks to native crop from
        if(empty($crop_from)) $crop_from = $this->crop_from;
        //if(is_null($face_detection)) $face_detection = $this->face_detection;


        return 'style="background-image: url(' . $this->getPath($parameters, $crop_from, $face_detection) . '); background-position: ' . cropToCss($crop_from) . '; ' . $otherStyles . '"';
    }

}
