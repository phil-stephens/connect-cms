<?php namespace Fastrack\Images;

use Fastrack\Meta\UuidRepositoryTrait;

class ImageRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Image::class;

    public function getOrFalse($uuid)
    {
        $image = Image::whereUuid($uuid)->first();

        if( empty($image)) return false;

        return $image;
    }

    public function getById($imageId)
    {
        return Image::findOrFail($imageId);
    }

    public function update($uuid, $formData)
    {
        $image = $this->get($uuid);

        $image->fill($formData);

        $image->save();

        return;
    }

    public function destroy($uuid)
    {
        $image = $this->get($uuid);

        // Remove from storage here

        return $image->delete();
    }

}