<?php namespace Fastrack\Content;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Seo
 * @package Fastrack\Content
 */
class Seo extends Model
{

    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'og_title',
        'og_description',
        'twitter_site',
        'twitter_title',
        'twitter_creator',
        'twitter_description',
        'google_plus_publisher',
        'google_plus_author',
        'javascript_head',
        'javascript_body_top',
        'javascript_body_bottom',
        'hide_on_sitemap'
    ];

    /**
     * @var string
     */
    protected $table = 'seo';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function parent()
    {
        return $this->morphTo();
    }

}
