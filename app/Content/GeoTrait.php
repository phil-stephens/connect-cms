<?php

namespace Fastrack\Content;


/**
 * Class GeoTrait
 * @package Fastrack\Content
 */
trait GeoTrait
{
    public function location()
    {
        return $this->belongsTo('Fastrack\Locations\Location');
    }


    public function addressString($includeCountry = true, $delimiter = ', ')
    {
        $address = [];

        foreach (['address1', 'address2', 'city', 'state', 'postcode'] as $column) {
            if($this->location && ! empty($this->location->$column))
            {
                $address[] = $this->location->$column;
            } elseif ( ! empty($this->attributes[$column])) {
                $address[] = $this->attributes[$column];
            }

        }

        // country
        if($includeCountry)
        {
            if($this->location && ! empty($this->location->country))
            {
                $address[] = config('countries.iso_countries.' . $this->location->country);
            } elseif ( ! empty($this->attributes['country'])) {
                $address[] = config('countries.iso_countries.' . $this->attributes['country']);
            }
        }

        return implode($delimiter, $address);
    }

    // set attributes here
    // this is to bridge the gap for legacy sites
    // location check is temporary to cover the migration
    public function getAddress1Attribute($value = null)
    {
        if($this->location)
        {
            return $this->location->address1;
        }

        return $value;
    }

    public function getAddress2Attribute($value = null)
    {
        if($this->location)
        {
            return $this->location->address2;
        }

        return $value;
    }

    public function getCityAttribute($value = null)
    {
        if($this->location)
        {
            return $this->location->city;
        }

        return $value;
    }

    public function getStateAttribute($value = null)
    {
        if($this->location)
        {
            return $this->location->state;
        }

        return $value;
    }

    public function getPostcodeAttribute($value = null)
    {
        if($this->location)
        {
            return $this->location->postcode;
        }

        return $value;
    }

    public function getLongitudeAttribute($value = null)
    {
        if($this->location)
        {
            return $this->location->longitude;
        }

        return $value;
    }

    public function getLatitudeAttribute($value = null)
    {
        if($this->location)
        {
            return $this->location->latitude;
        }

        return $value;
    }
}