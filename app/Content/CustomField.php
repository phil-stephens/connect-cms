<?php namespace Fastrack\Content;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomField
 * @package Fastrack\Content
 */
class CustomField extends Model
{

    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['type', 'key', 'value', 'image_id', 'file_id', 'order'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function content()
    {
        return $this->belongsTo('Fastrack\Content\Content');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo('Fastrack\Images\Image');
    }

    public function file()
    {
        return $this->belongsTo('Fastrack\Files\File');
    }

    // Move these onto an image presenter - these functions will be shorthand for those functions
    /**
     * @param array $parameters
     * @return bool
     */
    public function getImagePath($parameters = [])
    {
        if ($this->image) {
            return $this->image->getPath($parameters);
        }

        return false;
    }

    /**
     * @param array $urlParams
     * @param array $tagParams
     * @return bool
     */
    public function getImageTag($urlParams = [], $tagParams = [])
    {
        if ($this->image) {
            return $this->image->getTag($urlParams, $tagParams);
        }

        return false;
    }

    public function getImageUuid()
    {
        if ($this->image) {
            return $this->image->uuid;
        }

        return false;
    }
}
