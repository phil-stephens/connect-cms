<?php namespace Fastrack\Content;

use Fastrack\Sites\Site;


/**
 * Class URLTrait
 * @package Fastrack\Content
 */
trait URLTrait
{

    public function getCanonical()
    {
        // If it's a page
        if(class_basename(get_class($this)) == 'Page')
        {
            $site = $this->site;
        } elseif( ! $site = $this->canonical_site)
        {
            $site = Site::first();
        }

        return $this->getUrl($site);
    }

    public function getUrl($site = null)
    {
        if( ! empty($site) && ! is_object($site))
        {
            $site = Site::find($site);
        } elseif( is_null($site))
        {
            // Hopefully this won't break anything :-P
            $site = site();
        }

        $prefix = ($site->secure) ? 'https://' : 'http://';

        if( ! empty($site->compendium_property_id))
        {
            $class = class_basename(get_class($this));
            return $prefix . $site->primary_domain->domain . '/.c/.' . strtolower( substr($class, 0, 2) ) . '/' . $this->uuid;
        }

        $method = 'get' . class_basename(get_class($this)) . 'Url';

        if (method_exists($this, $method)) {
            $path = $this->$method($site);
        } else {
            $path = '/' . $this->slug;
        }

        if( empty($site)) return url($path);

        return $prefix . $site->primary_domain->domain . $path;
    }

    private function getPageUrl($site = null)
    {
        if( ! empty($site) && isSame($site->homepage, $this) ) return '/';

        return '/' . $this->slug;
    }

    private function getArticleUrl($site = null)
    {
        return '/feed/' . $this->feed->slug . '/' . $this->slug;
    }

    private function getOfferUrl($site = null)
    {
        return '/offer/' . $this->property->id . '/' . $this->slug;
    }

    private function getPropertyUrl($site = null)
    {
        if (empty($site)) {
            $property_noun = setting('property_noun');
        } else
        {
            $property_noun = $site->setting('property_noun');
        }

        return '/' . $property_noun . '/' . $this->slug;
    }


    private function getRoomUrl($site = null)
    {
        if (empty($site)) {
            $site = site();
        }

        if ($site->properties()->count() > 1) {

            $property_noun = $site->setting('property_noun');

            return '/' . $property_noun . '/' . $this->property->slug . '/' . $this->property->room_noun . '/' . $this->slug;
        }

        return '/' . $this->property->room_noun . '/' . $this->slug;
    }

    private function getRoomSubTypeUrl($site = null)
    {
        if (empty($site)) {
            $site = site();
        }

        if ($site->properties()->count() > 1) {
            $property_noun = $site->setting('property_noun');

            return '/' . $property_noun . '/' . $this->room->property->slug . '/' . $this->room->property->room_noun . '/' . $this->room->slug . '/type/' . $this->slug;
        }

        return '/' . $this->room->property->room_noun . '/' . $this->room->slug . '/type/' . $this->slug;
    }

    private function getVenueUrl($site = null)
    {
        return '/location/venue/' . $this->slug;
    }


    private function getCalendarEventUrl($site = null)
    {
        return '/calendar/' . $this->calendar->slug . '/event/' . $this->slug;
    }


    public function getAdminUrl()
    {
        switch(class_basename($this))
        {
            case 'Page':
                return route('edit_page_path', [$this->site_id, $this->id]);
                break;

            case 'Property':
                return route('edit_property_path', $this->id);
                break;

            case 'Room':
                return route('edit_room_path', [$this->property_id, $this->id]);
                break;
        }
    }

    public function getAdminContentUrl()
    {
        switch(class_basename($this))
        {
            case 'Page':
                return route('content_page_path', [$this->site_id, $this->id]);
                break;

            case 'Property':
                return route('edit_property_content_path', $this->id);
                break;

            case 'Room':
                return route('content_room_path', [$this->property_id, $this->id]);
                break;
        }
    }
}