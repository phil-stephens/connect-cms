<?php namespace Fastrack\Content;

use Fastrack\Sites\Taxonomy;

/**
 * Class SeoTrait
 * @package Fastrack\Content
 */
trait SeoTrait
{

    /**
     * @return mixed
     */
    public function seo()
    {
        return $this->morphOne('Fastrack\Content\Seo', 'parent');
    }

    // SEO-based variables - this will take advantage of information hierarchy

    /**
     * @param $tag
     * @param $model
     * @return bool|mixed
     */
    public function getSeoTagVenue($tag, $model)
    {
        if ( ! empty($model->seo->$tag)) {
            return $model->seo->$tag;
        }

        if ($taxonomy = $this->getTaxonomySettings($tag, $model)) {
            return $taxonomy;
        }

        return $this->getSeoTagSite($tag, site());

        //return;

        // Maybe default to page with 'location' slug
        //$parentPage = getPage('location');

        //return $this->getSeoTagPage($tag, $parentPage);
    }

    /**
     * @param $tag
     * @param $model
     * @return bool
     */
    public function getTaxonomySettings($tag, $model)
    {
        if ($taxonomy = Taxonomy::whereSiteId(site('id'))->whereModel(class_basename($model))->first()) {
            if ( ! empty($taxonomy->$tag)) {
                return $taxonomy->$tag;
            }
        }

        return false;
    }

    // Handles the cascade of values

    /**
     * @param $tag
     * @param $model
     * @return bool|mixed
     */
    public function getSeoTagPage($tag, $model)
    {
        if ( ! empty($model->seo->$tag)) {
            return $model->seo->$tag;
        }

        if ($taxonomy = $this->getTaxonomySettings($tag, $model)) {
            return $taxonomy;
        }

        return $this->getSeoTagSite($tag, site());
    }

    /**
     * @param $tag
     * @param $model
     * @return mixed
     */
    public function getSeoTagSite($tag, $model)
    {
        return $model->seo->$tag;
    }

    /**
     * @param $tag
     * @param $model
     * @return bool|mixed
     */
    public function getSeoTagCalendarEvent($tag, $model)
    {
        if ( ! empty($model->seo->$tag)) {
            return $model->seo->$tag;
        }

        if ($taxonomy = $this->getTaxonomySettings($tag, $model)) {
            return $taxonomy;
        }

        // Maybe default to page with 'calendar' slug
        $parentPage = getPage('calendar');

        return $this->getSeoTagPage($tag, $parentPage);
    }

    /**
     * @param $tag
     * @param $model
     * @return bool|mixed
     */
    public function getSeoTagRoom($tag, $model)
    {
        if ( ! empty($model->seo->$tag)) {
            return $model->seo->$tag;
        }

        if ($taxonomy = $this->getTaxonomySettings($tag, $model)) {
            return $taxonomy;
        }

        return $this->getSeoTagProperty($tag, $model->property);
    }

    public function getSeoTagRoomSubType($tag, $model)
    {
        if ( ! empty($model->seo->$tag)) {
            return $model->seo->$tag;
        }

        if ($taxonomy = $this->getTaxonomySettings($tag, $model)) {
            return $taxonomy;
        }

        return $this->getSeoTagRoom($tag, $model->room);
    }

    /**
     * @param $tag
     * @param $model
     * @return bool|mixed
     */
    public function getSeoTagProperty($tag, $model)
    {
        if ( ! empty($model->seo->$tag)) {
            return $model->seo->$tag;
        }

        if ($taxonomy = $this->getTaxonomySettings($tag, $model)) {
            return $taxonomy;
        }

        return $this->getSeoTagSite($tag, site());
    }

    /**
     * @param $tag
     * @param $model
     * @return bool|mixed
     */
    public function getSeoTagOffer($tag, $model)
    {
        if ( ! empty($model->seo->$tag)) {
            return $model->seo->$tag;
        }

        if ($taxonomy = $this->getTaxonomySettings($tag, $model)) {
            return $taxonomy;
        }

        return $this->getSeoTagProperty($tag, $model->property);
    }

    /**
     * @param $tag
     * @param $model
     * @return bool|mixed
     */
    public function getSeoTagArticle($tag, $model)
    {
        if ( ! empty($model->seo->$tag)) {
            return $model->seo->$tag;
        }

        if ($taxonomy = $this->getTaxonomySettings($tag, $model)) {
            return $taxonomy;
        }

        return $this->getSeoTagSite($tag, site());
    }

    /**
     * @param $tag
     */
    public function getSeoImagePath($tag)
    {
        if ($id = $this->getSeo($tag)) {
            return Image::findOrFail($id)->getPath(['w' => 500]);
        }

        return;
    }

    /**
     * @param $tag
     * @return mixed
     */
    public function getSeo($tag)
    {
        if( ! $this->seo) return;

        $method = 'getSeoTag' . class_basename(get_class($this));

        if (method_exists($this, $method)) {
            return $this->$method($tag, $this);
        }

        return $this->getSeoTagSite($tag, site());
    }
}