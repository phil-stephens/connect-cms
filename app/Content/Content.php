<?php namespace Fastrack\Content;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Content
 * @package Fastrack\Content
 */
class Content extends Model
{

    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['title', 'excerpt', 'body'];

    /**
     * @var string
     */
    protected $table = 'content';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function parent()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo('Fastrack\Images\Image');
    }

    /**
     * @return mixed
     */
    public function customFields()
    {
        return $this->hasMany('Fastrack\Content\CustomField')->orderBy('order');
    }

    public function atoms()
    {
        return $this->hasMany('Fastrack\Atoms\Atom')->orderBy('order');
    }
}
