<?php

namespace Fastrack\Content;


trait CanonicalTrait
{
    public function canonical_site()
    {
        return $this->belongsTo('Fastrack\Sites\Site');
    }
}