<?php
namespace Fastrack\Content;

use Fastrack\Locations\Location;
use Geocoder\Geocoder;
use Geocoder\HttpAdapter\CurlHttpAdapter;
use Geocoder\Provider\ChainProvider;
use Geocoder\Provider\GoogleMapsProvider;

/**
 * Class GeoRepositoryTrait
 * @package Fastrack\Content
 */
trait GeoRepositoryTrait
{

    /**
     * @param $model
     * @param $formData
     */
    public function updateLocation($model, $formData)
    {
        $updated = false;

        if($model->location)
        {
            foreach (['address1', 'address2', 'city', 'state', 'postcode', 'country'] as $column) {
                if (isset($formData[$column]) && $model->location->$column != $formData[$column]) {
                    $model->location->$column = $formData[$column];
                    $updated = true;
                }
            }

            // do lat/lng separately
            foreach (['latitude', 'longitude'] as $column) {
                if ( ! empty($formData[$column]) && $model->location->$column != $formData[$column]) {
                    $model->location->$column = $formData[$column];
                    $updated = true;
                }
            }
        } else
        {
            $location = Location::create($formData);

            $model->location_id = $location->id;

            $model->save();

            $model->load('location');

            $updated = true;
        }

        if ($updated) {
            $model->location->save();

            if (empty($formData['latitude']))
            {
                $this->updateGeoCode($model);
            }
        }

        //if (empty($formData['latitude']) && $updated) {
        //    $this->updateGeoCode($model);
        //}


        //foreach (['address1', 'address2', 'city', 'state', 'postcode', 'country', 'latitude', 'longitude'] as $column) {
        //    if (isset($formData[$column]) && $model->$column != $formData[$column]) {
        //        $model->$column = $formData[$column];
        //        $updated = true;
        //    }
        //}
        //
        //// if the address has been updated and there is no long/lat set then geocode
        //if (empty($formData['latitude']) && $updated) {
        //    $this->updateGeoCode($model);
        //}
        //
        //if ($updated) {
        //    $model->save();
        //}

        return;
    }

    /**
     * @param $model
     * @return mixed
     */
    public function updateGeoCode($model)
    {
        try {
            $geocoded = $this->geocode($model->present()->addressString());
            $model->location->latitude = $geocoded->getLatitude();
            $model->location->longitude = $geocoded->getLongitude();
        } catch (\Exception $exception) {

        }

        return $model->location->save();
    }

    /**
     * @param $addressString
     * @return \Geocoder\Result\ResultInterface|\Geocoder\ResultInterface
     */
    public function geocode($addressString)
    {
        $adapter = new CurlHttpAdapter();
        $chain = new ChainProvider(
            array(
                new GoogleMapsProvider($adapter),
            )
        );

        $geocoder = new Geocoder();

        $geocoder->registerProvider($chain);

        return $geocoder->geocode($addressString);
    }
}