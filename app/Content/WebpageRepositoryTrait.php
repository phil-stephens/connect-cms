<?php namespace Fastrack\Content;

use Fastrack\Files\File;
use Fastrack\Images\Image;

/**
 * Class WebpageRepositoryTrait
 * @package Fastrack\Content
 */
trait WebpageRepositoryTrait
{

    /**
     * @param $formData
     * @return mixed
     */
    public function createStubs($formData)
    {
        $formData['content'] = ( ! empty($formData['content'])) ? $formData['content'] : [];
        $formData['seo'] = ( ! empty($formData['seo'])) ? $formData['seo'] : [];

        return $formData;
    }

    /**
     * @param $model
     * @param $formData
     */
    public function createContent($model, $formData)
    {
        if ( ! isset($formData['content'])) {
            $formData['content'] = [];
        }

        $content = new Content($formData['content']);


        //if ( ! empty($formData['content']['upload_hash'])) {
        //    $image = Image::whereUploadHash($formData['content']['upload_hash'])->latest()->first();
        //
        //    if ( ! empty($image)) {
        //        $content->image_id = $image->id;
        //    }
        //}

        $model->content()->save($content);

        $this->assignFeaturedImage($content, $formData['content']);

        $this->updateCustomFields($model, $formData['content']);
    }

    private function assignFeaturedImage(Content $content, $formData)
    {
        if(isset($formData['featured_image']))
        {
            if(empty($formData['featured_image']))
            {
                $content->image_id = 0;
                $content->save();
            } else
            {
                try
                {
                    $image = Image::whereUuid($formData['featured_image'])->firstOrFail();

                    $content->image_id = $image->id;
                    $content->save();
                } catch( \Exception $e)
                {
                    $content->image_id = 0;
                    $content->save();
                }
            }
        }

        return;

        //if( ! empty($formData['featured_image']))
        //{
        //    // Find the image
        //    try
        //    {
        //        $image = Image::whereUuid($formData['featured_image'])->firstOrFail();
        //
        //        $content->image_id = $image->id;
        //        $content->save();
        //    } catch( \Exception $e)
        //    {
        //
        //    }
        //}

        //return;
    }

    /**
     * @param $model
     * @param $formData
     */
    public function createSeo($model, $formData)
    {
        $seo = new Seo($formData['seo']);

        $model->seo()->save($seo);
    }

    /**
     * @param $model
     * @param $formData
     */
    public function updateCustomFields($model, $formData)
    {
        if ( ! empty($formData['custom'])) {
            foreach ($formData['custom'] as $hash => $data) {
                switch ($data['type']) {
                    case 'html':
                        $data['value'] = $data['value_html'];
                        $data['image_id'] = 0;
                        $data['file_id'] = 0;
                        break;

                    case 'plain':
                        $data['value'] = $data['value_plain'];
                        $data['image_id'] = 0;
                        $data['file_id'] = 0;
                        break;

                    case 'image':
                        $data['value'] = null;
                        $data['file_id'] = 0;
                        break;

                    case 'file':
                        $data['value'] = null;
                        $data['image_id'] = 0;
                        break;

                    default:
                        $data['value'] = null;
                }


                if (isset($data['id'])) {
                    // update
                    $field = CustomField::findOrFail($data['id']);

                    if (isset($data['remove'])) {
                        $field->delete();
                    } else {
                        $field->fill($data)->save();
                    }
                } else {
                    if ($data['type'] == 'file') {
                        try {
                            $file = File::whereUploadHash('cf-' . $hash)->firstOrFail();

                            $data['file_id'] = $file->id;
                        } catch (\Exception $e) {
                            continue;
                        }
                    }

                    $field = new CustomField($data);

                    $model->content->customFields()->save($field);
                }

                if($data['type'] == 'image')
                {
                    $this->assignImage($field, $data);
                }
            }
        }
    }

    private function assignImage(CustomField $field, $formData)
    {
        if(isset($formData['value_image'])) {
            if (empty($formData['value_image'])) {
                $field->image_id = 0;
                $field->save();
            } else
            {
                // Find the image
                try {
                    $image = Image::whereUuid($formData['value_image'])->firstOrFail();

                    $field->image_id = $image->id;
                    $field->save();
                } catch (\Exception $e) {
                    $field->image_id = 0;
                    $field->save();
                }
            }
        }

        return;
    }

    /**
     * @param $model
     * @param $formData
     */
    public function updateContent($model, $formData)
    {
        if (isset($formData['content'])) {
            $formData = $formData['content'];
        }

        if( ! $model->content)
        {
            $this->createContent($model, []);

            $model->load('content');
        }

        $model->content->fill($formData);

        $model->content->save();

        $this->assignFeaturedImage($model->content, $formData);

        $this->updateCustomFields($model, $formData);

        return;
    }

    /**
     * @param $model
     * @param $formData
     */
    public function updateSeo($model, $formData)
    {
        if(isset($formData['canonical_site_id']))
        {
            $model->canonical_site_id = $formData['canonical_site_id'];

            $model->save();
        }

        if (isset($formData['seo'])) {
            $formData = $formData['seo'];
        }

        if (empty($formData['hide_on_sitemap'])) {
            $formData['hide_on_sitemap'] = 0;
        }

        $model->seo->fill($formData);

        $model->seo->save();

        return;
    }

    /**
     * @param $model
     */
    public function destroyIt($model)
    {
        if ($model->content) {
            $model->content->delete();
        }
        if ($model->seo) {
            $model->seo->delete();
        }

        if ($model->navigationItems) {
            foreach ($model->navigationItems as $item) {
                $item->delete();
            }
        }

        $model->delete();

        return;
    }
}