<?php namespace Fastrack\Content;

use League\CommonMark\DocParser;
use League\CommonMark\Environment;
use League\CommonMark\HtmlRenderer;

/**
 * Class WebpageTrait
 * @package Fastrack\Content
 */
trait WebpageTrait
{

    /**
     * @return mixed
     */
    public function asHomepage()
    {
        return $this->morphOne('Fastrack\Sites\Site', 'homepage');
    }

    /**
     * @return mixed
     */
    public function links()
    {
        return $this->morphMany('Fastrack\Sites\Navigation_item', 'link');
    }

    /**
     * @return mixed
     */
    public function content()
    {
        return $this->morphOne('Fastrack\Content\Content', 'parent');
    }

    public function slideshow()
    {
        return $this->belongsTo('Fastrack\Slideshows\Slideshow');
    }

    // Legacy alias
    public function gallery()
    {
        return $this->belongsTo('Fastrack\Slideshows\Slideshow');
    }

    /**
     * @return mixed
     */
    public function navigationItems()
    {
        return $this->morphMany('Fastrack\Navigations\NavigationItem', 'link');
    }


    // Presentation functions - should *really* be in a presenter but attempting to keep function calls short
    /**
     * @param null $before
     * @param null $after
     * @return string
     */
    public function getTitle($before = null, $after = null)
    {
        if( ! $this->content) return;

        return $before . $this->content->title . $after;
    }

    /**
     * @param bool|true $parseMarkdown
     * @param null $before
     * @param null $after
     * @return string|void
     */
    public function getExcerpt($parseMarkdown = true, $before = null, $after = null)
    {
        if( ! $this->content) return;

        $content = trim($this->content->excerpt);

        if (empty($content)) {
            return;
        }

        if ($parseMarkdown) {
            $environment = Environment::createCommonMarkEnvironment();
            $parser = new DocParser($environment);
            $htmlRenderer = new HtmlRenderer($environment);

            $text = $parser->parse($content);
            $content = $htmlRenderer->renderBlock($text);
        }

        return $before . $content . $after;
    }

    /**
     * @param bool|true $parseMarkdown
     * @param null $before
     * @param null $after
     * @return string|void
     */
    public function getBody($parseMarkdown = true, $before = null, $after = null)
    {
        if( ! $this->content) return;

        if($this->content->atoms()->count()) return $this->renderAtoms();

        $content = trim($this->content->body);

        if (empty($content)) {
            return;
        }

        if ($parseMarkdown) {
            $environment = Environment::createCommonMarkEnvironment();
            $parser = new DocParser($environment);
            $htmlRenderer = new HtmlRenderer($environment);

            $text = $parser->parse($content);
            $content = $htmlRenderer->renderBlock($text);
        }

        return $before . $content . $after;
    }

    private function renderAtoms()
    {
        $rendered = '';

        foreach($this->content->atoms as $atom)
        {
            $rendered .= $atom->render();
        }

        return $rendered;
    }

    // Move these onto an image presenter - these functions will be shorthand for those functions
    /**
     * @param array $parameters
     * @return bool
     */
    public function getImagePath($parameters = [])
    {
        if( ! $this->content) return;

        if ($this->content->image) {
            return $this->content->image->getPath($parameters);
        }

        return false;
    }

    public function getImageUuid()
    {
        if( ! $this->content) return;

        if ($this->content->image) {
            return $this->content->image->uuid;
        }

        return false;
    }

    public function getBackgroundAttribute($parameters = [], $otherStyles = null, $crop_from = null, $face_detection = null)
    {
        if( ! $this->content) return;

        if ($this->content->image) {
            return $this->content->image->getBackgroundAttribute($parameters, $otherStyles, $crop_from, $face_detection);
        }

        return null;
    }

    // Legacy alias
    public function getBackgroundImage($parameters = [], $otherStyles = null, $crop_from = null, $face_detection = null)
    {
        return $this->getBackgroundAttribute($parameters, $otherStyles, $crop_from, $face_detection);
    }

    /**
     * @param array $urlParams
     * @param array $tagParams
     * @return bool
     */
    public function getImageTag($urlParams = [], $tagParams = [])
    {
        if( ! $this->content) return;

        if ($this->content->image) {
            return $this->content->image->getTag($urlParams, $tagParams);
        }

        return false;
    }

    /**
     * @param $key
     * @param bool|false $default
     * @return bool
     */
    public function getCustom($key, $default = false)
    {
        if( ! $this->content) return;

        try {
            $customField = CustomField::whereContentId($this->content->id)->whereKey($key)->firstOrFail();

            switch ($customField->type) {
                case 'image':
                    return $customField->image;
                    break;

                case 'file':
                    return $customField->file;
                    break;

                default:
                    return $customField->value;
                    break;
            }
            //return ( $customField->type == 'image') ? $customField->image : $customField->value;

        } catch (\Exception $e) {
            return $default;
        }
    }

    /**
     * @return mixed
     */
    public function getAllCustom()
    {
        if( ! $this->content) return;

        return $this->content->customFields;
    }
}