<?php

namespace Fastrack\Uuid;

use Webpatser\Uuid\Uuid;


trait HasUuid
{
    protected static function bootHasUuid()
    {
        /**
         * Attach to the 'creating' Model Event to provide a UUID
         * for the `uuid` field
         */
        static::creating(function ($model) {
            $model->uuid = Uuid::generate(4);
        });
    }
}