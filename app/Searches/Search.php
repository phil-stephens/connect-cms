<?php

namespace Fastrack\Searches;

//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    use HasUuid;

    protected $fillable = ['payload'];
}
