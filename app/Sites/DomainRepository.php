<?php namespace Fastrack\Sites;

use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class DomainRepository
 * @package Fastrack\Sites
 */
class DomainRepository
{

    use UuidRepositoryTrait;

    protected $repoClass = Domain::class;

    /**
     * @param $formData
     * @param $siteId
     * @return Domain
     */
    public function create($formData, $siteId)
    {
        $domain = new Domain(['domain' => trim($formData['domain'])]);

        $this->save($domain, $siteId);

        return $domain;
    }

    /**
     * @param Domain $domain
     * @param $siteId
     * @return mixed
     */
    public function save(Domain $domain, $siteId)
    {
        return Site::findOrFail($siteId)
            ->domains()
            ->save($domain);
    }

    /**
     * @param $domainId
     * @param $formData
     * @param $siteId
     */
    public function update($domainId, $formData, $siteId)
    {
        $domain = $this->getById($domainId, $siteId);

        $domain->fill($formData);

        $domain->save();
    }

    /**
     * @param $domainId
     * @param $siteId
     * @return mixed
     */
    public function getById($domainId, $siteId)
    {
        return Domain::whereSiteId($siteId)
            ->findOrFail($domainId);
    }

    /**
     * @param $formData
     * @param $siteId
     */
    public function setPrimary($formData, $siteId)
    {
        $domain = $this->getById($formData['id'], $siteId);

        $domain->site->primary_domain_id = $domain->id;

        $domain->site->save();
    }

    /**
     * @param $domainId
     * @param $siteId
     * @return mixed
     */
    public function destroy($domainId, $siteId)
    {
        $domain = $this->getById($domainId, $siteId);

        if ($domain->site->primary_domain_id == $domain->id) {
            $newDomain = Domain::whereSiteId($siteId)->where('id', '!=', $domainId)->firstOrFail();

            $domain->site->primary_domain_id = $newDomain->id;

            $domain->site->save();
        }

        return $domain->delete();
    }
}