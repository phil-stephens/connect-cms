<?php namespace Fastrack\Sites;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Domain
 * @package Fastrack\Sites
 */
class Domain extends Model
{

    use HasUuid;

    /**
     * @var array
     */
    protected $fillable = ['domain'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo('Fastrack\Sites\Site');
    }
}
