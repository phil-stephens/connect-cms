<?php namespace Fastrack\Sites;


use Cache;
use Fastrack\Content\Seo;
use Fastrack\Pages\Chapter;
use Fastrack\Pages\PageRepository;
use Fastrack\Properties\Property;
use Fastrack\Sites\Domain;
use Fastrack\Sites\Site;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class SiteRepository
 * @package Fastrack\Sites
 */
class SiteRepository extends RouteRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Site::class;

    //public function get($uuid)
    //{
    //    return Site::whereUuid($uuid)->firstOrFail();
    //}

    /**
     * @return mixed
     */
    public function getAll() // eventually pass in user_id for filtering
    {
        return Site::with('domains')->whereCompendiumPropertyId(null)->get();
    }

    /**
     * @param $domain
     * @return mixed
     */
    public function getByDomain($domain)
    {
        if(app()->environment() != 'local')
        {
            return Cache::remember('site_' . $domain, 5, function () use ($domain) {
                return $this->matchDomain($domain);
            });
        }

        return $this->matchDomain($domain);

        //if($match = Domain::with('site')->whereDomain($domain)->first())
        //{
        //    //die('matched');
        //    return $match->site;
        //}
        //
        //// strip the www. of the beginning
        //// allows for automatically checking for www. variants
        //$domain = preg_replace('#^www\.(.+\.)#i', '$1', $domain);
        //
        //$match = Domain::with('site')->whereDomain($domain)->firstOrFail();
        //
        ////dd($match);
        //
        //return $match->site;
    }

    private function matchDomain($domain)
    {
        if ($match = Domain::with('site')->whereDomain($domain)->first()) {
            // if it's found, check whether it is a
            return $match->site;
        }

        //check for www variant
        $domain = preg_replace('#^www\.(.+\.)#i', '$1', $domain);

        $match = Domain::with('site')->whereDomain($domain)->firstOrFail();

        // if it's found, redirect to the non-www version
        return $match->site;
    }

    /**
     * @param $siteId
     * @return mixed
     */
    public function getById($siteId)
    {
        return Site::with('seo')->findOrFail($siteId);
    }

    /**
     * @param $formData
     * @return static
     */
    public function create($formData)
    {
        //$formData['settings'] = ( ! empty($formData['settings'])) ? $formData['settings'] : ['property_noun' => 'hotel'];

        $site = Site::create(['name' => $formData['name']]);

        $domains = explode(',', $formData['domains']);

        $i = 0;
        foreach ($domains as $d) {
            $domain = new Domain(['domain' => trim($d)]);

            $site->domains()->save($domain);

            if ($i == 0) {
                $site->primary_domain_id = $domain->id;
                $site->save();
            }

            $i ++;
        }

        $seo = new Seo(['title' => $formData['name']]);

        $site->seo()->save($seo);

        $repo = new PageRepository();

        // Create a default home page
        $page = $repo->create([
            'name'    => 'Homepage',
            'slug'    => 'home',
            //'content' => [
            //    'title' => $site->name
            //],
            //'seo'     => []
        ], $site->id);

        // And set it as the homepage
        $page->asHomepage()->save($site);

        return $site;
    }

    /**
     * @param $site
     * @param $formData
     * @return mixed
     */
    public function update($site, $formData)
    {
        if( ! is_object($site)) $site = $this->getById($site);

        $site->fill($formData);

        if (isset($formData['homepage_type'])) {

            // Move to 'set Homepage' method
            if ($formData['homepage_type'] == 'Fastrack\Pages\Page') {
                $home = $site->pages->find($formData['homepage_page_id']);
            } else {
                $home = $site->properties->find($formData['homepage_property_id']);
            }

            $home->asHomepage()->save($site);
        }

        // SEO
        if (isset($formData['seo'])) {
            $this->updateSeo(null, $formData['seo'], $site);
        }

        return $site->save();
    }

    public function updateByUuid($uuid, $formData)
    {
        $site = $this->get($uuid);

        return $this->update($site, $formData);
    }

    /**
     * @param $siteId
     * @param $formData
     * @param null $site
     */
    public function updateSeo($siteId, $formData, $site = null)
    {
        if (empty($site)) {
            $site = $this->getById($siteId);
        }

        if (isset($formData['seo'])) {
            $formData = $formData['seo'];
        }

        $site->seo->fill($formData);

        $site->seo->save();

        return;
    }

    /**
     * @param $siteId
     * @param $formData
     */
    public function updateProperties($siteId, $formData)
    {
        $site = $this->getById($siteId);

        foreach ($formData['properties'] as $key => $value) {
            if ($value && ! $site->properties->contains($key)) {
                $site->properties()->attach($key);
            } elseif ( ! $value) {
                $site->properties()->detach($key);
            }
        }

        // Refresh custom routes
        $this->buildSiteRoutes();

        return;
    }

    /**
     * @param $homepageId
     * @param $homepageType
     */
    public function setHomepage($homepageId, $homepageType)
    {
        // needs some work

        // reason for
    }

    /**
     * @param $siteId
     */
    public function destroy($siteId)
    {
        $site = $this->getById($siteId);

        $site->seo->delete();

        $site->delete();

        return;
    }




    public function bootstrapCompendium(Property $property)
    {
        $compendium = new Site(
            [
                'name' => $property->name,
                'theme' => 'guest-portal'
            ]
        );

        $compendium->compendium_property_id = $property->id;
        $compendium->account_id = 1;

        $compendium->save();

        // This is a little bit hacky - in order to make it work properly
        $this->updateProperties($compendium->id, ['properties' => [$property->id => $property->id]]);

        $domain = new Domain([
            'domain'    => str_slug($property->name) . '.fastrackguests.com'
        ]);

        $domain->site_id = $compendium->id;

        $domain->save();

        $compendium->primary_domain_id = $domain->id;

        $compendium->save();

        // create a general chapter
        $chapter = new Chapter([
            'name' => 'General'
        ]);

        $chapter->site_id = $compendium->id;

        $chapter->save();

        $seo = new Seo(['title' => $property->name]);

        $compendium->seo()->save($seo);

        $repo = new PageRepository();

        // Create a default home page
        $page = $repo->create([
            'name'    => 'Homepage',
            'slug'    => 'home',
            'chapter_id'    => $chapter->id
        ], $compendium->id);

        // And set it as the homepage
        $page->asHomepage()->save($compendium);

        return $compendium;
    }
}