<?php namespace Fastrack\Sites;


use Storage;

/**
 * Class RouteRepository
 * @package Fastrack\Sites
 */
class RouteRepository
{

    /**
     *
     */
    public function buildSiteRoutes()
    {
        // Assume cache is duuuurty!
        Storage::disk('app')->deleteDirectory('.cache');

        $prefix = ltrim(env('FILESYSTEM_PREFIX', '') . '/', '/');

        $routeFile = $prefix . 'routes.php';
        // Initialise
        if (Storage::disk('app')->exists($routeFile)) {
            Storage::disk('app')->delete($routeFile);
        }

        // Only continue if the installation contains any properties

        if(\DB::table('properties')->count('name') > 0)
        {
            Storage::disk('app')->put($routeFile, '<?php');

            // First get all the sites
            $sites = Site::all();

            foreach ($sites as $site) {
                $properties = $site->properties;

                //$settings = $site->settings;

                $property_noun = $site->setting('property_noun') ?: 'property';
                $property_plural = str_plural($property_noun, 2);

                // Just need to define specific terminology
                foreach ($site->domains as $domain) {
                    foreach (['', 'www.'] as $prefix) {
                        Storage::disk('app')->append($routeFile,
                            '$router->group([\'domain\' => \'' . $prefix . $domain->domain . '\'], function($router) {');


                        if (count($properties) == 1) {
                            $property = $properties[0];

                            $room_noun = $property->room_noun;
                            $room_plural = str_plural($room_noun);

                            Storage::disk('app')->append($routeFile,
                                '$router->get(\'' . $room_plural . '\', \'PropertiesController@rooms\');');
                            Storage::disk('app')->append($routeFile,
                                '$router->get(\'' . $room_noun . '/{roomSlug}\', \'PropertiesController@room\');');

                            Storage::disk('app')->append($routeFile,
                                '$router->get(\'' . $room_noun . '/{roomSlug}/types\', \'PropertiesController@subTypes\');');

                            Storage::disk('app')->append($routeFile,
                                '$router->get(\'' . $room_noun . '/{roomSlug}/type/{subTypeSlug}\', \'PropertiesController@subType\');');

                        } else {
                            Storage::disk('app')->append($routeFile,
                                '$router->get(\'' . $property_plural . '\', \'PropertiesController@index\');');

                            Storage::disk('app')->append($routeFile,
                                '$router->get(\'' . $property_noun . '/{propertySlug}\', \'PropertiesController@property\');');

                            foreach ($properties as $property) {
                                $room_noun = $property->room_noun;
                                $room_plural = str_plural($room_noun);

                                Storage::disk('app')->append($routeFile,
                                    '$router->get(\'' . $property_noun . '/{propertySlug}/' . $room_plural . '\', \'PropertiesController@rooms\');');

                                Storage::disk('app')->append($routeFile,
                                    '$router->get(\'' . $property_noun . '/{propertySlug}/' . $room_noun . '/{roomSlug}\', \'PropertiesController@room\');');

                                Storage::disk('app')->append($routeFile,
                                    '$router->get(\'' . $property_noun . '/{propertySlug}/' . $room_noun . '/{roomSlug}/types\', \'PropertiesController@subTypes\');');

                                Storage::disk('app')->append($routeFile,
                                    '$router->get(\'' . $property_noun . '/{propertySlug}/' . $room_noun . '/{roomSlug}/type/{subTypeSlug}\', \'PropertiesController@subType\');');
                            }
                        }


                        Storage::disk('app')->append($routeFile, '});');
                    }
                }
            }

            return true;
        }

        return false;

        //\Artisan::call('route:cache');
    }
}