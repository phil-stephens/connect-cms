<?php namespace Fastrack\Sites;

use Fastrack\Content\SeoTrait;
use Fastrack\Properties\Room;
use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Site
 * @package Fastrack\Sites
 */
class Site extends Model
{

    use HasUuid, Versionable, SeoTrait, SettingTrait, TaggableTrait;

    /**
     * @var array
     */
    protected $fillable = ['name', 'settings', 'theme', 'business_feed', 'in_development', 'secure'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function domains()
    {
        return $this->hasMany('Fastrack\Sites\Domain');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function primary_domain()
    {
        return $this->belongsTo('Fastrack\Sites\Domain', 'primary_domain_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function aliases()
    {
        return $this->hasMany('Fastrack\Sites\Alias');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function taxonomies()
    {
        return $this->hasMany('Fastrack\Sites\Taxonomy');
    }

    /**
     * @return mixed
     */
    public function pages()
    {
        return $this->hasMany('Fastrack\Pages\Page')->orderBy('order');
    }

    public function chapters()
    {
        return $this->hasMany('Fastrack\Pages\Chapter');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function properties()
    {
        return $this->belongsToMany('Fastrack\Properties\Property');
    }

    public function property()
    {
        return $this->belongsTo('Fastrack\Properties\Property', 'compendium_property_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function homepage()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function navigations()
    {
        return $this->hasMany('Fastrack\Navigations\Navigation');
    }

    public function subscribers()
    {
        return $this->morphMany('Fastrack\Subscribers\Subscriber', 'parent');
        //return $this->hasMany('Fastrack\Subscribers\Subscriber');
    }

    public function calendars()
    {
        return $this->morphToMany('Fastrack\Calendars\Calendar', 'calendarable');
    }

    public function businesses()
    {
        return $this->belongsToMany('Fastrack\Locations\Venue')->withPivot('order')->orderBy('order');
    }

    public function messages()
    {
        return $this->hasMany('Fastrack\Messages\Message');
    }

    public function campaigns()
    {
        return $this->hasMany('Fastrack\Campaigns\Campaign');
    }

    ///**
    // * @param $settings
    // */
    //public function setSettingsAttribute($settings)
    //{
    //    $this->attributes['settings'] = serialize($settings);
    //}
    //
    ///**
    // * @param $settings
    // * @return mixed
    // */
    //public function getSettingsAttribute($settings)
    //{
    //    return unserialize($settings);
    //}

    /**
     * @return null
     */
    public function getHomepagePageIdAttribute()
    {
        if ($this->attributes['homepage_type'] == 'Fastrack\Pages\Page') {
            return $this->attributes['homepage_id'];
        }

        return null;
    }

    /**
     * @return null
     */
    public function getHomepagePropertyIdAttribute()
    {
        if ($this->attributes['homepage_type'] == 'Fastrack\Properties\Property') {
            return $this->attributes['homepage_id'];
        }

        return null;
    }

    public function rooms()
    {
        return Room::whereIn('property_id', $this->properties->modelKeys())->get();
    }
}
