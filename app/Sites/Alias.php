<?php namespace Fastrack\Sites;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Alias
 * @package Fastrack\Sites
 */
class Alias extends Model
{

    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['from', 'to', 'type'];

    /**
     * @param $from
     * @param $to
     * @param string $type
     * @return static
     */
    public static function generate($from, $to, $type = '301')
    {
        return new static(compact('from', 'to', 'type'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo('Fastrack\Sites\Site');
    }

    /**
     * @param $from
     */
    public function setFromAttribute($from)
    {
        $this->attributes['from'] = ltrim($from, '/');
    }
}
