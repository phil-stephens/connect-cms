<?php namespace Fastrack\Sites;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Taxonomy
 * @package Fastrack\Sites
 */
class Taxonomy extends Model
{

    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['model', 'title', 'description'];

    /**
     * @param $data
     * @return static
     */
    public static function generate($data)
    {
        return new static($data);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo('Fastrack\Sites\Site');
    }

}
