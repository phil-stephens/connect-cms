<?php namespace Fastrack\Sites;


use Fastrack\Files\File;
use Fastrack\Images\Image;
use Fastrack\Pages\Page;
use Fastrack\Sites\Site;
use Fastrack\Meta\UuidRepositoryTrait;

class SettingRepository
{

    use UuidRepositoryTrait;

    protected $repoClass = Setting::class;


    public function updateAll($model, $formData)
    {
        if( ! $formData) return;

        foreach ($formData as $key => $data) {
            if ( ! empty($data['id'])) {

                $setting = Setting::findOrFail($data['id']);


                switch ($data['type']) {
                    case 'plain':
                    case 'html':
                    case 'colour':
                        $setting->value = $data['value'];
                        break;
                }

                $setting->save();
            } else {
                // Create a setting
                $setting = new Setting([
                    'key'  => $key,
                    'type' => $data['type']
                ]);

                switch ($data['type']) {
                    case 'plain':
                    case 'html':
                    case 'colour':
                        $setting->value = $data['value'];
                        break;
                }

                $model->settings()->save($setting);

            }

            if($data['type'] == 'image') $this->assignImage($setting, $data);
            if($data['type'] == 'file') $this->assignFile($setting, $data);
        }
    }

    private function assignImage(Setting $setting, $formData)
    {
        if(isset($formData['uuid']))
        {
            if(empty($formData['uuid'])) {
                $setting->image_id = 0;
                $setting->save();
            } else
            {
                // Find the image
                try
                {
                    $image = Image::whereUuid($formData['uuid'])->firstOrFail();

                    $setting->image_id = $image->id;
                    $setting->save();
                } catch( \Exception $e)
                {
                    $setting->image_id = 0;
                    $setting->save();
                }
            }
        }

        return;
    }

    private function assignFile(Setting $setting, $formData)
    {
        if( ! empty($formData['uuid']))
        {
            // Find the image
            try
            {
                $file = File::whereUuid($formData['uuid'])->firstOrFail();

                $setting->file_id = $file->id;
                $setting->save();
            } catch( \Exception $e)
            {
                $setting->file_id = 0;
                $setting->save();
            }
        }

        return;
    }

    public function gatherSettings($attribute)
    {
        // Gather manifests from templates being used...
        $themes  = Site::lists('theme')->all();

        $settings = [];

        foreach($themes as $theme)
        {
            try
            {
                $manifest = file_get_contents(public_path('themes/' . $theme . '/manifest.json'));

                $manifest = json_decode($manifest)->$attribute;

                foreach($manifest as $setting)
                {
                    $settings[$setting->key] = $setting;
                }
            } catch(\Exception $e)
            {

            }
        }

        return $settings;
    }

    public function getPageSettings($site, $page)
    {
        if( ! is_object($site)) $site = Site::findOrFail($site);

        if( ! is_object($page)) $page = Page::whereSiteId($site->id)->findOrFail($page);

        $settings = [];

        try {
            // Get the settings skeleton from the manifest file

            $manifest = file_get_contents(public_path('themes/' . $site->theme . '/manifest.json'));

            $manifest = json_decode($manifest)->templates;


            if (isset($manifest->settings)) {
                $settings = array_merge($settings, $manifest->settings);
            }

            $template = $page->template;

            if (isset($manifest->files->$template)) {

                $settings = array_merge($settings, $manifest->files->$template);
            }

        } catch (\Exception $e) {

        }

        return $settings;
    }

}