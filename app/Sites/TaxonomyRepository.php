<?php namespace Fastrack\Sites;

use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class TaxonomyRepository
 * @package Fastrack\Sites
 */
class TaxonomyRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Taxonomy::class;

    /**
     * @param $siteId
     * @return array
     */
    public function getAllForSite($siteId)
    {
        $taxonomies = Taxonomy::whereSiteId($siteId)->get();

        $array = [];

        foreach ($taxonomies as $taxonomy) {
            $array[$taxonomy->model] = $taxonomy;
        }

        return $array;
    }

    /**
     * @param $formData
     * @param $siteId
     */
    public function create($formData, $siteId)
    {
        foreach ($formData['taxonomy'] as $data) {
            $this->createIndividual($data, $siteId);
        }

        return;
    }

    /**
     * @param $data
     * @param $siteId
     */
    public function createIndividual($data, $siteId)
    {
        $taxonomy = Taxonomy::generate($data);

        $this->save($taxonomy, $siteId);
    }

    /**
     * @param Taxonomy $taxonomy
     * @param $siteId
     * @return mixed
     */
    public function save(Taxonomy $taxonomy, $siteId)
    {
        return Site::findOrFail($siteId)
            ->taxonomies()
            ->save($taxonomy);
    }

    /**
     * @param $formData
     * @param $siteId
     */
    public function update($formData, $siteId)
    {
        foreach ($formData['taxonomy'] as $data) {
            if (empty($data['id'])) {
                $this->createIndividual($data, $siteId);
            } else {
                $taxonomy = $this->getById($data['id']);

                $taxonomy->fill($data);

                //dd($taxonomy);
                $taxonomy->save();
            }

        }

        return;
    }

    /**
     * @param $taxonomyId
     * @return mixed
     */
    public function getByid($taxonomyId)
    {
        return Taxonomy::findOrFail($taxonomyId);
    }
}