<?php

namespace Fastrack\Sites;

use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class AliasRepository
 * @package Fastrack\Sites
 */
class AliasRepository
{

    use UuidRepositoryTrait;

    protected $repoClass = Alias::class;

    /**
     * @param $path
     * @param $siteId
     * @return bool
     */
    public function getByFrom($path, $siteId)
    {
        // Need to be able to do wildcard/regex lookups here...

        // Create an array of 'froms'

        $aliases = $this->getAll($siteId);

        foreach ($aliases as $alias) {
            //echo '/' . str_replace('/', '\/', '/' . $alias->from) . '/ig' . '<br />';

            try {

                //echo $path;
                $match = preg_match_all('/' . str_replace('/', '\/', strtolower($alias->from)) . '\/?/',
                    strtolower($path), $out);


            } catch (\Exception $e) {
                dd($e);
            }

            if ($match) {
                array_shift($out);

                if (count($out)) {
                    foreach ($out as $key => $replace) {
                        $alias->to = str_replace('$' . ($key + 1), $replace[0], $alias->to);
                    }
                }

                return $alias;

            }

            //die($foo);

            //if (preg_match('/' . $alias->from . '/', $path)) return $alias;

            //if(strtolower($alias->from) == strtolower($path)) return $alias;

//            if(strtolower($alias->from) == strtolower($path) . '/') return $alias;
        }

        return false;
    }

    public function getByHashbang($siteId, $str)
    {
        return Alias::whereSiteId($siteId)->whereFrom('#!' . $str)->first();
    }

    /**
     * @param $siteId
     * @return mixed
     */
    public function getAll($siteId)
    {
        return Alias::whereSiteId($siteId)->get();
    }

    public function createMultiple($formData, $siteId)
    {
        $count = 0;

        foreach ($formData['alias'] as $alias) {
            if ( ! empty($alias['insert'])) {
                if ($this->create($alias, $siteId)) {
                    $count ++;
                }
            }
        }

        return $count;
    }

    /**
     * @param $formData
     * @param $siteId
     * @return static
     */
    public function create($formData, $siteId)
    {
        $formData['type'] = ( ! empty($formData['type'])) ? $formData['type'] : 301;

        if (Alias::whereFrom($formData['from'])->first()) {
            return false;
        }

        $alias = Alias::generate($formData['from'], $formData['to'], $formData['type']);

        $this->save($alias, $siteId);

        return $alias;

    }

    /**
     * @param Alias $alias
     * @param $siteId
     * @return mixed
     */
    public function save(Alias $alias, $siteId)
    {
        return Site::findOrFail($siteId)
            ->aliases()
            ->save($alias);
    }

    /**
     * @param $aliasId
     * @param $formData
     * @param $siteId
     */
    public function update($aliasId, $formData, $siteId)
    {
        $alias = $this->getById($aliasId, $siteId);

        $formData['type'] = ( ! empty($formData['type'])) ? $formData['type'] : 301;

        $alias->fill($formData);

        $alias->save();

        return;
    }

    /**
     * @param $aliasId
     * @param $siteId
     * @return mixed
     */
    public function getById($aliasId, $siteId)
    {
        return Alias::whereSiteId($siteId)->findOrFail($aliasId);
    }

    /**
     * @param $aliasId
     * @param $siteId
     */
    public function destroy($aliasId, $siteId)
    {
        $alias = $this->getById($aliasId, $siteId);

        $alias->delete();

        return;
    }
}