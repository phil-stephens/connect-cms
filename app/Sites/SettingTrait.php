<?php namespace Fastrack\Sites;

trait SettingTrait
{

    public function settings()
    {
        return $this->morphMany('Fastrack\Sites\Setting', 'parent');
    }

    public function setting($key, $default = false)
    {
        try {
            $setting = Setting::whereParentId($this->id)->whereParentType(get_class($this))->whereKey($key)->firstOrFail();

            switch ($setting->type) {
                case 'image':
                    return $setting->image;
                    break;

                case 'file':
                    return $setting->file;
                    break;

                default:
                    return $setting->value;
                    break;
            }

        } catch (\Exception $e) {
            return $default;
        }
    }

    public function settingRow($key)
    {
        try {
            return Setting::whereParentId($this->id)->whereParentType(get_class($this))->whereKey($key)->firstOrFail();
        } catch (\Exception $e) {
            return false;
        }
    }
}