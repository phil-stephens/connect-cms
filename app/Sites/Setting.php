<?php namespace Fastrack\Sites;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasUuid;

    protected $fillable = ['type', 'key', 'value', 'image_id', 'file_id', 'label', 'description', 'order'];

    public function parent()
    {
        return $this->morphTo();
    }

    public function image()
    {
        return $this->belongsTo('Fastrack\Images\Image');
    }

    public function file()
    {
        return $this->belongsTo('Fastrack\Files\File');
    }
}
