<?php namespace Fastrack\News;

use Fastrack\Content\CanonicalTrait;
//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Feed
 * @package Fastrack\News
 */
class Feed extends Model
{

    use HasUuid, Versionable, CanonicalTrait;
    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'canonical_site_id'];

    /**
     * @return mixed
     */
    public function articles()
    {
        return $this->hasMany('Fastrack\News\Article')->orderBy('published_at', 'desc');
    }

}
