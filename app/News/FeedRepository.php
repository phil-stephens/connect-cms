<?php
/**
 * Created by PhpStorm.
 * User: fastrack
 * Date: 9/07/15
 * Time: 4:00 PM
 */

namespace Fastrack\News;

use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class FeedRepository
 * @package Fastrack\News
 */
class FeedRepository
{

    use UuidRepositoryTrait;

    protected $repoClass = Feed::class;

    /**
     * @return mixed
     */
    public function getAll()
    {
        return Feed::get();
    }

    /**
     * @param null $feedSlug
     * @param $siteId
     * @param null $limit
     * @return mixed
     */
    public function getFeedArticles($feedSlug = null, $siteId, $limit = null)
    {
        if ( ! empty($feedSlug)) {

            $feed = $this->getBySlug($feedSlug);

            $feedIds = [$feed->id];
        } else {
            $feedIds = Feed::get()->lists('id')->all();
        }

        $query = Article::whereIn('feed_id', $feedIds)->where('published_at', '<=',
            \DB::raw("NOW()"))->whereDraft(false)->orderBy('published_at', 'desc');

        if ( ! empty($limit)) {
            $query->take($limit);
        }

        return $query->get();
    }

    /**
     * @param $feedSlug
     * @return mixed
     */
    public function getBySlug($feedSlug)
    {

        return Feed::whereSlug($feedSlug)->firstOrFail();
    }

    /**
     * @param $feedSlug
     * @param $siteId
     * @param null $paginationType
     * @return mixed
     */
    public function getFeedArticlesPaginated($feedSlug, $siteId, $paginationType = null)
    {
        if ( ! empty($feedSlug)) {

            $feed = $this->getBySlug($feedSlug);

            $feedIds = [$feed->id];
        } else {
            $feedIds = Feed::get()->lists('id')->all();
        }

        $query = Article::whereIn('feed_id', $feedIds)->where('published_at', '<=',
            \DB::raw("NOW()"))->whereDraft(false)->orderBy('published_at', 'desc');

        switch ($paginationType) {
            case 'simple':
                return $query->simplePaginate(env('ARTICLES_PER_PAGE', 10));
                break;

            default:
                return $query->paginate(env('ARTICLES_PER_PAGE', 10));
                break;
        }


    }

    /**
     * @param $formData
     * @return static
     */
    public function create($formData)
    {
        $feed = Feed::create($formData);

        return $feed;
    }

    /**
     * @param $feedId
     * @param $formData
     */
    public function update($feedId, $formData)
    {
        $feed = $this->getById($feedId);

        $feed->fill($formData);

        $feed->save();

        return;
    }

    /**
     * @param $feedId
     * @return mixed
     */
    public function getById($feedId)
    {
        return Feed::findOrFail($feedId);
    }

    /**
     * @param $feedId
     */
    public function destroy($feedId)
    {
        $feed = $this->getById($feedId);

        $feed->delete();

        return;
    }
}