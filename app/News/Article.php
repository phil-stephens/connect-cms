<?php namespace Fastrack\News;

use Fastrack\Content\SeoTrait;
use Fastrack\Content\URLTrait;
use Fastrack\Content\WebpageTrait;
use Fastrack\Sites\SettingTrait;
use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Laracasts\Presenter\PresentableTrait;
//use Fastrack\Versions\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 * @package Fastrack\News
 */
class Article extends Model
{

    use HasUuid, Versionable, WebpageTrait, PresentableTrait, SeoTrait, URLTrait, SettingTrait, TaggableTrait;

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'published_at', 'user_id', 'draft'];

    /**
     * @var string
     */
    protected $presenter = 'Fastrack\Presenters\ArticlePresenter';


    /**
     * @return array
     */
    public function getDates()
    {
        return ['created_at', 'updated_at', 'published_at'];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feed()
    {
        return $this->belongsTo('Fastrack\News\Feed');
    }

    public function canonical_site()
    {
        return $this->feed->canonical_site;
    }



    public function getCanonicalSiteAttribute()
    {
        if ( ! array_key_exists('canonical_site', $this->relations))
        {
            $site = $this->feed->canonical_site;

            $this->setRelation('canonical_site', $site);
        }

        return $this->getRelation('canonical_site');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Fastrack\Users\User');
    }

}
