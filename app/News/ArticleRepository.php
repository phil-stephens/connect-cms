<?php namespace Fastrack\News;


use Fastrack\Content\Seo;
use Fastrack\Content\WebpageRepositoryTrait;
use Fastrack\Slideshows\SlideshowRepositoryTrait;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class ArticleRepository
 * @package Fastrack\News
 */
class ArticleRepository
{
    use UuidRepositoryTrait, WebpageRepositoryTrait, SlideshowRepositoryTrait;

    protected $repoClass = Article::class;

    /**
     * @param $articleId
     * @param $feedId
     * @return mixed
     */
    public function getById($articleId, $feedId)
    {
        return Article::with('content', 'seo')
            ->whereFeedId($feedId)
            ->findOrFail($articleId);
    }

    /**
     * @param $articleSlug
     * @param $feedId
     * @return mixed
     */
    public function getBySlug($articleSlug, $feedId)
    {
        return Article::with('content', 'seo')
            ->whereFeedId($feedId)
            ->whereSlug($articleSlug)
            ->firstOrFail();
    }

    /**
     * @param Article $article
     * @param $feedId
     * @return mixed
     */
    public function save(Article $article, $feedId)
    {
        return Feed::findOrFail($feedId)
            ->articles()
            ->save($article);
    }

    /**
     * @param $formData
     * @param $feedId
     * @return Article
     */
    public function create($formData, $feedId)
    {
        $formData = $this->createStubs($formData);

        $article = new Article($formData);

        $this->save($article, $feedId);

        $this->createContent($article, $formData);

        // SEO bootstrapping
        $seo = new Seo($formData['seo']);

        $article->seo()->save($seo);

        // Refresh custom routes
        //$this->buildSiteRoutes();
        $this->bootstrapSlideshow($article);

        return $article;
    }

    /**
     * @param $articleId
     * @param $formData
     * @param $feedId
     * @return bool
     */
    public function update($articleId, $formData, $feedId)
    {
        $article = $this->getById($articleId, $feedId);

        $article->fill($formData);

        $article->save();

        // Now do the content...
        if (isset($formData['content'])) {
            $this->updateContent($article, $formData['content']);
        }

        // Now do the SEO..
        if (isset($formData['seo'])) {
            $this->updateSeo($article, $formData['seo']);
        }

        // Refresh custom routes
        //$this->buildSiteRoutes();

        return true;
    }


    /**
     * @param $articleId
     * @param $feedId
     */
    public function destroy($articleId, $feedId)
    {
        return $this->destroyIt(
            $this->getById($articleId, $feedId)
        );
    }


}