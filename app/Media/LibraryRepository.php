<?php

namespace Fastrack\Media;


use Fastrack\Images\Image;
use Fastrack\Meta\UuidRepositoryTrait;

class LibraryRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Library::class;

    //public function get($uuid)
    //{
    //    return Library::whereUuid($uuid)->firstOrFail();
    //}

    public function getAll()
    {
        return Library::all();
    }

    public function getImages($libraryId, $count = 18)
    {
        $library = $this->get($libraryId);

        return Image::whereLibraryId($library->id)->latest()->paginate($count);
    }

    public function create($formData)
    {
        return Library::create($formData);
    }

    public function update($libraryId, $formData)
    {
        $library = $this->get($libraryId);

        $library->fill($formData);

        $library->save();

        return;
    }
}