<?php

namespace Fastrack\Media;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    use HasUuid, Versionable;

    protected $fillable = ['name', 'default'];

    public function images()
    {
        return $this->hasMany('Fastrack\Images\Image');
    }
}
