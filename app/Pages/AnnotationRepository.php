<?php

namespace Fastrack\Pages;

use Fastrack\Meta\UuidRepositoryTrait;

class AnnotationRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Annotation::class;

    public function getForPage($pageId)
    {
        try
        {
            return Annotation::wherePageId($pageId)->firstOrFail();
        } catch( \Exception $e) { }

        return $this->create($pageId);
    }

    public function create($pageId)
    {
        $annotation = new Annotation([
            'title' => true,
            'excerpt'   => true,
            'body'      => true,
            'image'     => true
        ]);

        $annotation->page_id = $pageId;

        $annotation->save();

        return $annotation;
    }

    public function update($formData, $pageId)
    {
        $annotation = $this->getForPage($pageId);

        foreach(['title', 'excerpt', 'body', 'image'] as $key)
        {
            if(empty($formData[$key])) $formData[$key] = false;
        }

        $annotation->fill($formData);

        $annotation->save();

        return;
    }
}