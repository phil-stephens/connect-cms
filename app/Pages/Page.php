<?php namespace Fastrack\Pages;

use Fastrack\Content\SeoTrait;
use Fastrack\Content\URLTrait;
use Fastrack\Content\WebpageTrait;
use Fastrack\Sites\SettingTrait;
use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Page
 * @package Fastrack\Pages
 */
class Page extends Model
{

    use HasUuid, Versionable, WebpageTrait, SeoTrait, URLTrait, SettingTrait, TaggableTrait;

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'template', 'order', 'chapter_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo('Fastrack\Sites\Site');
    }


    public function relatedTo()
    {
        return $this->belongsToMany('Fastrack\Pages\Page', 'related_pages', 'page_id', 'related_id')->withPivot('key');
    }

    public function relatedBy()
    {
        return $this->belongsToMany('Fastrack\Pages\Page', 'related_pages', 'related_id', 'page_id')->withPivot('key');
    }

    public function getRelatedAttribute()
    {
        if ( ! array_key_exists('related', $this->relations)) $this->loadRelated();

        return $this->getRelation('related');
    }

    protected function loadRelated()
    {
        if ( ! array_key_exists('related', $this->relations))
        {
            $related = $this->mergeRelated();

            $this->setRelation('related', $related);
        }
    }

    protected function mergeRelated()
    {
        return $this->relatedTo->merge($this->relatedBy);
    }


    /**
     * @param $name
     * @param $slug
     * @param string $template
     * @return static
     */
    public static function generate($name, $slug, $template = 'page')
    {
        return new static(compact('name', 'slug', 'template'));
    }

}
