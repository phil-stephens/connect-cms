<?php namespace Fastrack\Pages;


use Fastrack\Content\Seo;
use Fastrack\Content\WebpageRepositoryTrait;
use Fastrack\Sites\RouteRepository;
use Fastrack\Sites\Site;
use Fastrack\Slideshows\SlideshowRepositoryTrait;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class PageRepository
 * @package Fastrack\Pages
 */
class PageRepository extends RouteRepository
{

    use WebpageRepositoryTrait, SlideshowRepositoryTrait, UuidRepositoryTrait;

    protected $repoClass = Page::class;

    //public function get($uuid)
    //{
    //    return Page::whereUuid($uuid)->firstOrFail();
    //}

    /**
     * @param $siteId
     * @return mixed
     */
    public function getAllDropdown($siteId)
    {
        return Page::whereSiteId($siteId)->lists('name', 'id')->all();
    }

    /**
     * @param $slug
     * @param null $siteId
     * @return mixed
     */
    public function getBySlug($slug, $siteId = null)
    {
        if (is_null($siteId)) {
            $siteId = site('id');
        }

        return Page::with('content', 'seo')->whereSlug($slug)->whereSiteId($siteId)->firstOrFail();
    }

    /**
     * @param null $siteId
     * @return mixed
     */
    public function getBySite($siteId = null)
    {
        if (is_null($siteId)) {
            $siteId = site('id');
        }

        return Page::whereSiteId($siteId)->orderBy('order')->get();
    }

    public function getAllPaginated($siteId = null, $chapterId = null)
    {
        if (is_null($siteId)) {
            $siteId = site('id');
        }

        $query = Page::whereSiteId($siteId);

        if ( ! empty($chapterId)) {
            $query->whereChapterId($chapterId);
        }

        return $query->orderBy('order')->paginate();
    }

    /**
     * @param $pageId
     * @param null $siteId
     * @return mixed
     */
    public function getById($pageId, $siteId = null)
    {
        $query = Page::with('content', 'seo');

        if ( ! empty($siteId)) {
            $query->whereSiteId($siteId);
        }

        return $query->findOrFail($pageId);
    }

    /**
     * @param $formData
     * @param $siteId
     */
    public function sortPages($formData, $siteId)
    {
        foreach ($formData['data'] as $pageId => $order) {
            if ($order == '') {
                continue;
            }

            $page = $this->getById($pageId, $siteId);

            $page->order = $order;

            $page->save();
        }
    }

    /**
     * @param $formData
     * @param $siteId
     * @param bool|false $internal
     * @return static
     */
    public function create($formData, $siteId, $internal = false)
    {
        $formData = $this->createStubs($formData);

        //$page = new Page($formData);

        $formData['template'] = (isset($formData['template'])) ? $formData['template'] : 'page';

        $page = new Page($formData);

        //$page = Page::generate($formData['name'], $formData['slug'], $template);
        $this->save($page, $siteId);

        if ($internal) {
            $page->internal = true;
            $page->save();
        }

        $this->createContent($page, $formData);

        // SEO bootstrapping
        $seo = new Seo($formData['seo']);

        $page->seo()->save($seo);

        // Refresh custom routes
        //$this->buildSiteRoutes();

        $this->bootstrapSlideshow($page);

        return $page;
    }

    /**
     * @param $pageId
     * @param $formData
     * @param null $siteId
     * @return bool
     */
    public function update($pageId, $formData, $siteId = null)
    {
        $page = $this->getById($pageId, $siteId);

        $page->fill($formData);



        $page->save();

        // Now do the content...
        if (isset($formData['content'])) {
            $this->updateContent($page, $formData['content']);
        }

        // Now do the SEO..
        if (isset($formData['seo'])) {
            $this->updateSeo($page, $formData['seo']);
        }

        // Refresh custom routes
        //$this->buildSiteRoutes();

        return true;
    }

    public function updateByUuid($uuid, $formData)
    {
        $page = $this->get($uuid);

        $page->fill($formData);



        $page->save();

        // Now do the content...
        if (isset($formData['content'])) {
            $this->updateContent($page, $formData['content']);
        }

        // Now do the SEO..
        if (isset($formData['seo'])) {
            $this->updateSeo($page, $formData['seo']);
        }

        // Refresh custom routes
        //$this->buildSiteRoutes();

        return true;
    }


    /**
     * @param Page $page
     * @param $siteId
     */
    public function save(Page $page, $siteId)
    {
        $site = Site::findOrFail($siteId);


        // Automatically fill in the sort order
        //$order = \DB::table('pages')->whereSiteId($siteId)->max('order');
        //
        //$page->order = $order + 1;

        $page->site_id = $site->id;

        $page->save();

        return;
        $site->pages()->save($page);


    }

    /**
     * @param $pageId
     * @param $siteId
     */
    public function destroy($pageId, $siteId)
    {
        return $this->destroyIt(
            $this->getById($pageId, $siteId)
        );
    }

    public function destroyByUuid($uuid)
    {
        $page = $this->get($uuid);

        $siteUuid = $page->site->uuid;

        $this->destroyIt($page);

        return $siteUuid;
    }

    public function getUnrelatedPages($page)
    {
        // Need to organise this by chapter

        if(is_string($page))
        {
            $page = $this->get($page);
        }

        $ignore = array_merge([$page->id], $page->related->lists('id')->all());

        $chapters = [];

        foreach($page->site->chapters as $chapter)
        {
            $chapters[$chapter->name] = $chapter->pages->except($ignore);
        }

        return $chapters;

    }

    public function updateRelated($pageId, $formData)
    {
        $page = $this->get($pageId);

        // clear relations
        $page->relatedTo()->detach();
        $page->relatedBy()->detach();

        foreach($formData['page'] as $relate)
        {
            $page->relatedTo()->attach($relate['id']);

            $page->relatedTo()->updateExistingPivot($relate['id'], ['key' => $relate['key']]);
        }
    }

    // DEPRECATED?
    public function relatePage($pageId, $relatedId, $key = null)
    {
        $page = $this->get($pageId);

        $related = $this->get($relatedId);

        $page->relatedTo()->attach($related->id);
    }

    public function unRelatePage($pageId, $relatedId)
    {
        $page = $this->get($pageId);

        $page->relatedTo()->detach($relatedId);
        $page->relatedBy()->detach($relatedId);

        return $relatedId;

    }
}