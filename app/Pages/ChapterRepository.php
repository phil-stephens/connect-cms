<?php

namespace Fastrack\Pages;


use Fastrack\Sites\Site;
use Fastrack\Meta\UuidRepositoryTrait;

class ChapterRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Chapter::class;

    //public function get($uuid)
    //{
    //    return Chapter::whereUuid($uuid)->firstOrFail();
    //}

    public function getAll($siteId)
    {
        return Chapter::whereSiteId($siteId)->get();
    }

    public function create($formData, $siteId)
    {
        $chapter = new Chapter($formData);

        $this->save($chapter, $siteId);

        return $chapter;
    }

    public function save(Chapter $chapter, $siteId)
    {
        return Site::findOrFail($siteId)
                    ->chapters()
                    ->save($chapter);
    }

    public function getPages($uuid)
    {
        return $this->get($uuid)->pages()->orderBy('order')->paginate(10);
    }

    public function update($formData, $uuid)
    {
        $chapter = $this->get($uuid);

        $chapter->fill($formData);

        $chapter->save();

        return;
    }
}