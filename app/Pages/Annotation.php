<?php

namespace Fastrack\Pages;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class Annotation extends Model
{
    use HasUuid, Versionable;

    protected $fillable = ['title', 'excerpt', 'body', 'image', 'excerpt_character_limit', 'title_note', 'excerpt_note', 'body_note', 'image_note'];
}
