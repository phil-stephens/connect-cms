<?php namespace Fastrack\Pages;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{

    use HasUuid, Versionable;

    protected $fillable = ['name', 'slug', 'prefix_slug'];

    public function site()
    {
        return $this->belongsTo('Fastrack\Sites\Site');
    }

    public function pages()
    {
        return $this->hasMany('Fastrack\Pages\Page');
    }

}
