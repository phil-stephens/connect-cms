<?php

namespace Fastrack\Campaigns;


use Fastrack\Sites\Site;
use Fastrack\Meta\UuidRepositoryTrait;

class CampaignRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Campaign::class;

    public function create($formData, $siteUuid)
    {
        $campaign = new Campaign($formData);

        $this->save($campaign, $siteUuid);

        return $campaign;
    }

    public function save(Campaign $campaign, $siteUuid)
    {
        return Site::whereUuid($siteUuid)
            ->firstOrFail()
            ->campaigns()
            ->save($campaign);
    }

    public function update($campaignUuid, $formData)
    {
        $campaign = $this->get($campaignUuid);

        $campaign->fill($formData);

        $campaign->save();

        return;
    }
}