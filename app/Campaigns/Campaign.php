<?php

namespace Fastrack\Campaigns;

//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    use HasUuid;

    protected $fillable = ['name', 'start_at', 'finish_at'];

    public function getDates()
    {
        return ['created_at', 'updated_at', 'start_at', 'finish_at'];
    }

    public function site()
    {
        return $this->belongsTo('Fastrack\Sites\Site');
    }

    public function variations()
    {
        return $this->hasMany('Fastrack\Campaigns\Variation')->orderBy('visits', 'asc');
    }
}
