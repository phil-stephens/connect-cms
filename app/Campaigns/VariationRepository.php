<?php

namespace Fastrack\Campaigns;

use Fastrack\Meta\UuidRepositoryTrait;

class VariationRepository
{

    use UuidRepositoryTrait;

    protected $repoClass = Variation::class;

    public function create($formData, $campaignUuid)
    {
        $variation = new Variation($formData);

        $this->save($variation, $campaignUuid);

        return $variation;
    }

    public function save(Variation $variation, $campaignUuid)
    {
        return Campaign::whereUuid($campaignUuid)
            ->firstOrFail()
            ->variations()
            ->save($variation);
    }

    public function update($variationUuid, $formData)
    {
        $variation = $this->get($variationUuid);

        $variation->fill($formData);

        $variation->save();

        return;
    }
}