<?php

namespace Fastrack\Versions;


trait Versionable
{

    protected static function bootVersionable()
    {
        if(env('USE_VERSIONING', true))
        {
            static::updating(function ($model) {
                $previous = $model->getOriginal();

                $version = new Version([
                    'payload' => serialize($previous)
                ]);

                $model->versions()->save($version);
            });
        }
    }

    public function versions()
    {
        return $this->morphMany('Fastrack\Versions\Version', 'parent');
    }
}