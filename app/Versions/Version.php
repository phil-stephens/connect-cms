<?php

namespace Fastrack\Versions;

use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    use HasUuid;

    protected $fillable = ['payload'];

    public function parent()
    {
        return $this->morphTo();
    }

    public function setUpdatedAtAttribute($value)
    {
        return;
    }
}
