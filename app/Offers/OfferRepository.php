<?php

namespace Fastrack\Offers;


use Fastrack\Content\WebpageRepositoryTrait;
use Fastrack\Properties\Property;
use Fastrack\Sites\Site;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class OfferRepository
 * @package Fastrack\Offers
 */
class OfferRepository
{
    use UuidRepositoryTrait, WebpageRepositoryTrait;

    protected $repoClass = Offer::class;

    /**
     * @param $offerId
     * @param $propertyId
     * @return mixed
     */
    public function getById($offerId, $propertyId)
    {
        return Offer::with('content', 'seo')
            ->wherePropertyId($propertyId)
            ->findOrFail($offerId);
    }

    /**
     * @param $offerSlug
     * @param $propertyId
     * @return mixed
     */
    public function getBySlug($offerSlug, $propertyId)
    {
        return Offer::with('content', 'seo')
            ->wherePropertyId($propertyId)
            ->whereSlug($offerSlug)
            ->firstOrFail();
    }

    public function getAllForSite($siteId)
    {
        // Get all of the properties
        $propertyIds = Site::findOrFail($siteId)->properties()->lists('id')->all();

        return $this->getAllForProperties($propertyIds);

    }

    public function getAllForProperty($propertyId)
    {
        return $this->getAllForProperties([$propertyId]);
    }

    private function getAllForProperties($propertyIds)
    {
        return Offer::with('content', 'seo')
            ->whereIn('property_id', $propertyIds)
            ->whereActive(true)
            ->where('start_at', '<=', \DB::raw("NOW()"))
            ->where('finish_at', '>', \DB::raw("NOW()"))
            ->orderBy('order')
            ->get();
    }

    /**
     * @param $formData
     * @param $propertyId
     * @return Offer
     */
    public function create($formData, $propertyId)
    {
        $formData = $this->createStubs($formData);

        $offer = new Offer($formData);

        $this->save($offer, $propertyId);

        $this->createContent($offer, $formData);
        $this->createSeo($offer, $formData);


        return $offer;
    }

    /**
     * @param Offer $offer
     * @param $propertyId
     * @return mixed
     */
    public function save(Offer $offer, $propertyId)
    {
        $property = Property::findOrFail($propertyId);

        // Automatically fill in the sort order
        $order = \DB::table('offers')->wherePropertyId($propertyId)->max('order');


        $offer->order = $order + 1;

        return $property->offers()
            ->save($offer);
    }

    /**
     * @param $formData
     * @param $propertyId
     */
    public function sortOffers($formData, $propertyId)
    {
        foreach ($formData['data'] as $offerId => $order) {
            if ($order == '') {
                continue;
            }

            $offer = $this->getById($offerId, $propertyId);

            $offer->order = $order;

            $offer->save();
        }
    }

    /**
     * @param $offerId
     * @param $formData
     * @param $propertyId
     * @return bool
     */
    public function update($offerId, $formData, $propertyId)
    {
        //print_r($formData);
        //die;
        $offer = $this->getById($offerId, $propertyId);

        $offer->fill($formData);

        $offer->save();

        // Now do the content...
        if (isset($formData['content'])) {
            $this->updateContent($offer, $formData['content']);
        }

        // Now do the SEO..
        if (isset($formData['seo'])) {
            $this->updateSeo($offer, $formData['seo']);
        }

        // Refresh custom routes
        //$this->buildSiteRoutes();

        return true;
    }

    /**
     * @param $offerId
     * @param $propertyId
     */
    public function destroy($offerId, $propertyId)
    {
        return $this->destroyIt(
            $this->getById($offerId, $propertyId)
        );
    }
}