<?php namespace Fastrack\Offers;

use Fastrack\Content\SeoTrait;
use Fastrack\Content\URLTrait;
use Fastrack\Content\WebpageTrait;
use Fastrack\Sites\SettingTrait;
use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use League\CommonMark\DocParser;
use League\CommonMark\Environment;
use League\CommonMark\HtmlRenderer;
//use Fastrack\Versions\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Offer
 * @package Fastrack\Offers
 */
class Offer extends Model
{

    use HasUuid, Versionable, WebpageTrait, SeoTrait, URLTrait, SettingTrait, TaggableTrait;

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'headline', 'link', 'start_at', 'finish_at', 'terms', 'active', 'order'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo('Fastrack\Properties\Property');
    }

    /**
     * @return array
     */
    public function getDates()
    {
        return ['created_at', 'updated_at', 'start_at', 'finish_at'];
    }

    public function getHeadline($parseMarkdown = true, $before = null, $after = null)
    {
        $content = trim($this->headline);

        if (empty($content)) {
            return;
        }

        if ($parseMarkdown) {
            $environment = Environment::createCommonMarkEnvironment();
            $parser = new DocParser($environment);
            $htmlRenderer = new HtmlRenderer($environment);

            $text = $parser->parse($content);
            $content = $htmlRenderer->renderBlock($text);
        }

        return $before . $content . $after;
    }
}
