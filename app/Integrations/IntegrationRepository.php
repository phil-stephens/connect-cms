<?php

namespace Fastrack\Integrations;

use Fastrack\Meta\UuidRepositoryTrait;

class IntegrationRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Integration::class;


    public function getAll()
    {
        return Integration::all();
    }

    public function getSelected($properties)
    {
        return Integration::whereHas('properties', function($query) use ($properties)
            {
                $query->whereIn('id', $properties);
            })->get();
    }

    public function getByModel($model, $integrationId)
    {
        foreach($model->integrations() as $integration)
        {
            if($integration->uuid == $integrationId) return $integration;
        }

        return false;
    }

    public function create($formData)
    {
        // extract the additional fields
        $type = class_basename($formData['type']);

        if(isset($formData[$type])) $formData = array_merge($formData, $formData[$type]);

        return Integration::create($formData);
    }

    public function attach($model, $formData)
    {
        // Get the integration
        $integration = $this->get($formData['integration']);

        switch(class_basename($model))
        {
            case 'Property':
                $integration->properties()->attach($model);
                $integration->properties()->updateExistingPivot($model->id, ['client_id' => $formData['client_id']]);
                break;

            case 'Room':
                $integration->rooms()->attach($model);
                $integration->rooms()->updateExistingPivot($model->id, ['client_id' => $formData['client_id']]);
                break;
        }

        return;
    }

    public function updateAttachment($integrationId, $model, $formData)
    {
        $integration = $this->get($integrationId);

        switch(class_basename($model))
        {
            case 'Property':
                $integration->properties()->updateExistingPivot($model->id, ['client_id' => $formData['client_id']]);
                break;

            case 'Room':
                $integration->rooms()->updateExistingPivot($model->id, ['client_id' => $formData['client_id']]);
                break;
        }

        return;
    }
}