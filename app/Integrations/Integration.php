<?php

namespace Fastrack\Integrations;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{
    use HasUuid, Versionable;

    protected $fillable = ['type', 'name', 'username', 'password', 'custom1', 'active'];

    public function properties()
    {
        return $this->morphedByMany('Fastrack\Properties\Property', 'integratable')->withPivot('client_id');
    }

    public function rooms()
    {
        return $this->morphedByMany('Fastrack\Properties\Room', 'integratable')->withPivot('client_id');
    }
}
