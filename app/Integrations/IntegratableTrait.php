<?php

namespace Fastrack\Integrations;


trait IntegratableTrait
{
    public function integrations()
    {
        return $this->morphToMany('Fastrack\Integrations\Integration', 'integratable')->withPivot('client_id');
    }
}