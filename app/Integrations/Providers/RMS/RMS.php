<?php

namespace Fastrack\Integrations\Providers\RMS;


use Carbon\Carbon;
use Fastrack\Integrations\Integration;
use Fastrack\Integrations\Providers\BookingProviderInterface;
use Fastrack\Properties\Availability;
use GuzzleHttp\Client;
use SimpleXMLElement;
use Illuminate\Support\Collection;


class RMS implements BookingProviderInterface
{

    /**
     * @var Integration
     */
    private $integration;

    private $start;

    private $finish;

    protected $cacheable = true;

    protected $cacheDays = 180;

    private $endpoint = 'https://api2.rms.com.au/rmsxml/rms_api.aspx';

    private $basePropertyUrl = 'https://bookings2.rms.com.au/obookings3/Rates/Index/';

    public function __construct(Integration $integration)
    {
        $this->integration = $integration;
    }

    public function search($start, $finish, $channel = null)
    {
        $limit = new Carbon();
        $limit->addDays( $this->cacheDays() );

        $this->start = new Carbon($start);
        $this->finish = new Carbon($finish);

        // is the start/finish date more than 180 days away?
        // if so, do the straight $this->getData($start, $finish)
        if($limit->lte($this->start) || $limit->lte($this->finish))
        {
            return $this->getData($start, $finish, $channel);
        }


        $rooms = [];

        foreach($this->integration->rooms as $r)
        {
            $room = new \stdClass();

            $room->room = $r;

            $dates = [];

            $range = Availability::whereRoomId($r->id)->where('date', '>=', $this->start->format('Y-m-d'))->where('date', '<=', $this->finish->format('Y-m-d'))->get();

            foreach($range as $d)
            {
                $dates[] = [
                    'date'          => $d->date,
                    'available'     => $d->available,
                    'rate'          => $d->rate
                ];
            }

            $room->dates = new Collection($dates);

            $rooms[] = $room;

        }

        return new Collection($rooms);
    }

    public function getData($start, $finish, $channel = null)
    {
        $this->start = new Carbon($start);
        $this->finish = new Carbon($finish);

        return $this->buildResponse($this->handleRequest($channel));
    }

    public function getRoomIds($channel)
    {
        $this->start = Carbon::tomorrow();
        $this->finish = Carbon::tomorrow()->addDays(1);

        $response = $this->handleRequest($channel);

        $rooms = [];

        foreach($response->Responses->Response->RoomTypes->RoomType as $type)
        {
            $rooms[] = [
                'client_id' => (string) $type->RoomTypeId,
                'name'      => (string) $type->Name
            ];
        }

        return new Collection($rooms);
    }

    public function buildResponse($data)
    {
        $rooms = [];

        try
        {
            foreach($data->Responses->Response as $res)
            {
                if( ! empty($res->RoomTypes))
                {
                    foreach($res->RoomTypes->RoomType as $type)
                    {
                        foreach($this->integration->rooms as $r)
                        {
                            if($r->pivot->client_id == $type->RoomTypeId)
                            {
                                $room = new \stdClass();

                                $room->room = $r;

                                $dates = [];

                                foreach($type->ChargeTypes->ChargeType[0]->Charge as $date)
                                {
                                    $dates[] = [
                                        'date'          => new Carbon($date['Date']),
                                        'available'     => (string) $date['NoOfRoomsAvailable'],
                                        'rate'          => (string) $date['Price']
                                    ];
                                }


                                $room->dates = new Collection($dates);

                                $rooms[] = $room;
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e)
        {
            dd($data);
        }

        return new Collection($rooms);
    }

    public function handleRequest($channel = null)
    {
        $xml = new SimpleXMLElement('<RMSAvailRateChartRQ/>');

        $requests = $xml->addChild('Requests');

        if( ! empty($channel))
        {
            $request = $requests->addChild('Request');

            $request->addChild('AgentId', 1); // this might need to be variable?
            $request->addChild('RMSClientId', $channel);
            $request->addChild('Start', $this->start->toDateString());
            $request->addChild('End', $this->finish->toDateString());
        } else
        {
            foreach($this->integration->properties as $property)
            {
                $request = $requests->addChild('Request');

                $request->addChild('AgentId', 1); // this might need to be variable?
                $request->addChild('RMSClientId', $property->pivot->client_id);
                $request->addChild('Start', $this->start->toDateString());
                $request->addChild('End', $this->finish->toDateString());
            }
        }

        $client = new Client();

        $response = $client->post($this->endpoint,
            [
                'headers'   => ['Content-Type' => 'application/xml'],
                'body'      => $xml->asXML(),
                'auth'      => [$this->integration->username, $this->integration->password]
            ]);



        $payload = $response->getBody();

        return new \SimpleXMLElement($payload);
    }

    public function bookingUrl($model, $dates = null, $adults = 1)
    {
        //return null;

        $query = [];

        if(! empty($dates[0])) $query['A'] = $dates[0]->format('m/d/Y');

        if(! empty($dates[1]))
        {
            $checkout = clone $dates[1];
            $query['D'] = $checkout->addDay()->format('m/d/Y');
        }

        $query['Ad'] = $adults;

        if(class_basename($model) == 'Room')
        {
            $channel = $this->integration->properties()->find($model->property_id)->pivot->client_id;

            // For now we will go directly to the main property page
            return $this->basePropertyUrl . $channel . '/1/' . '?' . http_build_query($query);
        } elseif(class_basename($model) == 'Property')
        {
            return $this->basePropertyUrl . $this->integration->pivot->client_id . '/1/' . '?' . http_build_query($query);
        }
    }

    public function cacheable()
    {
        return ! (empty($this->cacheable));
    }

    public function cacheDays()
    {
        return (isset($this->cacheDays)) ? $this->cacheDays : 180;
    }
}