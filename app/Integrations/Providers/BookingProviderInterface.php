<?php

namespace Fastrack\Integrations\Providers;

use Fastrack\Integrations\Integration;

interface BookingProviderInterface
{

    public function __construct(Integration $integration);

    public function search($start, $finish, $channel = null);

    public function getData($start, $finish, $channel = null);

    public function getRoomIds($channel);

    public function buildResponse($data);

    public function handleRequest($channel = null);

    public function bookingUrl($model, $dates = null, $adults = 1);

    public function cacheable();

    public function cacheDays();
}