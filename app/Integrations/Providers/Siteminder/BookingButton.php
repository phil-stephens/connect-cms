<?php

namespace Fastrack\Integrations\Providers\Siteminder;

use Carbon\Carbon;
use Fastrack\Integrations\Integration;
use Fastrack\Integrations\Providers\BookingProviderInterface;
use GuzzleHttp\Client as Guzzle;
use Illuminate\Support\Collection;

class BookingButton implements BookingProviderInterface
{

    /**
     * @var Integration
     */
    private $integration;

    private $start;

    private $finish;

    protected $cacheable = false;

    private $endpoint = 'https://www.thebookingbutton.com.au/api/v1/properties/';

    private $basePropertyUrl = 'https://www.thebookingbutton.com.au/properties/';

    private $baseRoomUrl = 'https://www.thebookingbutton.com.au/reservations/';

    public function __construct(Integration $integration)
    {
        $this->integration = $integration;
    }

    public function search($start, $finish, $channel = null)
    {
        return $this->getData($start, $finish, $channel = null);
    }

    public function getData($start, $finish, $channel = null)
    {
        $requests = [];

        // BookingButton only has one property per integration...
        if(empty($channel)) $channel = $this->integration->properties()->firstOrFail()->pivot->client_id;

        // calculate size of data range
        $this->start = new Carbon($start);
        $this->finish = new Carbon($finish);
        $this->finish->subDay(1);

        $range = $this->finish->diffInDays($this->start);

        // if it's bigger than the BookingButton max request range then split the range up into multiple requests
        if($range > 10)
        {

        }

        $requests[] = [
            'start'     => $this->start,
            'finish'    => $this->finish
        ];

        //dd($requests);

        // trigger each request
        foreach($requests as $request)
        {
            $this->start = $request['start']->toDateString();
            $this->finish = $request['finish']->toDateString();

            $response[] = $this->buildResponse($this->handleRequest($channel));
        }

        // transform the responses into a single RatesResponse object

        // maps each room to it's actual object

        return $response[0];
    }

    public function getRoomIds($channel)
    {

        $this->start = Carbon::tomorrow()->toDateString();
        $this->finish = Carbon::tomorrow()->addDays(1)->toDateString();

        $response = $this->handleRequest($channel);

        //dd($response);
        $rooms = [];

        foreach($response as $type)
        {
            $rooms[] = [
                'client_id' => $type->id,
                'name'      => $type->name
            ];
        }

        return new Collection($rooms);

        // map date to a common format - maybe PropertyResponse object

    }

    public function buildResponse($data)
    {
        $rooms = [];

        foreach($data as $item)
        {
            foreach($this->integration->rooms as $r)
            {
                if($r->pivot->client_id == $item->id)
                {
                    $room = new \stdClass();

                    $room->room = $r;

                    $dates = [];

                    foreach($item->room_type_dates as $date)
                    {
                        $dates[] = [
                            'date'          => new Carbon($date->date),
                            'available'     => $date->available,
                            'rate'          => $date->rate
                        ];
                    }

                    $room->dates = new Collection($dates);

                    $rooms[] = $room;
                }
            }
        }

        return new Collection($rooms);
    }

    //public function getRaw()
    //{
    //    $tomorrow = Carbon::tomorrow();
    //    $this->start = $tomorrow->toDateString();
    //    $this->finish = $tomorrow->addDays(1)->toDateString();
    //
    //    // BookingButton only has one property per integration...
    //    if($property = $this->integration->properties()->first())
    //    {
    //        $rooms = $this->handleRequest($property->pivot->client_id);
    //
    //        dd($rooms);
    //    }
    //
    //    // map date to a common format - maybe PropertyResponse object
    //}

    public function handleRequest($channel = null)
    {
        $client = new Guzzle();

        $params = [
            'start_date' => $this->start,
            'end_date'   => $this->finish
        ];

        //if( ! empty($this->integration->custom1))
        //{
        //    $url = $this->integration->custom1;
        //} else
        //{
        //    $url = $this->endpoint . $channel . '/rates.json';
        //}

        $url = $this->endpoint . $channel . '/rates.json';

        $response = $client->get($url . '?' . http_build_query($params));

        $response = json_decode($response->getBody());

        return $response[0]->room_types;
    }

    public function bookingUrl($model, $dates = null, $adults = 1)
    {
        // Build the date query string

        $query = [];

        if(! empty($dates[0]))
        {
            $query['start_date'] = $dates[0]->format('j M Y');
        }

        if(! empty($dates[1]))
        {
            $checkout = clone $dates[1];
            $query['check_out_date'] = $checkout->addDay()->format('j M Y');
        }

        if(class_basename($model) == 'Room')
        {
            // BookingButton only has one property per integration...
            //$channel = $this->integration->properties()->firstOrFail()->pivot->client_id;

            $channel = $this->integration->properties()->find($model->property_id)->pivot->client_id;

            return $this->baseRoomUrl . $channel . '/' . $this->integration->pivot->client_id . '?' . http_build_query($query);

        } elseif(class_basename($model) == 'Property')
        {
            return $this->basePropertyUrl . $this->integration->pivot->client_id . '?' . http_build_query($query);
        }

        return null;
    }

    public function cacheable()
    {
        return ! (empty($this->cacheable));
    }

    public function cacheDays()
    {
        return null;
    }
}