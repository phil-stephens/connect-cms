<?php

namespace Fastrack\Exceptions;

use Exception;
use Fastrack\Sites\AliasRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
//use Bugsnag\BugsnagLaravel\BugsnagExceptionHandler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        //\UnderstandExceptionLogger::log($e);
        
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);


            try {
                $repo = new AliasRepository();

                $alias = $repo->getByFrom($request->path(), site('id'));

                return redirect($alias->to, $alias->type);
            } catch (\Exception $aliasException) {
                // Do nothing - maybe log something at some point
            }

            abort(404);
        }

        return parent::render($request, $e);
    }
}
