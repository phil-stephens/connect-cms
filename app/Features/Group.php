<?php

namespace Fastrack\Features;

use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasUuid;

    protected $table = 'feature_groups';

    public function amenities()
    {
        return $this->features();
    }

    public function features()
    {
        return $this->belongsToMany('Fastrack\Properties\Amenity', 'amenity_featuregroup', 'amenity_id', 'featuregroup_id')->withPivot(['description', 'order'])->orderBy('order');
    }

    public function property()
    {
        return $this->belongsTo('Fastrack\Properties\Property');
    }

    public function room()
    {
        return $this->belongsTo('Fastrack\Properties\Room');
    }
}
