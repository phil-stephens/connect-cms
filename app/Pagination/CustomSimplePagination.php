<?php namespace Fastrack\Pagination;

use Illuminate\Contracts\Pagination\Paginator as PaginatorContract;
use Illuminate\Pagination\BootstrapThreePresenter;

class CustomSimplePagination extends BootstrapThreePresenter
{

    /**
     * Create a simple Bootstrap 3 presenter.
     *
     * @param  \Illuminate\Contracts\Pagination\Paginator $paginator
     * @return void
     */
    public function __construct(PaginatorContract $paginator)
    {
        $this->paginator = $paginator;
    }

    public function render($previousLabel = 'Previous', $nextLabel = 'Next')
    {
        if ($this->hasPages()) {
            return sprintf(
                '<ul class="pager">%s %s</ul>',
                $this->getPreviousButton($previousLabel),
                $this->getNextButton($nextLabel)
            );
        }

        return '';
    }

    /**
     * Determine if the underlying paginator being presented has pages to show.
     *
     * @return bool
     */
    public function hasPages()
    {
        return $this->paginator->hasPages() && count($this->paginator->items()) > 0;
    }

}
