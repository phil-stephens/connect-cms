<?php

namespace Fastrack\Atoms;

use Fastrack\Sites\SettingTrait;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class Atom extends Model
{
    use SettingTrait, HasUuid, Versionable;

    protected $fillable = ['key', 'order', 'template', 'theme'];

    public function content()
    {
        return $this->belongsTo('Fastrack\Content\Content');
    }

    public function render()
    {
        if(view()->exists($this->template)) return view($this->template, $this->templateVariables());

        return;
    }

    public function templateVariables()
    {
        $settings = $this->settings;

        $variables = [];

        foreach($settings as $setting)
        {
            $variables[$setting->key] = $this->setting($setting->key, null);
        }

        return $variables;
    }

    public function handlebarsJson()
    {
        $data = [
            'key' => $this->key,
            'id'    => $this->id,
            'order' => $this->order
        ];

        foreach($this->settings as $setting)
        {
            $data['fields'][$setting->key] = [
                'type'  => $setting->type,
                'value' => $setting->value,
                'id'    => $setting->id,
            ];

            if( $setting->type == 'image')
            {
                $setting->load('image');

                if( ! empty($setting->image))
                {
                    $data['fields'][$setting->key]['uuid'] = $setting->image->uuid;
                    $data['fields'][$setting->key]['img'] = $setting->image->getTag(config('image.preview_params'), ['class' => 'img-responsive content-image']);
                }
            }
        }

        return json_encode($data);
    }
}
