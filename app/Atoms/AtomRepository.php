<?php

namespace Fastrack\Atoms;

use Fastrack\Content\Content;
use Fastrack\Sites\SettingRepository;
use Fastrack\Sites\Site;
use Fastrack\Meta\UuidRepositoryTrait;

class AtomRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Atom::class;

    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * AtomRepository constructor.
     */
    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    public function getContent($uuid)
    {
        return Content::with('parent')->whereUuid($uuid)->firstOrFail();
    }

    public function update($uuid, $formData)
    {
        $content = $this->getContent($uuid);

        // used for cross-referencing
        $templates = $this->gatherTemplates();

        foreach($formData as $a)
        {
            if( ! empty($a['id']))
            {
                $atom = Atom::find($a['id']);

                if( ! empty($a['remove']))
                {
                    // Remove the settings
                    $atom->settings()->delete();

                    $atom->delete();

                    continue;
                }

                $atom->fill($a);

                $atom->save();
            } elseif( ! empty($a['remove']))
            {
                continue;
            } else
            {

                // will only create for now
                $atom = new Atom($a);

                $content->atoms()->save($atom);
            }


            $this->settingRepository->updateAll($atom, $a['field']);

            //dd($templates[$atom['key']]);
            //dd($a);


            // Render the template out...
            //$this->render($atom);
        }
    }

    public function render(Atom $atom)
    {
//        //die( view(
//        //    public_path('themes/' . $atom->theme . '/views/' . $atom->template),
//        //    $atom->templateVariables()) );
//
//        $view = \View::make(public_path('themes/' . $atom->theme . '/views/' . $atom->template),
//                $atom->templateVariables());
//        //$contents = (string) $view;
//// or
//        $contents = $view->render();

    }

    public function gatherTemplates()
    {
        $themes = Site::lists('theme')->all();

        $templates = [];

        foreach($themes as $theme)
        {
            try
            {
                $manifest = file_get_contents(public_path('themes/' . $theme . '/manifest.json'));

                $atoms = json_decode($manifest)->atoms;

                foreach($atoms as $atom)
                {
                    $templates[$atom->key] = $atom;

                    $templates[$atom->key]->theme = $theme;

                    foreach($templates[$atom->key]->fields as $key => $field)
                    {
                        $templates[$atom->key]->fields[$field->key] = $field;

                        unset($templates[$atom->key]->fields[$key]);
                    }
                }
            } catch(\Exception $e)
            {

            }
        }

        return $templates;
    }
}