<?php namespace Fastrack\Calendars;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasUuid, Versionable;

    protected $fillable = ['name', 'email'];

    public function purchase()
    {
        return $this->belongsTo('Fastrack\Calendars\TicketPurchase');
    }
}
