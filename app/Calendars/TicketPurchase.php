<?php namespace Fastrack\Calendars;

//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Fastrack\Versions\Model;
use Illuminate\Database\Eloquent\Model;

class TicketPurchase extends Model
{
    use SoftDeletes, HasUuid, Versionable;

    protected $fillable = [
        'name',
        'email',
        'company',
        'position',
        'phone',
        'address1',
        'address2',
        'city',
        'state',
        'postcode',
        'country',
        'ticket_type_id',
        'price',
        'price_tickets',
        'price_discount',
        'price_tax',
        'price_cc',
        'manual',
        'paid',
        'notes'
    ];

    protected $dates = ['deleted_at'];

    public function ticket_type()
    {
        return $this->belongsTo('Fastrack\Calendars\TicketType');
    }

    public function tickets()
    {
        return $this->hasMany('Fastrack\Calendars\Ticket');
    }
}
