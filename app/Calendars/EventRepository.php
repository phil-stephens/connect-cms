<?php namespace Fastrack\Calendars;

use Fastrack\Content\Seo;
use Fastrack\Content\WebpageRepositoryTrait;
use Fastrack\Sites\Site;
use Fastrack\Slideshows\SlideshowRepositoryTrait;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class EventRepository
 * @package Fastrack\Calendars
 */
class EventRepository
{
    use WebpageRepositoryTrait, SlideshowRepositoryTrait, UuidRepositoryTrait;

    protected $repoClass = CalendarEvent::class;
    
    public function getById($eventId, $calendarId = null)
    {
        $query = CalendarEvent::with('content', 'seo');

        if ( ! empty($calendarId)) {
            $query->whereCalendarId($calendarId);
        }

        return $query->findOrFail($eventId);
    }

    /**
     * @param $eventSlug
     * @return mixed
     */
    public function getBySlug($eventSlug, $calendarSlug)
    {
        $calendar = Calendar::whereSlug($calendarSlug)->firstOrFail();

        return CalendarEvent::with('content',
            'seo')->whereSlug($eventSlug)->whereCalendarId($calendar->id)->firstOrFail();
    }

    public function getByEventSlug($eventSlug, $calendarId)
    {
        return CalendarEvent::with('content',
            'seo')->whereSlug($eventSlug)->whereCalendarId($calendarId)->firstOrFail();
    }

    /**
     * @return mixed
     */
    public function getAll($calendarId)
    {
        return CalendarEvent::with('content', 'seo')->whereCalendarId($calendarId)->latest()->get();
    }

    /**
     * @return mixed
     */
    public function getAllUpcoming($siteId = null, $calendarSlug = null)
    {
        if (empty($siteId)) {
            $siteId = site('id');
        }

        $site = Site::findOrFail($siteId);

        $query = CalendarEvent::with('content', 'seo')->where('finish_at', '>=',
            \DB::raw("NOW()"))->orderBy('start_at');

        if ( ! empty($calendarSlug)) {
            $calendar = Calendar::whereSlug($calendarSlug)->firstOrFail();
            $query->whereCalendarId($calendar->id);
        } else {
            $query->whereIn('calendar_id', $site->calendars->lists('id')->all());
        }

        return $query->get();
    }

    public function save(CalendarEvent $event, $calendarId)
    {
        return Calendar::findOrFail($calendarId)
            ->events()
            ->save($event);
    }


    public function create($formData, $calendarId)
    {

        $formData = $this->createStubs($formData);

        //$event = CalendarEvent::generate($formData['name'], $formData['slug']);
        //$event->save();
        $event = new CalendarEvent($formData);

        $event->calendar_id = $calendarId;

        $event->save();

        //$this->save($event, $calendarId);

        $this->createContent($event, $formData);
        //$content = new Content($formData['content']);
        //
        //// Find the image
        //if( ! empty($formData['content']['upload_hash']))
        //{
        //    $image = Image::whereUploadHash($formData['content']['upload_hash'])->latest()->first();
        //
        //    if( ! empty($image)) $content->image_id = $image->id;
        //}
        //
        //$event->content()->save($content);

        // SEO bootstrapping
        $seo = new Seo($formData['seo']);

        $event->seo()->save($seo);


        // Refresh custom routes
        // $this->buildSiteRoutes();
        $this->bootstrapSlideshow($event);

        return $event;
    }

    /**
     * @param $eventId
     * @param $formData
     * @return bool
     */
    public function update($eventId, $formData, $calendarId)
    {
        $event = $this->getById($eventId, $calendarId);

        $event->fill($formData);

        $event->save();

        // Now do the content...
        //$event->content->fill($formData['content']);
        //
        //$event->content->save();
        if (isset($formData['content'])) {
            $this->updateContent($event, $formData['content']);
        }


        // Now do the SEO...
        //$event->seo->fill($formData['seo']);
        //
        //$event->seo->save();

        if (isset($formData['seo'])) {
            $this->updateSeo($event, $formData['seo']);
        }

        // Refresh custom routes
        //$this->buildSiteRoutes();

        return true;
    }

    /**
     * @param $eventId
     */
    public function destroy($eventId, $calendarId)
    {
        return $this->destroyIt(
            $this->getById($eventId, $calendarId)
        );
    }
}