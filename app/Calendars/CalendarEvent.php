<?php namespace Fastrack\Calendars;

use Fastrack\Content\SeoTrait;
use Fastrack\Content\URLTrait;
use Fastrack\Content\WebpageTrait;
use Fastrack\Sites\SettingTrait;
use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CalendarEvent
 * @package Fastrack\Calendars
 */
class CalendarEvent extends Model
{

    use WebpageTrait, SeoTrait, URLTrait, SettingTrait, TaggableTrait, HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'price', 'venue', 'website', 'start_at', 'finish_at'];


    /**
     * @return array
     */
    public function getDates()
    {
        return ['created_at', 'updated_at', 'start_at', 'finish_at'];
    }

    public function calendar()
    {
        return $this->belongsTo('Fastrack\Calendars\Calendar');
    }

    public function ticket_types()
    {
        return $this->hasMany('Fastrack\Calendars\TicketType');
    }

    public function getCanonicalSiteAttribute()
    {
        if ( ! array_key_exists('canonical_site', $this->relations))
        {
            $site = $this->calendar->canonical_site;

            $this->setRelation('canonical_site', $site);
        }

        return $this->getRelation('canonical_site');
    }

    public function ticketUrl($stage = 'details')
    {
        return '/calendar/' . $this->calendar->slug . '/event/' . $this->slug . '/tickets/' . $stage;
    }

    /**
     * @param $name
     * @param $slug
     * @return static
     */
    public static function generate($name, $slug)
    {
        return new static(compact('name', 'slug'));
    }
}
