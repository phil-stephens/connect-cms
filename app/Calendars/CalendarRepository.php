<?php namespace Fastrack\Calendars;

use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class CalendarRepository
 * @package Fastrack\Calendars
 */
class CalendarRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Calendar::class;

    public function getAll()
    {
        return Calendar::all();
    }

    public function create($formData)
    {
        $calendar = Calendar::create($formData);

        return $calendar;
    }

    public function update($calendarId, $formData)
    {
        $calendar = $this->getById($calendarId);

        $calendar->fill($formData);

        $calendar->save();

        return;
    }

    public function getById($calendarId)
    {
        return Calendar::findOrFail($calendarId);
    }

    public function destroy($calendarId)
    {
        $calendar = $this->getById($calendarId);

        $calendar->delete();

        return;
    }
}