<?php namespace Fastrack\Calendars;


trait CalendarableTrait
{

    public function calendars()
    {
        return $this->morphToMany('Fastrack\Calendars\Calendar', 'calendarable');
    }
}