<?php namespace Fastrack\Calendars;

use Fastrack\Content\CanonicalTrait;
//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    use CanonicalTrait, HasUuid, Versionable;

    protected $fillable = ['name', 'slug', 'description', 'canonical_site_id'];

    public function sites()
    {
        return $this->morphedByMany('Fastrack\Sites\Site', 'calendarable');
    }

    public function venues()
    {
        return $this->morphedByMany('Fastrack\Locations\Venue', 'calendarable');
    }

    public function properties()
    {
        return $this->morphedByMany('Fastrack\Properties\Property', 'calendarable');
    }

    public function events()
    {
        return $this->hasMany('Fastrack\Calendars\CalendarEvent');
    }
}
