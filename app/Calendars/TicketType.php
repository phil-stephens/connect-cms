<?php namespace Fastrack\Calendars;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class TicketType extends Model
{

    use HasUuid, Versionable;

    public function event()
    {
        return $this->belongsTo('Fastrack\Calendars\CalendarEvent');
    }

    public function purchases()
    {
        return $this->hasMany('Fastrack\Calendars\TicketPurchase');
    }

}
