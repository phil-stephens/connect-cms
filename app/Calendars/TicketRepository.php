<?php

namespace Fastrack\Calendars;


class TicketRepository
{

    public function getTypes($eventId)
    {
        return TicketType::whereCalendarEventId($eventId)->get();
    }

    public function getPurchase($purchaseId)
    {
        return TicketPurchase::with('ticket_type', 'tickets')->findOrFail($purchaseId);
    }
}