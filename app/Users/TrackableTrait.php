<?php namespace Fastrack\Users;

use Request;

/**
 * Class TrackableTrait
 * @package Fastrack\Users
 */
trait TrackableTrait
{

    /**
     * @param array $options
     */
    public function save(array $options = array())
    {
        $request = Request::instance();

        $this->attributes['ip'] = $request->ip();
        $this->attributes['user_agent'] = $request->server('HTTP_USER_AGENT');

        parent::save($options);
    }

    /**
     * @param $value
     */
    public function setUpdatedAtAttribute($value)
    {
        return;
    }
}