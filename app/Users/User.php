<?php namespace Fastrack\Users;

use Fastrack\Uuid\HasUuid;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Laracasts\Presenter\PresentableTrait;
use League\CommonMark\DocParser;
use League\CommonMark\Environment;
use League\CommonMark\HtmlRenderer;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @package Fastrack\Users
 */
class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{

    use Authenticatable, Authorizable, CanResetPassword, PresentableTrait, HasUuid;

    /**
     * @var string
     */
    protected $presenter = 'Fastrack\Presenters\UserPresenter';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'biography', 'developer'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logins()
    {
        return $this->hasMany('Fastrack\Users\Login');
    }

    public function parent()
    {
        return $this->morphTo();
    }

    public function image()
    {
        return $this->belongsTo('Fastrack\Images\Image');
    }

    /**
     * @param $password
     */
    //public function setPasswordAttribute($password)
    //{
    //    $this->attributes['password'] = bcrypt($password);
    //}

    /**
     * @param bool|true $parseMarkdown
     * @param null $before
     * @param null $after
     * @return string|void
     */
    public function getBio($parseMarkdown = true, $before = null, $after = null)
    {
        $content = trim($this->biography);

        if (empty($content)) {
            return;
        }

        if ($parseMarkdown) {
            $environment = Environment::createCommonMarkEnvironment();
            $parser = new DocParser($environment);
            $htmlRenderer = new HtmlRenderer($environment);

            $text = $parser->parse($content);
            $content = $htmlRenderer->renderBlock($text);
        }

        return $before . $content . $after;
    }

    public function getImagePath($parameters = [], $crop_from = null, $face_detection = false, $defaultPath = null)
    {
        if ($this->image) {
            return $this->image->getPath($parameters, $crop_from, $face_detection);
        }

        return false;
    }

    public function getImageTag($urlParams = [], $tagParams = [], $crop_from = null, $face_detection = null, $defaultPath = null)
    {
        if ($this->image) {
            return $this->image->getTag($urlParams, $tagParams, $crop_from, $face_detection);
        }

        return false;
    }

    public function getImageUuid()
    {
        if ($this->image) {
            return $this->image->uuid;
        }

        return false;
    }
}
