<?php namespace Fastrack\Users;

use Auth, Mail;
use Fastrack\Images\Image;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class UserRepository
 * @package Fastrack\Users
 */
class UserRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = User::class;

    /**
     * @param bool|false $includeHidden
     * @return mixed
     */
    public function getAll($includeHidden = false)
    {
        if ($includeHidden) {
            return User::get();
        }

        return User::whereHidden(false)->get();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getById($userId)
    {
        return User::findOrFail($userId);
    }

    /**
     * @param null $user
     * @param array $loginData
     * @return bool
     */
    public function login($user = null, $loginData = [])
    {
        if ( ! empty($user)) {
            $id = (is_object($user)) ? $user->id : $user;

            if ( ! Auth::loginUsingId($id)) {
                return false;
            }
        } else {
            if ( ! Auth::attempt($loginData)) {
                return false;
            }
        }

        Auth::user()->logins()->save(new Login);

        return true;
    }

    /**
     * @param $formData
     * @return static
     */
    public function create($formData)
    {
        $password = $formData['password'];

        $formData['password'] = bcrypt($formData['password']);

        $user = User::create($formData);

        $this->assignAvatar($user, $formData);

        // Shortcut to assign to client
        //Client::first()->users()->save($user);

        //if ( ! $user->hidden) {
        //    try {
        //        // Add to the central Vision6 mailing list
        //        $api = new \Fastrack\Mail\Vision6(config('services.user_mailing_list.endpoint'),
        //            config('services.user_mailing_list.api_key'), '3.0');
        //
        //        // Enable request debugging
        //        $api->setDebug(true);
        //
        //        $parts = explode(' ', $user->name);
        //
        //        if (count($parts) > 1) {
        //            $last_name = array_pop($parts);
        //            $first_name = implode(' ', $parts);
        //        } else {
        //            $last_name = '';
        //            $first_name = $user->name;
        //        }
        //
        //        $contact_details = [
        //            'Email'        => $user->email,
        //            'First Name'   => $first_name,
        //            'Last Name'    => $last_name,
        //            'Company Name' => env('CMS_TITLE')
        //        ];
        //
        //        $api->invokeMethod('subscribeContact', config('services.user_mailing_list.list_id'), $contact_details);
        //    } catch (\Exception $e) {
        //
        //    }
        //
        //}

        // send an email
        Mail::send('emails.new-user', compact('user', 'password'), function ($message) use ($user) {
            $message->to($user->email,
                $user->name)->subject('Your new FastrackConnect account');
        });

        return $user;
    }

    private function assignAvatar(User $user, $formData)
    {
        if( ! empty($formData['avatar']))
        {
            // Find the image
            try
            {
                $image = Image::whereUuid($formData['avatar'])->firstOrFail();

                $user->image_id = $image->id;
                $user->save();
            } catch( \Exception $e)
            {

            }
        }

        return;
    }

    /**
     * @param $userId
     * @param $formData
     */
    public function update($userId, $formData)
    {
        $user = $this->getById($userId);

        if (empty($formData['password'])) {
            unset($formData['password']);
        } else
        {
            $formData['password'] = bcrypt($formData['password']);
        }

        if(empty($formData['developer']))
        {
            $formData['developer'] = false;
        }

        $user->fill($formData);

        $user->save();

        $this->assignAvatar($user, $formData);

        return;
    }

    /**
     * @param $userId
     */
    public function destroy($userId)
    {
        $user = $this->getById($userId);

        $user->delete();

        return;
    }
}