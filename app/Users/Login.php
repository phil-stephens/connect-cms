<?php namespace Fastrack\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Login
 * @package Fastrack\Users
 */
class Login extends Model
{

    use TrackableTrait;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Fastrack\Users\User');
    }

}
