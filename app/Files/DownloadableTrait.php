<?php
/**
 * Created by PhpStorm.
 * User: philstephens
 * Date: 1/08/15
 * Time: 12:24 PM
 */

namespace Fastrack\Files;

use Auth;
use Crypt;

trait DownloadableTrait
{
    public function downloadPath()
    {
        $details = [
            'id'      => $this->id,
            'type'    => class_basename($this),
            'user_id' => Auth::id()
        ];

        return route('download_file_path', Crypt::encrypt(json_encode($details)));
    }
}