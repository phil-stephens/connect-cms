<?php namespace Fastrack\Files;

use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    use DownloadableTrait, TaggableTrait, HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['original_name', 'file_name', 'path', 'mime_type', 'size', 'upload_hash'];

}
