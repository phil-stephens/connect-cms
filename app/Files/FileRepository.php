<?php
/**
 * Created by PhpStorm.
 * User: philstephens
 * Date: 1/08/15
 * Time: 12:02 PM
 */

namespace Fastrack\Files;

use Fastrack\Meta\UuidRepositoryTrait;

class FileRepository
{

    use UuidRepositoryTrait;

    protected $repoClass = File::class;

    public function getById($fileId)
    {
        return File::findOrFail($fileId);
    }
}