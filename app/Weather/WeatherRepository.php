<?php
/**
 * Created by PhpStorm.
 * User: fastrack
 * Date: 21/07/15
 * Time: 9:43 AM
 */

namespace Fastrack\Weather;


use Cmfcmf\OpenWeatherMap\Forecast;
use Fastrack\Properties\Property;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class WeatherRepository
 * @package Fastrack\Weather
 */
class WeatherRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = WeatherForecast::class;

    /**
     * @param Forecast $f
     * @param $propertyId
     * @return mixed
     */
    public function saveForecast(Forecast $f, $propertyId)
    {
        if ( ! $forecast = $this->getByDate($f->time->day->format('Y-m-d'), $propertyId)) {
            $forecast = new WeatherForecast();
        }

        $forecast->date = $f->time->day->format('Y-m-d');
        $forecast->code = $f->weather->id;
        $forecast->description = $f->weather->description;
        $forecast->min_temp = $f->temperature->min->getValue();
        $forecast->max_temp = $f->temperature->max->getValue();
        $forecast->raw = serialize($f);

        return Property::findOrFail($propertyId)
            ->weatherForecasts()
            ->save($forecast);
    }

    /**
     * @param $date
     * @param $propertyId
     * @return mixed
     */
    public function getByDate($date, $propertyId)
    {
        return WeatherForecast::wherePropertyId($propertyId)
            ->where('date', '=', $date)
            ->first();
    }

    /**
     * @param $propertyId
     * @return mixed
     */
    public function getToday($propertyId)
    {
        return $this->getByDate(date('Y-m-d'), $propertyId);
    }

    /**
     * @param $propertyId
     * @param int $limit
     * @return mixed
     */
    public function getUpcoming($propertyId, $limit = 4)
    {
        return WeatherForecast::wherePropertyId($propertyId)
            ->where('date', '>', date('Y-m-d'))
            ->take($limit)
            ->get();
    }

    public function clearHistoric()
    {

    }
}