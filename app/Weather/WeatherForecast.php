<?php namespace Fastrack\Weather;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WeatherForecast
 * @package Fastrack\Weather
 */
class WeatherForecast extends Model
{

    use HasUuid;

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo('Fastrack\Properties\Property');
    }

    /**
     * @return array
     */
    public function getDates()
    {
        return ['created_at', 'updated_at', 'date'];
    }

}
