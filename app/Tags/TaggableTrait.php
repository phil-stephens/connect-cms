<?php namespace Fastrack\Tags;


trait TaggableTrait
{

    public function tags()
    {
        return $this->morphToMany('Fastrack\Tags\Tag', 'taggable');
    }
}