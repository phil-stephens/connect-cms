<?php namespace Fastrack\Tags;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    use HasUuid;

    public $timestamps = false;

    protected $fillable = ['name'];

    public function sites()
    {
        return $this->morphedByMany('Fastrack\Sites\Site', 'taggable');
    }

    public function pages()
    {
        return $this->morphedByMany('Fastrack\Pages\Page', 'taggable');
    }

    public function venues()
    {
        return $this->morphedByMany('Fastrack\Locations\Venue', 'taggable');
    }

    public function properties()
    {
        return $this->morphedByMany('Fastrack\Properties\Property', 'taggable');
    }

    public function rooms()
    {
        return $this->morphedByMany('Fastrack\Properties\Room', 'taggable');
    }

    public function events()
    {
        return $this->morphedByMany('Fastrack\Calendars\CalendarEvent', 'taggable');
    }

    public function articles()
    {
        return $this->morphedByMany('Fastrack\News\Article', 'taggable');
    }

    public function offers()
    {
        return $this->morphedByMany('Fastrack\Offers\Offer', 'taggable');
    }

    public function images()
    {
        return $this->morphedByMany('Fastrack\Images\Image', 'taggable');
    }

    public function files()
    {
        return $this->morphedByMany('Fastrack\Files\File', 'taggable');
    }


}
