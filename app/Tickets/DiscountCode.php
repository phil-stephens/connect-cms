<?php namespace Fastrack\Tickets;

//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class DiscountCode extends Model
{
    use HasUuid;

    protected $fillable = ['code', 'discount'];

}
