<?php namespace Fastrack\Properties;

use Crypt;
use Fastrack\Content\SeoTrait;
use Fastrack\Content\URLTrait;
use Fastrack\Content\WebpageTrait;
use Fastrack\Integrations\IntegratableTrait;
use Fastrack\Sites\SettingTrait;
use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Laracasts\Presenter\PresentableTrait;
//use Fastrack\Versions\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Room
 * @package Fastrack\Properties
 */
class Room extends Model
{

    use HasUuid, Versionable, WebpageTrait, PresentableTrait, SeoTrait, URLTrait, SettingTrait, TaggableTrait, IntegratableTrait;

    /**
     * @var string
     */
    protected $presenter = 'Fastrack\Presenters\WebpagePresenter';

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'settings', 'price', 'dynamic_rate_data', 'bookingbutton_id', 'capacity', 'type', 'people', 'active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo('Fastrack\Properties\Property');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function amenities()
    {
        return $this->belongsToMany('Fastrack\Properties\Amenity')->withPivot('order', 'description')->orderBy('order');
    }

    // This will become deprecated...probably make it an alias for statistics()
    public function stats()
    {
        //return $this->hasMany('Fastrack\Properties\RoomStatistic')->orderBy('order');
        return $this->statistics();
    }

    // This will be the new way of working...
    public function statistics()
    {
        return $this->belongsToMany('Fastrack\Properties\Statistic')->withPivot('value', 'description')->orderBy('order');
    }

    // This will need updating
    public function getStat($key)
    {
        //try {
        //    return RoomStatistic::whereRoomId($this->id)->whereKey($key)->firstOrFail();
        //} catch (\Exception $e) {
        //    return false;
        //}

        foreach($this->statistics as $stat)
        {
            if($stat->key == $key) return $stat;
        }

        return false;
    }


    public function subTypes()
    {
        return $this->hasMany('Fastrack\Properties\RoomSubType')->orderBy('order');
    }

    public function availabilities()
    {
        return $this->hasMany('Fastrack\Properties\Availability')->orderBy('date');
    }


    public function getCanonicalSiteAttribute()
    {
        if ( ! array_key_exists('canonical_site', $this->relations))
        {
            $site = $this->property->canonical_site;

            $this->setRelation('canonical_site', $site);
        }

        return $this->getRelation('canonical_site');
    }

    /**
     * @param $settings
     */
    public function setSettingsAttribute($settings)
    {
        $this->attributes['settings'] = serialize($settings);
    }

    /**
     * @param $settings
     * @return mixed
     */
    public function getSettingsAttribute($settings)
    {
        return unserialize($settings);
    }

    /**
     * @param $data
     */
    public function setDynamicRateDataAttribute($data)
    {
        $this->attributes['dynamic_rate_data'] = serialize($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getDynamicRateDataAttribute($data)
    {
        return unserialize($data);
    }

    public function getAmenities($includeGeneral = false)
    {
        $roomAmenities = $this->amenities;

        if ($includeGeneral) {
            $propertyAmenities = $this->property->generalAmenities;

            if ($includeGeneral == 'before') {
                return $propertyAmenities->merge($roomAmenities)->unique();
            } elseif ($includeGeneral == 'after') {
                return $roomAmenities->merge($propertyAmenities)->unique();
            }
        }

        return $roomAmenities;
    }

    public function getBookingUrl($dates = null)
    {
        // does it have an integration?
        $integration = $this->integrations->first();

        if(empty($integration)) return $this->getUrl();

        $type = $integration->type;

        $provider = new $type($integration);

        return $provider->bookingUrl($this, $dates);
    }

    public function nextRoom()
    {
        //if($this->order == $this->property->rooms()->count()) return $this->property->rooms->first();


    }

    public function previousRoom()
    {

    }

    public function relatedTo()
    {
        return $this->belongsToMany('Fastrack\Properties\Room', 'related_rooms', 'room_id', 'related_id')->withPivot('key');
    }

    public function relatedBy()
    {
        return $this->belongsToMany('Fastrack\Properties\Room', 'related_rooms', 'related_id', 'room_id')->withPivot('key');
    }

    public function getRelatedAttribute()
    {
        if ( ! array_key_exists('related', $this->relations)) $this->loadRelated();

        return $this->getRelation('related');
    }

    protected function loadRelated()
    {
        if ( ! array_key_exists('related', $this->relations))
        {
            $related = $this->mergeRelated();

            $this->setRelation('related', $related);
        }
    }

    protected function mergeRelated()
    {
        return $this->relatedTo->merge($this->relatedBy);
    }
}
