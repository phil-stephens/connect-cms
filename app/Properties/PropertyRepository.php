<?php namespace Fastrack\Properties;


use Fastrack\Content\GeoRepositoryTrait;
use Fastrack\Content\Seo;
use Fastrack\Content\WebpageRepositoryTrait;
use Fastrack\Sites\RouteRepository;
use Fastrack\Sites\Site;
use Fastrack\Sites\SiteRepository;
use Fastrack\Slideshows\SlideshowRepositoryTrait;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class PropertyRepository
 * @package Fastrack\Properties
 */
class PropertyRepository extends RouteRepository
{
    use UuidRepositoryTrait, WebpageRepositoryTrait, GeoRepositoryTrait, SlideshowRepositoryTrait;


    protected $repoClass = Property::class;
    protected $getWith = ['content', 'seo'];


    //public function get($uuid)
    //{
    //    return $this->getByUuid($uuid);
    //}

    public function getByUuid($uuid)
    {
        return Property::with('content', 'seo')->whereUuid($uuid)->firstOrFail();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return Property::get();
    }

    /**
     * @param null $siteId
     * @return mixed
     */
    public function getAllDropdown($siteId = null)
    {
        if ( ! empty($siteId)) {
            return Site::findOrFail($siteId)->properties->lists('name', 'id')->all();
        }

        return Property::lists('name', 'id');
    }

    /**
     * @param $propertyId
     * @return mixed
     */
    public function getById($propertyId)
    {
        return Property::with('content', 'seo')->findOrFail($propertyId);
    }

    public function getCheapestRoom($propertyId)
    {
        return Room::wherePropertyId($propertyId)->whereNotNull('dynamic_rate_data')->orderBy('price', 'asc')->first();
    }

    /**
     * @param $propertySlug
     * @param null $siteId
     * @return mixed
     */
    public function getBySlug($propertySlug, $siteId = null)
    {
        return Property::with('content', 'seo')->whereSlug($propertySlug)->firstOrFail();
    }

    /**
     * @param $formData
     * @return static
     */
    public function create($formData)
    {
        $formData = $this->createStubs($formData);

        $property = Property::create([
            'name'      => $formData['name'],
            'slug'      => $formData['slug'],
            'room_noun' => $formData['room_noun']
        ]);

        $this->updateLocation($property, $formData);

        // Content
        $this->createContent($property, $formData);
        //$content = new Content($formData['content']);
        //
        //// Find the image
        //if( ! empty($formData['content']['upload_hash']))
        //{
        //    $image = Image::whereUploadHash($formData['content']['upload_hash'])->latest()->first();
        //
        //    if( ! empty($image)) $content->image_id = $image->id;
        //}
        //
        //$property->content()->save($content);

        // SEO
        $seo = new Seo($formData['seo']);

        $property->seo()->save($seo);

        $this->bootstrapSlideshow($property);

        // Bootstrap the compendium
        $repo = new SiteRepository();

        $repo->bootstrapCompendium($property);

        // Refresh custom routes
        $this->buildSiteRoutes();

        return $property;
    }

    /**
     * @param $propertyId
     * @param $formData
     * @return bool
     */
    public function update($propertyId, $formData)
    {
        $property = $this->getById($propertyId);

        $property->fill($formData);

        $property->save();

        $this->updateLocation($property, $formData);

        // Now do the content...
        //if(isset($formData['content']))
        //{
        //    $property->content->fill($formData['content']);
        //
        //    $property->content->save();
        //}

        if (isset($formData['content'])) {
            $this->updateContent($property, $formData['content']);
        }

        // SEO
        //if(isset($formData['seo']))
        //{
        //    $property->seo->fill($formData['seo']);
        //
        //    $property->seo->save();
        //}

        if (isset($formData['seo'])) {
            $this->updateSeo($property, $formData['seo']);
        }


        // Refresh custom routes
        $this->buildSiteRoutes();

        return true;
    }

    /**
     * @param $propertyId
     */
    public function destroy($propertyId)
    {
        return $this->destroyIt(
            $this->getById($propertyId)
        );
    }

    public function getAvailableAmenities($propertyId)
    {
        $property = $this->getById($propertyId);

        if(configuration('system-wide-features', false))
        {
            $all = Amenity::wherePropertyId(0)->get();

            return $all->diff($property->generalAmenities);
            //return $property->account->amenities->diff($)
        } else
        {
            return $property->amenities->diff($property->generalAmenities);
        }

        //$ignore = $property->generalAmenities->lists('id');
        //
        //return Amenity::wherePropertyId($propertyId)
        //    ->whereNotIn('id', $ignore)
        //    ->get();
    }


    public function updateAmenities($propertyId, $formData)
    {
        if (empty($formData['amenity'])) {
            return;
        }

        $property = $this->getById($propertyId);

        $property->generalAmenities()->detach();

        foreach ($formData['amenity'] as $amenity) {

            $property->generalAmenities()->attach($amenity['id']);

            $property->generalAmenities()->updateExistingPivot($amenity['id'],
                ['order' => $amenity['order'], 'description' => $amenity['description']]);
        }

        return;
    }

    public function removeAmenity($propertyId, $amenityId)
    {
        $property = $this->getById($propertyId);

        $property->generalAmenities()->detach($amenityId);

        return;
    }

    public function bestRate($propertyId)
    {
        $property = $this->getById($propertyId);

        return Room::wherePropertyId($property->id)->whereActive(1)->orderBy('price', 'asc')->first();
    }
}