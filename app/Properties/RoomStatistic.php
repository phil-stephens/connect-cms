<?php namespace Fastrack\Properties;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RoomStatistic
 * @package Fastrack\Properties
 */
class RoomStatistic extends Model
{

    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['key', 'title', 'value', 'order'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo('Fastrack\Properties\Room');
    }
}
