<?php namespace Fastrack\Properties;


use Fastrack\Content\Seo;
use Fastrack\Content\WebpageRepositoryTrait;
use Fastrack\Sites\RouteRepository;
use Fastrack\Slideshows\SlideshowRepositoryTrait;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class RoomRepository
 * @package Fastrack\Properties
 */
class RoomRepository extends RouteRepository
{

    use WebpageRepositoryTrait, SlideshowRepositoryTrait, UuidRepositoryTrait;

    protected $repoClass = Room::class;
    protected $getWith = ['content', 'seo', 'amenities'];

    public function getByUuid($uuid)
    {
        return $this->get($uuid);
        //return Room::with('content', 'seo', 'amenities')
        //    ->whereUuid($uuid)
        //    ->firstOrFail();
    }

    /**
     * @param $roomId
     * @param $propertyId
     * @return mixed
     */
    public function getById($roomId, $propertyId)
    {
        return Room::with('content', 'seo', 'amenities')
            ->wherePropertyId($propertyId)
            ->findOrFail($roomId);
    }

    /**
     * @param $roomSlug
     * @param $propertyId
     * @param null $propertySlug
     * @param null $siteId
     * @return mixed
     */
    public function getBySlug($roomSlug, $propertyId, $propertySlug = null, $siteId = null)
    {
        return Room::with('content', 'seo', 'property', 'amenities')
            ->wherePropertyId($propertyId)
            ->whereSlug($roomSlug)
            ->firstOrFail();
    }

    /**
     * @param $formData
     * @param $propertyId
     */
    public function sortRooms($formData, $propertyId)
    {
        foreach ($formData['data'] as $roomId => $order) {
            if ($order == '') {
                continue;
            }

            $room = $this->getById($roomId, $propertyId);

            $room->order = $order;

            $room->save();
        }
    }

    /**
     * @param $formData
     * @param $propertyId
     * @return Room
     */
    public function create($formData, $propertyId)
    {
        $formData = $this->createStubs($formData);

        $room = new Room([
            'name' => $formData['name'],
            'slug' => $formData['slug'],
        ]);

        if ( ! empty($formData['price'])) {
            $room->price = $formData['price'];
        }

        if ( ! empty($formData['settings'])) {
            $room->settings = $formData['settings'];
        }

        $this->save($room, $propertyId);

        $this->createContent($room, $formData);

        // SEO
        $seo = new Seo($formData['seo']);

        $room->seo()->save($seo);

        // Refresh custom routes
        $this->buildSiteRoutes();

        $this->bootstrapSlideshow($room);

        return $room;
    }

    /**
     * @param Room $room
     * @param $propertyId
     * @return mixed
     */
    public function save(Room $room, $propertyId)
    {
        $property = Property::findOrFail($propertyId);

        $order = \DB::table('rooms')->wherePropertyId($propertyId)->max('order');

        $room->order = $order + 1;

        return $property->rooms()
            ->save($room);
    }

    /**
     * @param $roomId
     * @param $formData
     * @param $propertyId
     * @return bool
     */
    public function update($roomId, $formData, $propertyId)
    {
        $room = $this->getById($roomId, $propertyId);

        $room->fill($formData);

        $room->save();

        if (isset($formData['stats'])) {
            $this->updateStats($room, $formData['stats']);
        }

        // Now do the content...
        if (isset($formData['content'])) {
            $this->updateContent($room, $formData['content']);
        }

        // SEO
        if (isset($formData['seo'])) {
            $this->updateSeo($room, $formData['seo']);
        }

        // Refresh custom routes
        $this->buildSiteRoutes();


        return true;
    }

    /**
     * @param $room
     * @param $formData
     */
    public function updateStats($room, $formData)
    {
        // Just detach all then reattach those that have a value set
        $room->statistics()->detach();

        foreach ($formData as $key => $stat) {
            if( ! empty(trim($stat['value'])))
            {
                $room->statistics()->attach($key);
                $room->statistics()->updateExistingPivot($key, $stat);
            }


        }
    }

    //public function updateStats($room, $formData)
    //{
    //    foreach ($formData as $stat) {
    //        if (isset($stat['delete'])) {
    //            if (isset($stat['id'])) {
    //                RoomStatistic::find($stat['id'])->delete();
    //            }
    //        } else {
    //            if (isset($stat['id'])) {
    //                // Update
    //                $s = RoomStatistic::find($stat['id']);
    //
    //                $s->fill($stat);
    //
    //                $s->save();
    //            } else {
    //                // Create
    //                $s = new RoomStatistic($stat);
    //
    //                $room->stats()->save($s);
    //            }
    //        }
    //    }
    //}

    public function getAvailableAmenities($roomId, $propertyId)
    {
        $room = $this->getById($roomId, $propertyId);

        if(env('HAS_ACCOUNT_AMENITIES', false))
        {
            $all = Amenity::wherePropertyId(0)->get();

            return $all->diff($room->amenities);
            //return $property->account->amenities->diff($)
        } else
        {
            return $room->property->amenities->diff($room->amenities);
        }

    }

    public function updateAmenities($roomId, $formData, $propertyId)
    {
        if (empty($formData['amenity'])) {
            return;
        }

        $room = $this->getById($roomId, $propertyId);

        $room->amenities()->detach();

        foreach ($formData['amenity'] as $amenity) {
            $room->amenities()->attach($amenity['id']);
            $room->amenities()->updateExistingPivot($amenity['id'],
                ['order' => $amenity['order'], 'description' => $amenity['description']]);


        }

        return;
    }

    public function removeAmenity($roomId, $amenityId, $propertyId)
    {
        $room = $this->getById($roomId, $propertyId);

        $room->amenities()->detach($amenityId);

        return;
    }

    /**
     * @param $bookingButtonId
     * @param $price
     */
    public function updateDynamicRate($bookingButtonId, $price, $dynamicData = null)
    {
        $room = Room::where('bookingbutton_id', '=', $bookingButtonId)->first();

        if ( ! empty($room)) {
            $room->price = $price;
            $room->dynamic_rate_data = $dynamicData;

            $room->save();
        }

        return;
    }

    public function getUnrelatedRooms($room)
    {
        if(is_string($room))
        {
            $room = $this->get($room);
        }

        $ignore = array_merge([$room->id], $room->related->lists('id')->all());

        return $room->property->rooms->except($ignore);
    }

    public function updateRelated($roomUuid, $formData)
    {
        $room = $this->get($roomUuid);

        // clear relations
        $room->relatedTo()->detach();
        $room->relatedBy()->detach();

        foreach($formData['room'] as $relate)
        {
            $room->relatedTo()->attach($relate['id']);

            $room->relatedTo()->updateExistingPivot($relate['id'], ['key' => $relate['key']]);
        }
    }

    public function unrelateRoom($roomUuid, $relatedId)
    {
        $room = $this->get($roomUuid);

        $room->relatedTo()->detach($relatedId);
        $room->relatedBy()->detach($relatedId);

        return $relatedId;
    }

    /**
     * @param $roomId
     * @param $propertyId
     */
    public function destroy($roomId, $propertyId)
    {
        return $this->destroyIt(
            $this->getById($roomId, $propertyId)
        );
    }
}