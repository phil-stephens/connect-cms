<?php namespace Fastrack\Properties;

use Crypt;
use Fastrack\Content\CanonicalTrait;
use Fastrack\Content\GeoTrait;
use Fastrack\Content\SeoTrait;
use Fastrack\Content\URLTrait;
use Fastrack\Content\WebpageTrait;
use Fastrack\Integrations\IntegratableTrait;
use Fastrack\Sites\SettingTrait;
use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Laracasts\Presenter\PresentableTrait;
//use Fastrack\Versions\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Property
 * @package Fastrack\Properties
 */
class Property extends Model
{

    use HasUuid, Versionable, WebpageTrait, PresentableTrait, GeoTrait, SeoTrait, URLTrait, SettingTrait, TaggableTrait, IntegratableTrait, CanonicalTrait;

    /**
     * @var string
     */
    protected $presenter = 'Fastrack\Presenters\PropertyPresenter';

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'room_noun', 'bookingbutton_channel', 'phone', 'email', 'star_rating'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sites()
    {
        return $this->belongsToMany('Fastrack\Sites\Site');
    }

    public function statistics()
    {
        return $this->hasMany('Fastrack\Properties\Statistic')->orderby('order');
    }

    /**
     * @return mixed
     */
    public function rooms()
    {
        return $this->hasMany('Fastrack\Properties\Room')->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function amenities()
    {
        return $this->hasMany('Fastrack\Properties\Amenity');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function generalAmenities()
    {
        return $this->belongsToMany('Fastrack\Properties\Amenity')->withPivot('order', 'description')->orderBy('order');
    }

    // Just a helper function/alias
    public function getAmenities()
    {
        return $this->generalAmenities;
    }

    /**
     * @return mixed
     */
    public function offers()
    {
        return $this->hasMany('Fastrack\Offers\Offer')->orderBy('order');
    }

    /**
     * @return mixed
     */
    public function weatherForecasts()
    {
        return $this->hasMany('Fastrack\Weather\WeatherForecast')->orderBy('date');
    }

    public function calendars()
    {
        return $this->morphToMany('Fastrack\Calendars\Calendar', 'calendarable');
    }

    public function compendium()
    {
        return $this->hasOne('Fastrack\Sites\Site', 'compendium_property_id');
    }

    public function reviews()
    {
        return $this->hasMany('Fastrack\Reviews\Review')->latest();
    }

    public function publishedReviews()
    {
        return $this->hasMany('Fastrack\Reviews\Review')->wherePublished(true)->latest();
    }

    public function featureGroups()
    {
        return $this->hasMany('Fastrack\Features\Group');
    }

    // ensure it is always stored as lowercase for consistency
    /**
     * @param $noun
     */
    public function setRoomNounAttribute($noun)
    {
        $this->attributes['room_noun'] = strtolower($noun);
    }

    public function getBookingUrl($dates = null)
    {
        // does it have an integration?
        $integration = $this->integrations->first();

        if(empty($integration)) return $this->getUrl();

        $type = $integration->type;

        $provider = new $type($integration);

        return $provider->bookingUrl($this, $dates);
    }
}
