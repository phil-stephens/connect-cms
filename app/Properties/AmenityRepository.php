<?php
/**
 * Created by PhpStorm.
 * User: fastrack
 * Date: 11/06/15
 * Time: 2:31 PM
 */

namespace Fastrack\Properties;

use Fastrack\Images\Image;
use Fastrack\Meta\UuidRepositoryTrait;


/**
 * Class AmenityRepository
 * @package Fastrack\Properties
 */
class AmenityRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Amenity::class;

    //public function get($uuid)
    //{
    //    return Amenity::whereUuid($uuid)->firstOrFail();
    //}

    /**
     * @param $propertyId
     * @return mixed
     */
    public function getForProperty($propertyId)
    {
        return Property::findOrFail($propertyId)->amenities;
    }

    /**
     * @param $roomId
     * @return mixed
     */
    public function getForRoom($roomId)
    {
        return Room::findOrFail($roomId)->amenities;
    }

    public function getAccount()
    {
        return Amenity::wherePropertyId(0)->get();
    }

    /**
     * @param $formData
     * @param $propertyId
     * @param null $roomId
     */
    public function create($formData, $propertyId = null, $roomId = null)
    {
        $amenity = Amenity::generate($formData);

        if( ! empty($propertyId))
        {
            $this->save($amenity, $propertyId);

            if ( ! empty($roomId)) {
                $this->assignToRoom($amenity, $roomId);
            }
        } else
        {
            $amenity->save();
        }

        $this->assignIcon($amenity, $formData);

    }

    private function assignIcon(Amenity $amenity, $formData)
    {
        if( ! empty($formData['icon']))
        {
            // Find the image
            try
            {
                $image = Image::whereUuid($formData['icon'])->firstOrFail();

                $amenity->image_id = $image->id;
                $amenity->save();
            } catch( \Exception $e)
            {

            }
        }

        return;
    }

    /**
     * @param Amenity $amenity
     * @param $propertyId
     * @return mixed
     */
    public function save(Amenity $amenity, $propertyId)
    {
        return Property::findOrFail($propertyId)
            ->amenities()
            ->save($amenity);
    }

    /**
     * @param Amenity $amenity
     * @param $roomId
     * @return mixed
     */
    public function assignToRoom(Amenity $amenity, $roomId)
    {
        return Room::findOrFail($roomId)
            ->amenities()
            ->attach($amenity->id);
    }

    /**
     * @param $amenityId
     * @param $formData
     * @return bool
     */
    public function update($amenityId, $formData)
    {
        $amenity = $this->getById($amenityId);

        $amenity->fill($formData);

        $amenity->save();

        $this->assignIcon($amenity, $formData);

        return true;
    }

    public function updateByUuid($uuid, $formData)
    {
        $amenity = $this->get($uuid);

        $amenity->fill($formData);

        $amenity->save();

        $this->assignIcon($amenity, $formData);

        return true;
    }

    /**
     * @param $amenityId
     * @return mixed
     */
    public function getById($amenityId)
    {
        return Amenity::findOrFail($amenityId);
    }

    /**
     * @param $amenityId
     */
    public function destroy($amenityId)
    {
        $amenity = $this->getById($amenityId);

        $amenity->delete();

        return;
    }

    public function destroyByUuid($amenityId)
    {
        $amenity = $this->get($amenityId);

        $amenity->delete();

        return;
    }
}