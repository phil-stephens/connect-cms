<?php namespace Fastrack\Properties;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Amenity
 * @package Fastrack\Properties
 */
class Amenity extends Model
{
    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['name', 'class'];

    /**
     * @param $fields
     * @return static
     */
    public static function generate($fields)
    {
        return new static($fields);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo('Fastrack\Properties\Property');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rooms()
    {
        return $this->belongsToMany('Fastrack\Properties\Room');
    }

    public function groups()
    {
        return $this->belongsToMany('Fastrack\Features\Group');
    }

    public function image()
    {
        return $this->belongsTo('Fastrack\Images\Image');
    }

    public function getImagePath($parameters = [])
    {
        if ($this->image) {
            return $this->image->getPath($parameters);
        }

        return false;
    }

    public function getBackgroundImage($parameters = [])
    {
        if ($this->image) {
            return 'style="background-image: url(' . $this->image->getPath($parameters) . ');"';
        }

        return null;
    }

    public function getImageTag($urlParams = [], $tagParams = [])
    {
        if ($this->image) {
            return $this->image->getTag($urlParams, $tagParams);
        }

        return false;
    }

    public function getImageUuid()
    {
        if ($this->image) {
            return $this->image->uuid;
        }

        return false;
    }
}
