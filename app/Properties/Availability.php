<?php

namespace Fastrack\Properties;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    protected $fillable = ['date', 'available', 'rate'];

    public function room()
    {
        return $this->belongsTo('Fastrack\Properties\Room');
    }

    public function getDates()
    {
        return ['created_at', 'updated_at', 'date'];
    }
}
