<?php

namespace Fastrack\Properties;


use Fastrack\Images\Image;
use Fastrack\Meta\UuidRepositoryTrait;

class StatisticRepository
{

    use UuidRepositoryTrait;

    protected $repoClass = Statistic::class;

    public function create($formData, $propertyUuid)
    {
        $statistic = new Statistic($formData);

        $this->save($statistic, $propertyUuid);

        $this->assignIcon($statistic, $formData);

    }

    public function save(Statistic $statistic, $propertyUuid)
    {
        return Property::whereUuid($propertyUuid)->firstOrFail()
            ->statistics()
            ->save($statistic);
    }

    public function update($uuid, $formData)
    {
        $statistic = $this->get($uuid);

        $statistic->fill($formData);

        $statistic->save();

        $this->assignIcon($statistic, $formData);

        return true;
    }

    private function assignIcon(Statistic $statistic, $formData)
    {
        if(empty($formData['icon']))
        {
            $statistic->image_id = 0;
            $statistic->save();
        } else
        {
            // Find the image
            try
            {
                $image = Image::whereUuid($formData['icon'])->firstOrFail();

                $statistic->image_id = $image->id;
                $statistic->save();
            } catch( \Exception $e)
            {
                $statistic->image_id = 0;
                $statistic->save();
            }
        }

        return;
    }

    public function destroy($uuid)
    {
        $statistic = $this->get($uuid);

        //$property = $statistic->property;

        $statistic->delete();

        //return $property;

        return;
    }
}