<?php

namespace Fastrack\Properties;


use Fastrack\Content\WebpageRepositoryTrait;
use Fastrack\Slideshows\SlideshowRepositoryTrait;
use Fastrack\Meta\UuidRepositoryTrait;

class SubTypeRepository
{

    use WebpageRepositoryTrait, SlideshowRepositoryTrait, UuidRepositoryTrait;

    protected $repoClass = RoomSubType::class;

    //public function get($uuid)
    //{
    //    return RoomSubType::whereUuid($uuid)->firstOrFail();
    //
    //}

    public function sortTypes($formData)
    {
        foreach ($formData['data'] as $order => $typeId) {
            if ($order === '') {
                continue;
            }

            $type = $this->get($typeId);

            $type->order = $order;

            $type->save();
        }
    }

    public function save(RoomSubType $type, $roomId)
    {
        return Room::whereUuid($roomId)
            ->firstOrFail()
            ->subTypes()
            ->save($type);

    }

    public function create($formData, $roomId)
    {
        $formData = $this->createStubs($formData);

        $type = new RoomSubType();

        $type->fill($formData);

        $this->save($type, $roomId);

        $this->createContent($type, $formData);

        $this->createSeo($type, $formData);

        $this->bootstrapSlideshow($type);

        return $type;
    }

    public function update($typeId, $formData)
    {
        $type = $this->get($typeId);

        $type->fill($formData);

        $type->save();

        // Now do the content...
        if (isset($formData['content'])) {
            $this->updateContent($type, $formData['content']);
        }

        // Now do the SEO..
        if (isset($formData['seo'])) {
            $this->updateSeo($type, $formData['seo']);
        }

        return true;
    }

    public function destroy($subTypeId)
    {
        return $this->destroyIt(
            $this->get($subTypeId)
        );
    }
}