<?php

namespace Fastrack\Properties;

use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    use HasUuid;

    protected $fillable = ['title', 'key', 'order', 'image_id', 'custom'];

    public function property()
    {
        return $this->belongsTo('Fastrack\Properties\Property');
    }

    public function rooms()
    {
        return $this->belongsToMany('Fastrack\Properties\Room')->withPivot('value', 'description');
    }

    public function image()
    {
        return $this->belongsTo('Fastrack\Images\Image');
    }

    // Alias for legacy implementations
    public function getValueAttribute($str)
    {
        return $this->pivot->value;
    }

    public function getDescriptionAttribute($str)
    {
        return $this->pivot->description;
    }

    public function getImagePath($parameters = [])
    {
        if ($this->image) {
            return $this->image->getPath($parameters);
        }

        return false;
    }

    public function getImageUuid()
    {
        if ($this->image) {
            return $this->image->uuid;
        }

        return false;
    }

    public function getBackgroundAttribute($parameters = [])
    {
        if ($this->image) {
            return $this->image->getBackgroundAttribute($parameters);
        }

        return null;
    }

    /**
     * @param array $urlParams
     * @param array $tagParams
     * @return bool
     */
    public function getImageTag($urlParams = [], $tagParams = [])
    {
        if ($this->image) {
            return $this->image->getTag($urlParams, $tagParams);
        }

        return false;
    }
}
