<?php

namespace Fastrack\Properties;

use Fastrack\Content\SeoTrait;
use Fastrack\Content\URLTrait;
use Fastrack\Content\WebpageTrait;
//use Fastrack\Uuid\Model;
use Fastrack\Sites\SettingTrait;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

class RoomSubType extends Model
{
    use HasUuid, Versionable, WebpageTrait, SeoTrait, URLTrait, SettingTrait;

    protected $fillable = ['name', 'slug'];

    public function room()
    {
        return $this->belongsTo('Fastrack\Properties\Room');
    }

    public function getCanonicalSiteAttribute()
    {
        if ( ! array_key_exists('canonical_site', $this->relations))
        {
            $site = $this->room->property->canonical_site;

            $this->setRelation('canonical_site', $site);
        }

        return $this->getRelation('canonical_site');
    }

}
