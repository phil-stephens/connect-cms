<?php

if ( ! function_exists('distanceBetween')) {

    function distanceBetween($location1, $location2) {

        $lon1 = $location1->longitude;
        $lon2 = $location2->longitude;


        $lat1 = $location1->latitude;
        $lat2 = $location2->latitude;

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        $distance = $miles * 1.609344;

        if($distance < 1)
        {
            $distance = round($distance * 1000);

            return $distance . 'm';
        }

        return round($distance, 1) . 'km';

    }
}

if ( ! function_exists('venues')) {

    function venues($categorySlug = null)
    {
        return places($categorySlug);
    }
}

if ( ! function_exists('places')) {

    function places($categorySlug = null, $siteId = null)
    {
        if (empty($siteId)) {
            $siteId = site('id');
        }

        $repo = new \Fastrack\Locations\VenueRepository();

        return $repo->getByCategory($categorySlug, $siteId);
    }
}