<?php

if ( ! function_exists('configuration')) {
    function configuration($key, $default = false)
    {
        try
        {
            $config = \Fastrack\Configurations\Configuration::whereKey($key)->firstOrFail();

            $value = $config->value;
        } catch( \Exception $e)
        {
            $value = $default;
        }

        return $value;

    }
}