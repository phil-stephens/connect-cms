<?php

if ( ! function_exists('contactFormTarget')) {

    function contactFormTarget($email)
    {
        $fields = Form::hidden('target', base64_encode($email));
        $fields .= Form::hidden(md5($email), time());

        $fields .= '<div class="form-group hidden">';
        $fields .= Form::label('uri', 'Leave this empty:');
        $fields .= Form::text('uri', null, ['class' => 'form-control']);
        $fields .= '</div>';

        return $fields;
    }
}

if ( ! function_exists('mimeIconClass')) {

    function mimeIconClass($mimeType)
    {
        switch ($mimeType) {
            case 'application/pdf':
                return 'fa-file-pdf-o';
                break;

            case 'image/jpeg':
            case 'image/png':
            case 'image/gif':
            case 'image/vnd.adobe.photoshop':
                return 'fa-file-image-o';
                break;

            default:
                return 'fa-file-o';
                break;
        }
    }
}

if ( ! function_exists('byte_format')) {

    function byte_format($num, $precision = 1)
    {
        if ($num >= 1000000000000) {
            $num = round($num / 1099511627776, $precision);
            $unit = 'TB';
        } elseif ($num >= 1000000000) {
            $num = round($num / 1073741824, $precision);
            $unit = 'GB';
        } elseif ($num >= 1000000) {
            $num = round($num / 1048576, $precision);
            $unit = 'MB';
        } elseif ($num >= 1000) {
            $num = round($num / 1024, $precision);
            $unit = 'KB';
        } else {
            $unit = 'B';

            return number_format($num) . ' ' . $unit;
        }

        return number_format($num, $precision) . ' ' . $unit;
    }
}

if ( ! function_exists('isSame')) {
    function isSame($objectOne, $objectTwo)
    {
        return ($objectOne->id == $objectTwo->id && get_class($objectOne) == get_class($objectTwo));
    }
}

if ( ! function_exists('alternator')) {
    /**
     * Alternator
     *
     * Allows strings to be alternated. See docs...
     *
     * @param    string (as many parameters as needed)
     * @return    string
     */
    function alternator($args)
    {
        static $i;
        if (func_num_args() === 0) {
            $i = 0;

            return '';
        }
        $args = func_get_args();

        return $args[($i ++ % count($args))];
    }
}


if ( ! function_exists('prepUrl'))
{
    function prepUrl($str = '')
    {
        if ($str == 'http://' OR $str == '')
        {
            return '';
        }

        if (substr($str, 0, 7) != 'http://' && substr($str, 0, 8) != 'https://')
        {
            $str = 'http://' . $str;
        }

        return $str;
    }
}

if ( ! function_exists('safeMailto'))
{
    function safeMailto($email, $title = '', $attributes = '')
    {
        $title = (string) $title;

        if ($title == "")
        {
            $title = $email;
        }

        for ($i = 0; $i < 16; $i++)
        {
            $x[] = substr('<a href="mailto:', $i, 1);
        }

        for ($i = 0; $i < strlen($email); $i++)
        {
            $x[] = "|" . ord(substr($email, $i, 1));
        }

        $x[] = '"';

        if ($attributes != '')
        {
            if (is_array($attributes))
            {
                foreach ($attributes as $key => $val)
                {
                    $x[] =  ' ' . $key . '="';
                    for ($i = 0; $i < strlen($val); $i++)
                    {
                        $x[] = "|" . ord(substr($val, $i, 1));
                    }
                    $x[] = '"';
                }
            }
            else
            {
                for ($i = 0; $i < strlen($attributes); $i++)
                {
                    $x[] = substr($attributes, $i, 1);
                }
            }
        }

        $x[] = '>';

        $temp = array();
        for ($i = 0; $i < strlen($title); $i++)
        {
            $ordinal = ord($title[$i]);

            if ($ordinal < 128)
            {
                $x[] = "|" . $ordinal;
            }
            else
            {
                if (count($temp) == 0)
                {
                    $count = ($ordinal < 224) ? 2 : 3;
                }

                $temp[] = $ordinal;
                if (count($temp) == $count)
                {
                    $number = ($count == 3) ? (($temp['0'] % 16) * 4096) + (($temp['1'] % 64) * 64) + ($temp['2'] % 64) : (($temp['0'] % 32) * 64) + ($temp['1'] % 64);
                    $x[] = "|" . $number;
                    $count = 1;
                    $temp = array();
                }
            }
        }

        $x[] = '<'; $x[] = '/'; $x[] = 'a'; $x[] = '>';

        $x = array_reverse($x);
        ob_start();

        ?><script type="text/javascript">
        //<![CDATA[
        var l=new Array();
        <?php
        $i = 0;
        foreach ($x as $val){ ?>l[<?php echo $i++; ?>]='<?php echo $val; ?>';<?php } ?>

        for (var i = l.length-1; i >= 0; i=i-1){
            if (l[i].substring(0, 1) == '|') document.write("&#"+unescape(l[i].substring(1))+";");
            else document.write(unescape(l[i]));}
        //]]>
    </script><?php

        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }
}