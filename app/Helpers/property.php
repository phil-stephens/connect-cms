<?php

if ( ! function_exists('properties')) {

    /**
     * @return mixed|object
     */
    function properties()
    {
        return site()->properties;
    }
}


if ( ! function_exists('offers')) {

    function offers($siteId = null)
    {
        if (empty($siteId)) {
            $siteId = site('id');
        }

        $repo = new \Fastrack\Offers\OfferRepository();

        return $repo->getAllForSite($siteId);
    }
}

if ( ! function_exists('propertyOffers')) {

    function propertyOffers($propertyId = null)
    {
        $repo = new \Fastrack\Offers\OfferRepository();

        if (empty($propertyId)) {
            $propertyId = getProperty()->id;
        }

        return $repo->getAllForProperty($propertyId);
    }
}

if ( ! function_exists('getProperty')) {

    /**
     * @param null $slug
     * @return mixed
     */
    function getProperty($slug = null)
    {
        $repo = new \Fastrack\Properties\PropertyRepository();

        if ( ! empty($slug)) {
            return $repo->getBySlug($slug, site('id'));
        }

        return site()->properties->first();

    }
}

if ( ! function_exists('rateDate')) {

    function rateDate($room, $format = 'l j F Y')
    {
        if( ! is_array($room->dynamic_rate_data) && ! is_object($room->dynamic_rate_data))
        {
            $data = unserialize($room->dynamic_rate_data);
        } else
        {
            $data = $room->dynamic_rate_data;
        }

        if(is_array($data))
        {
            return $data['date']->format($format);
        } elseif(is_object($data))
        {
            return date($format, strtotime($data->date));
        }

    }
}

if ( ! function_exists('propertyRate'))
{
    function propertyRate($propertyId)
    {
        $repo = new \Fastrack\Properties\PropertyRepository();

        return $repo->bestRate($propertyId);
    }
}