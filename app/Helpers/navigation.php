<?php

if ( ! function_exists('getNavigation')) {

    /**
     * @param $slug
     * @return bool|mixed
     */
    function getNavigation($slug)
    {
        $repo = new \Fastrack\Navigations\NavigationRepository();

        try {
            return $repo->getBySlug($slug, site('id'));
        } catch (\Exception $e) {
            return false;
        }
    }
}

if ( ! function_exists('nextItemUrl')) {
    function nextItemUrl($navigationSlug, $model, $infinite = true)
    {
        return getNavigation($navigationSlug)->nextItem($model, $infinite)->link->getUrl();
    }
}

if ( ! function_exists('previousItemUrl')) {
    function previousItemUrl($navigationSlug, $model, $infinite = true)
    {
        return getNavigation($navigationSlug)->previousItem($model, $infinite)->link->getUrl();
    }
}