<?php

if ( ! function_exists('getABcampaign')) {

    function getABcampaign($id)
    {
        $repo = new \Fastrack\Campaigns\CampaignRepository();

        try
        {
            // Get the campaign (check for validity)
            $campaign = $repo->get($id);



            if($campaign->start_at->lt(\Carbon\Carbon::now()) && $campaign->finish_at->gt(\Carbon\Carbon::now()))
            {
                // If campaign found, first check for the cookie relating to it
                if($cookie = Cookie::get('campaign_' . $campaign->uuid))
                {
                    // If cookie is found, load up the content for the require variation
                    //return $cookie;
                }

                // If no cookie, get the next available variation, increment the visit count, set the cookie and return the content
                $variation = $campaign->variations->first();

                $variation->visits++;

                $variation->save();

                //return $variation->payload;

                $generated = \Blade::compileString($variation->payload);
                //dd($generated);
                //Cookie::queue(Cookie::make('campaign_' . $campaign->uuid, $generated));

                return $generated;
            }

        } catch( \Exception $e)
        {

        }

        // If no campaign, just return false and output the default content
        return false;
    }
}
