<?php

if ( ! function_exists('cropToCSS')) {
    /**
     * @param null $cropString
     * @return string
     */
    function cropToCss($cropString = null)
    {
        switch ($cropString) {
            case 'top,left':
                return 'left top';
                break;

            case 'top':
                return '50% top';
                break;

            case 'top,right':
                return 'right top';
                break;

            case 'left':
                return 'left 50%';
                break;

            case 'right':
                return 'right 50%';
                break;

            case 'bottom,left':
                return 'left bottom';
                break;

            case 'bottom':
                return '50% bottom';
                break;

            case 'bottom,right':
                return 'right bottom';
                break;

            default:
                return '50%';
                break;
        }
    }
}