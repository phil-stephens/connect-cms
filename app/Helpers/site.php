<?php

if ( ! function_exists('fontawesome')) {

    function fontawesome($version = '4.5.0')
    {
        return '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/' . $version . '/css/font-awesome.min.css">';
    }
}

if ( ! function_exists('theme')) {

    /**
     * @param $template
     * @param array $data
     * @return \Illuminate\View\View
     */
    function theme($template, $data = [])
    {
        // if that view does exist in the theme, use it
        if (view()->exists($template)) {
            return view($template, $data);

            // if not, check to see if it is available in the default theme
        } elseif (view()->exists('default.' . $template)) {
            return view('default.' . $template, $data);
        }

        // otherwise use the standard page template from either the theme or core
        $template = (view()->exists('page')) ? 'page' : 'default.page';

        return view($template, $data);
    }

}

if ( ! function_exists('site')) {

    /**
     * @param null $key
     * @return \Illuminate\Foundation\Application|mixed|object
     */
    function site($key = null)
    {
        $site = app('site');

        if ( ! is_null($key)) {
            return $site->$key;
        }

        return $site;
    }
}

if ( ! function_exists('settings')) {

    /**
     * @param null $key
     * @param bool|false $default
     * @return bool|\Illuminate\Foundation\Application|mixed|object
     */
    function settings($key = null, $default = false)
    {
        $settings = site('settings');

        if ( ! is_null($key)) {
            return isset($settings[$key]) ? $settings[$key] : $default;
        }

        return $settings;
    }
}

if ( ! function_exists('setting')) {
    function setting($key, $default = false)
    {
        return site()->setting($key, $default);
    }
}

if ( ! function_exists('theme_folder')) {

    /**
     * @return string
     */
    function theme_folder($path = '')
    {
        if ( ! empty($path)) {
            $path = '/' . ltrim($path, '/');
        }

        $asset = env('THEME_LOCATION', '/themes') . '/' . \Request::get('theme', site('theme')) . $path;

        if ( ! env('BYPASS_CDN', true)) {
            return \Cdn::asset($asset);
        }

        return $asset;
    }
}

if ( ! function_exists('core_asset')) {

    function core_asset($path = '')
    {
        if ( ! empty($path)) {
            $path = '/core/' . ltrim($path, '/');
        }

        if ( ! env('BYPASS_CDN', true)) {
            return \Cdn::asset($path);
        }

        return $path;
    }
}

if ( ! function_exists('getFeed')) {

    /**
     * @param null $slug
     * @param null $limit
     * @return bool|mixed
     */
    function getFeed($slug = null, $limit = null)
    {
        $repo = new \Fastrack\News\FeedRepository();

        try {
            return $repo->getFeedArticles($slug, site('id'), $limit);
        } catch (\Exception $e) {
            return false;
        }
    }
}

if ( ! function_exists('getPage')) {

    /**
     * @param $slug
     * @return bool|mixed
     */
    function getPage($slug)
    {
        $repo = new \Fastrack\Pages\PageRepository();

        try {
            return $repo->getBySlug($slug);
        } catch (\Exception $e) {
            return false;
        }
    }
}

if ( ! function_exists('getGallery')) {

    /**
     * @param $slug
     * @return bool|mixed
     */
    function getGallery($slug)
    {
        $repo = new \Fastrack\Galleries\GalleryRepository();

        try {
            return $repo->getBySlug($slug);
        } catch (\Exception $e) {
            return false;
        }
    }
}

if ( ! function_exists('getEvent')) {

    function getEvent($slug, $calendarSlug)
    {
        $repo = new \Fastrack\Calendars\EventRepository();

        return $repo->getBySlug($slug, $calendarSlug);
    }
}

if ( ! function_exists('inject_js')) {

    /**
     * @param $content
     * @param string $position
     * @return string
     */
    function inject_js($content, $position = 'head')
    {
        // Site stuff always gets added...
        $script = '';

        if($position == 'head' && site('wix_redirects') && isSame(site()->homepage, $content))
        {
            $script .= hashbang_redirect();
        }

        if ($js = $content->getSeo('javascript_' . $position)) {
            $script .= $js;
        }

        $site = site()->getSeo('javascript_' . $position);

        if($script != $site) $script .= "\n"  . $site;

        return $script;
    }
}

if ( ! function_exists('hashbang_redirect')) {

    function hashbang_redirect()
    {
        $aliases = site()->aliases;

        $script = '<script>var redirects = {';

        foreach($aliases as $index => $alias)
        {
            if($index) $script .= ',';
            $script .= "'{$alias->from}':'{$alias->to}'";
        }

        $script .= '}; if(window.location.hash != \'\' && redirects.hasOwnProperty(window.location.hash)) { window.location.replace(redirects[window.location.hash]); }</script>';

        return $script;
    }
}

if ( ! function_exists('getWeather')) {
    /**
     * @param null $propertySlug
     * @return array
     */
    function getWeather($propertySlug = null)
    {
        $repo = new \Fastrack\Weather\WeatherRepository();

        $property = getProperty($propertySlug);

        return [
            'today'    => $repo->getToday($property->id),
            'forecast' => $repo->getUpcoming($property->id)
        ];
    }
}

if ( ! function_exists('core_icons')) {

    /**
     * @param array $sets
     * @return string
     */
    function core_icons($sets = [])
    {
        $css = [];

        foreach ($sets as $set) {
            //$css[] = '<link rel="stylesheet" href="/core/css/icon-ftg-' . $set . '.css"/>';
            $css[] = '<link rel="stylesheet" href="' . core_asset('css/icon-ftg-' . $set . '.css') . '"/>';
        }

        return implode("\n", $css);
    }
}

if ( ! function_exists('render_seo_title')) {
    /**
     * @param $content
     * @return string|void
     */
    function render_seo_title($content)
    {
        //$title = str_replace('{title}', $content->getTitle(), $content->getSeo('title'));

        //return $title;

        //{page_title}
        //{page_name}
        //{category}
        //{site_name}
        //{separator}

        // if there are separators, explode on the separator - allows us to clear out any empty cells

        $template = $content->getSeo('title');

        if (strpos($template, '%%separator%%') !== false) {
            $elements = explode('%%separator%%', $template);
        } else {
            $elements[] = $template;
        }

        foreach ($elements as $key => $element) {
            $str = $element;
            $str = str_replace('%%page_title%%', $content->getTitle(), $str);
            $str = str_replace('%%page_name%%', $content->name, $str);
            $str = str_replace('%%site_name%%', site('name'), $str);

            if (strpos($str, '%%category%%') !== false) {
                switch (class_basename($content)) {
                    case 'Room':
                        $replace = 'Rooms';
                        break;

                    case 'Venue':
                        $replace = 'Location';
                        break;

                    case 'CalendarEvent':
                        $replace = 'What\'s On';
                        break;

                    default:
                        $replace = '';
                        break;
                }

                $str = str_replace('%%category%%', $replace, $str);
            }

            if (trim($str) == '') {
                unset($elements[$key]);
            } else {
                $elements[$key] = trim($str);
            }
        }

        if (count($elements)) {
            // implode on the separator
            return implode(' - ', $elements);
        }

        return;
    }
}
