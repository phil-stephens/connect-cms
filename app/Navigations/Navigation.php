<?php namespace Fastrack\Navigations;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Carbon\Carbon;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Navigation
 * @package Fastrack\Navigations
 */
class Navigation extends Model
{

    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo('Fastrack\Sites\Site');
    }

    /**
     * @return mixed
     */
    public function items()
    {
        return $this->hasMany('Fastrack\Navigations\NavigationItem')->orderBy('order');
    }

    public function filteredItems()
    {

        $now = Carbon::now();

        $items = $this->items->filter(function ($item) use ($now) {

            switch(class_basename($item->link))
            {
                case 'Offer':
                    if($item->link->active &&
                    $item->link->start_at->lt($now) &&
                    $item->link->finish_at->gt($now))
                        return true;

                    return false;

                    break;

                case 'CalendarEvent':
                    if($item->link->finish_at->gt($now)) return true;

                    return false;
                    break;

                case 'Article':
                    if( ! $item->link->draft && $item->link->published_at->lt($now)) return true;

                    return false;
                    break;

                default:
                    return true;
                    break;
            }

            return true;

        });

        return $items;
    }


    public function indexify($sortUsing = 'label', $filtered = true)
    {
        $seed = 'abcdefghijklmnopqrstuvwxyz#';

        $index = [];

        for($i = 0; $i < strlen($seed); $i++)
        {
            $index[$seed[$i]] = [];
        }

        $source = ($filtered) ? $this->filteredItems() : $this->items;

        foreach($source as $item)
        {
            switch($sortUsing)
            {
                case 'label':
                    $item->sortString = $item->label;
                    break;

                case 'title':
                    $item->sortString = $item->link->getTitle();
                    break;

                case 'name':
                    $item->sortString = $item->link->name;
                    break;
            }

            $firstCharacter = strtolower($item->sortString[0]);

            //$item->sortString = $item->label;

            if(is_numeric($firstCharacter))
            {
                $index['#'][] = $item;
            } else
            {
                $index[$firstCharacter][] = $item;
            }
        }

        foreach($index as $key => $items)
        {
            $i = collect($items);

            $sorted = $i->sortBy('sortString');
            $index[$key] = $sorted;
        }

        //return $index;

        $collection = collect($index);

        return $collection;
    }

    public function nextItem($model, $infinite = true)
    {
        // find the item...
        $item = $this->getItemByModel($model);

        if(($item->order + 1) == $this->items()->count())
        {
            if($infinite)
            {
                return $this->items->first();
            } else
            {
                return null;
            }
        } else
        {
            $order = $item->order + 1;
        }

        return NavigationItem::whereNavigationId($this->id)
            ->whereOrder($order)
            ->firstOrFail();
    }

    public function previousItem($model, $infinite = true)
    {
        // find the item...
        $item = $this->getItemByModel($model);

        if($item->order == 0)
        {
            if($infinite)
            {
                return $this->items->last();
            } else
            {
                return null;
            }
        } else
        {
            $order = $item->order - 1;
        }

        return NavigationItem::whereNavigationId($this->id)
            ->whereOrder($order)
            ->firstOrFail();
    }

    private function getItemByModel($model)
    {
        return NavigationItem::whereNavigationId($this->id)
            ->whereLinkId($model->id)
            ->whereLinkType(get_class($model))
            ->firstOrFail();
    }
}
