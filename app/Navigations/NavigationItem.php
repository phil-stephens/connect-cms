<?php namespace Fastrack\Navigations;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NavigationItem
 * @package Fastrack\Navigations
 */
class NavigationItem extends Model
{
    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['label', 'order'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function navigation()
    {
        return $this->belongsTo('Fastrack\Navigations\Navigation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function link()
    {
        return $this->morphTo();
    }

}
