<?php namespace Fastrack\Navigations;

use Fastrack\Sites\Site;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class NavigationRepository
 * @package Fastrack\Navigations
 */
class NavigationRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Navigation::class;
    protected $getWith = ['items'];

    //public function get($uuid)
    //{
    //    return Navigation::with('items')->whereUuid($uuid)->firstOrFail();
    //}

    /**
     * @param $slug
     * @param null $siteId
     * @return mixed
     */
    public function getBySlug($slug, $siteId = null)
    {
        $query = Navigation::with('items')->whereSlug($slug);

        if ( ! empty($siteId)) {
            $query->whereSiteId($siteId);
        }

        //if( ! empty($count)) $query->take($count);

        return $query->firstOrFail();
    }

    /**
     * @param $formData
     * @param $siteId
     * @return Navigation
     */
    public function create($formData, $siteId)
    {
        $navigation = new Navigation($formData);

        $this->save($navigation, $siteId);

        return $navigation;
    }

    /**
     * @param Navigation $navigation
     * @param $siteId
     * @return mixed
     */
    public function save(Navigation $navigation, $siteId)
    {
        return Site::findOrFail($siteId)
            ->navigations()
            ->save($navigation);
    }

    public function update($navigationId, $formData, $siteId)
    {
        $navigation = $this->getById($navigationId, $siteId);

        $navigation->fill($formData);

        $navigation->save();

        return;
    }

    /**
     * @param $navigationId
     * @param null $siteId
     * @return mixed
     */
    public function getById($navigationId, $siteId = null)
    {
        $query = Navigation::with('items');

        if ( ! empty($siteId)) {
            $query->whereSiteId($siteId);
        }

        return $query->findOrFail($navigationId);
    }

    /**
     * @param $navigationId
     * @param $formData
     * @param $siteId
     */
    public function updateItems($navigationId, $formData, $siteId)
    {
        if (empty($formData['item'])) {
            return;
        }

        $navigation = $this->getById($navigationId, $siteId);

        foreach ($formData['item'] as $item) {
            if (isset($item['id'])) {
                $navigationItem = $this->getItem($item['id'], $navigationId);

                $navigationItem->fill($item);

                $navigationItem->save();
            } else {
                // Create a new Navigation Item
                $navigationItem = new NavigationItem($item);

                $navigationItem->link_id = $item['class_id'];
                $navigationItem->link_type = $item['class'];

                $navigation->items()->save($navigationItem);
            }
        }

        return;
    }

    /**
     * @param $itemId
     * @param $navigationId
     * @return mixed
     */
    public function getItem($itemId, $navigationId)
    {
        return NavigationItem::whereNavigationId($navigationId)->findOrFail($itemId);
    }

    public function getAvailableItems($navigationId, $siteId)
    {
        $navigation = $this->getById($navigationId, $siteId);
        $site = Site::findOrFail($siteId);

        $ignore = [
            'pages'      => [],
            'properties' => [],
            'rooms'      => [],
            'offers'     => [],
            'places'     => [],
            'navigations'   => [ $navigationId ]
        ];

        foreach ($navigation->items as $item) {
            switch ($item->link_type) {
                case 'Fastrack\Pages\Page':
                    $ignore['pages'][] = $item->link_id;
                    break;

                case 'Fastrack\Properties\Property':
                    $ignore['properties'][] = $item->link_id;
                    break;

                case 'Fastrack\Properties\Room':
                    $ignore['rooms'][] = $item->link_id;
                    break;

                case 'Fastrack\Offers\Offer':
                    $ignore['offers'][] = $item->link_id;
                    break;

                case 'Fastrack\Locations\Venue':
                    $ignore['places'][] = $item->link_id;
                    break;

                case 'Fastrack\Navigations\Navigation':
                    $ignore['navigations'][] = $item->link_id;
                    break;
            }
        }

        $available = [];

        // Pages
        $available['pages'] = $site->pages->except($ignore['pages']);

        // Properties
        $available['properties'] = $site->properties->except($ignore['properties']);

        // Rooms
        $available['rooms'] = $site->rooms()->except($ignore['rooms']);

        // Offers
        $available['offers'] = offers($siteId)->except($ignore['offers']);

        // Places
        $available['places'] = places(null, $siteId)->except($ignore['places']);

        // Navigation Groups
        $available['navigations'] = $site->navigations->except($ignore['navigations']);

        return $available;
    }

    /**
     * @param $itemId
     * @param $navigationId
     */
    public function destroyItem($itemId, $navigationId)
    {
        $item = $this->getItem($itemId, $navigationId);

        $item->delete();

        return;
    }

    /**
     * @param $navigationId
     * @param $siteId
     */
    public function destroy($navigationId, $siteId)
    {
        $navigation = $this->getById($navigationId, $siteId);

        $navigation->delete();

        return;
    }
}