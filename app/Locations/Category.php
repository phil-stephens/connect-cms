<?php namespace Fastrack\Locations;

//use Fastrack\Uuid\Model;
use Fastrack\Content\WebpageTrait;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package Fastrack\Locations
 */
class Category extends Model
{
    use HasUuid, WebpageTrait;

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function venues()
    {
        return $this->belongsToMany('Fastrack\Locations\Venue')->orderBy('id');
    }

}
