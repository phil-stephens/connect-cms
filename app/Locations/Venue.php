<?php namespace Fastrack\Locations;

use Fastrack\Content\CanonicalTrait;
use Fastrack\Content\GeoTrait;
use Fastrack\Content\SeoTrait;
use Fastrack\Content\URLTrait;
use Fastrack\Content\WebpageTrait;
use Fastrack\Sites\SettingTrait;
use Fastrack\Tags\TaggableTrait;
//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Laracasts\Presenter\PresentableTrait;
//use Fastrack\Versions\Model;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Venue
 * @package Fastrack\Locations
 */
class Venue extends Model
{

    use HasUuid, Versionable, WebpageTrait, PresentableTrait, GeoTrait, SeoTrait, URLTrait, SettingTrait, TaggableTrait, CanonicalTrait;

    /**
     * @var string
     */
    protected $presenter = 'Fastrack\Presenters\VenuePresenter';

    /**
     * @var array
     */
    protected $fillable = ['name', 'slug', 'address', 'website', 'phone', 'email', 'custom', 'active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('Fastrack\Locations\Category');
    }

    public function calendars()
    {
        return $this->morphToMany('Fastrack\Calendars\Calendar', 'calendarable');
    }

    /**
     * @param $name
     * @param $slug
     * @return static
     */
    public static function generate($name, $slug)
    {
        return new static(compact('name', 'slug'));
    }

    /**
     * @param $custom
     */
    public function setCustomAttribute($custom)
    {
        $this->attributes['custom'] = serialize($custom);
    }

    /**
     * @param $custom
     * @return mixed
     */
    public function getCustomAttribute($custom)
    {
        return unserialize($custom);
    }

}
