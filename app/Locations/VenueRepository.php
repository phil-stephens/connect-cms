<?php namespace Fastrack\Locations;

use Fastrack\Content\GeoRepositoryTrait;
use Fastrack\Content\Seo;
use Fastrack\Content\WebpageRepositoryTrait;
use Fastrack\Sites\Site;
use Fastrack\Slideshows\SlideshowRepositoryTrait;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class VenueRepository
 * @package Fastrack\Locations
 */
class VenueRepository
{
    use UuidRepositoryTrait, WebpageRepositoryTrait, GeoRepositoryTrait, SlideshowRepositoryTrait;

    protected $repoClass = Venue::class;
    protected $getWith = ['content', 'seo'];


    /**
     * @param $categorySlug
     * @return mixed
     */
    public function getByCategory($categorySlug, $siteId = null)
    {
        if (empty($siteId)) {
            $siteId = site('id');
        }

        $site = Site::findOrFail($siteId);

        // still need to do geo

        switch($site->business_feed)
        {
            case 'manual':
                if ($categorySlug) {
                    $ids = Category::whereSlug($categorySlug)->firstOrFail()->venues->lists('id')->all();

                    $places = $site->businesses->only($ids);
                    //return $site->businesses->only($ids)->whereActive(true);

                    $filtered = $places->filter(function ($item) {
                        return $item->active;
                    });

                    //return $filtered->all();
                    return $filtered;
                } else {
                    $places = $site->businesses;

                    $filtered = $places->filter(function ($item) {
                        return $item->active;
                    });

                    //return $filtered->all();
                    return $filtered;
                }
                break;

            default:

                if ($categorySlug) {
                    $places = Category::whereSlug($categorySlug)->firstOrFail()->venues;

                    $filtered = $places->filter(function ($item) {
                        return $item->active;
                    });

                    //return $filtered->all();
                    return $filtered;
                } else {
                    return Venue::orderBy('id', 'asc')->whereActive(true)->get();
                }
                break;
        }
    }

    /**
     * @param $venueId
     * @return mixed
     */
    public function getById($venueId)
    {
        return Venue::with('content', 'seo')->findOrFail($venueId);
    }

    /**
     * @param $venueSlug
     * @return mixed
     */
    public function getBySlug($venueSlug)
    {
        return Venue::with('content', 'seo')->whereSlug($venueSlug)->firstOrFail();
    }

    /**
     * @param $formData
     * @return static
     */
    public function create($formData)
    {
        $formData = $this->createStubs($formData);

        $venue = Venue::generate($formData['name'], $formData['slug']);

        foreach (['website', 'phone', 'email', 'custom'] as $field) {
            if ( ! empty($formData[$field])) {
                $venue->$field = $formData[$field];
            }
        }

        //if( empty($venue->latitude) && ! empty($venue->address))
        //{
        //    try {
        //        $geocoded = $this->geocode($venue->address);
        //        $venue->latitude = $geocoded->getLatitude();
        //        $venue->longitude = $geocoded->getLongitude();
        //    } catch( \Exception $exception)
        //    {
        //
        //    }
        //}

        $venue->save();

        $this->updateLocation($venue, $formData);

        if (isset($formData['categories'])) {
            $venue->categories()->sync($formData['categories']);
        }

        $this->createContent($venue, $formData);

        //$content = new Content($formData['content']);
        //
        //// Find the image
        //if( ! empty($formData['content']['upload_hash']))
        //{
        //    $image = Image::whereUploadHash($formData['content']['upload_hash'])->latest()->first();
        //
        //    if( ! empty($image)) $content->image_id = $image->id;
        //}
        //
        //$venue->content()->save($content);

        // SEO bootstrapping
        $seo = new Seo($formData['seo']);

        $venue->seo()->save($seo);


        // Refresh custom routes
        //$this->buildSiteRoutes();

        $this->bootstrapSlideshow($venue);

        return $venue;
    }



    //public function updateGeoCode(Venue $venue)
    //{
    //    try {
    //        $geocoded = $this->geocode($venue->address);
    //        $venue->latitude = $geocoded->getLatitude();
    //        $venue->longitude = $geocoded->getLongitude();
    //    } catch( \Exception $exception)
    //    {
    //
    //    }
    //
    //    return $venue->save();
    //}

    /**
     * @param $venueId
     * @param $formData
     * @return bool
     */
    public function update($venueId, $formData)
    {
        $venue = $this->getById($venueId);

        //$addressChanged = ! ($venue->address == $formData['address']);

        $venue->fill($formData);
        $venue->save();

        //if( empty($venue->latitude) && ! empty($venue->address) && $addressChanged)
        //{
        //    $this->updateGeoCode($venue, $venue->address);
        //}

        $this->updateLocation($venue, $formData);

        if (isset($formData['categories'])) {
            $venue->categories()->detach();

            if (isset($formData['categories'])) {
                $venue->categories()->sync($formData['categories']);
            }
        }

        // Now do the content...
        //$venue->content->fill($formData['content']);
        //
        //$venue->content->save();

        if (isset($formData['content'])) {
            $this->updateContent($venue, $formData['content']);
        }

        // Now do the SEO...
        //$venue->seo->fill($formData['seo']);
        //
        //$venue->seo->save();

        if (isset($formData['seo'])) {
            $this->updateSeo($venue, $formData['seo']);
        }

        // Refresh custom routes
        //$this->buildSiteRoutes();

        return true;
    }


    /**
     * @return mixed
     */
    public function getAll()
    {
        //return $this->getByCategory(false);
        return Venue::orderBy('id', 'asc')->get();

    }

    /**
     * @param $venueId
     */
    public function destroy($venueId)
    {
        return $this->destroyIt(
            $this->getById($venueId)
        );
    }

    //public function geocode($addressString)
    //{
    //    $adapter = new CurlHttpAdapter();
    //    $chain = new ChainProvider(
    //        array(
    //            new GoogleMapsProvider($adapter),
    //        )
    //    );
    //
    //    $geocoder = new Geocoder();
    //
    //    $geocoder->registerProvider($chain);
    //
    //    return $geocoder->geocode($addressString);
    //}

    // Categories
    public function getCategories()
    {
        return Category::get();
    }

    public function getCategory($categoryId)
    {
        return Category::findOrFail($categoryId);
    }

    public function createCategory($formData)
    {
        return Category::create($formData);
    }

    public function updateCategory($categoryId, $formData)
    {
        $category = $this->getCategory($categoryId);

        $category->fill($formData);

        return $category->save();
    }

    public function destroyCategory($categoryId, $formData)
    {
        $category = $this->getCategory($categoryId);

        return $category->delete();
    }

    public function getUnrelated($siteId)
    {
        $site = Site::findOrFail($siteId);

        $ignore = $site->businesses->lists('id')->all();

        $v = Venue::whereNotIn('id', $ignore)->get();

        return $v;
    }

    public function updateSite($siteId, $businesses)
    {
        $site = Site::findOrFail($siteId);

        $site->businesses()->detach();

        foreach ($businesses as $business) {
            $site->businesses()->attach($business['id']);
            $site->businesses()->updateExistingPivot($business['id'],
                ['order' => $business['order']]);
        }

        return;
    }

    public function removeFromSite($siteId, $businessId)
    {
        $site = Site::findOrFail($siteId);

        $site->businesses()->detach($businessId);

        return $businessId;
    }
}