<?php

namespace Fastrack\Locations;


use Fastrack\Content\WebpageRepositoryTrait;
use Fastrack\Meta\UuidRepositoryTrait;

class CategoryRepository
{
    use UuidRepositoryTrait, WebpageRepositoryTrait;

    protected $repoClass = Category::class;

    public function getById($id)
    {
        return Category::findOrFail($id);
    }

    public function getAll()
    {
        return Category::get();
    }

    public function create($formData)
    {
        $formData = $this->createStubs($formData);

        $category = Category::create($formData);

        $this->createContent($category, $formData);

        return $category;
    }

    public function update($uuid, $formData)
    {
        $category = $this->get($uuid);

        $category->fill($formData);

        $category->save();

        if (isset($formData['content'])) {
            $this->updateContent($category, $formData['content']);
        }

        return;
    }

    public function destroy($uuid)
    {
        $category = $this->get($uuid);

        return $category->delete();
    }

}