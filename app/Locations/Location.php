<?php

namespace Fastrack\Locations;

//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasUuid;

    protected $fillable = ['address1', 'address2', 'city' , 'state', 'postcode', 'country', 'latitude', 'longitude'];
}
