<?php namespace Fastrack\Slideshows;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Slide
 * @package Fastrack\Slideshows
 */
class Slide extends Model
{
    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['headline', 'excerpt', 'order', 'link', 'crop_from', 'face_detection'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function slideshow()
    {
        return $this->belongsTo('Fastrack\Slideshows\Slideshow');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo('Fastrack\Images\Image');
    }

    /**
     * @param array $parameters
     * @param null $crop_from
     * @param bool|false $face_detection
     * @return bool
     */
    public function getImagePath($parameters = [], $crop_from = null, $face_detection = false)
    {
        if ($this->image) {
            return $this->image->getPath($parameters, $crop_from, $face_detection);
            //dd($this->content->image);
            // Get the image file_name and upload_hash;
            //$file_path = $this->image->path . '/' . $this->image->file_name;
            //
            //$urlBuilder = UrlBuilderFactory::create( url('/img') );
            //
            //return $urlBuilder->getUrl($file_path, $parameters);
        }

        return false;

        // Handle Glide URL building

        // Possible divide into imageTag and imageURL
    }

    public function getImageUuid()
    {
        if ($this->image) {
            return $this->image->uuid;
        }

        return false;

    }

    /**
     * @param array $urlParams
     * @param array $tagParams
     * @param null $crop_from
     * @param null $face_detection
     * @return bool
     */
    public function getImageTag($urlParams = [], $tagParams = [], $crop_from = null, $face_detection = null)
    {
        if ($this->image) {
            return $this->image->getTag($urlParams, $tagParams, $crop_from, $face_detection);
        }
        //if($url = $this->getImagePath($urlParams))
        //{
        //
        //    $tag = '<img src="' . $url . '"';
        //
        //    if( ! empty($tagParams))
        //    {
        //        foreach($tagParams as $key => $value)
        //        {
        //            $tag .= " {$key}=\"{$value}\"";
        //        }
        //    }
        //
        //    $tag .= ' />';
        //
        //    return $tag;
        //}

        return false;

    }

    public function getBackgroundAttribute($parameters = [], $otherStyles = null, $crop_from = null, $face_detection = null)
    {
        if ($this->image) {
            if( is_null($crop_from)) $crop_from = $this->crop_from;
            if( is_null($face_detection)) $face_detection = $face_detection;

            return $this->image->getBackgroundAttribute($parameters, $otherStyles, $crop_from, $face_detection);
        }

        return null;
    }
}
