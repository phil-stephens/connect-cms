<?php

namespace Fastrack\Slideshows;


trait SlideshowRepositoryTrait
{

    public function bootstrapSlideshow($model)
    {
        $slideshow = Slideshow::create([
            'name'  => $model->name
        ]);

        $model->slideshow_id = $slideshow->id;

        $model->save();
    }
}