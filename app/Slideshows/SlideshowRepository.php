<?php namespace Fastrack\Slideshows;

use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class SlideshowRepository
 * @package Fastrack\Slideshows
 */
class SlideshowRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Slideshow::class;

    /**
     * @param $name
     * @return static
     */
    public function create($name)
    {
        return Slideshow::create(['name' => $name]);
    }
}