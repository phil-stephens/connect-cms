<?php namespace Fastrack\Slideshows;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Fastrack\Versions\Versionable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Slideshow
 * @package Fastrack\Slideshows
 */
class Slideshow extends Model
{

    use HasUuid, Versionable;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return mixed
     */
    public function slides()
    {
        return $this->hasMany('Fastrack\Slideshows\Slide')->orderBy('order');
    }
}
