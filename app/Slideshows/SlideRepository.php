<?php namespace Fastrack\Slideshows;

use Fastrack\Images\Image;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class SlideRepository
 * @package Fastrack\Slideshows
 */
class SlideRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Slide::class;

    /**
     * @param $image
     * @param $slideshow
     * @return Slide
     */
    public function create($image, $slideshow)
    {
        $slide = new Slide();

        $slide->slideshow()->associate($slideshow);
        $slide->image()->associate($image);

        $slide->save();

        //$this->assignImage($slide, $data);

        return $slide;
    }

    /**
     * @param $formData
     * @param null $slideshowId
     */
    public function updateMany($formData, $slideshowId = null)
    {
        foreach ($formData['slide'] as $data) {
            if ( ! empty($data['destroy']) && ! empty($data['id']) ) {
                $this->getById($data['id'])->delete();
            } else {
                if ( ! empty($data['id'])) {
                    $slide = $this->getById($data['id']);

                    if (empty($data['face_detection'])) {
                        $data['face_detection'] = false;
                    }

                    $slide->fill($data);

                    $slide->save();

                    //$this->assignImage($slide, $data);
                } else {
                    $slide = new Slide($data);

                    Slideshow::findOrFail($slideshowId)->slides()->save($slide);

                }

                $this->assignImage($slide, $data);


            }
        }
    }

    private function assignImage(Slide $slide, $formData)
    {
        if( ! empty($formData['image']))
        {
            // Find the image
            try
            {
                $image = Image::whereUuid($formData['image'])->firstOrFail();

                $slide->image_id = $image->id;
                $slide->save();
            } catch( \Exception $e)
            {
                //$slide->image_id = 0;
                //$slide->save();

                $slide->delete();
            }
        } else
        {
            $slide->delete();
        }

        return;
    }

    /**
     * @param $slideId
     * @return mixed
     */
    public function getById($slideId)
    {
        return Slide::findOrFail($slideId);
    }
}