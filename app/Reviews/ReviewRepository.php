<?php

namespace Fastrack\Reviews;


use Fastrack\Meta\UuidRepositoryTrait;
use Fastrack\Properties\Property;

class ReviewRepository
{

    use UuidRepositoryTrait;

    protected $repoClass = Review::class;

    public function getPaginated($propertyUuid)
    {
        return Property::whereUuid($propertyUuid)->firstOrFail()->reviews()->paginate();
    }

    public function create($propertyUuid, $formData)
    {
        $review = new Review($formData);

        $this->save($review, $propertyUuid);

        return;
    }

    public function save(Review $review, $propertyUuid)
    {
        return Property::whereUuid($propertyUuid)
                        ->firstOrFail()
                        ->reviews()
                        ->save($review);
    }

    public function update($reviewUuid, $formData)
    {
        $review = $this->get($reviewUuid);

        $review->fill($formData);

        $review->save();

        return;
    }

    public function destroy($reviewUuid)
    {
        $review = $this->get($reviewUuid);

        $review->delete();

        return;
    }
}