<?php

namespace Fastrack\Reviews;

use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;
use League\CommonMark\DocParser;
use League\CommonMark\Environment;
use League\CommonMark\HtmlRenderer;

class Review extends Model
{
    use HasUuid;

    protected $fillable = ['published', 'name', 'email', 'stars', 'message', 'date', 'source', 'url', 'room_id'];

    public function getDates()
    {
        return ['created_at', 'updated_at', 'date'];
    }

    public function property()
    {
        return $this->belongsTo('Fastrack\Properties\Property');
    }

    public function room()
    {
        return $this->belongsTo('Fastrack\Properties\Room');
    }

    public function getMessage($parseMarkdown = true, $before = null, $after = null)
    {
        $content = trim($this->message);

        if (empty($content)) return;

        if ($parseMarkdown) {
            $environment = Environment::createCommonMarkEnvironment();
            $parser = new DocParser($environment);
            $htmlRenderer = new HtmlRenderer($environment);

            $text = $parser->parse($content);
            $content = $htmlRenderer->renderBlock($text);
        }

        return $before . $content . $after;
    }
}
