<?php namespace Fastrack\Subscribers;

//use Fastrack\Uuid\Model;
//use Fastrack\Versions\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Subscriber
 * @package Fastrack\Subscribers
 */
class Subscriber extends Model
{

    use HasUuid;

    /**
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    //public function site()
    //{
    //    return $this->belongsTo('Fastrack\Sites\Site');
    //}

    public function parent()
    {
        return $this->morphTo();
    }

}
