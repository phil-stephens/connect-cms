<?php namespace Fastrack\Subscribers;


use Fastrack\Sites\Site;
use Fastrack\Meta\UuidRepositoryTrait;

/**
 * Class SubscriberRepository
 * @package Fastrack\Subscribers
 */
class SubscriberRepository
{

    use UuidRepositoryTrait;

    protected $repoClass = Subscriber::class;

    /**
     * @param Subscriber $subscriber
     * @param $siteId
     * @return mixed
     */
    public function save(Subscriber $subscriber, $siteId)
    {
        return Site::findOrFail($siteId)
            ->subscribers()
            ->save($subscriber);
    }

    /**
     * @param $formData
     * @param $parent
     * @return Subscriber
     */
    public function create($formData, $parent)
    {
        if (isset($formData['name'])) {
            $parts = explode(' ', $formData['name']);

            if (count($parts) > 1) {
                $formData['last_name'] = array_pop($parts);
                $formData['first_name'] = implode(' ', $parts);
            } else {
                $formData['last_name'] = '';
                $formData['first_name'] = $formData['name'];
            }

            unset($formData['name']);
        }

        $subscriber = new Subscriber($formData);

        $parent->subscribers()->save($subscriber);

        //$this->save($subscriber, $siteId);

        return $subscriber;
    }
}