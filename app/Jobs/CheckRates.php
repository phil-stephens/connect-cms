<?php

namespace Fastrack\Jobs;

use Fastrack\Integrations\Integration;
use Fastrack\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class CheckRates extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var Integration
     */
    private $integration;

    public function __construct(Integration $integration)
    {
        //
        $this->integration = $integration;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if( ! $this->integration->active ) return;

        $type = $this->integration->type;

        $provider = new $type($this->integration);

        $start = Carbon::now()->toDateString();
        $finish = Carbon::now()->addDays(21)->toDateString();

        $data = $provider->getData($start, $finish);

        //dd($data);

        //foreach($data as $property)
        //{
        //    dd($property);

            foreach($data as $item)
            {
                $best = [];

                foreach($item->dates as $date)
                {
                    //dd($date);
                    if($date['available'] && (empty($best) || $date['rate'] < $best['rate'])) $best = $date;
                }

                // Save the rate data
                try
                {
                    $item->room->price = number_format((float) $best['rate'], 2, '.', '');
                    $item->room->dynamic_rate_data = serialize($best);

                    //$item->room->touch();
                    $item->room->save();
                } catch(\Exception $e) { }

            }
        //}

    }
}
