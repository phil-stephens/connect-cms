<?php

namespace Fastrack\Jobs;

use Fastrack\Jobs\Job;
use Carbon\Carbon;
use Fastrack\Properties\Availability;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Fastrack\Integrations\Integration;

class CacheAvailability extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var Integration
     */
    private $integration;

    public function __construct(Integration $integration)
    {
        //
        $this->integration = $integration;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $type = $this->integration->type;

        $provider = new $type($this->integration);

        if($provider->cacheable())
        {
            $start = new Carbon();
            $finish = new Carbon();
            $finish->addDays( $provider->cacheDays() );

            $data = $provider->getData($start, $finish);

            foreach($data as $r)
            {
                foreach($r->dates as $date)
                {
                    if ( ! $availability = $this->getByDate($date['date']->format('Y-m-d'), $r->room->id)) {
                        $availability = new Availability();
                    }

                    $availability->date         = $date['date']->format('Y-m-d');
                    $availability->available    = $date['available'];
                    $availability->rate         = $date['rate'];

                    $r->room->availabilities()->save($availability);
                }
            }
        }

        // Clear old items here


    }

    private function getByDate($date, $roomId)
    {
        return Availability::whereRoomId($roomId)
            ->where('date', '=', $date)
            ->first();
    }
}
