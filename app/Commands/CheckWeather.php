<?php namespace Fastrack\Commands;

use Cmfcmf\OpenWeatherMap;
use Fastrack\Properties\Property;
use Fastrack\Weather\WeatherRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class CheckWeather
 * @package Fastrack\Commands
 */
class CheckWeather extends Command implements SelfHandling, ShouldBeQueued
{

    use InteractsWithQueue, SerializesModels;

    /**
     * @var Property
     */
    private $property;

    /**
     * @param Property $property
     */
    public function __construct(Property $property)
    {
        $this->property = $property;
    }

    /**
     * @param WeatherRepository $weatherRepository
     * @throws OpenWeatherMap\Exception
     */
    public function handle(WeatherRepository $weatherRepository)
    {
        $owm = new OpenWeatherMap();

        $forecast = $owm->getWeatherForecast(['lat' => $this->property->latitude, 'lon' => $this->property->longitude],
            'metric', 'en', '6315e933d5486a786119e39c7e6f7538', 10);

        for ($i = 0; $i < 10; $i ++) {
            $weatherRepository->saveForecast($forecast->current(), $this->property->id);

            $forecast->next();
        }

        return;
    }

}
