<?php namespace Fastrack\Commands;

use Fastrack\Images\Image;
use Illuminate\Contracts\Bus\SelfHandling;
use GuzzleHttp\Client;

/**
 * Class ExtractColour
 * @package Fastrack\Commands
 */
class ExtractColour extends Command implements SelfHandling
{

    /**
     * @var Image
     */
    private $image;

    /**
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }


    public function handle()
    {
        if(env('IMAGE_PROCESSOR') == 'imgix')
        {
            $path = $this->image->getPath(['colors' => 1, 'palette' => 'json']);

            $client = new Client();

            $response = $client->get($path);

            $payload = $response->getBody();

            $palette = json_decode($payload);

            $r = floor( $palette->colors[0]->red * 255);
            $g = floor( $palette->colors[0]->green * 255);
            $b = floor( $palette->colors[0]->blue * 255);

            $this->image->primary_colour = "rgb({$r},{$g},{$b})";

            $this->image->save();
        }

        return;
    }

}
