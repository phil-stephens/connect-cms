<?php namespace Fastrack\Commands;

use Carbon\Carbon;
use Fastrack\Properties\Property;
use Fastrack\Properties\RoomRepository;
use GuzzleHttp\Client as Guzzle;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class CheckBookingButton
 * @package Fastrack\Commands
 */
class CheckBookingButton extends Command implements SelfHandling, ShouldBeQueued
{

    use InteractsWithQueue, SerializesModels;

    /**
     * @var Property
     */
    private $property;


    /**
     * @param Property $property
     */
    public function __construct(Property $property)
    {
        $this->property = $property;
    }


    /**
     * @param RoomRepository $roomRepository
     */
    public function handle(RoomRepository $roomRepository)
    {
        $client = new Guzzle();

        $days = 21;
        $params = [
            'start_date' => Carbon::tomorrow()->toDateString(),
            'end_date'   => Carbon::now()->addDays($days)->toDateString()
        ];

        $response = $client->get('https://www.thebookingbutton.com.au/api/v1/properties/' . $this->property->bookingbutton_channel . '/rates.json?' . http_build_query($params));

        // Now using Guzzle PSR7 response - json() not available
        //$response = $response->json();
        $response = json_decode($response->getBody());

        //dd($response[0]->room_types);
        //$rooms = $response[0]['room_types'];
        $rooms = $response[0]->room_types;

        $overall_rate = null;
        $best_rate_room = null;

        foreach ($rooms as $room) {
            $best_rate = null;
            $rate_data = null;

            //foreach ($room['room_type_dates'] as $date) {
            foreach ($room->room_type_dates as $date) {

                //if ($date['available']) {
                if ($date->available) {
                    //if ($date['rate'] < $best_rate || empty($best_rate)) {
                    if ($date->rate < $best_rate || empty($best_rate)) {
                        //$best_rate = $date['rate'];
                        $best_rate = $date->rate;
                        $rate_data = $date;
                    }
                }
            }

            if ( ! empty($best_rate)) {

                //$roomRepository->updateDynamicRate($room['id'], $best_rate, $rate_data);
                $roomRepository->updateDynamicRate($room->id, $best_rate, $rate_data);
            }

        }

        return;

    }

}
