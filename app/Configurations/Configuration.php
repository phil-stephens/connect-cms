<?php

namespace Fastrack\Configurations;

use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    use HasUuid;

    protected $fillable = ['key', 'value', 'image_id', 'file_id'];

    public function image()
    {
        return $this->belongsTo('Fastrack\Images\Image');
    }

    public function file()
    {
        return $this->belongsTo('Fastrack\Files\File');
    }
}
