<?php

namespace Fastrack\Configurations;


use Fastrack\Meta\UuidRepositoryTrait;

class ConfigurationRepository
{
    use UuidRepositoryTrait;

    protected $repoClass = Configuration::class;

    public function updateSingle($key, $value)
    {
        $config = Configuration::firstOrNew(['key' => $key]);

        $config->value = $value;

        $config->save();

        return $config;
    }
}