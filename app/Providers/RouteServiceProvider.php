<?php

namespace Fastrack\Providers;

use Fastrack\Sites\RouteRepository;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{

    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Fastrack\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function map(Router $router)
    {
        //$router->group(['namespace' => $this->namespace], function ($router) {
        //    require app_path('Http/routes.php');
        //});

        $router->group(['namespace' => $this->namespace, 'cache' => true], function ($router) {
            require app_path('Http/routes.php');

            try {

                $prefix = ltrim(env('FILESYSTEM_PREFIX', '') . '/', '/');

                $routeFile = 'app/' . $prefix . 'routes.php';

                if ( ! $exists = file_exists(storage_path($routeFile))) {
                    // Fire the route generator
                    $repo = new RouteRepository();
                    $exists = $repo->buildSiteRoutes();
                }

                // Even after running buildSiteRoutes() the file might not exist
                if($exists) require storage_path($routeFile);

            } catch (\Exception $e) {
                // Do nothing...
            }
        });

        $router->group([
            'namespace'  => $this->namespace . '\Admin',
            'prefix'     => env('ADMIN_PREFIX', '.admin'),
            'middleware' => 'auth'
        ], function ($router) {
            require app_path('Http/admin.php');
        });

        $router->group(['namespace' => $this->namespace, 'cache' => true], function ($router) {
            require app_path('Http/wildcards.php');
        });
    }
}
