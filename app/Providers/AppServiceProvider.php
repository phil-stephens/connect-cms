<?php

namespace Fastrack\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //\Blade::compileString(\$variation);
        Blade::directive('campaign', function($expression) {

            return;
            $expression = str_replace("('", '', $expression);
            $expression = str_replace("')", '', $expression);

            $variation = getABcampaign($expression);

            //dd($variation);

            if($variation = getABcampaign($expression))
            {
                $output = "<?php if(1 == 1): ?>";

                $output .= $variation;
            } else
            {
                $output = "<?php if(0 == 1): ?>";
            }

            $output .= "<?php else: ?>";

            return $output;

            //$output = "<?php if(\$variation = getABcampaign({$expression})): ";
            //$output .= "echo eval( \$variation );";
            //$output .= " else: ";
            //
            //return $output;

            return "<?php if(\$variation = getABcampaign({$expression})):\n echo \$variation; \nelse: ?>";
        });

        Blade::directive('endcampaign', function($expression) {
            return;
            return "<?php endif; ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }
    }
}
