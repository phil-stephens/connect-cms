<?php

namespace Fastrack\Meta;


trait UuidRepositoryTrait
{
    public function get($uuid)
    {
        $class = $this->repoClass;

        if(isset($this->getWith))
        {
            return $class::with($this->getWith)->whereUuid($uuid)->firstOrFail();
        }

        return $class::whereUuid($uuid)->firstOrFail();
    }

}