<?php

namespace Fastrack\Messages;

//use Fastrack\Uuid\Model;
use Fastrack\Uuid\HasUuid;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasUuid;

    protected $fillable = ['payload'];
}
