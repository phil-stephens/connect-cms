<?php

namespace Fastrack\Console\Commands;

use Illuminate\Console\Command;

class CacheAvailability extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:availability';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the availability caching from the CLI';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $integrations = \Fastrack\Integrations\Integration::all(); // this will need to change...

        foreach($integrations as $i)
        {
            \Bus::dispatch(
                new \Fastrack\Jobs\CacheAvailability($i)
            );
        }
    }
}
