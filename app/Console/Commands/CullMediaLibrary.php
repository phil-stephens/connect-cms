<?php

namespace Fastrack\Console\Commands;

use Fastrack\Images\Image;
use Illuminate\Console\Command;

class CullMediaLibrary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:cull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears out any unused images in the media libraries.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Finding unused images');

        // First get an array of all image IDs currently in use...
        $tables = ['amenities', 'content', 'custom_fields', 'gallery_image', 'settings', 'slides'];
        $seo = ['og_image_id', 'twitter_image_id'];

        $used = [];

        foreach($tables as $table)
        {
            $result =  \DB::table($table)->lists('image_id');

            foreach($result as $value)
            {
                $used[$value] = $value;
            }
        }

        foreach($seo as $column)
        {
            $result = \DB::table('seo')->lists($column);

            foreach($result as $value)
            {
                $used[$value] = $value;
            }
        }

        $unused = Image::whereNotIn('id', $used)->get();

        if ($this->confirm('Do you wish to remove ' . count($unused) . ' unused images? [y|N]')) {

            $this->info('Removing unused images');

            foreach($unused as $image)
            {
                $this->info('Removing ' . $image->path . '/' . $image->file_name);

                $this->removeImage($image);
            }

        }

        $this->info('Removing duplicate images');

        $toDelete = [];

        $images = Image::all();

        // cycle through each image
        foreach($images as $image)
        {
            if( ! in_array($image->id, $toDelete))
            {
                $duplicates = Image::whereOriginalName($image->original_name)
                    ->whereMimeType($image->mime_type)
                    ->whereSize($image->size)
                    ->whereWidth($image->width)
                    ->whereHeight($image->height)
                    ->where('id', '!=', $image->id)
                    ->get();

                if(count($duplicates))
                {
                    $this->info('Removing ' . count($duplicates) . ' duplicates for ' . $image->original_name);

                    foreach($duplicates as $duplicate)
                    {
                        $toDelete[$duplicate->id] = $duplicate->id;

                        foreach($tables as $table)
                        {
                            \DB::table($table)->whereImageId($duplicate->id)->update(['image_id' => $image->id]);
                        }

                        foreach($seo as $column)
                        {
                            \DB::table('seo')->where($column, '=', $duplicate->id)->update([$column => $image->id]);
                        }

                        $this->removeImage($duplicate);
                    }
                }
            }

        }
    }

    private function removeImage($image)
    {
        // First remove from the filesystem
        if(app()->environment() != 'local')
        {
            try
            {
                Storage::delete($image->path . '/' . $image->file_name);
            } catch( \Exception $e) {}
        }

        // Then destroy the sucker
        $image->delete();
    }
}
