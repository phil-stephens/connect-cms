<?php namespace Fastrack\Console\Commands;

use Fastrack\Properties\AmenityRepository;
use Fastrack\Properties\Property;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class DuplicatePropertyAmenities extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cms:copy:amenities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy the amenities of one property onto another';
    /**
     * @var AmenityRepository
     */
    private $amenityRepository;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AmenityRepository $amenityRepository)
    {
        parent::__construct();
        $this->amenityRepository = $amenityRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $sourceProperty = Property::findOrFail($this->argument('sourceId'));
        $targetProperty = Property::findOrFail($this->argument('targetId'));

        if ($this->confirm("Do you wish to copy the amenities from '{$sourceProperty->name}' to '{$targetProperty->name}'? [yes|no]")) {
            $i = 0;
            foreach ($sourceProperty->amenities as $amenity) {
                $i ++;
                $this->amenityRepository->create($amenity->toArray(), $targetProperty->id);
            }

            $this->info($i . ' ' . str_plural('amenity', $i) . ' copied');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['sourceId', InputArgument::REQUIRED, 'The ID of the property whose amenities to duplicate'],
            ['targetId', InputArgument::REQUIRED, 'The ID of the property to which the amenities should be copied'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
