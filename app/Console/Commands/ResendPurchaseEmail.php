<?php namespace Fastrack\Console\Commands;

use Fastrack\Calendars\TicketPurchase;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ResendPurchaseEmail extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cms:ticketemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {


        //\View::addLocation(public_path('themes/' . $this->argument('theme') . '/views'));
        //$site = Site::find(1);
        //
        //$this->info("Duplicating site '{$site->name}'");
        //$this->confirm("Do you wish to duplicate site '" . $this->argument('id') . "'? [yes|no]");

        $purchase = TicketPurchase::findOrFail($this->argument('id'));

        $event = $purchase->ticket_type->event;

        \Mail::send('emails.registered', compact('purchase', 'event'), function ($message) use ($purchase) {
            $message->to('phil.stephens@fastrackg.com',
                $purchase->name)->subject('Thank you for registering with Maximum Occupancy 2015');
        });
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['id', InputArgument::REQUIRED, 'The ID of the purchase'],
            ['theme', InputArgument::REQUIRED, 'Theme to use'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
