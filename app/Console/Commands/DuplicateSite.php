<?php namespace Fastrack\Console\Commands;

use Fastrack\Content\Content;
use Fastrack\Content\CustomField;
use Fastrack\Content\Seo;
use Fastrack\Navigations\Navigation;
use Fastrack\Navigations\NavigationItem;
use Fastrack\Pages\Chapter;
use Fastrack\Pages\Page;
use Fastrack\Sites\Alias;
use Fastrack\Sites\Domain;
use Fastrack\Sites\Setting;
use Fastrack\Sites\Site;
use Fastrack\Sites\Taxonomy;
use Fastrack\Slideshows\Slide;
use Fastrack\Slideshows\Slideshow;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class DuplicateSite extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cms:duplicate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Duplicate a website in FastrackCMS';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $sourceSite = Site::findOrFail($this->argument('id'));

        if ($this->confirm("Do you wish to duplicate site '{$sourceSite->name}'? [yes|no]")) {
            $this->info("Duplicating site '{$sourceSite->name}'");

            $destinationSite = new Site([
                'name'  => $sourceSite->name . ' COPY',
                'theme' => $sourceSite->theme
            ]);

            $destinationSite->account_id = $sourceSite->account_id;

            $destinationSite->save();

            $destinationSite->seo()->save(new Seo($sourceSite->seo->toArray()));

            // Site settings
            $this->info("Duplicating site settings");

            foreach ($sourceSite->settings as $setting) {
                $destinationSite->settings()->save(new Setting($setting->toArray()));
            }

            // Properties
            $this->info("Duplicating property assignments");

            foreach ($sourceSite->properties as $property) {
                $destinationSite->properties()->attach($property->id);

                if (isSame($sourceSite->homepage, $property)) {
                    $property->asHomepage()->save($destinationSite);
                }
            }

            // Domains
            $this->info("Duplicating domains");

            foreach ($sourceSite->domains as $domain) {

                $newDomain = new Domain([
                    'domain' => 'copy.' . $domain->domain
                ]);

                $newDomain->site_id = $destinationSite->id;
                $newDomain->save();

                if ($domain->id == $sourceSite->primary_domain_id) {
                    $destinationSite->primary_domain_id = $newDomain->id;

                    $destinationSite->save();
                }
            }

            // Taxonomies
            $this->info("Duplicating taxonomies");

            foreach ($sourceSite->taxonomies as $taxonomy) {
                //$newTaxonomy = new Taxonomy([
                //	'model'		=> $taxonomy->model,
                //	'title'		=> $taxonomy->title,
                //	'description'	=> $taxonomy->description
                //]);
                //
                //$newTaxonomy->site_id = $destinationSite->id;
                //$newTaxonomy->save();

                $destinationSite->taxonomies()->save(new Taxonomy($taxonomy->toArray()));
            }

            // Aliases
            $this->info("Duplicating aliases");

            foreach ($sourceSite->aliases as $alias) {
                //$newAlias = new Alias([
                //	'site_id'	=> $destinationSite->id,
                //	'from'		=> $alias->from,
                //	'to'		=> $alias->to,
                //	'type'		=> $alias->type
                //]);
                //
                //$newAlias->save();

                $destinationSite->aliases()->save(new Alias($alias->toArray()));
            }

            $pageRef = [];

            // Pages
            $this->info("Duplicating pages");

            foreach($sourceSite->chapters as $chapter)
            {
                $newChapter = new Chapter($chapter->toArray());

                $destinationSite->chapters()->save($newChapter);

                foreach($chapter->pages as $page)
                {
                    $newPage = new Page($page->toArray());

                    $destinationSite->pages()->save($newPage);

                    $newChapter->pages()->save($newPage);

                    $pageRef[$page->id] = $newPage->id;

                    if ($page->internal) {
                        $newPage->internal = true;
                        $newPage->save();
                    }

                    if (isSame($sourceSite->homepage, $page)) {
                        $newPage->asHomepage()->save($destinationSite);
                    }

                    // Content
                    $newPage->content()->save(new Content($page->content->toArray()));

                    $newPage->content->image_id = $page->content->image_id;
                    $newPage->content->save();

                    foreach ($page->content->customFields as $field) {
                        $newPage->content->customFields()->save(
                            new CustomField($field->toArray())
                        );
                    }

                    // Seo
                    $newPage->seo()->save(new Seo($page->seo->toArray()));

                    // Settings
                    foreach ($page->settings as $setting) {
                        $newPage->settings()->save(new Setting($setting->toArray()));
                    }

                    // Slideshows
                    if ($page->slideshow) {
                        $newSlideshow = new Slideshow($page->slideshow->toArray());

                        $newSlideshow->save();

                        $newPage->slideshow()->associate($newSlideshow);

                        foreach ($page->slideshow->slides as $slide) {
                            $newSlide = new Slide($slide->toArray());

                            $newSlide->image_id = $slide->image_id;

                            $newSlideshow->slides()->save($newSlide);
                        }
                    }
                }
            }


            //foreach ($sourceSite->pages as $page) {
            //    // Create the page
            //    $newPage = new Page($page->toArray());
            //
            //    $destinationSite->pages()->save($newPage);
            //
            //    $pageRef[$page->id] = $newPage->id;
            //
            //    if ($page->internal) {
            //        $newPage->internal = true;
            //        $newPage->save();
            //    }
            //
            //    if (isSame($sourceSite->homepage, $page)) {
            //        $newPage->asHomepage()->save($destinationSite);
            //    }
            //
            //    // Content
            //    $newPage->content()->save(new Content($page->content->toArray()));
            //
            //    $newPage->content->image_id = $page->content->image_id;
            //    $newPage->content->save();
            //
            //    foreach ($page->content->customFields as $field) {
            //        $newPage->content->customFields()->save(
            //            new CustomField($field->toArray())
            //        );
            //    }
            //
            //    // Seo
            //    $newPage->seo()->save(new Seo($page->seo->toArray()));
            //
            //    // Settings
            //    foreach ($page->settings as $setting) {
            //        $newPage->settings()->save(new Setting($setting->toArray()));
            //    }
            //
            //    // Slideshows
            //    if ($page->slideshow) {
            //        $newSlideshow = new Slideshow($page->slideshow->toArray());
            //
            //        $newSlideshow->save();
            //
            //        $newPage->slideshow()->associate($newSlideshow);
            //
            //        foreach ($page->slideshow->slides as $slide) {
            //            $newSlide = new Slide($slide->toArray());
            //
            //            $newSlide->image_id = $slide->image_id;
            //
            //            $newSlideshow->slides()->save($newSlide);
            //        }
            //    }
            //
            //
            //}

            // Nav groups
            $this->info("Duplicating navigation groups");

            foreach ($sourceSite->navigations as $navigation) {
                $newNavigation = new Navigation($navigation->toArray());

                $destinationSite->navigations()->save($newNavigation);

                foreach ($navigation->items as $item) {
                    $newItem = new NavigationItem($item->toArray());

                    if ($item->link_type == 'Fastrack\Pages\Page') {
                        $newItem->link_id = $pageRef[$item->link_id];
                    } else {
                        $newItem->link_id = $item->link_id;
                    }

                    $newItem->link_type = $item->link_type;

                    $newNavigation->items()->save($newItem);

                    //if(class_basename($item->link) == 'Page')
                    //{
                    //	$newLink = Page::find($pageRef[ $item->link->id ]);
                    //} else
                    //{
                    //	$newLink = $item->link;
                    //}
                    //
                    //$newItem->link()->associate($newLink);
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['id', InputArgument::REQUIRED, 'The ID of the site to duplicate'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }

}
