<?php

namespace Fastrack\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Fastrack\Console\Commands\Inspire::class,
        \Fastrack\Console\Commands\DuplicateSite::class,
        \Fastrack\Console\Commands\DuplicatePropertyAmenities::class,
        \Fastrack\Console\Commands\ResendPurchaseEmail::class,
        \Fastrack\Console\Commands\CullMediaLibrary::class,
        \Fastrack\Console\Commands\CacheAvailability::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')
        //    ->hourly();

        $schedule->call(function()
        {
            $integrations = \Fastrack\Integrations\Integration::all(); // this will need to change...

            foreach($integrations as $i)
            {
                \Bus::dispatch(
                    new \Fastrack\Jobs\CheckRates($i)
                );
            }
        })->hourly();

        //$schedule->call(function()
        //{
        //    $integrations = \Fastrack\Integrations\Integration::all(); // this will need to change...
        //
        //    foreach($integrations as $i)
        //    {
        //        \Bus::dispatch(
        //            new \Fastrack\Jobs\CacheAvailability($i)
        //        );
        //    }
        //})->everyTenMinutes()->name('cache-availability')->withoutOverlapping();

        $schedule->command('cms:availability')->everyTenMinutes()->withoutOverlapping();

        $schedule->call(function()
        {
            $properties = \Fastrack\Properties\Property::get();

            foreach ($properties as $property) {
                \Bus::dispatch(
                    new \Fastrack\Commands\CheckWeather($property)
                );
            }
        })->hourly();
    }
}
