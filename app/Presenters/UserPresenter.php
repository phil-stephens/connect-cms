<?php namespace Fastrack\Presenters;

use Fastrack\Images\ProcessorTrait;
use Laracasts\Presenter\Presenter;

/**
 * Class UserPresenter
 * @package Fastrack\Presenters
 */
class UserPresenter extends Presenter
{

    use ProcessorTrait;

    /**
     * @param string $format
     * @return string
     */
    public function lastLogin($format = 'j M Y')
    {
        $login = $this->entity->logins->last();

        if (empty($login)) {
            return 'Never';
        }

        return $login->created_at->format($format);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function avatar($params = [])
    {

        //$file = ( ! empty($this->entity->avatar)) ? $this->entity->avatar : 'default-avatar.jpg';
        //
        //$method = env('IMAGE_PROCESSOR', 'glide') . 'CachePath';
        //
        //return $this->$method($file, $params);
    }

    //public function avatarImage($preset = 'thumbnail', $class = null)
    //{
    //    $attr = [
    //        'src'   => $this->avatar($preset),
    //        'alt'   => $this->name,
    //        'class' => $class
    //    ];
    //
    //    $tag = '<img ';
    //
    //    foreach($attr as $key => $value)
    //    {
    //        $tag .= "{$key}=\"{$value}\" ";
    //    }
    //
    //    return $tag . '/>';
    //}
}