<?php namespace Fastrack\Presenters;

use Fastrack\Properties\Room;

/**
 * Class PropertyPresenter
 * @package Fastrack\Presenters
 */
class PropertyPresenter extends WebpagePresenter
{

    use GeoPresenterTrait;

    /**
     * @return string
     */
    public function roomCount()
    {
        $count = $this->entity->rooms()->count();

        $plural = $this->roomNoun($count);

        return "{$count} {$plural}";
    }

    /**
     * @param int $count
     * @param bool|true $ucfirst
     * @return string
     */
    public function roomNoun($count = 1, $ucfirst = true)
    {
        $noun = str_plural($this->room_noun, $count);

        return ($ucfirst) ? ucfirst($noun) : $noun;
    }

    public function priceFrom()
    {
        $price = Room::wherePropertyId($this->id)->whereActive(1)->min('price');

        return (is_null($price)) ? 0 : $price;
    }
}