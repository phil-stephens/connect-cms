<?php
/**
 * Created by PhpStorm.
 * User: fastrack
 * Date: 17/07/15
 * Time: 10:02 AM
 */

namespace Fastrack\Presenters;


/**
 * Class GeoPresenterTrait
 * @package Fastrack\Presenters
 */
trait GeoPresenterTrait
{


    //public function addressString()
    //{
    //    $address = [];
    //
    //    foreach (['address1', 'address2', 'city', 'state', 'postcode'] as $column) {
    //
    //        if ( ! empty($this->entity->$column)) {
    //            $address[] = $this->entity->$column;
    //        }
    //    }
    //
    //    // country
    //    if ( ! empty($this->entity->country)) {
    //        $address[] = config('countries.iso_countries.' . $this->entity->country);
    //    }
    //
    //    return implode(', ', $address);
    //}

    public function addressString($includeCountry = true, $delimiter = ', ')
    {
        $address = [];

        foreach (['address1', 'address2', 'city', 'state', 'postcode'] as $column) {
            if($this->entity->location && ! empty($this->entity->location->$column))
            {
                $address[] = $this->entity->location->$column;
            } elseif ( ! empty($this->attributes[$column])) {
                $address[] = $this->attributes[$column];
            }
        }

        // country
        if($includeCountry)
        {
            if($this->entity->location && ! empty($this->entity->location->country))
            {
                $address[] = config('countries.iso_countries.' . $this->entity->location->country);
            } elseif ( ! empty($this->attributes['country'])) {
                $address[] = config('countries.iso_countries.' . $this->attributes['country']);
            }
        }

        return implode($delimiter, $address);
    }
}