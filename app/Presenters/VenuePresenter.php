<?php namespace Fastrack\Presenters;


use Laracasts\Presenter\Presenter;

/**
 * Class VenuePresenter
 * @package Fastrack\Presenters
 */
class VenuePresenter extends Presenter
{

    use GeoPresenterTrait;

    /**
     * @return string
     */
    public function categoryList()
    {
        $categories = $this->entity->categories->lists('name')->all();

        return implode(', ', $categories);
    }
}