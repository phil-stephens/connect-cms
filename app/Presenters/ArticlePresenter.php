<?php namespace Fastrack\Presenters;


use Laracasts\Presenter\Presenter;

/**
 * Class ArticlePresenter
 * @package Fastrack\Presenters
 */
class ArticlePresenter extends Presenter
{

    /**
     * @return null
     */
    public function author()
    {
        if ($this->entity->user) {
            return $this->entity->user->name;
        }

        return null;
    }
}