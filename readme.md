# Connect CMS 

'Connect' was a proprietary content management system built in 2015-2016 to bolster the offering of a marketing agency working largely within the hotel and tourism sector.

I was the lead back-end developer for the agency and built the platform from the ground-up and assisted in the marketing and product strategy, as well as generating tender and training documentation for the system.

Whilst the code is somewhat legacy, I feel it demonstrates the development of a reasonably large feature-rich system using Laravel.  I have briefly outlined the features and product history below, as well as a an overview of how I would approach this in a different way if tasked with building the same system again.

The source code for this system can be found on BitBucket at the following:  
[https://bitbucket.org/phil-stephens/connect-cms/src/main/](https://bitbucket.org/phil-stephens/connect-cms/src/main/)


## Features

- Multi-site and multi-property - a single installation of the CMS was able to power multiple websites for one or more properties (hotels).
- Specific tools for authoring content for hotels - custom content types for room types and amenities.
- Built-in SEO - worked with SEO specialists to build SEO best-practices into every site without the need for third-party platforms such as Google Tag Manager.
- Calendars, Events and basic ticketing (only used internally for an annual conference the agency organised)
- Local points of interest/recommendations
- Internal venues (e.g. cafes, restaurants, health clubs) with relevant metadata (opening times, menus etc)
- Special offers/campaigns - including A/B campaign functionality
- Availability/dynamic pricing pulled in from a number of major third-party property management systems.
- Local weather forecasts
- Review management - either directly entered by guests, or transcribed from third-party services (e.g. TripAdvisor) with link-backs.
- Performance improvements - caching of responses for better performance.
- Image optimisation handled on-the-fly via Imgix (in production)
- Custom themes built for each client with customisable content fields defined at the theme level.  Themes are just Blade templates, requiring no special theme-building expertise.


## Approach

The system was born out of an existing commitment by the agency to build a multi-website platform for a group of hotels in Surry Hills, Sydney.  This not only had to offer custom content types for hotel room definitions, but also had to offer a shared library of local events, news and recommendations to avoid duplicate data entry/maintenance.  WordPress and other similar offerings at the time were deemed unsuitable so a working prototype was rapidly built for the client.

Functionality was decoupled into an abstract system that could be reused for further clients, and was built up over time as clients required further features.

All clients were hosted on separate DigitalOcean droplets (or clusters, depending on size of client/traffic) that were spun-up/maintained by Laravel Forge.  Deployments were handled by Laravel Envoyer.

All clients used the same core codebase (updated multiple times a week) with their custom themes pulled in from a separate Git repository and placed in the `/public/themes/` directory as a deployment step in Envoyer.


## Architecture

The system was built using a fairly simple model-repository structure.  The version provided here pre-dates unit tests, but work had begun on adding test coverage with a rudimentary CI/CD pipeline at the time of my departure.


## How My Approach Would Be Different Now

In the first instance, test coverage would be at the core of every component developed for the system.  Whilst for the most part the operation of the platform is fairly straightforward, there are some more complex aspects with multiple dependencies that I would feel much more comfortable having the safety net of test.  Additionally, I was the sole back-end developer/architect for this system - with front-end work being carried out separately on individual 'theme' repositories - so keeping track of the functionality and dependencies was a lot easier.  This would be unfeasible in a team-based approach.

Whilst there was a great deal of documentation generated for the system - both technical and client-facing - I no longer have access to it.  With that in mind I would include a lot more documentation with the code - as inline comments, easy-to-read 'self-documenting' code and tests - as well as actual long-form documentation in the form of `.txt` or `.md` files.  This would enable me to not only keep the docs as close to the code as possible, but to also leverage version control.

A lot of what happens in the system happens synchronously within the code, with very little use of events and listeners to decouple functionality - this is something I tend to use a lot of in my current work to separate out different concerns and reuse code.  There is also very limited use of Laravel's job queues which I tend to leverage a lot more - even if you use the synchronous queue configuration initially, makes it easy to later hook into asynchronous queues for performance boosts.

Whilst the performance of this system easily beat that of a comparable WordPress site, there are still a lot of database queries occurring for each page request (where full page caching isn't being utilised).  I would definitely pay attention to ways to remove unnecessary requests, whether it be through in-memory caching for common data or identifying duplicate requests per session and moving them to some sort of data transfer object.

In terms of the project structure, I would definitely look at rationalising the directory/namespace structure a little to make the relationships between different components of the system a little easier to understand.  Given that the bulk of the features were used by all clients using the system, I'm not sure it would be necessary to break up the platform into separate sub-services to aid maintainability.  This would only complicate deployments.  The only thing I would consider is breaking items apart into separate Composer packages (perhaps a core CMS package, an SEO package, Hotel Rooms etc) to be stored in a private Packagist repository.

With regards to the `Http/Controllers` structure - for the most part they follow a RESTful resource structure (i.e. methods for index, show, create, store, edit, update and destroy), but I can see that this started to become a little unstuck in some places.  I would likely take a different approach, breaking these controllers up into smaller controllers with either a single `__invoke` method or a smaller subset of methods that are specific to a function.  For example, take the `App/Http/Controllers/Admin/ArticleController`.  This controller handles the authoring of new news articles or blog posts.  However the article model itself is decoupled from its content, which in itself is a model that is shared by *anything* that can have website content.  I would likely break the controller up into something like the following:

- ArticleListController
- ArticleController
- ArticleContentController
- ArticleSeoController