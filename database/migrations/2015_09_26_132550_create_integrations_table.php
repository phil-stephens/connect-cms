<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrations', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->string('type');
            $table->string('name');

            // auth
            $table->string('username')->nullable()->default(null);
            $table->string('password')->nullable()->default(null);

            $table->string('custom1')->nullable()->default(null);
            $table->timestamps();
        });

        Schema::create('integratables', function (Blueprint $table) {
            $table->integer('integration_id')->unsigned();
            $table->integer('integratable_id')->unsigned();
            $table->string('integratable_type');
            $table->string('client_id')->nullable()->default(null); // might be redundant...
        });

        $properties = \Fastrack\Properties\Property::all();

        foreach($properties as $property)
        {
            if( ! empty($property->bookingbutton_channel))
            {
                $integration = \Fastrack\Integrations\Integration::create([
                   'type'   => 'Fastrack\Integrations\Providers\Siteminder\BookingButton',
                    'name'  => 'BookingButton - ' . $property->name
                ]);

                $integration->properties()->attach($property);

                $integration->properties()->updateExistingPivot($property->id, ['client_id' => $property->bookingbutton_channel]);

                foreach($property->rooms as $room)
                {
                    if( ! empty($room->bookingbutton_id))
                    {
                        $integration->rooms()->attach($room);

                        $integration->rooms()->updateExistingPivot($room->id, ['client_id' => $room->bookingbutton_id]);
                    }
                }
            }
        }

        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('bookingbutton_channel');
        });


        Schema::table('rooms', function (Blueprint $table) {
            $table->dropColumn('bookingbutton_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->string('bookingbutton_channel')->nullable()->default(null);
        });


        Schema::table('rooms', function (Blueprint $table) {
            $table->string('bookingbutton_id')->nullable()->default(null);
        });

        $integrations = \Fastrack\Integrations\Integration::all();

        foreach($integrations as $i)
        {
            $property = $i->properties()->first();

            $property->bookingbutton_channel = $property->pivot->client_id;

            $property->save();

            foreach($i->rooms as $room)
            {
                $room->bookingbutton_id = $room->pivot->client_id;

                $room->save();
            }
        }

        Schema::drop('integrations');
        Schema::drop('integratables');
    }
}
