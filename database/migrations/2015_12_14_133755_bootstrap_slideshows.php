<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BootstrapSlideshows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $models = [
            'Fastrack\Calendars\CalendarEvent',
            'Fastrack\Locations\Venue',
            'Fastrack\News\Article',
            'Fastrack\Pages\Page',
            'Fastrack\Properties\Property',
            'Fastrack\Properties\Room',
            'Fastrack\Properties\RoomSubType'
        ];

        foreach($models as $model)
        {
            $empty = $model::whereSlideshowId(0)->get();

            foreach($empty as $e)
            {
                $slideshow = \Fastrack\Slideshows\Slideshow::create([
                    'name'  => $e->name
                ]);

                $e->slideshow_id = $slideshow->id;

                $e->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
