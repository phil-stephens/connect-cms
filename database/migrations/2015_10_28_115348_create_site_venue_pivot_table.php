<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteVenuePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_venue', function(Blueprint $table) {
            $table->integer('site_id')->unsigned()->index();
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->integer('venue_id')->unsigned()->index();
            $table->foreign('venue_id')->references('id')->on('venues')->onDelete('cascade');
            $table->integer('order')->default(0);
        });

        Schema::table('sites', function (Blueprint $table) {
            $table->string('business_feed')->default('native')->after('theme'); // can be native, manual or geo (if a site has one property)
        });

        Schema::table('venues', function (Blueprint $table) {
            $table->integer('order')->default('0')->after('slideshow_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_venue');

        Schema::table('sites', function (Blueprint $table) {
            $table->dropColumn('business_feed');
        });

        Schema::table('venues', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
