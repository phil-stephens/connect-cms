<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Fastrack\Properties\Property;
use Fastrack\Locations\Venue;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);

            $table->string('address1')->nullable()->default(null);
            $table->string('address2')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->string('state')->nullable()->default(null);
            $table->string('postcode')->nullable()->default(null);
            $table->string('country')->nullable()->default(null);

            $table->decimal('latitude', 14, 10);
            $table->decimal('longitude', 14, 10);

            $table->timestamps();
        });

        foreach(['\Fastrack\Properties\Property' => 'properties', '\Fastrack\Locations\Venue' => 'venues'] as $class => $table)
        {
            Schema::table($table, function (Blueprint $t) {
                $t->integer('location_id')->nullable()->default(null)->after('longitude');
            });

            $models = $class::all();

            foreach($models as $model)
            {
                $location = \Fastrack\Locations\Location::create([
                    'address1'      => $model->address1,
                    'address2'      => $model->address2,
                    'city'          => $model->city,
                    'state'         => $model->state,
                    'postcode'      => $model->postcode,
                    'country'       => $model->country,
                    'latitude'      => $model->latitude,
                    'longitude'     => $model->longitude
                ]);

                $model->location_id = $location->id;
                $model->save();
            }

            Schema::table($table, function (Blueprint $t) {
                $t->dropColumn(['address1', 'address2', 'city', 'state', 'country', 'postcode', 'latitude', 'longitude']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach(['\Fastrack\Properties\Property' => 'properties', '\Fastrack\Locations\Venue' => 'venues'] as $class => $table)
        {
            Schema::table($table, function (Blueprint $t) {
                $t->dropColumn(['location_id']);

            });

            // add the columns back in
            Schema::table($table, function (Blueprint $t) {
                $t->string('address1')->nullable()->default(null);
                $t->string('address2')->nullable()->default(null);
                $t->string('city')->nullable()->default(null);
                $t->string('state')->nullable()->default(null);
                $t->string('postcode')->nullable()->default(null);
                $t->string('country')->nullable()->default(null);

                $t->decimal('latitude', 14, 10);
                $t->decimal('longitude', 14, 10);
            });


            $models = $class::all();

            foreach($models as $model)
            {
                $location = $model->location;

                foreach(['address1', 'address2', 'city', 'state', 'country', 'postcode', 'latitude', 'longitude'] as $field)
                {
                    $model->$field = $location->$field;
                }

                $model->save();
            }
        }

        Schema::drop('locations');
    }
}
