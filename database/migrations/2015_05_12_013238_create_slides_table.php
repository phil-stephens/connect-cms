<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlidesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->integer('slideshow_id')->unsigned()->index();
            $table->foreign('slideshow_id')->references('id')->on('slideshows')->onDelete('cascade');
            $table->integer('order')->unsigned();
            $table->integer('image_id')->unsigned()->default(0); // Can't add foreign key as this can be optional
            $table->string('link')->nullable()->default(null);
            $table->text('excerpt')->nullable()->default(null);
            $table->string('headline')->nullable()->default(null);
            $table->string('class')->nullable()->default(null);
            $table->text('html')->nullable()->default(null);
            $table->string('crop_from')->default('');
            $table->boolean('face_detection')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slides');
    }

}
