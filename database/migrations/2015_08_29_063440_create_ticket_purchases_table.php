<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketPurchasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ticket_purchases', function(Blueprint $table)
		{


			$table->increments('id');
			$table->binary('uuid', 16);

			$table->integer('ticket_type_id')->unsigned()->index();
			$table->foreign('ticket_type_id')->references('id')->on('ticket_types')->onDelete('cascade');

			$table->double('price')->default(0);
			$table->double('price_tickets')->default(0);
			$table->double('price_discount')->default(0);
			$table->double('price_tax')->default(0);
			$table->double('price_cc')->default(0);

			$table->string('reference');

			$table->string('name');
			$table->string('email')->nullable()->default(null);

			$table->string('company')->nullable()->default(null);
			$table->string('position')->nullable()->default(null);

			$table->string('phone')->nullable()->default(null);

			$table->string('address1')->nullable()->default(null);
			$table->string('address2')->nullable()->default(null);
			$table->string('city')->nullable()->default(null);
			$table->string('state')->nullable()->default(null);
			$table->string('postcode')->nullable()->default(null);
			$table->string('country')->nullable()->default(null);


			$table->string('transaction_id')->nullable()->default(null);

			$table->text('notes')->nullable()->default(null);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ticket_purchases');
	}

}
