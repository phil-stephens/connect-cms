    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavigationItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('navigation_items', function(Blueprint $table)
		{
            $table->increments('id');
			$table->binary('uuid', 16);
            $table->integer('navigation_id')->unsigned()->index();
            $table->foreign('navigation_id')->references('id')->on('navigations')->onDelete('cascade');
            $table->morphs('link');
            $table->string('external')->nullable()->default(null);
			$table->string('label');
            $table->integer('order')->unsigned();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('navigation_items');
	}

}
