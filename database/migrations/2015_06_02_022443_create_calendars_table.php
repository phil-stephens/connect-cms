<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->string('name');
            $table->string('slug');
            $table->text('description')->nullable()->default(null);
            $table->timestamps();
        });

        Schema::create('calendarables', function (Blueprint $table) {
            $table->integer('calendar_id')->unsigned();
            $table->integer('calendarable_id')->unsigned();
            $table->string('calendarable_type');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calendarables');
        Schema::drop('calendars');
    }

}
