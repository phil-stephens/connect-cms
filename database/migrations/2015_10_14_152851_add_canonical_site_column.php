<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCanonicalSiteColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach(['properties', 'feeds', 'calendars', 'venues'] as $tableName)
        {
            Schema::table($tableName, function (Blueprint $table) {
                $table->integer('canonical_site_id')->unsigned()->index()->after('uuid');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach(['properties', 'feeds', 'calendars', 'venues'] as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('canonical_site_id');
            });
        }
    }
}
