<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('custom_fields', function(Blueprint $table)
		{
            $table->increments('id');
			$table->binary('uuid', 16);
            $table->integer('content_id')->unsigned()->index();
            $table->foreign('content_id')->references('id')->on('content')->onDelete('cascade');
            $table->string('type');
            $table->string('key');
            $table->text('value')->nullable()->default(null);
            $table->integer('image_id')->unsigned()->default(0); // Can't add foreign key as this can be optional
			$table->integer('file_id')->unsigned()->default(0); // Can't add foreign key as this can be optional
			$table->integer('order')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('custom_fields');
	}

}
