<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeoTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->morphs('parent');

            $table->string('title')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);

            $table->integer('og_image_id')->unsigned()->default(0); // Can't add foreign key as this can be optional
            $table->string('og_title')->nullable()->default(null);
            $table->text('og_description')->nullable()->default(null);

            $table->integer('twitter_image_id')->unsigned()->default(0);
            $table->string('twitter_site')->nullable()->default(null);
            $table->string('twitter_title')->nullable()->default(null);
            $table->string('twitter_creator')->nullable()->default(null);
            $table->text('twitter_description')->nullable()->default(null);

            $table->string('google_plus_publisher')->nullable()->default(null);
            $table->string('google_plus_author')->nullable()->default(null);

            $table->text('javascript_head')->nullable()->default(null);
            $table->text('javascript_body_top')->nullable()->default(null);
            $table->text('javascript_body_bottom')->nullable()->default(null);

            $table->boolean('hide_on_sitemap')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seo');
    }

}
