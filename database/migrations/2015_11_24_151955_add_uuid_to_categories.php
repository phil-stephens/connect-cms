<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUuidToCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->binary('uuid', 16)->after('id');
        });

        $categories = \Fastrack\Locations\Category::all();

        foreach($categories as $category)
        {
            $category->uuid = \Webpatser\Uuid\Uuid::generate(4);

            $category->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('uuid');
        });
    }
}
