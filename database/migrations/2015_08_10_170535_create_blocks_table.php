<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlocksTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {

            $table->increments('id');
            $table->binary('uuid', 16);

            // Always belong to content
            $table->integer('content_id')->unsigned()->index();
            $table->foreign('content_id')->references('id')->on('content')->onDelete('cascade');

            // Plain, HTML, Image or File - maybe URL and Video later?
            $table->string('type');

            // Template specific type defined in manifest.json - i.e. youtube_video or results_text
            $table->string('key');

            // Field-specific label for reference - i.e. 'YouTube video of beach'
            $table->string('label');

            // Plain or HTML text value
            $table->text('value')->nullable()->default(null);

            $table->integer('image_id')->unsigned()->default(0);
            $table->integer('file_id')->unsigned()->default(0);

            $table->integer('order')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blocks');
    }

}
