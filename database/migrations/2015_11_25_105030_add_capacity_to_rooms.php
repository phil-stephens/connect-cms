<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCapacityToRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rooms', function (Blueprint $table) {
            $table->integer('capacity')->default(1)->after('slug');
            $table->dropColumn(['booking_system_id', 'settings', 'people']);
        });

        $rooms = \Fastrack\Properties\Room::all();

        foreach($rooms as $room)
        {
            if($capacity = $room->getStat('people'))
            {
                $room->capacity = $capacity;
            } elseif($capacity = $room->getStat('persons'))
            {
                $room->capacity = $capacity;
            } elseif($capacity = $room->getStat('guests'))
            {
                $room->capacity = $capacity;
            }

            $room->save();
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rooms', function (Blueprint $table) {
            $table->dropColumn(['capacity']);

            $table->integer('people')->nullable()->default(null);
            $table->string('booking_system_id')->nullable()->default(null);
            $table->text('settings')->nullable()->default(null);
        });
    }
}
