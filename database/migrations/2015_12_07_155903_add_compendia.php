<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompendia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $repo = new \Fastrack\Sites\SiteRepository();

        $properties = \Fastrack\Properties\Property::all();

        foreach($properties as $property)
        {
            $repo->bootstrapCompendium($property);

            //$compendium = new \Fastrack\Sites\Site(
            //    [
            //        'name' => $property->name,
            //        'theme' => 'guest-portal'
            //    ]
            //);
            //
            //$compendium->compendium_property_id = $property->id;
            //$compendium->account_id = 1;
            //
            //$compendium->save();
            //
            //$domain = new \Fastrack\Sites\Domain([
            //    'domain'    => str_slug($property->name) . '.fastrackguests.com'
            //]);
            //
            //$domain->site_id = $compendium->id;
            //
            //$domain->save();
            //
            //$compendium->primary_domain_id = $domain->id;
            //
            //$compendium->save();
            //
            //// create a general chapter
            //$chapter = new \Fastrack\Pages\Chapter([
            //    'name' => 'General'
            //]);
            //
            //$chapter->site_id = $compendium->id;
            //
            //$chapter->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // delete any site with an ID
    }
}
