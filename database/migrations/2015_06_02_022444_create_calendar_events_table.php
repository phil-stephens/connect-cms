<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendar_events', function(Blueprint $table)
		{
            $table->increments('id');
			$table->binary('uuid', 16);
			$table->integer('calendar_id')->unsigned()->index();
			$table->foreign('calendar_id')->references('id')->on('calendars')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->integer('slideshow_id')->unsigned()->default(0);
            $table->date('start_at');
            $table->date('finish_at');
            $table->string('website')->nullable()->default(null);
            $table->string('venue');
            $table->string('price')->nullable()->default(null);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendar_events');
	}

}
