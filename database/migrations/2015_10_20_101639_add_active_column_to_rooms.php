<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveColumnToRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach(['rooms', 'room_sub_types'] as $tableName)
        {
            Schema::table($tableName, function (Blueprint $table) {
                $table->boolean('active')->default(true)->after('slug');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach(['rooms', 'room_sub_types'] as $tableName)
        {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('active');
            });
        }
    }
}
