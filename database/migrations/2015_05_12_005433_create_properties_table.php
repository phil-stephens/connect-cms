<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->string('name');
            $table->string('slug');
            $table->string('room_noun')->default('room');

            $table->string('phone')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->decimal('star_rating', 2, 1)->default(1);

            $table->string('address1')->nullable()->default(null);
            $table->string('address2')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->string('state')->nullable()->default(null);
            $table->string('postcode')->nullable()->default(null);
            $table->string('country')->nullable()->default(null);

            $table->decimal('latitude', 14, 10);
            $table->decimal('longitude', 14, 10);

            $table->integer('slideshow_id')->unsigned()->default(0);
            $table->string('bookingbutton_channel')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }

}
