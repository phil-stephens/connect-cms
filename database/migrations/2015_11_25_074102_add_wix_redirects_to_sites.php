<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWixRedirectsToSites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sites', function (Blueprint $table) {
            $table->boolean('wix_redirects')->default(false)->after('primary_domain_id');
            $table->integer('compendium_property_id')->nullable()->default(null)->after('account_id');
            $table->boolean('in_development')->default(true)->after('account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sites', function (Blueprint $table) {
            $table->dropColumn(['wix_redirects', 'compendium_property_id', 'in_development']);
        });
    }
}
