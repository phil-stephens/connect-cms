<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atoms', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);

            $table->integer('content_id')->unsigned()->index();
            $table->foreign('content_id')->references('id')->on('content')->onDelete('cascade');

            $table->integer('order')->default(0);
            $table->string('key');
            $table->string('label')->nullable()->default(null);
            $table->string('theme')->nullable()->default(null);
            $table->string('template')->nullable()->default(null);
            $table->text('rendered')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('atoms');
    }
}
