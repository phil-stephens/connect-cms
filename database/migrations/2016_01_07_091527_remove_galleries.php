<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveGalleries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $chapterRepo = new \Fastrack\Pages\ChapterRepository();
        $navRepo = new \Fastrack\Navigations\NavigationRepository();
        $pageRepo = new \Fastrack\Pages\PageRepository();

        $sites = \Fastrack\Sites\Site::all();

        foreach($sites as $site)
        {
            $galleries = $site->galleries;

            if( ! empty($galleries))
            {
                // create a chapter for the gallery pages
                $chapter = $chapterRepo->create(['name' => 'Galleries', 'slug' => 'galleries'], $site->id);

                $nav = $navRepo->create(['name' => 'Galleries', 'slug' => 'galleries'], $site->id);

                foreach($galleries as $index => $gallery)
                {
                    // create a new page
                    $page = $pageRepo->create([
                        'name'  => $gallery->name,
                        'slug'  => 'gallery-' . $gallery->slug,
                        'chapter_id'    => $chapter->id,
                        'content'   => [
                            'title' => $gallery->name
                        ]
                    ], $site->id);

                    // Add the page to the gallery nav group
                    $navigationItem = new \Fastrack\Navigations\NavigationItem(['label' => $page->name, 'order' => $index]);

                    $navigationItem->link_id = $page->id;
                    $navigationItem->link_type = get_class($page);

                    $nav->items()->save($navigationItem);

                    foreach($gallery->images as $image)
                    {
                        // create a new Slide
                        $slide = new \Fastrack\Slideshows\Slide();
                        $slide->slideshow_id = $page->slideshow->id;
                        $slide->image_id = $image->id;
                        $slide->order = $image->pivot->order;

                        $slide->save();

                    }
                }
            }
        }

        Schema::drop('gallery_image');
        Schema::drop('galleries');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->integer('site_id')->unsigned()->index();
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->string('name');
            $table->string('slug');
            $table->integer('slideshow_id')->unsigned()->default(0);
            $table->timestamps();
        });

        Schema::create('gallery_image', function(Blueprint $table)
        {
            $table->integer('gallery_id')->unsigned()->index();
            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('cascade');
            $table->integer('image_id')->unsigned()->index();
            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->integer('order')->default(0);
        });
    }
}
