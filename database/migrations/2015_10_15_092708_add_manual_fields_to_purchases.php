<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManualFieldsToPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticket_purchases', function (Blueprint $table) {
            $table->boolean('paid')->default(false)->after('price_cc');
            $table->boolean('manual')->default(false)->after('price_cc');

            $table->softDeletes();
        });

        $purchases = \Fastrack\Calendars\TicketPurchase::wherePaid(false)->get();

        foreach($purchases as $purchase)
        {
            $purchase->paid = true;
            $purchase->save();
        }

        Schema::table('ticket_types', function (Blueprint $table) {
            $table->double('tax_rate')->default(0)->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticket_purchases', function (Blueprint $table) {
            $table->dropColumn(['paid', 'manual', 'deleted_at']);
        });

        Schema::table('ticket_types', function (Blueprint $table) {
            $table->dropColumn('tax_rate');
        });
    }
}
