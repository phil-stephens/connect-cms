<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRoomStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $stats = \Fastrack\Properties\RoomStatistic::orderBy('room_id')->get();

        foreach($stats as $stat)
        {
            // Does a property statistic entry already exist?
            $entry = \Fastrack\Properties\Statistic::wherePropertyId($stat->room->property_id)
                                                ->whereKey($stat->key)
                                                ->first();

            if(empty($entry))
            {
                $entry = new \Fastrack\Properties\Statistic([
                       'key'    => $stat->key,
                        'title' => $stat->title,
                    'order'     => $stat->order
                    ]);

                $stat->room->property->statistics()->save($entry);
            }


            $stat->room->statistics()->attach($entry->id);
            $stat->room->statistics()->updateExistingPivot($entry->id,
                ['value' => $stat->value]);

        }


        Schema::drop('room_statistics');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('room_statistics', function(Blueprint $table)
        {
            $table->increments('id');
            $table->binary('uuid', 16);

            $table->integer('room_id')->unsigned()->index();
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');

            $table->string('key');
            $table->string('title')->nullable()->default(null);
            $table->string('value')->nullable()->default(null);

            $table->integer('order')->default(0);

            $table->timestamps();
        });

        $rooms = \Fastrack\Properties\Room::all();

        foreach($rooms as $room)
        {
            foreach($room->statistics as $statistic)
            {
                $rs = new \Fastrack\Properties\RoomStatistic();

                $rs->room_id = $room->id;
                $rs->key = $statistic->key;
                $rs->title = $statistic->title;
                $rs->value = $statistic->value;
                $rs->order = $statistic->order;

                $rs->save();
            }
        }
    }
}
