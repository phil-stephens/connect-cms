<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annotations', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->integer('page_id')->unsigned()->index();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->boolean('title')->default(true);
            $table->boolean('excerpt')->default(true);
            $table->boolean('body')->default(true);
            $table->boolean('image')->default(true);
            $table->integer('excerpt_character_limit')->unsigned()->default(0);
            $table->text('title_note')->nullable()->default(null);
            $table->text('excerpt_note')->nullable()->default(null);
            $table->text('body_note')->nullable()->default(null);
            $table->text('image_note')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('annotations');
    }
}
