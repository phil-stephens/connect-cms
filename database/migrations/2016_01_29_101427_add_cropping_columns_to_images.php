<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCroppingColumnsToImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn('upload_hash');
            $table->boolean('face_detection')->default(false)->after('primary_colour');
            $table->string('crop_from')->default('')->after('primary_colour');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn(['crop_from', 'face_detection']);
            $table->string('upload_hash')->index()->nullable()->default(null)->after('primary_colour');
        });
    }
}
