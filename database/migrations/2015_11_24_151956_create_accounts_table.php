<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{

    protected $tables = ['sites', 'properties', 'amenities',
                    'venues', 'images', 'files', 'libraries',
                    'calendars', 'feeds', 'integrations', 'tags',
                    'users', 'categories'];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->string('name');
            $table->timestamps();
        });

        // Lots of foreign key table constraints
        $account = \Fastrack\Accounts\Account::create(['name' => env('DEFAULT_CLIENT', 'Default Account')]);

        foreach($this->tables as $t)
        {
            Schema::table($t, function (Blueprint $table) {
                $table->integer('account_id')->unsigned()->index()->after('uuid');
            });

            \DB::table($t)->update(['account_id' => $account->id]);

            Schema::table($t, function (Blueprint $table) {
                $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
            });
        }

        // Some other housekeeping
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['parent_id', 'parent_type']);
        });

        // Drop the unique key on the user email - make it unique to account ID

        Schema::drop('clients');
        Schema::drop('places');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach($this->tables as $t)
        {
            Schema::table($t, function (Blueprint $table) use ($t) {
                $table->dropForeign($t . '_account_id_foreign');
                $table->dropColumn('account_id');
            });
        }

        Schema::drop('accounts');

        // Undo housekeeping
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);

            $table->string('address1')->nullable()->default(null);
            $table->string('address2')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->string('state')->nullable()->default(null);
            $table->string('postcode')->nullable()->default(null);
            $table->string('country')->nullable()->default(null);

            $table->decimal('latitude', 14, 10);
            $table->decimal('longitude', 14, 10);

            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->morphs('parent');
        });
    }
}
