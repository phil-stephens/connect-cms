<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeatherForecastsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('weather_forecasts', function(Blueprint $table)
		{

            $table->increments('id');
			$table->binary('uuid', 16);
            $table->integer('property_id')->unsigned()->index();
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');

            $table->date('date');
            $table->integer('code');
            $table->string('description')->nullable()->default(null);

            $table->double('min_temp');
            $table->double('max_temp');

            $table->text('raw')->nullable()->default(null);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('weather_forecasts');
	}

}
