<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmenityFeaturegroupPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amenity_featuregroup', function (Blueprint $table) {
            $table->integer('amenity_id')->unsigned()->index();
            $table->foreign('amenity_id')->references('id')->on('amenities')->onDelete('cascade');
            $table->integer('featuregroup_id')->unsigned()->index();
            $table->foreign('featuregroup_id')->references('id')->on('feature_groups')->onDelete('cascade');

            $table->text('description')->nullable()->default(null);
            $table->integer('order')->default(0);

            $table->primary(['amenity_id', 'featuregroup_id']);
        });


        // Create a default amenity group for the property
        //$properties = \Fastrack\Properties\Property::all();
        //
        //foreach($properties as $property)
        //{
        //    $group = new \Fastrack\Features\Group();
        //
        //    $group->name = 'General Features';
        //    $group->key = 'general-features';
        //    $group->property_id = $property->id;
        //
        //    $group->save();
        //}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('amenity_featuregroup');
    }
}
