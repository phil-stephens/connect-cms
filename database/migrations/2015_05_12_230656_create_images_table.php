<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->string('original_name');
            $table->string('file_name');
            $table->string('path');
            $table->string('mime_type');
            $table->double('size');
            $table->integer('width')->default(0);
            $table->integer('height')->default(0);
            $table->text('exif')->nullable()->default(null);
            $table->string('primary_colour')->default('#FFFFFF');
            $table->string('upload_hash')->index()->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');
    }

}
