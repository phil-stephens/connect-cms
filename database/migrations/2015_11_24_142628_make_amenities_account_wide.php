<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAmenitiesAccountWide extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('amenities', function (Blueprint $table) {
            $table->dropForeign('amenities_property_id_foreign');
        });

        $all = \Fastrack\Properties\Amenity::get();

        foreach($all as $amenity)
        {
            $a = \Fastrack\Properties\Amenity::create([
               'name'   => $amenity->name
            ]);

            $a->image_id = $amenity->image_id;

            $a->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Fastrack\Properties\Amenity::wherePropertyId(0)->delete();

        Schema::table('amenities', function (Blueprint $table) {
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
        });
    }
}
