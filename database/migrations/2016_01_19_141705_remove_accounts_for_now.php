<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAccountsForNow extends Migration
{
    protected $tables = ['sites', 'properties', 'amenities',
        'venues', 'images', 'files', 'libraries',
        'calendars', 'feeds', 'integrations', 'tags',
        'users', 'categories'];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->tables as $t)
        {
            Schema::table($t, function (Blueprint $table) use ($t) {
                $table->dropForeign($t . '_account_id_foreign');
                $table->dropColumn('account_id');
            });
        }

        Schema::drop('accounts');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->string('name');
            $table->timestamps();
        });

        // Lots of foreign key table constraints
        $account = \Fastrack\Accounts\Account::create(['name' => env('DEFAULT_CLIENT', 'Default Account')]);

        foreach($this->tables as $t)
        {
            Schema::table($t, function (Blueprint $table) {
                $table->integer('account_id')->unsigned()->index()->after('uuid');
            });

            \DB::table($t)->update(['account_id' => $account->id]);

            Schema::table($t, function (Blueprint $table) {
                $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
            });
        }
    }
}
