<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);

            $table->string('address1')->nullable()->default(null);
            $table->string('address2')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->string('state')->nullable()->default(null);
            $table->string('postcode')->nullable()->default(null);
            $table->string('country')->nullable()->default(null);

            $table->decimal('latitude', 14, 10);
            $table->decimal('longitude', 14, 10);

            $table->timestamps();
        });

        //Schema::table('properties', function (Blueprint $table) {
        //    $table->integer('place_id')->unsigned()->default(0)->after('star_rating');
        //});
        //
        //$properties = \Fastrack\Properties\Property::all();
        //
        //foreach($properties as $property)
        //{
        //    $place = new \Fastrack\Locations\Place( $property->toArray() );
        //
        //    $property->place()->save($place);
        //}
        //
        //Schema::table('properties', function (Blueprint $table) {
        //    $table->dropColumn(['address1', 'address2', 'city', 'state', 'postcode', 'country', 'latitude', 'longitude']);
        //});
        //
        //
        //
        //Schema::table('venues', function (Blueprint $table) {
        //    $table->integer('place_id')->unsigned()->default(0)->after('slug');
        //});
        //
        //$venues = \Fastrack\Locations\Venue::all();
        //
        //foreach($venues as $venue)
        //{
        //    $place = new \Fastrack\Locations\Place( $venue->toArray() );
        //
        //    $venue->place()->save($place);
        //}
        //
        //Schema::table('venues', function (Blueprint $table) {
        //    $table->dropColumn(['address1', 'address2', 'city', 'state', 'postcode', 'country', 'latitude', 'longitude']);
        //});
        //
        //
        //
        //Schema::table('calendar_events', function (Blueprint $table) {
        //    $table->integer('place_id')->unsigned()->default(0)->after('venue');
        //});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('places');
    }
}
