<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->integer('property_id')->unsigned()->index();
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->string('booking_system_id')->nullable()->default(null);
            $table->double('price')->default(0);
            $table->text('dynamic_rate_data')->nullable()->default(null);
            $table->text('settings')->nullable()->default(null);
            $table->integer('slideshow_id')->unsigned()->default(0);
            $table->string('name');
            $table->string('slug');
            $table->integer('order')->default(0);
            $table->string('bookingbutton_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rooms');
    }

}
