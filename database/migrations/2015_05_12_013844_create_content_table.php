<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->morphs('parent');
            $table->integer('image_id')->unsigned()->default(0); // Can't add foreign key as this can be optional
            $table->string('title')->nullable()->default(null);
            $table->text('excerpt')->nullable()->default(null);
            $table->text('body')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content');
    }

}
