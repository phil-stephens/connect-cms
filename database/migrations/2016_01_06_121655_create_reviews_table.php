<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);

            $table->integer('property_id')->unsigned()->index();
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');

            $table->boolean('approved')->default(false);
            $table->boolean('published')->default(true);

            $table->string('name');
            $table->string('email')->nullable()->default(null);
            $table->integer('stars')->default(1);
            $table->text('message');

            $table->date('date');

            $table->string('source')->nullable()->default(null);
            $table->string('url')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
