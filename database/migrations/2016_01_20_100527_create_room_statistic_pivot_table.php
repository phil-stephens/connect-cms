<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomStatisticPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_statistic', function (Blueprint $table) {
            $table->integer('room_id')->unsigned()->index();
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->integer('statistic_id')->unsigned()->index();
            $table->foreign('statistic_id')->references('id')->on('statistics')->onDelete('cascade');

            $table->string('value');
            $table->text('description');

            $table->primary(['room_id', 'statistic_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_statistic');
    }
}
