<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {

            $table->increments('id');
            $table->binary('uuid', 16);

            $table->integer('ticket_purchase_id')->unsigned()->index();
            $table->foreign('ticket_purchase_id')->references('id')->on('ticket_purchases')->onDelete('cascade');

            $table->string('reference'); // Probably the combination of purchase reference and ticket ID?

            $table->string('name');
            $table->string('email');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tickets');
    }

}
