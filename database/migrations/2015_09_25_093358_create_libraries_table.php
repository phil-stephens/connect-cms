<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibrariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libraries', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->string('name');
            $table->boolean('default')->default(false);
            $table->timestamps();
        });

        $library = \Fastrack\Media\Library::create(['name' => 'General', 'default' => true]);

        Schema::table('images', function (Blueprint $table) {
            $table->integer('library_id')->unsigned()->default(0)->after('uuid');
        });

        $images = \Fastrack\Images\Image::all();

        foreach($images as $image)
        {
            $library->images()->save($image);
        }


        Schema::table('users', function (Blueprint $table) {
            $table->integer('image_id')->unsigned()->default(0)->after('password');
            $table->dropColumn('avatar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('avatar')->nullable()->default(null)->after('password');
            $table->dropColumn('image_id');
        });

        Schema::table('images', function (Blueprint $table) {
            $table->dropColumn('library_id');
        });

        Schema::drop('libraries');
    }
}
