<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TidyVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('versions_copy', function (Blueprint $table) {
            $table->increments('id');
            $table->binary('uuid', 16);
            $table->morphs('parent');
            $table->text('payload');
            $table->timestamp('created_at');
        });

        DB::statement('INSERT INTO versions_copy SELECT * FROM versions WHERE parent_type NOT LIKE \'%WeatherForecast\'');
        DB::statement('RENAME TABLE versions TO versions_old, versions_copy TO versions');
        DB::statement('DROP TABLE versions_old');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Nothing that can be rolled back
    }
}
