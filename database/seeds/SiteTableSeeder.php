<?php

use Fastrack\Pages\PageRepository;
use Fastrack\Properties\PropertyRepository;
use Fastrack\Properties\RoomRepository;
use Fastrack\Sites\SettingRepository;
use Fastrack\Sites\SiteRepository;
use Fastrack\Users\User;
use Illuminate\Database\Seeder;

class SiteTableSeeder extends Seeder
{

    /**
     * @var
     */
    private $siteRepository;
    /**
     * @var PropertyRepository
     */
    private $propertyRepository;
    /**
     * @var PageRepository
     */
    private $pageRepository;
    /**
     * @var RoomRepository
     */
    private $roomRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    function __construct(
        SiteRepository $siteRepository,
        PropertyRepository $propertyRepository,
        RoomRepository $roomRepository,
        PageRepository $pageRepository,
        SettingRepository $settingRepository
    ) {
        $this->siteRepository = $siteRepository;
        $this->propertyRepository = $propertyRepository;
        $this->pageRepository = $pageRepository;
        $this->roomRepository = $roomRepository;
        $this->settingRepository = $settingRepository;
    }

    public function run()
    {
        // First create the master user
        User::create([
            'name'     => 'Fastrack Group - Dev',
            'email'    => 'development@fastrackg.com',
            'password' => 'fast21',
            'hidden'   => true
        ]);

        User::create([
            'name'     => 'Fastrack Group - Marketing',
            'email'    => 'marketing@fastrackg.com',
            'password' => 'fast21',
            'hidden'   => true
        ]);

        $client = \Fastrack\Clients\Client::create(['name' => env('CMS_TITLE', 'Default Client')]);

        $users = \Fastrack\Users\User::whereHidden(false)->get();

        foreach ($users as $user) {
            $client->users()->save($user);
        }

        $site = $this->siteRepository->create([
            'name'    => env('DEFAULT_SITE'),
            'domains' => env('DEFAULT_SITE_DOMAIN')
        ]);

        //$site
        $this->settingRepository->updateAll($site, [
            [
                'key'   => 'property_noun',
                'type'  => 'plain',
                'value' => 'hotel'
            ]
        ]);

        // Create taxonomy stubs
        $models = ['Page', 'Property', 'Room', 'Gallery', 'Venue', 'CalendarEvent', 'Article', 'Offer'];

        $repo = new \Fastrack\Sites\TaxonomyRepository();

        foreach ($models as $model) {
            $repo->createIndividual(['model' => $model], $site->id);
        }

        // Create hidden 404 template
        $repo = new \Fastrack\Pages\PageRepository();
        $repo->create(['name' => '404', 'slug' => '404'], $site->id, true);
    }
}
